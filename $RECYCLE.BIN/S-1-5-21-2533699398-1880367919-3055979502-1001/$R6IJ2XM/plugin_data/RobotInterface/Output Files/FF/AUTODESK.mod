%%%
  VERSION:1
  LANGUAGE:ENGLISH
%%%

MODULE AUTODESK
  ! Home position = (0,0,0,0,0,0,9E9,9E9,9E9,9E9,9E9,9E9)
  !        Tools
  PERS tooldata tAutodesk1:=[TRUE,[[294.615,-1.425,93.435],[0.69986600,-0.00221402,0.71426700,0.00225961]],[7,[92.5,6.9,76.9],[1,0,0,0],0,0,0]];
  !        Origins
  PERS wobjdata wAutodesk1:=[FALSE,TRUE,"",[[580.000,0.000,150.000],[1.00000000,0.00000000,0.00000000,0.00000000]],[[0,0,0],[1,0,0,0]]];
  !        User defined feedrate v2
  PERS speeddata v2:=[2,500,5000,1000];
  !        User defined Zone data z0 (if not available)
  !CONST zonedata z0:=[FALSE,0.2,0.2,0.2,0.2,0.2,0.2];

  PROC main()
    ! Creation date: 2018/08/13 20:47:34
    ! Set robot acceleration and jerk
    AccSet 20,20;
    ! Configurations
    ConfJ\On;
    ConfL\Off;
    ! Tool: tAutodesk1
    ! LOAD_TOOL 1;
    ! Start position = (-0.798,1.7288,36.6161,0,-36.6161,0,9E9,9E9,9E9,9E9,9E9,9E9)
    ! Tool Number    = 1
    ! Spindle Speed  = 1500 RPM
    Proc_mwcl_postOperation.CL_1;
    ! UNLOAD_TOOL 1;
    ConfJ\On;
    ConfL\On;
    Stop;
  ENDPROC
ENDMODULE
