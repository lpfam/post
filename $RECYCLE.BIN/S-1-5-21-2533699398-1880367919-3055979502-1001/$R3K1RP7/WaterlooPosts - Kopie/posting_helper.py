﻿import os, datetime
from ppframework.pplib.util.helper import check_create_dir


def create_post_file_string(dir, file_name, ext, operation=None):
    if not operation:
        operation = "main"
    if not os.path.isabs(dir):
        dir = os.path.join(os.path.dirname(__file__), dir)
    check_create_dir(dir)
    # eg. <directroy>\<post_name>-<operation>.<ext>
    return "{0}{4}{1}{2}{3}".format(dir, file_name, operation, ext, os.path.sep)


def create_main_banner(writer, post_name, cam_info):
    from ppframework import settings
    with open(os.path.join(settings.CUSTOMER_DIRECTORY, "post_version.info"), 'r') as f:
        post_version = f.readline().strip("\n")
    import ppframework
    writer <<= "CREATED BY  : MW PPFramework Version %s" % ppframework.__version__
    writer <<= "POST VERSION: %s" % post_version
    writer <<= "CREATED ON  : " + datetime.datetime.now().isoformat()
    writer <<= "CAM SYSTEM  : {0}, Version {1}".format(cam_info.name, cam_info.version)
    writer <<= "CAM FILE    : {0}".format(cam_info.file_name)
    writer <<= "MACHINE FILE: {0}".format(cam_info.machine_information)
    writer <<= "POST FILE   : {0}".format(post_name)
    writer.newline()