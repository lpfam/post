﻿from ppframework.pplib.machine.controller import Controller, VendorItem
from ppframework.pplib.machine.iso_controller import ISOController
from ppframework.pplib.types import Misc_Commands, M_Commands
from ppframework.external.enum import Enum
from ppframework.pplib import types
from ppframework.pplib.util import helper


class MWCLCommands(Enum):
    MW_5AXMOVE_RAPID = 0
    MW_5AXMOVE_FEED = 1


class MWCLController(ISOController):
    @VendorItem(cmd=MWCLCommands.MW_5AXMOVE_RAPID,
                output_cmd="MW_5AXMOVE RAPID")
    def mw_5_axis_move_rapid(self, move, index, **kwargs):
        return self._get_linear_move(move, **kwargs) + " MOVE%d" % (index+1)

    @VendorItem(cmd=MWCLCommands.MW_5AXMOVE_FEED,
                output_cmd="MW_5AXMOVE FEED")
    def mw_5_axis_move_feed(self, move, index, **kwargs):
        return self._get_linear_move(move, **kwargs) + " MOVE%d" % (index+1)

    def do_move(self, move, index, line_numbering=True, part_mode=None, **kwargs):
        """dispatcher"""
        if move.move_type is types.MoveType.Cut or not move.is_rapid:
            self.mw_5_axis_move_feed(move, index, **kwargs)
        else:
            self.mw_5_axis_move_rapid(move, index, **kwargs)

    def _get_linear_move(self, move, **kwargs):
        """ 2 - works on channel and iterates over axes"""
        ncline=""

        ## axes
        axes_rv = []
        for axis in self.active_channel.output_axes:
            formatted_axis = self._axis_create_linear_cut_move(axis, move, **kwargs)
            if formatted_axis:
                axes_rv.append(formatted_axis)

        ncline += " ".join(axes_rv)
        ncline += self.configuration.separator1
        ncline += self._get_feedrate(move)
        return ncline.rstrip()

    def _axis_create_linear_cut_move(self, axis, move, **kwargs):
        """ 3a - works on axis """
        pos_new = None

        # get new position from move
        if axis.characteristics.physical.iso_type == types.AxIsoType.Translational:
            pos_new = move.part_position[axis.id] * axis.characteristics.controller.scale_factor
        # ToDo: own AxisIsoType?
        elif axis.characteristics.physical.iso_type is types.AxIsoType.Minor:
            pos_new = move.orientation[axis.id] * axis.characteristics.controller.scale_factor
        else:
            raise NotImplementedError("No implementation for rotary axes.")

        out_pos_new = pos_new

        # TODO tolerance depending on unit?

        # modal check - in tolerance
        if (axis.characteristics.controller.modal and axis.last_written_position != None
            and abs(axis.last_written_position - pos_new) < axis.characteristics.controller.tolerance):
            rv = ""
        else:
            rv = helper.format_axis_helper(axis.format_template % out_pos_new, axis.min_decimal_places)
            axis.last_written_position = pos_new

        if axis.characteristics.controller.modal:
            # update pos
            if axis.last_position is None:
                axis.last_written_position = pos_new
            axis.last_position = pos_new

        return rv

    def _get_feedrate(self, move):
        """ 3b """
        feed_rate_new = ""
        result = ""

        if not move.is_rapid:
            feed_rate_new += (self.active_channel.format_template_feedrate).format(move.feed_rate)
            #feed_rate_new += sep
            if ( self.active_channel.feed_rate_last != feed_rate_new ) or not self.active_channel.feed_rate_modal:
                result = feed_rate_new

        self.active_channel.feed_rate_last = feed_rate_new
        return result
