from ppframework import settings
import logging
logger = logging.getLogger(settings.LOGGER_NAME)


class LAMConfiguration(object):
    def __init__(self, operation):
        self.spot_focus_length= "VC100" # Alternativ auch ans Tool anhängen möglich operation.tool_assembly_data['VC']

        self.activated_powder_feeder_indices = []

        '''try:
            self.dwell_time = operation.tool_assembly.additional_data['DwellTime']
        except:
            self.dwell_time = None
            logger.warning("Please set a value for DwellTime in Toolassembly")'''

        #a=len(operation.tool_assembly.additional_data)
        #self.lpws=[]
        #self.pdc=[]

        '''for x in range(1,a):
            try:
                self.lpws.append(operation.tool_assembly.additional_data[('{lpw}').format(lpw='LPW%d') %x])
            except:
                self.lpw = None
                logger.warning("LPW not found in Toolassembly")'''

        # check the spot size
        try:
            self.sps = operation.tool_assembly.additional_data['SPS']
        except:
            self.sps = None
            logger.warning("Please set a value for spot size in Toolassembly")

        # check maximum laser power
        try:
            self.max_laser_power = operation.tool_assembly.additional_data['MaxLPW']
        except:
            self.max_laser_power = 0
            logger.warning("Please set a value for the maximum laser power")

        # check which powder feeder is used
        try:
            self.is_powder_feeder_1 = operation.tool_assembly.additional_data['isPowderFeeder1'] == 'True'
            if self.is_powder_feeder_1:
                self.activated_powder_feeder_indices.append(1)
        except:
            self.is_powder_feeder_1 = None
            logger.warning("No value set for isPowderFeeder1")
        try:
            self.is_powder_feeder_2 = operation.tool_assembly.additional_data['isPowderFeeder2'] == 'True'
            if self.is_powder_feeder_2:
                self.activated_powder_feeder_indices.append(2)
        except:
            self.is_powder_feeder_2 = None
            logger.warning("No value set for isPowderFeeder2")
        try:
            self.is_powder_feeder_3 = operation.tool_assembly.additional_data['isPowderFeeder3'] == 'True'
            if self.is_powder_feeder_3:
                self.activated_powder_feeder_indices.append(3)
        except:
            self.is_powder_feeder_3 = None
            logger.warning("No value set for isPowderFeeder3")
        try:
            self.is_powder_feeder_4 = operation.tool_assembly.additional_data['isPowderFeeder4'] == 'True'
            if self.is_powder_feeder_4:
                self.activated_powder_feeder_indices.append(4)
        except:
            self.is_powder_feeder_4 = None
            logger.warning("No value set for isPowderFeeder4")

        '''#check maximum powder feed
        try:
            self.max_powder_feed = operation.tool_assembly.additional_data['MaxPowderFeed']
        except:
            self.max_powder_feed = None
            logger.warning("Please set a value for the maximum powder feed")'''
