# ModuleWorks 2018
import os.path as path
from ppframework.pplib.util.ncfilewriter import NCFileWriter
from ppframework.pplib.util.iterator import OperationNodeProcessor
from ppframework.pplib.operation import OperationGroup
import ppframework.settings as settings
from ppframework.fwmain import post_main
import posting_helper as helper
from LAMConfig import LAMConfiguration
from ppframework.pplib.types import RTCPMode


def setup():
    """ returns a configured controller
        just return the machine_setup for old posts
    """

    # setup Controller Configuration
    from ppframework.pplib.machine.controller import ControllerConfig
    config = ControllerConfig()

    # init Controller
    from post_setup.controller import MyController
    controller = MyController(config)

    from ppframework.pplib.machine.config import get_channels
    for channel in get_channels():
        controller.add_channel(channel)

    # setup ouput-writer
    post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, "", ".min")
    writer = NCFileWriter(post_location, numbering=False, start=1, incr=1)
    controller.set_writer(writer)

    return controller


def nc_main(cam_info, operations, controller):

    post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, "", ".min")
    controller.writer.open(post_location)

    helper.create_main_banner(controller.writer, __file__, cam_info)

    controller.writer <<= "sub program locations"
    for o in operations:
        # ignore OperationGroup
        if isinstance(o, OperationGroup):
            continue
        sub_file = helper.create_post_file_string(settings.NC_OUTPUT_DIR, "", ".min",
                                                  o.name)
        controller.call_sub(sub_file)
    controller.writer.newline()

    controller.end_of_program()
    operations.iterator = OperationNodeProcessor

    # loop to go through all selected operations
    for operation_index, operation in enumerate(operations):
        # ignore OperationGroup
        if isinstance(operation, OperationGroup):
            continue
        # check if RTCP mode is on or off
        if operation.RTCP_mode is RTCPMode.On:
            controller.part_mode_on()
        elif operation.RTCP_mode is RTCPMode.Off:
            controller.part_mode_off()

        # to output part coordinates (independent from RTCP mode setting) uncomment line below
        # controller.part_mode_on()

        # assign current operation to operation
        controller.current_operation = operation
        # consider LAMConfig file
        config = LAMConfiguration(operation)
        # set the path to output folder
        post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, "",
                                                       ".min", operation.name)
        controller.writer.open(post_location)
        # create Header for each NC code
        helper.create_main_banner(controller.writer, __file__, cam_info)

        controller.writer << "G30 P2"
        controller.writer << "G94 G90 G53 G40 G17 G0"
        controller.writer << "ZER1=1"
        controller.writer << "ZER2=1+49"
        controller.writer << "G15 H=ZER1 (ZERO POINT)"

        controller.writer.newline()
        controller.writer.newline()

        # assign feedrates to different variables
        controller.writer << "FF=" + str(int(operation.feed_rate)) + "(F[mm/min])"
        controller.writer << "PF=" + str(int(operation.plunge_feed_rate)) + "(F[mm/min])"
        controller.writer << "RF=" + str(int(operation.retract_feed_rate)) + "(F[mm/min])"


        # get laser power from user interface for each layer
        for layer_index,layer in enumerate(operation.layers):
            LP = layer.laser_power_percentage/100 *int(config.max_laser_power)
            controller.writer << "LP%s=%s" %(layer_index+1,LP)
        controller.laser_spot_size(sps=config.sps, VC_Value=config.spot_focus_length)

        # get the index of the powder feeder which is set to True in the tool library
        for index in config.activated_powder_feeder_indices:
            controller.writer << "PDC{index}=15{index}1(POWDER CONVEYOR M CODE SLOT {index})".format(index=index)
        # dwell time from the first layer
        controller.dwell_time(operation.layers[0].laser_on_delay, append_comment=True)
        controller.writer <<= "**************"
        controller.writer.newline()

        # output laser power from user interface for each layer and check if it is higher than the maximum laser power from the tool library
        for layer_index,layer in enumerate(operation.layers):
            str_LPW = "IF[LP%s GE %s] NERR (LASER POWER PREVENTING ERROR)" % (layer_index+1 ,config.max_laser_power)
            controller.writer << str_LPW

        controller.writer.newline()
        controller.writer <<= "TOOL CHANGE"
        controller.writer << "IF[VTLCN EQ 120] NSTR \n T120M6\n NSTR"
        controller.writer.newline()
        controller.writer <<= "**LASER READY**"
        # M130
        controller.ignore_warning_spindle()
        # M510
        controller.collision_avoidance_off()
        # G131 "F20000 E0.3 D0.3 J2"
        controller.use_super_nurbs(append_comment=True)
        controller.writer.newline()
        # M1510
        controller.head_down(append_comment=True)
        # (M1505) (PILOT LASER ON)
        controller.pilot_on(append_comment=True)
        # M1507
        controller.drive_control_on(append_comment=True,is_laser_pointer_on=True)
        # "LHD= VC100(SPOT FOCUS LENGTH)"
        controller.spot_focus_length(VC_Value=config.spot_focus_length,append_comment=True,is_laser_pointer_on=True)
        # M1500
        controller.focus_shift(append_comment=True,is_laser_pointer_on=True)
        # M1508
        controller.drive_control_off(append_comment=True,is_laser_pointer_on=True)
        # M1501
        controller.laser_ready(append_comment=True,is_laser_pointer_on=True)
        # G611 HL=1
        controller.swivel_compensation(append_comment=True)
        controller.writer << "G30 P25"
        controller.writer << "M11"
        controller.writer << "M27"

        # loop to through all moves
        for index, move in enumerate(operation.move_list):
            controller.do_move(move=move, current_operation=operation, lam_config=config, move_index=index)

        controller.active_channel.reset()

        controller.writer.newline()
        controller.writer.newline()
        controller.writer << "G170"
        controller.writer <<= "**LASER OFF**"
        # M1504
        controller.laser_off(is_laser_pointer_on=True)

        # turn off all used powder feeder
        for index in config.activated_powder_feeder_indices:
            controller.writer << "/M=PDC{index}+1(POWDER STOP)".format(index=index)

        # M1502
        controller.laser_cancel(is_laser_pointer_on=True)
        # G4 P=DWL
        controller.dwell_time_command(is_laser_pointer_on=True)
        controller.writer << "G30 P2"
        # M1509
        controller.head_up()
        controller.end_of_program()

        # reset controller default values after each operation
        controller.is_first_marker = True

    controller.writer.close()


@post_main
def main(ppframework_input):
    print("Post")

    controller = setup()
    nc_main(ppframework_input.cam_info, ppframework_input.operations, controller)


if __name__ == '__main__':
    main()
