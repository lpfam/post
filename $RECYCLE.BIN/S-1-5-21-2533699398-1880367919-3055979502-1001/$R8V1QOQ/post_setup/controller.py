﻿from ppframework.pplib.machine.controller import VendorItem
from ppframework.pplib.machine.iso_controller import ISOController
from ppframework.pplib.types import Misc_Commands, M_Commands, G_Commands
from ppframework.external.enum import Enum
from ppframework.pplib.types import MoveType
from ppframework.pplib.move import LAMMoveMarker
from ppframework.pplib import types
from ppframework import settings
import logging
logger = logging.getLogger(settings.LOGGER_NAME)


class MyMCommands(Enum):
    M126 = 126
    M130 = 130
    M510 = 510
    M1500 = 1500
    M1501 = 1501
    M1502 = 1502
    M1503 = 1503
    M1504 = 1504
    M1505 = 1505
    M1506 = 1506
    M1507 = 1507
    M1508 = 1508
    M1509 = 1509
    M1510 = 1510
    M1511 = 1511
    M1512 = 1512
    M1513 = 1513
    M1514 = 1514
    M1521 = 1521
    M1522 = 1522
    M1523 = 1523
    M1524 = 1524
    M1531 = 1531
    M1532 = 1532
    M1533 = 1533
    M1534 = 1534
    M1541 = 1541
    M1542 = 1542
    M1543 = 1543
    M1544 = 1544


class MyGCommands(Enum):
    G0 = 00
    G1 = 01
    G2 = 02
    G3 = 03
    G4 = 04
    G15 = 15
    G115 = 115
    G131 = 131
    G611 = 611
    G901 = 901


class MyController(ISOController):
    def __init__(self, conf, devmode=None):
        super(MyController, self).__init__(conf, devmode)
        self.linear_interpolation.output_cmd = "G01"

        self.linear_interpolation.before_event += self.on_before_linear_interpolation

        self.linear_interpolation.after_event += self.on_after_move_process_lam_marker

        self.current_operation = None
        self.is_shutter_open = False
        self.layer_index = None
        self.is_first_marker = True
        self.first_linear_move = False
        self.is_inside_welding_area = False

    def on_after_move_process_lam_marker(self, sender, *args, **kwargs):
        if 'move' in kwargs:
            move = kwargs['move']
        else:
            move = args[0]

        if 'lam_config' in kwargs:
            lam_config = kwargs['lam_config']
        else:
            lam_config = args[2]

        # open and close shutter depending on LAM Marker
        if move.marker is not None and isinstance(move.marker, LAMMoveMarker):
            if self.is_first_marker:
                self.is_first_marker = False
                if 'lam_config' in kwargs:
                    lam_config = kwargs['lam_config']
                else:
                    lam_config = args[2]
                self.writer.newline()
                self.writer <<= "**LASER START**"
                self.writer << "/LPW=0"
                # M1503
                self.laser_on(append_comment=True, is_laser_pointer_on=True)
                # start powder feeder which were set to True in tool lbrary
                self.start_powder_feeder(config=lam_config)
                # G4 P=DWL
                self.dwell_time_command(is_laser_pointer_on=True)

                self.writer.newline()
                self.writer <<= "ADDITIVE MOVEMENT"

            # start powder and respect dwell time
            if move.marker.is_infill_boundary_start:
                pass
                #self.start_powder_feeder(config=lam_config)
                #self.dwell_time(self.current_operation.layers[move.marker.layer_index].laser_on_delay)
            # close shutter if infill_boundary_end/is_infill_end or is_slice_end
            if move.marker.is_infill_boundary_end or move.marker.is_infill_end or move.marker.is_slice_end:
                self.shutter_closed(is_laser_pointer_on=True, append_comment=True)
                self.is_shutter_open = False
                self.is_inside_welding_area = False
            # open shutter if is_infill_boundary_start or is_slice_start
            if move.marker.is_infill_boundary_start or move.marker.is_slice_start:
                self.shutter_open(layer_index=move.marker.layer_index, is_laser_pointer_on=True, append_comment=True)
                self.is_shutter_open = True
                self.is_inside_welding_area = True
                # use layer_index for bug fix
                self.layer_index = move.marker.layer_index

    def on_before_linear_interpolation(self, sender, *evt_args, **evt_kwargs):
        """A handler called before each rapid move.

        For the first rapid move, we want to output the rotational axes in a different line than the translational axes.
        Additional, further "header-commands" are written.

        :param sender:
        :param evt_args:
        :param evt_kwargs:
        :return:
        """
        if 'move_index' in evt_kwargs:
            move_index = evt_kwargs['move_index']
        else:
            raise Exception("You have to pass 'move_index=<index>' as keyword argument.")

        if 'move' in evt_kwargs:
            move = evt_kwargs['move']
        else:
            move = evt_args[0]

        if 'lam_config' in evt_kwargs:
            lam_config = evt_kwargs['lam_config']
        else:
            lam_config = evt_args[2]

        if self.is_inside_welding_area:
            # bug fix for LAM Marker
            # open and close shutter considering check surfaces
            if self.is_shutter_open:
                if move.move_type is not MoveType.Cut:
                    self.shutter_closed(is_laser_pointer_on=True, append_comment=True)
                    self.is_shutter_open = False
            elif not self.is_shutter_open:
                if move.move_type is MoveType.Cut:
                    self.shutter_open(layer_index=self.layer_index, is_laser_pointer_on=True, append_comment=True)
                    self.is_shutter_open = True

        # before operation processing
        if move_index == 0:
            # consider output of axes in RTCP mode
            if self.current_operation.RTCP_mode.name == 'On':
                self.part_mode_on()
                self.writer << "CALL OO88 PX=%s PY=%s PZ=%s PC=%s  PA=%s PH=ZER1 PP=ZER2( **STARTPOINT** )" % \
                (str(format(move.part_position.x, '.4f')),
                 str(format(move.part_position.y, '.4f')),
                 str(format(move.part_position.z, '.4f')), str(format(move.rotation_axis_values[0], '.4f')),
                 str(format(move.rotation_axis_values[1], '.4f')))
            else:
                self.writer << "CALL OO88 PX=%s PY=%s PZ=%s PC=%s  PA=%s PH=ZER1 PP=ZER2( **STARTPOINT** )" % \
                (str(format(move.absolute_machine_position.x, '.4f')),
                 str(format(move.absolute_machine_position.y, '.4f')),
                 str(format(move.absolute_machine_position.z, '.4f')), str(format(move.rotation_axis_values[0], '.4f')),
                 str(format(move.rotation_axis_values[1], '.4f')))

            # append values of only rotational axes to an array
            rot_axes = list()
            rot_axes.append(self.active_channel.get_axis_by_name('A'))
            rot_axes.append(self.active_channel.get_axis_by_name('C'))
            self.active_channel.output_axes = rot_axes

            # output only rotational axes here
            self.rapid_positioning(move_index=None, move=move, lam_config=lam_config)

            self.writer << "G00 X0. Y0."
            self.writer << "G00 Z0."
            self.writer << "G612"
            self.writer << "G15 H=ZER1"
            self.writer.newline()
            self.writer <<= "**STARTPOINT**"
            self.writer << "G169 HL=1"

            # append values of only translational axes to an array
            lin_axes = list()
            lin_axes.append(self.active_channel.get_axis_by_name('X'))
            lin_axes.append(self.active_channel.get_axis_by_name('Y'))
            lin_axes.append(self.active_channel.get_axis_by_name('Z'))
            self.active_channel.output_axes = lin_axes

            # output feedrate for first linear move
            self.first_linear_move = True
            # output only translational axes here
            self.linear_interpolation(move_index=None, move=move, lam_config=lam_config)

            ######################################
            # Reset to originally output axes.
            self.active_channel.output_axes = list(self.active_channel.axes)

    @VendorItem(cmd=Misc_Commands.T,
                comm="SELECT TOOL",
                do_overwrite=True)
    def select_tool(self, tool_assembly, **kwargs):
        return "T{tool_assembly.tool_assembly_number}".format(tool_assembly=tool_assembly)

    @VendorItem(cmd=M_Commands.M98,
                desc="call subprogramm",
                scope="controller")
    def call_sub(self, nc_file, **kwargs):
        return "( {nc_file} )".format(nc_file=nc_file)

    @VendorItem(desc="return from subprogramm",
                comm="RETURN FORM SUB",
                cmd=M_Commands.M99,
                scope="controller")
    def return_from_sub(self, **kwargs):
        pass

    @VendorItem(desc="Move to zero point",
                comm="ZERO POINT",
                cmd=MyGCommands.G15,
                scope="controller")
    def zero_point(self, **kwargs):
        return "H"

    @VendorItem(desc="Move to zero point",
                comm="LASER SPOT SIZE:LHD VC100 OUTPUT",
                cmd=MyGCommands.G115,
                scope="controller")
    def laser_spot_size(self, sps, VC_Value, **kwargs):
        return "SPS="+str(sps) + " (LASER SPOT SIZE:LHD " + str(VC_Value) + " OUTPUT)"

    @VendorItem(desc="Dwell time",
                comm="G4 DWELL TIME POWDER STABILIZATION",
                cmd="",
                scope= "controller")
    def dwell_time(self,dwl, **kwargs):
        return "DWL=" + str(dwl)

    @VendorItem(desc="tool change condition",
                comm="tool change condition",
                cmd="",
                scope="controller")
    def tool_change_condition(self, **kwargs):
        return "IF[VTLCN EQ 120] NSTR \n T120M6 \n NSTR"

    @VendorItem(desc="Cutting feed",
                comm="CUTTING FEED",
                cmd=MyMCommands.M130,
                scope="controller")
    def ignore_warning_spindle(self, **kwargs):
        pass

    @VendorItem(desc="Collision control off",
                comm="COLLISION CONTROL OFF",
                cmd=MyMCommands.M510,
                scope="controller")
    def collision_avoidance_off(self, **kwargs):
        pass

    @VendorItem(desc="Super nurb",
                # do_overwrite=True,
                comm="SUPER NURBS PARAMETER",
                cmd=MyGCommands.G131,
                scope="controller")
    def use_super_nurbs(self, **kwargs):
        return "F20000 E0.3 D0.3 J2"

    @VendorItem(desc="Spot focus length",
                do_overwrite=True,
                comm="SPOT FOCUS LENGTH",
                cmd="",
                scope="controller")
    def spot_focus_length(self, VC_Value, is_laser_pointer_on=False, **kwargs):
        rv = "LHD=" + VC_Value + " (SPOT FOCUS LENGTH)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Swivel compensation",
                comm="SWIVEL COMPENSATION H1:FINE H2:SO12",
                cmd=MyGCommands.G611,
                scope="controller")
    def swivel_compensation(self, **kwargs):
        return "HL=1"

    @VendorItem(desc="Dwell Time",
                do_overwrite=True,
                comm="G4 DWELL TIME POWDER STABILIZATION",
                cmd=MyGCommands.G4,
                scope="controller")
    def dwell_time_command(self, is_laser_pointer_on=False,**kwargs):
        rv = "G4 P=DWL"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Shutter closed",
                do_overwrite=True,
                comm="SHUTTER CLOSED",
                cmd="",
                scope="controller")
    def shutter_closed(self, is_laser_pointer_on=False, **kwargs):
        rv = "LPW=0 (SHUTTER CLOSED)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Shutter open",
                do_overwrite=True,
                comm="SHUTTER OPEN",
                cmd="",
                scope="controller")
    def shutter_open(self, layer_index, is_laser_pointer_on=False, **kwargs):
        rv = "LPW=LP{layer_index} ({comment})".format(layer_index=layer_index+1, comment=self.shutter_open.comm)
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Focus shift start",
                do_overwrite=True,
                comm="FOCUS MOVE START",
                cmd=MyMCommands.M1500,
                scope="controller")
    def focus_shift(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1500 (FOCUS MOVE START)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Get laser ready",
                do_overwrite=True,
                comm="LASER READY ON",
                cmd=MyMCommands.M1501,
                scope="controller")
    def laser_ready(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1501 (LASER READY ON)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Cancels laser ready mode",
                do_overwrite= True,
                comm="LASER READY OFF",
                cmd=MyMCommands.M1502,
                scope="controller")
    def laser_cancel(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1502 (LASER READY OFF)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Laser activation",
                do_overwrite=True,
                comm="LASER ON",
                cmd=MyMCommands.M1503,
                scope="controller")
    def laser_on(self,is_laser_pointer_on=False, **kwargs):
        rv = "M1503 (LASER ON)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Laser stop",
                do_overwrite= True,
                comm="LASER OFF",
                cmd=MyMCommands.M1504,
                scope="controller")
    def laser_off(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1504 (LASER OFF)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Pilot laser activation",
                do_overwrite=True,
                comm="PILOT LASER ON",
                cmd=MyMCommands.M1505,
                scope="controller")
    def pilot_on(self, **kwargs):
        return "(M1505) (PILOT LASER ON)"

    @VendorItem(desc="Pilot laser stop",
                comm="PILOT LASER OFF",
                cmd=MyMCommands.M1506,
                scope="controller")
    def pilot_off(self, **kwargs):
        pass

    @VendorItem(desc="Drive control ON",
                do_overwrite=True,
                comm="DRIVING CONTROL ON",
                cmd=MyMCommands.M1507,
                scope="controller")
    def drive_control_on(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1507 (DRIVING CONTROL ON)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Drive control OFF",
                do_overwrite=True,
                comm="DRIVING CONTROL OFF",
                cmd=MyMCommands.M1508,
                scope="controller")
    def drive_control_off(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1508 (DRIVING CONTROL OFF)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Laser machining head up",
                comm="HEAD UP",
                cmd=MyMCommands.M1509,
                scope="controller")
    def head_up(self, **kwargs):
        pass

    @VendorItem(desc="Laser machining head down",
                comm="HEAD DOWN",
                cmd=MyMCommands.M1510,
                scope="controller")
    def head_down(self, **kwargs):
        pass

    @VendorItem(desc="Powder 1 activation",
                do_overwrite=True,
                comm="POWDER START",
                cmd=MyMCommands.M1511,
                scope="controller")
    def powder1_start(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1511 (POWDER START)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Powder 1 stop",
                comm="POWDER STOP",
                cmd=MyMCommands.M1512,
                scope="controller")
    def powder1_stop(self, **kwargs):
        pass

    @VendorItem(desc="Powder 1 washing",
                comm="POWDER WASHING START",
                cmd=MyMCommands.M1513,
                scope="controller")
    def powder1_washing_start(self, **kwargs):
        pass

    @VendorItem(desc="Powder 1 washing stop",
                comm="POWDER WASHING STOP",
                cmd=MyMCommands.M1514,
                scope="controller")
    def powder1_washing_stop(self, **kwargs):
        pass

    def start_powder_feeder(self, config):
        if config.is_powder_feeder_1:
            self.powder1_start(is_laser_pointer_on=True, append_comment=True)
        if config.is_powder_feeder_2:
            self.powder2_start(is_laser_pointer_on=True, append_comment=True)
        if config.is_powder_feeder_3:
            self.powder3_start(is_laser_pointer_on=True, append_comment=True)
        if config.is_powder_feeder_4:
            self.powder4_start(is_laser_pointer_on=True, append_comment=True)

    def stop_powder_feeder(self, config):
        if config.is_powder_feeder_1:
            self.powder1_stop(append_comment=True)
        if config.is_powder_feeder_2:
            self.powder2_stop(append_comment=True)
        if config.is_powder_feeder_3:
            self.powder3_stop(append_comment=True)
        if config.is_powder_feeder_4:
            self.powder4_stop(append_comment=True)

    @VendorItem(desc="Powder 2 activation",
                do_overwrite=True,
                comm="POWDER START",
                cmd=MyMCommands.M1521,
                scope="controller")
    def powder2_start(self,is_laser_pointer_on=False, **kwargs):
        rv = "M1521 (POWDER START)"
        if is_laser_pointer_on:
            rv = "/"+rv
        return rv

    @VendorItem(desc="Powder 2 stop",
                comm="POWDER STOP",
                cmd=MyMCommands.M1522,
                scope="controller")
    def powder2_stop(self, **kwargs):
        pass

    @VendorItem(desc="Powder 2 washing",
                comm="POWDER WASHING START",
                cmd=MyMCommands.M1523,
                scope="controller")
    def powder2_washing_start(self, **kwargs):
        pass

    @VendorItem(desc="Powder 2 washing stop",
                comm="POWDER WASHING STOP",
                cmd=MyMCommands.M1524,
                scope="controller")
    def powder2_washing_stop(self, **kwargs):
        pass

    @VendorItem(desc="Powder 3 activation",
                do_overwrite= True,
                comm="POWDER START",
                cmd=MyMCommands.M1531,
                scope="controller")
    def powder3_start(self,is_laser_pointer_on=False, **kwargs):
        rv= "M1531 (POWDER START)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Powder 3 stop",
                comm="POWDER STOP",
                cmd=MyMCommands.M1532,
                scope="controller")
    def powder3_stop(self, **kwargs):
        pass

    @VendorItem(desc="Powder 3 washing",
                comm="POWDER WASHING START",
                cmd=MyMCommands.M1533,
                scope="controller")
    def powder3_washing_start(self, **kwargs):
        pass

    @VendorItem(desc="Powder 3 washing stop",
                comm="POWDER WASHING STOP",
                cmd=MyMCommands.M1534,
                scope="controller")
    def powder3_washing_stop(self, **kwargs):
        pass

    @VendorItem(desc="Powder 4 activation",
                do_overwrite= True,
                comm="POWDER START",
                cmd=MyMCommands.M1541,
                scope="controller")
    def powder4_start(self, is_laser_pointer_on=False, **kwargs):
        rv = "M1541 (POWDER START)"
        if is_laser_pointer_on:
            rv = "/" + rv
        return rv

    @VendorItem(desc="Powder 4 stop",
                comm="POWDER STOP",
                cmd=MyMCommands.M1542,
                scope="controller")
    def powder4_stop(self, **kwargs):
        pass

    @VendorItem(desc="Powder 4 washing",
                comm="POWDER WASHING START",
                cmd=MyMCommands.M1543,
                scope="controller")
    def powder4_washing_start(self, **kwargs):
        pass

    @VendorItem(desc="Powder 4 washing stop",
                comm="POWDER WASHING STOP",
                cmd=MyMCommands.M1544,
                scope="controller")
    def powder4_washing_stop(self, **kwargs):
        pass

    def do_move(self, move, line_numbering=True, part_mode=None, **kwargs):
        """dispatcher for G00, G01, G02 and G03"""
        comment = ""

        if self.devmode:
            comment += ( self.configuration.separator1 + move.move_type.name )
            if move.move_subtype:
                comment += ( self.configuration.separator1 + move.move_subtype.name )

        # dispatching
        if not move.is_arc:
            # is linear
            self.linear_interpolation(move, part_mode, **kwargs)
        else:  # is arc
            if move.arc_direction is types.ArcMoveDirection.Clockwise:
                self.circular_interpolation_arc_clockwise(move, **kwargs)
            elif move.arc_direction is types.ArcMoveDirection.CounterClockwise:
                self.circular_interpolation_arc_counter_clockwise(move, **kwargs)
            else:  # types.ArcMoveDirection.Undefined
                # output a linear move with comment
                move.is_arc = False
                self.do_move(move, line_numbering=line_numbering, part_mode=part_mode, **kwargs)
                logger.warning("Arc move posted as linear move. The normals of tool plane and arc are not parallel!")

    def _get_feedrate(self, move):
        """ 3b """
        feed_rate_new = ""
        result = ""

        if not move.is_rapid:
            if move.feed_rate == self.current_operation.feed_rate:
                feed_rate_new += (self.active_channel.format_template_feedrate).format("FF")
            elif move.feed_rate == self.current_operation.plunge_feed_rate:
                feed_rate_new += (self.active_channel.format_template_feedrate).format("PF")
            elif move.feed_rate == self.current_operation.retract_feed_rate:
                feed_rate_new += (self.active_channel.format_template_feedrate).format("RF")
            else:
                feed_rate_new += (self.active_channel.format_template_feedrate).format(move.feed_rate)
        elif self.first_linear_move:
            self.first_linear_move = False
            feed_rate_new += (self.active_channel.format_template_feedrate).format("PF")

        # feed_rate_new += sep
        if feed_rate_new and (self.active_channel.feed_rate_last != feed_rate_new) or not self.active_channel.feed_rate_modal:
            result = feed_rate_new

            self.active_channel.feed_rate_last = feed_rate_new
        return result
