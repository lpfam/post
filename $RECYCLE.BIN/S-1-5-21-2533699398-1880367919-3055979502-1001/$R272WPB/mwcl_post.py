# ModuleWorks 2017
import os.path as path

from ppframework.pplib.util.ncfilewriter import NCFileWriter
from ppframework.pplib.operation import OperationGroup
from ppframework.pplib.util.iterator import OperationNodeProcessor
import ppframework.settings as settings
from ppframework.fwmain import post_main
import posting_helper as helper
import logging
logger = logging.getLogger(settings.LOGGER_NAME)


def setup():
    """Returns a configured controller."""

    # setup Controller Configuration
    from ppframework.pplib.machine.controller import ControllerConfig
    config = ControllerConfig()

    # init Controller
    from post_setup.mwcl_controller import MWCLController
    controller = MWCLController(config)

    from ppframework.pplib.machine.config import get_channels
    for channel in get_channels():
        controller.add_channel(channel)

    # setup ouput-writer
    post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, path.basename(__file__).split('.')[0], ".nc")
    writer = NCFileWriter(post_location, numbering=False)
    controller.set_writer(writer)

    return controller


def nc_main(cam_info, operations, controller):

    operations.iterator = OperationNodeProcessor
        
    for operation_index, operation in enumerate(operations):
        if isinstance(operation, OperationGroup):
            continue

        post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, path.basename(__file__).split('.')[0],
                                                       ".CL", operation.name)
        controller.writer.open(post_location)

        controller.select_channel('mwcl')

        # helper.create_main_banner(controller.writer, path.basename(__file__).split('.')[0], cam_info)

        #### CL Header
        controller.writer << "MW_CL_VERSION 1.3"
        controller.writer << "MW_UNITS_METRIC 1"
        controller.writer << "MW_OP_START"
        controller.writer << "MW_OP_NUMBER %d" % operation_index
        controller.writer << "MW_OP_COMMENT %s" % operation.name
        #controller.writer << "MW_TOOL_NAME %s" % operation.tool_assembly.tool_name
        controller.writer << "MW_TOOL_NAME tool"
        controller.writer << "MW_TOOL_TYPE ADDITIVE"
        controller.writer << "MW_TOOL_NUMBER %s" % operation.tool_assembly.tool_assembly_number

        #controller.writer << "MW_TOOL_COMMENT %s" % operation.tool_assembly.tool_name
        controller.writer << "MW_TOOL_COMMENT Sample tool"

        try:
            controller.writer << "MW_TOOL_DEF_APT %s %f %f %s %f %f %f" % (
                operation.tool_assembly.additional_data["advanced"]["WeldDiam"], 2 , 20,
                operation.tool_assembly.additional_data["advanced"]["WeldDiam"], 0.0, 0.0, 0.0)
        except KeyError:
            logger.warning("No welding diameter set!")

        controller.writer << "MW_TOOL_HLD_DEF"
        controller.writer << "MW_POINT 0 0"
        controller.writer << "MW_POINT 0.1 0"
        controller.writer << "MW_POINT 0 %f" % operation.tool_assembly.holder.length
        controller.writer << "MW_TOOL_PROFILE_CHORDAL_TOL 0.1"

        controller.writer.newline()

        for index, move in enumerate(operation.move_list):
            controller.do_move(move, index)

        controller.active_channel.reset()

        controller.writer << "MW_OP_END"
        controller.writer.close()


@post_main
def main(ppframework_input):
    print("MWCL Post")

    controller = setup()
    nc_main(ppframework_input.cam_info, ppframework_input.operations, controller)


if __name__ == '__main__':
    main()
