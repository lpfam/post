Readme
++++++

To run PPFrameWorkDefaultPost you have to install ppframework module (see installation_ppframework_module_).

On how to start posting see following section in the docs how_to_run_post_.


.. _installation_ppframework_module: https://shared.moduleworks.com/projects/07_Products/02_PP_Development/04_TechDoc/PPFrameWork_doc/ppframework_doc_v1.1.2/user/installation.html

.. _how_to_run_post: https://shared.moduleworks.com/projects/07_Products/02_PP_Development/04_TechDoc/PPFrameWork_doc/ppframework_doc_v1.1.2/user/how_to_run_post.html

