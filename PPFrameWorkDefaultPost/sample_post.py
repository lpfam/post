# ModuleWorks 2017
import os.path as path
from ppframework.pplib.util.ncfilewriter import NCFileWriter
from ppframework.pplib.util.iterator import PPOperationListIterator
import ppframework.settings as settings
from ppframework.fwmain import post_main
import posting_helper as helper


def setup():
    """ returns a configured controller
        just return the machine_setup for old posts
    """

    # setup Controller Configuration
    from ppframework.pplib.machine.controller import ControllerConfig
    config = ControllerConfig()

    # init Controller
    from post_setup.sample_controller import VendorController
    controller = VendorController(config)

    from ppframework.pplib.machine.config import get_channels
    for channel in get_channels():
        controller.add_channel(channel)

    # setup ouput-writer
    post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, path.basename(__file__).split('.')[0], ".nc")
    writer = NCFileWriter(post_location, numbering=True, start=0, incr=1);
    controller.set_writer(writer)

    return controller


def nc_main(cam_info, operations, controller):

    post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, path.basename(__file__).split('.')[0], ".nc")
    controller.writer.open(post_location)

    helper.create_main_banner(controller.writer, __file__, cam_info)

    controller.writer <<= "sub program locations"
    for o in operations:
        sub_file = helper.create_post_file_string(settings.NC_OUTPUT_DIR, path.basename(__file__).split('.')[0], ".nc",
                                                  o.name)
        controller.call_sub(sub_file)
    controller.writer.newline()

    controller.end_of_program()

    ### controller.active_channel.g_group1_modal = False

    operations.iterator = PPOperationListIterator
    operations.iterator.controller = controller
        
    for operation_index, operation in enumerate(operations):
        post_location = helper.create_post_file_string(settings.NC_OUTPUT_DIR, path.basename(__file__).split('.')[0],
                                                       ".nc", operation.name)
        controller.writer.open(post_location)

        controller.writer <<= "OPERATION   : " + str(operation_index) + " : " + operation.name

        helper.create_main_banner(controller.writer, path.basename(__file__).split('.')[0], cam_info)

        # write tool assembly information as comment to nc file
        controller.writer <<= operation.tool_assembly

        controller.writer.newline()

        controller.select_tool(operation.tool_assembly, comment=True, append_buffer=True)
        controller.tool_change()

        controller.spindle_cw(append_comment=True)

        for index, move in enumerate(operation.move_list):
            controller.do_move(move)

        controller.active_channel.reset()

    controller.end_of_data()
    controller.writer.close()


@post_main
def main(ppframework_input):
    print("Sample Post")

    controller = setup()
    nc_main(ppframework_input.cam_info, ppframework_input.operations, controller)


if __name__ == '__main__':
    main()
