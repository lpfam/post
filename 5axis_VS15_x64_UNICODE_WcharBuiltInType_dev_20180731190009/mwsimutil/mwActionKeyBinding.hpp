/******************************************************************************
  (C) 2005 by ModuleWorks GmbH
  Author: 
******************************************************************************/

#ifndef MW_MWACTIONKEYBINDING_HPP_
#define MW_MWACTIONKEYBINDING_HPP_
#include	"mwDllImpExpDef.hpp"
#include	"mwString.hpp"
#include	"mwVKey.hpp"
#include "mwDeprecated.hpp"

namespace misc
{
	class MW_5AXUTIL_API mwActionKeyBinding
	{
	public:

		mwActionKeyBinding();
		mwActionKeyBinding(const mwVKey& boundKey);
		mwActionKeyBinding(const mwVKey& boundKey,const unsigned short keyModifier );
		
		const misc::mwVKey& GetBoundVKey()const;
		const unsigned short GetKeyModifier() const;

		const bool ModifierHasAlt() const;
		const bool ModifierHasShift() const;
		const bool ModifierHasCtrl() const;
		
		void SetBoundVKey(const mwVKey& newkey); 
		void SetKeyModifier( const unsigned short newModifier );

		const bool operator==(const mwActionKeyBinding &secondActionBind) const;
		const bool operator!=(const mwActionKeyBinding &secondActionBind) const;
		
		const misc::mwstring ToString() const;

		MW_DEPRECATED("Deprecated in 2015.12, Not supporting extended keys , please don't use")
			mwActionKeyBinding( unsigned int boundKey );		
		MW_DEPRECATED("Deprecated in 2015.12, Not supporting extended keys , please don't use")
			mwActionKeyBinding( unsigned int boundKey, unsigned short keyModifier );	

		MW_DEPRECATED("Deprecated in 2015.12, Not supporting extended keys , please don't use")
			const unsigned int GetBoundKey() const;
		MW_DEPRECATED("Deprecated in 2015.12, Not supporting extended keys , please don't use")
			void SetBoundKey( const unsigned int newKey );

		MW_DEPRECATED("Deprecated in 2015.12, Not supporting extended keys , please don't use")
			static unsigned short GetKeyCode( const misc::mwstring& keyName );	
		MW_DEPRECATED("Deprecated in 2015.12, Not supporting extended keys , please don't use")
			static const misc::mwstring GetKeyName( const unsigned short& keyCode );

		enum modifier
		{
			NONE = 0x00,
			SHIFT = 0x01,
			CTRL = 0x02,
			ALT = 0x04,
			EXT = 0x08 // HOTKEYF_EXT
		};

	private:
		misc::mwVKey m_virtualKey;
		unsigned short m_modifier;
	};

}
#endif	//	MW_MWACTIONKEYBINDING_HPP_
