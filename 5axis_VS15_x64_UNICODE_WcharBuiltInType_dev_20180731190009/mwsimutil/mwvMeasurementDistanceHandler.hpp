#ifndef mwvMeasurementDistanceHandler_h__
#define mwvMeasurementDistanceHandler_h__

#include <vector>
#include "mwMachSimVerifier.hpp"
#include "mwvMeasurementDistance.hpp"

namespace VerifierUtil
{
	class mwGLFunctions;
	class mwvMeasurementDistanceHandler
	{
	public:
		mwvMeasurementDistanceHandler(mwMachSimVerifier* const pVerifier);
		~mwvMeasurementDistanceHandler();

		int Select(int x, int y, const float3d &pos, float3d &vertex, bool removeOnly);

		void Clear();

		bool GetFirstClick() const
		{
			return firstClick;
		}

		void ResetFirstClick()
		{
			firstClick = true;
		}

		const mwvMeasurementDistance& GetUnfinishedDistance() const
		{
			return unfinishedDistance;
		}

		std::vector<mwvMeasurementDistance> m_selectedDistances;

	private:
		mwGLFunctions* m_gl;
		mwMachSimVerifier* const m_pVerifier;

		bool firstClick;
		mwvMeasurementDistance unfinishedDistance;

		bool WindowPosToScenePos(int x, int y, float2d &dest);
	};
}

#endif //mwvMeasurementDistanceHandler_h__