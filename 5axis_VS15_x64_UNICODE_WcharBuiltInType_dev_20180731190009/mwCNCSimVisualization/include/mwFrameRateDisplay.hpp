/******************************************************************************
  (C) 2015 by ModuleWorks GmbH
  Author: Michael Gras
******************************************************************************/
#pragma once

#include <mwColor.hpp>
#include <mwTPoint2d.hpp>

namespace mwCNCSimVisualization
{
	class mwFrameRateDisplay
	{
	public:
		typedef cadcam::mwTPoint2d<int> Point2d;

	public:		
		virtual const Point2d& GetPosition() const = 0;
		virtual void SetPosition(const Point2d&) = 0;
		virtual bool GetIsVisible() const = 0;
		virtual void SetIsVisible(const bool val) = 0;
		virtual const misc::mwColor& GetColor() const = 0;
		virtual void SetColor(const misc::mwColor& val) = 0;
	};
}
