/******************************************************************************
 (C) 2014 by ModuleWorks GmbH
  Author: Nini Abagiu
******************************************************************************/

#ifndef MW_MWTPOINT2DDECL_HPP_
#define MW_MWTPOINT2DDECL_HPP_
// DEPRECATED: please don't include this header
#pragma message ("Warning: please don't include " __FILE__ ", it is deprecated and will be removed")

#include "mwTPoint2d.hpp"
#endif	//	MW_MWTPOINT2DDECL_HPP_
