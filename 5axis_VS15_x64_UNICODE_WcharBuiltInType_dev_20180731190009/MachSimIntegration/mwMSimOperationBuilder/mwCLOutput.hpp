/******************************************************************************
*               File: mwCLOutput.hpp	                                      *
*******************************************************************************
*               Description: this module describes the						  *
*							mwCLOutput class                                  *
*******************************************************************************
*               History:                                                      *
*  22.6.2010 Created by: Tolbariu Ionut-Irinel		                          *
*******************************************************************************
*               (C) 2003-2010 by ModuleWorks GmbH                             *
******************************************************************************/
//#############################################################################
#ifndef __MW_CL_OUTPUT_HPP__
#define __MW_CL_OUTPUT_HPP__

#include "mwMSimOperationBuilderDllDef.hpp"
#include "mwString.hpp"
#include "mwAutoPointer.hpp"
#include "mwTool.hpp"
#include "mwMemoryPoolList.hpp"
#include "mwToolPath.hpp"
//#############################################################################
namespace misc
{
	class mwOFStream;
}
namespace post
{
	class mwPostedMove;
}
//#############################################################################
template <class TPostedMoveType>
class MWMACHSIMOPERATIONBUILDER_API mwCLOutput
{
public:
	//#############################################################################
	struct Operation
	{
		//#############################################################################
	public:
		//#############################################################################
		Operation(
			const long operationNumber,
			int operationUniqueId,
			int operationParent,
			const misc::mwstring& operationComment,
			misc::mwAutoPointer< const cadcam::mwTool >& tool,
			const misc::mwstring& toolName,
			const misc::mwstring& toolComment,
			const long toolNumber,
			const double toolProfileTolerance,
			const std::vector<misc::mwstring>& axisNames,
			unsigned short precision,
			const misc::mwAutoPointer< misc::mwMemoryPoolList<TPostedMoveType> >& listPtr,
			const cadcam::mwPoint3d& partOrigin = cadcam::mwPoint3d(0,0,0))
				: m_operationNumber(operationNumber)
				, m_operationUniqueId(operationUniqueId)
				, m_operationParent(operationParent)
				, m_operationComment(operationComment)
				, m_tool(tool)
				, m_toolName(toolName)
				, m_toolComment(toolComment)
				, m_toolNumber(toolNumber)
				, m_toolProfileTolerance(toolProfileTolerance)
				, m_axisNames(axisNames)
				, m_listPtr(listPtr)
				, m_precision(precision)
				, m_partOrigin(partOrigin)
		{
		}
		//#############################################################################
		long m_operationNumber;
		int m_operationUniqueId;
		int m_operationParent;
		misc::mwstring m_operationComment;
		misc::mwAutoPointer< const cadcam::mwTool > m_tool;
		misc::mwstring m_toolName;
		misc::mwstring m_toolComment;
		long m_toolNumber;
		double m_toolProfileTolerance;
		std::vector<misc::mwstring> m_axisNames;
		misc::mwAutoPointer< misc::mwMemoryPoolList<TPostedMoveType> > m_listPtr;
		short m_precision;
		cadcam::mwPoint3d m_partOrigin;
		//#############################################################################
	};//end class Operation
	//#############################################################################
	typedef std::vector<Operation>                     Operations;
	typedef cadcam::mwMatrix<double, 4, 4>			   mwMatrix4d;
	typedef misc::mwAutoPointer<mwMatrix4d>            mwMatrix4dPtr;
	//############################################################################
	//#############################################################################
	static void Write5axmove(
		const misc::mwstring& filePath,		
		const Operations& operations,
		const mwMatrix4dPtr& aditionalTransf = MW_NULL);
	//#############################################################################
	static void WriteMachmove(
		const misc::mwstring& filePath,
		const Operations& operations);
	//#############################################################################
	static void WriteRealmove(
		const misc::mwstring& filePath,
		const Operations& operations,
		const mwMatrix4dPtr& aditionalTransf = MW_NULL);
private:
	//#############################################################################
	mwCLOutput();
	//#############################################################################
	static void WriteCommonPart(
		misc::mwOFStream& outFile,
		const misc::mwstring& clDirectoryToOutputMeshes,
		const Operation& operation);
	//#############################################################################
	static void WriteCommonPart2(
		misc::mwOFStream& outFile,		
		const measures::mwMeasurable::Units& units);
	//#############################################################################
	static void WritePostedMoveAs5axmove(
		misc::mwOFStream& outFile,		
		const post::mwPostedMove& postedMove, 
		const misc::mwstring& comment,
		unsigned short precision,
		const mwMatrix4dPtr& TR,
		const mwMatrix4dPtr& ROT);
	//#############################################################################
	static void WritePostedMoveAsMachmove(
		misc::mwOFStream& outFile,
		const post::mwPostedMove& postedMove, 
		const std::vector<misc::mwstring>& axisNames,
		const misc::mwstring& comment,
		unsigned short precision);
	//#############################################################################
	static void WritePostedMoveAsRealmove(
		misc::mwOFStream& outFile,
		const post::mwPostedMove& postedMove, 
		const std::vector<misc::mwstring>& axisNames,
		const misc::mwstring& comment,
		unsigned short precision,
		const mwMatrix4dPtr& aditionalTransf);
};//end class mwCLOutput
//#############################################################################
#endif // !defined(__MW_CL_OUTPUT_HPP__)