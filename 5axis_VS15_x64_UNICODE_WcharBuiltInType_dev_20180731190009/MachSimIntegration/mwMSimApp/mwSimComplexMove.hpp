/******************************************************************************
*               File: mwSimComplexMove.hpp                                    *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10.03.2004 09:00:50 Created by: Jutta Hoever                               *
*******************************************************************************
*               (C) 2004 by ModuleWorks GmbH                                  *
******************************************************************************/

#ifndef __mwSimComplexMove_h__
#define __mwSimComplexMove_h__

#include "mwStringConversions.hpp" 
#include "mwMachSimComplexMoveDefs.hpp"
#include "mwMachSimUtils.hpp" //for TimeTypeSeconds
#include <list>
#include "mwEnrichedPostedMove.hpp"
#include "mwMemoryMultiAllocator.hpp"
#include "mwKinematicObjectCommonDefines.hpp"
#include "mwMachSimMultiStreamOperationBuilder.hpp"
//#############################################################################

namespace opCollection
{

//#############################################################################

  //!In this class a list of simple moves and the corresponding source line number are saved.
	class mwSimComplexMove : public mwEnrichedPostedMove
    {
    
	public:

		typedef mwMachSimComplexMoveDefs::additionalInfo		additionalInfo;
		typedef mwMachSimComplexMoveDefs::Types					Types;

		typedef misc::mwEfficientVector< misc::mwstring > infoList;
		typedef infoList::iterator       infoListIt;
		typedef infoList::const_iterator infoConstListIt;

		typedef mwExtendedMoveAttributeList						ExtendedAttributes;
		typedef ExtendedAttributes::iterator					ExtendedAttributesIt;
		typedef ExtendedAttributes::const_iterator				ExtendedAttributesConstIt;

		typedef mwMachSimUtils::TimeTypeSeconds					TimeTypeSeconds;
		typedef machsim::AxisValuesContainer					AxisValuesContainer;

		typedef mwMachSimMultiStreamOperationBuilder::ReferenceStreamLocations RefStreamLocations;
		typedef misc::mwAutoPointer<const RefStreamLocations> RefStreamLocationsPtr;

//########################## Constructors #####################################

		//!Default constructor
		mwSimComplexMove();

        //!Copy constructor
        mwSimComplexMove(const mwSimComplexMove& toCopy);

        mwSimComplexMove(const mwSimComplexMove& toCopy, misc::mwMemoryMultiAllocator& allocator);

		mwSimComplexMove(const mwEnrichedPostedMove& toCopy);
//########################## Destructor #######################################

        //!Default destructor
		~mwSimComplexMove();

//############################## Sets #########################################

        const mwSimComplexMove& operator=(const mwSimComplexMove& toCopy);

        //!Set the source line number.
		/*!Sets the source line number of a simple move
		   \param sNciNr the source line number
		 */
        void SetNciNr( const unsigned int& rNciNr );

		void SetBlockNr(const unsigned int& rBlkNr);

		//!Set the completion factor ( valid domain is (0..1] )
		void SetCompletionFactor( double completionFactor );

		//!Set additional info
		void SetAdditionalInfo( const int& newInfo );

		void SetReferenceStreamLocations(const RefStreamLocations& refStreamLocList);
		

//############################## Gets #########################################
				
		//!Get the source line number
		/*!Returns the source line number of a simple move
		   \return the source line number
		 */
	    const unsigned int &GetNciNr() const;

		const unsigned int &GetBlockNr() const;

        //!Get the completion factor
		/*!Returns the completion factor
		   \return the completion factor, valid domain (0..1]
		 */
		double GetCompletionFactor() const;

        //!returns true if completion factor is not 1.0
		const bool IsInterpolatedMove() const;

		//!Get additional info
		const int &GetAdditionalInfo() const;

		const RefStreamLocationsPtr& GetReferenceStreamLocations() const;

//#############################################################################



		const bool operator== ( const mwSimComplexMove& rSimComplexMove ) const;


//#############################################################################

		
		//! Return axes value in string format (debug).
		misc::mwstring ToString() const;

	 private:

		//!Internal uique id
	    unsigned int m_nciNr;

		//!Internal id common to all submoves of a visible move
		unsigned int m_blockNr;

        int m_addInfo;

		double m_completionFactor;

		RefStreamLocationsPtr m_refStreamLocationsPtr;
  };
};

//#############################################################################

#endif //__mwSimComplexMove_h__
