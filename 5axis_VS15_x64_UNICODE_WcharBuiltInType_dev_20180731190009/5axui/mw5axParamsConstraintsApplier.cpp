/******************************************************************************
*               File: mw5axParamsConstraintsApplier.cpp                       *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  8/4/2010 11:19:17 AM Created by: Octavian Stanila                         *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/

#include "StdAfx.h"
#include "mw5axParamsConstraintsApplier.hpp"
#include "mwAutomaticParamsHandler.hpp"
#include "mwUserLicenseHelper.hpp"
#include "mw5axcoreLicenseChecker.hpp"
#include "mwParamsConflictSolver.hpp"
#include "mw5axParamsValidator.hpp"
#include "mwCutterRadiusCompParams.hpp"
#include "mwParamsConflictSolver.hpp"
#include "mwResearchProject.hpp"

//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForOpticTpCalc(mw5axParams& paramsToSet)
{
	//Set calculation method to be optic
	mwTpCalcMethodsParams tpCalcMethodsParams(paramsToSet.GetTpCalculationMethodsParams());
	tpCalcMethodsParams.SetMethod(mwTpCalcMethodsParams::TCM_OPTIC);
	paramsToSet.SetTpCalculationMethodsParams(tpCalcMethodsParams);

	// This should match TPCreatorOptic::ApplyContraints()

	// tool axis control params
	mwToolAxisControlParams toolAxisControlParam(paramsToSet.GetToolAxisControlParams());
	toolAxisControlParam.SetTiltStrategy(mw5axParams::NO_TILT);
	toolAxisControlParam.SetCurOutputType(mw5axParams::OUTPUT_TYPE_5AXIS);
	paramsToSet.SetToolAxisControlParams(toolAxisControlParam);

	paramsToSet.GetToolAxisControlParams().SetRunTool(mw5axParams::RUN_AUTO);

	// clearance height
	mwLinkParams& linkParams = paramsToSet.GetLinkParams();
	if (linkParams.GetClearanceType() != mwLinkParams::CLEARANCE_PLANE)
	{
		linkParams.SetClearancePlaneHeightDefinition(mwLinkParams::CPHD_AUTOMATIC);
	}
	linkParams.SetClearanceType(mwLinkParams::CLEARANCE_PLANE);
	linkParams.SetClearanceAxis(mwLinkParams::AXIS_Z);
	linkParams.SetIncrementalClearanceAxis(mwLinkParams::ICA_AXIS_Z);

	// first entry/last exit links
	mwFirstEntry& firstEntry = linkParams.GetFirstEntry();
	if (firstEntry.GetType() != mwFirstEntry::DIRECT &&
		firstEntry.GetType() != mwFirstEntry::FROM_RAPID_PLANE &&
		firstEntry.GetType() != mwFirstEntry::FROM_INCREMENTAL_RAPID_PLANE)
	{
		firstEntry.SetType(mwFirstEntry::DIRECT);
	}
	mwLastExit& lastExit = linkParams.GetLastExit();
	if (lastExit.GetType() != mwLastExit::DIRECT &&
		lastExit.GetType() != mwLastExit::BACK_TO_RAPID_PLANE &&
		lastExit.GetType() != mwLastExit::BACK_TO_INCREMENTAL_RAPID_PLANE)
	{
		lastExit.SetType(mwLastExit::DIRECT);
	}
}
//#############################################################################
//This should be removed after main UI for tool clearances will be made available in UIs which now call this function (multiblade/port)
void mw5axParamsConstraintsApplier::SetClearanceTypeCylindrical(mw5axParams& paramsToSet)
{
	mwCollCtrlParams collCtrlParams(paramsToSet.GetCollControl());
	collCtrlParams.SetClearanceType(mwCollCtrlParams::CC_CT_CYLINDRICAL);
	paramsToSet.SetCollControl(collCtrlParams);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForTriMeshBasedCalc3AxisOutput(mw5axParams& paramsToSet)
{
		/*Force some settings in clearance params if current TpCalcMethod is 
		triangle mesh based and pattern is Roughing and output is 3 axis
		*/
		//Set calculation method to be trianglemesh rotary 4 axis output
		mwTpCalcMethodsParams tpCalcMethodsParams(paramsToSet.GetTpCalculationMethodsParams());
		tpCalcMethodsParams.SetMethod(mwTpCalcMethodsParams::TCM_TRIANGLE_MESH_BASED);
		mwTriangleMeshBasedTpCalcParams trianglemeshParams(tpCalcMethodsParams.GetTriangleMeshBasedTpCalcParams());
		trianglemeshParams.SetPattern(mwTriangleMeshBasedTpCalcParams::TC_TMB_ROUGH);
		tpCalcMethodsParams.SetTriangleMeshBasedTpCalcParams(trianglemeshParams);
		mwToolAxisControlParams tacParams(paramsToSet.GetToolAxisControlParams());
		tacParams.SetCurOutputType(mw5axParams::OUTPUT_TYPE_3AXIS);
		paramsToSet.SetToolAxisControlParams(tacParams);
		paramsToSet.SetTpCalculationMethodsParams(tpCalcMethodsParams);

		mwLinkParams linkParams (paramsToSet.GetLinkParams());
		linkParams.SetClearanceType(mwLinkParams::CLEARANCE_PLANE);
		paramsToSet.SetLinkParams(linkParams);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForTriMeshBasedCalcRotary4AxisOutput(mw5axParams& paramsToSet)
{
		mwLinkParams linkParams (paramsToSet.GetLinkParams());
		linkParams.SetClearanceType(mwLinkParams::CLEARANCE_CYLINDER);
		linkParams.SetClearanceAxis(mwLinkParams::ROTARY_AXIS);
		paramsToSet.SetLinkParams(linkParams);

		ApplyConstraintsForTriMeshBasedCalcRotary(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForTriMeshBasedCalcRotary(mw5axParams& paramsToSet)
{
		/*Force some settings in clearance params if current TpCalcMethod is 
		triangle mesh based and pattern is Rotary 
		*/

		//Set calculation method to be trianglemesh rotary
		mwTpCalcMethodsParams tpCalcMethodsParams(paramsToSet.GetTpCalculationMethodsParams());
		tpCalcMethodsParams.SetMethod(mwTpCalcMethodsParams::TCM_TRIANGLE_MESH_BASED);
		mwTriangleMeshBasedTpCalcParams trianglemeshParams(tpCalcMethodsParams.GetTriangleMeshBasedTpCalcParams());
		trianglemeshParams.SetPattern(mwTriangleMeshBasedTpCalcParams::TCB_TMB_ROTARY);
		tpCalcMethodsParams.SetTriangleMeshBasedTpCalcParams(trianglemeshParams);
		paramsToSet.SetTpCalculationMethodsParams(tpCalcMethodsParams);

		mwToolAxisControlParams tacParams (paramsToSet.GetToolAxisControlParams());
		tacParams.SetCurOutputType(mw5axParams::OUTPUT_TYPE_4AXIS)  ;
#pragma warning( push )
#pragma warning (disable : 4996)
		/// TMB Rotary 4 Axis must be X-axis
		tacParams.SetCur4Axis(cadcam::mwVector3d(1.,0.,0.));
#pragma warning( pop )
		paramsToSet.SetToolAxisControlParams(tacParams);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForTriMeshBasedNoContainment(mw5axParams& paramsToSet)
{
	
	//Set calculation method to be trianglemesh rough
	mwTpCalcMethodsParams tpCalcMethodsParams(paramsToSet.GetTpCalculationMethodsParams());
	tpCalcMethodsParams.SetMethod(mwTpCalcMethodsParams::TCM_TRIANGLE_MESH_BASED);
	mw2dContainmentParams containment2dParams (paramsToSet.Get2dContainmentParams());
	containment2dParams.SetIsUsedFlg(false);
	paramsToSet.Set2dContainmentParams(containment2dParams);

	mwTriangleMeshBasedTpCalcParams trianglemeshParams(tpCalcMethodsParams.GetTriangleMeshBasedTpCalcParams());
	trianglemeshParams.SetSilhouetteContainmentFlg(true);

	const bool is2DContainmentSelected = trianglemeshParams.GetLinksCheckContainmentType() ==  mwTriangleMeshBasedTpCalcParams::LCT_2D_CONTAINMENT_GEO;
	if(is2DContainmentSelected)
	{
		mwCollCtrlParams collParams=paramsToSet.GetCollControl();
		collParams.SetCheckLinksAgainstContainmentFlg(false);
		paramsToSet.SetCollControl(collParams);
	}

	trianglemeshParams.SetPattern(mwTriangleMeshBasedTpCalcParams::TC_TMB_ROUGH);
	tpCalcMethodsParams.SetTriangleMeshBasedTpCalcParams(trianglemeshParams);
	paramsToSet.SetTpCalculationMethodsParams(tpCalcMethodsParams);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForSwarfMillingTpCalc(mw5axParams& paramsToSet)
{
	//Apply constraints regarding collision checking for SwarfMilling calculation
	mwCollCtrlParams collCtrl = paramsToSet.GetCollControl();
	for( unsigned int i = 0; i < mwCollCtrlParams::GetNoOfCollCtrlOperations(); ++i )
	{
		if(collCtrl.GetCollCtrlOperations()[i].GetStatus())
		{
			mwCollCtrlOpParams ctrlOp = paramsToSet.GetCollControl().GetCollCtrlOperations()[i];
			ctrlOp.SetStatus(false);
			collCtrl.SetCollCtrlOperation(ctrlOp, i);
		}
	}
	paramsToSet.SetCollControl(collCtrl);

	mwParamsConflictSolver::ApplyCollisionCheckConstraints4SwarfingCalculation(paramsToSet);

	//This should be removed after main UI for tool clearances will be made visible in SwarfMilling UI
	SetClearanceTypeCylindrical(paramsToSet);

	/*If user doesn't have SwarfMilling Advanced license and parameters settings are for SwarfMilling Advanced calculation
	the we apply constraints for Basic SwarfMilling calculation*/
	if(mwUserLicenseHelper::IsSwarfingTpCalcAdvancedLicenseActive() == false 
		&& mw5axcoreLicenseChecker::IsSwarfingBasicCalculation(paramsToSet)==false)
	{
		mwParamsConflictSolver::ApplyContraints4BasicSwarfingCalculation(paramsToSet);
	}
	mwSwarfMillingBasedTpCalcParams swarfParams(paramsToSet.GetTpCalculationMethodsParams().GetSwarfMillingBasedTpCalcParams());
	//Apply cut method limitations
	if ( paramsToSet.GetCurMachType() == mw5axParams::MACHTYPE_ZIGZAG 
		&&!mw5axParamsValidator::IsZigZagCutMethodAllowed4SwarfMilling(swarfParams.GetNumberOfSlices(),
		swarfParams.GetNumberOfLayers(),
		swarfParams.GetSlicesCreationMode()))
	{
		paramsToSet.SetCurMachType(mw5axParams::MACHTYPE_ONEWAY); 
	}
	
	mwParamsConflictSolver::AdjustToolAxisControlParamsForSwarf(paramsToSet);

	paramsToSet.GetToolAxisControlParams().SetRunTool(mw5axParams::RUN_AUTO);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframe2dPatterns(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyToolAxisControlOutputFormatConstraints(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframe5axisProfile(mwToolAxisControlParams& tacParams)
{
	
	tacParams.SetPointToolToRotAxisFlg(true);
		
}

namespace
{
void ApplyLinksBetweenRegionsConstraints(mw5axParams &paramsToSet)
{
	const mwMoveHandling::Action action =
		paramsToSet.GetLinkParams().GetLinkBetweenPasses().GetSmallMoveHandling().GetAction();
	if (action == mwMoveHandling::ACTION_GAP_DIRECT
		|| action == mwMoveHandling::ACTION_GAP_BLEND_SPLINE)
	{
		paramsToSet.GetLinkParams().GetLinkBetweenPasses().
			GetSmallMoveHandling().SetAction(mwMoveHandling::ACTION_GAP_BROKEN_FEED);
	}
}
}  // namespace

void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframe2AxisProfile(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyToolAxisControlOutputFormatConstraints(paramsToSet);

	const mwCutterRadiusCompParams& crcParams = paramsToSet.GetTpCalculationMethodsParams().
		GetWireframeBasedTpCalcParams().GetCutterRadiusCompParams();

	if (crcParams.GetCompensationType() == mwCutterRadiusCompParams::CT_WEAR
			|| crcParams.GetCompensationType() == mwCutterRadiusCompParams::CT_INVERSE_WEAR)
	{
		mwLeadParams& leadInParams = paramsToSet.GetLinkParams().GetLeadInDefaultParams();
		if(leadInParams.GetType() != mwLeadParams::ORTHOGONAL_LINE
			&& leadInParams.GetType() != mwLeadParams::HORIZONTAL_TANG_ARC)
		{
			leadInParams.SetType(mwLeadParams::ORTHOGONAL_LINE);
			leadInParams.GetExtension().SetType(mwLeadExtensionParams::TP_NONE);
		}

		mwLeadParams& leadOutParams = paramsToSet.GetLinkParams().GetLeadInDefaultParams();
		if(leadOutParams.GetType() != mwLeadParams::ORTHOGONAL_LINE
			&& leadOutParams.GetType() != mwLeadParams::HORIZONTAL_TANG_ARC)
		{
			leadOutParams.SetType(mwLeadParams::ORTHOGONAL_LINE);
			leadOutParams.GetExtension().SetType(mwLeadExtensionParams::TP_NONE);
		}
	}
}

void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframeEngrave(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyToolAxisControlOutputFormatConstraints(paramsToSet);

	ApplyLinksBetweenRegionsConstraints(paramsToSet);

	paramsToSet.GetLinkParams().SetFeedDistanceMode(mwLinkParams::FDM_PREVIOUS_Z_HEIGHT);
}

void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframeFace(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForWireframeFace(paramsToSet);
}

void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframe3AxisProfile(mw5axParams& dst)
{
	mwParamsConflictSolver::ApplyLeadParametersFor3AxisOutputFormat(dst);
	mwParamsConflictSolver::ApplyToolAxisControlOutputFormatConstraints(dst);
}

void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframeTrochoidal(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyToolAxisControlOutputFormatConstraints(paramsToSet);

	ApplyLinksBetweenRegionsConstraints(paramsToSet);
}

//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForImpellerSwarf( mw5axParams &paramsToSet )
{
	//Set number of layers for Impeller Swarfing
	const mwImpellerParams impellerParams = paramsToSet.GetTpCalculationMethodsParams().GetImpellerParams();
	const bool isBladeFinishingPattern = impellerParams.GetPatternParams().GetPattern() == mwImpellerPatternParams::IP_BLADE_FINISHING;
	const bool isSwarf = impellerParams.GetPatternParams().GetSubPattern() == mwImpellerPatternParams::ISP_SWARF;

	if(isBladeFinishingPattern && isSwarf)
	{
		paramsToSet.GetTpCalculationMethodsParams().GetImpellerParams().GetMachLayerDefParams().SetNoLayers(1);
		paramsToSet.GetTpCalculationMethodsParams().GetSwarfMillingBasedTpCalcParams().SetCheckSwarfSurfacesForDegouge(true);
		paramsToSet.GetTpCalculationMethodsParams().GetSwarfMillingBasedTpCalcParams().SetCheckCheckAdditionalCheckSurfacesForDegouge(false);
	}
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForWireframeExtrude( mw5axParams &paramsToSet )
{
		mwCollCtrlParams control=paramsToSet.GetCollControl();

		//Disable coll control operations for wireframe extrude
		control.GetCollCtrlOperations()[0].SetStatus(false);
		control.GetCollCtrlOperations()[1].SetStatus(false);
		control.GetCollCtrlOperations()[2].SetStatus(false);
		control.GetCollCtrlOperations()[3].SetStatus(false);

		paramsToSet.SetCollControl(control);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForDrillingTpCalc(mw5axParams& paramsToSet)
{
	paramsToSet.GetToolAxisControlParams().SetRunTool(mw5axParams::RUN_AUTO);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForSawMachiningTpCalc(mw5axParams& paramsToSet)
{
	paramsToSet.GetToolAxisControlParams().SetRunTool(mw5axParams::RUN_AUTO);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForRotaryTpCalc(mw5axParams& paramsToSet)
{
	paramsToSet.GetToolAxisControlParams().SetRunTool(mw5axParams::RUN_AUTO);
	mwParamsConflictSolver::AdjustLinkParametersForRotary(paramsToSet);
}

//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForGeodesicMachiningTpCalc(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForGeodesicMachining(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForSurfaceBasedGeodesic(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForSurfaceBasedGeodesic(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForMultiBladeTpCalc(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForMultiBladeTpCalc(paramsToSet);
	if (mwUserLicenseHelper::IsMultiBladeTpCalcAdvancedLicenseActive() == false //If user doesn't have MultiBlade Advanced license and parameters settings are for MultiBlade Advanced calculation
		&& mw5axcoreLicenseChecker::IsMultiBladeBasicCalculation(paramsToSet) == false)
	{
		mwParamsConflictSolver::ApplyContraints4BasicImpellerCalculation(paramsToSet);//Apply constraints for Basic MultiBlade calculation
	}
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForPortMachiningTpCalc(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForPortMachiningTpCalc(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplySameHandling2InterlinkSmallAndLargeMoves(const mwMoveHandling::Action& handling2Apply, mwInterlinkHandeler& interLink)
{
	mwParamsConflictSolver::ApplySameHandling2InterlinkSmallAndLargeMoves(handling2Apply, interLink);
}
//#############################################################################
void mw5axParamsConstraintsApplier::SetAutomaticValues2ParamsForMultiBladeTpCalc(mw5axParams& toSet)
{
	mwParamsConflictSolver::SetAutomaticValues2ParamsForMultiBladeTpCalc(toSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::SetLinkParamsForPortMachining(mwLinkParams& paramsToSet, bool isFinishingAlong)
{
	mwParamsConflictSolver::SetLinkParamsForPortMachining(paramsToSet, isFinishingAlong);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForAutomatic3plus2AxisRoughing(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForAutomatic3plus2AxisRoughing(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForTurnMilling(mw5axParams& paramsToSet)
{
	//Apply cut direction limitations
	if (paramsToSet.GetMachDirForOneWay() == mw5axParams::DIR_CLOCKWISE || 
		paramsToSet.GetMachDirForOneWay() == mw5axParams::DIR_COUNTER_CLOCKWISE ||
		paramsToSet.GetMachDirForOneWay() == mw5axParams::DIR_FOLLOW_CURVE_CHAINING)
	{
		paramsToSet.SetMachDirForOneWay(mw5axParams::DIR_CLIMB); 
	}
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForDeburring(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForDeburring(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForLink2Toolpaths(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForLink2Toolpaths(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForTMBGeodesicTpCalc(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForTMBGeodesic(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForTMB3Plus2multipleDirections(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForTMB3Plus2multipleDirections(paramsToSet);
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForLinkParamsMachineAwareness(mw5axParams& paramsToSet)
{
	if (paramsToSet.GetTpCalculationMethodsParams().GetMethod() == mwTpCalcMethodsParams::TCM_EXISTING_TOOLPATH_BASED
		&& paramsToSet.GetTpCalculationMethodsParams().GetExistingToolpathBasedTpCalcParams().GetPattern()
		== mwExistingToolpathBasedTpCalcParams::TC_ETB_MACHINE_AWARENESS)
	{
		mwLinkParams& linkParams = paramsToSet.GetLinkParams();

		if (linkParams.GetGapsAlongCut().GetGapSize().IsPercent())
		{
			linkParams.GetGapsAlongCut().GetGapSize().SetValue(20.);
			linkParams.GetLinkBetweenSlices().GetGapSize().SetValue(20.);
		}
	}
}
//#############################################################################
void mw5axParamsConstraintsApplier::ApplyConstraintsForMachineAwareness(mw5axParams& paramsToSet)
{
	mwParamsConflictSolver::ApplyConstraintsForMachineAwareness(paramsToSet);
}
//#############################################################################
