/******************************************************************************
  (C) 2013 by ModuleWorks GmbH
  Author: Yunus Smailov
******************************************************************************/

#include "StdAfx.h"
#include "5axui.h"
#include "mwMessages.hpp"
#include "mwTriangleTurningTool.hpp"
#include "mwTriangleTurningToolDlg.h"
#include "mwException.hpp"
#include "mw5axuiException.hpp"
#include "mw5axuiCustomMsgs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

mwTriangleTurningToolDlg::mwTriangleTurningToolDlg(
	mw5axParams& params,
	const ToolPtr& crtTool,
	mwNotifyParamInteractor& interActor,
	CWnd* pParent)
	: mwTurningToolDlg(params, crtTool, interActor, mwTriangleTurningToolDlg::IDD, pParent),
	m_diameter(0),
	m_cornerRadius(0),
	m_thickness(0)
{
	m_triangleTurningTool = new cadcam::mwTriangleTurningTool(params.GetUnits());
	m_rcImage.SetCenter();
}

mwTriangleTurningToolDlg::mwTriangleTurningToolDlg(
	const cadcam::mwTriangleTurningTool& tool,
	mw5axParams& params,
	const ToolPtr& crtTool,
	mwNotifyParamInteractor& interActor,
	CWnd* pParent)
	: mwTurningToolDlg(params, crtTool, interActor, mwTriangleTurningToolDlg::IDD, pParent),
	m_diameter(tool.GetInscribedCircleDiameter()),
	m_cornerRadius(tool.GetCornerRadius())
{
	m_triangleTurningTool = new cadcam::mwTriangleTurningTool(tool);
	m_rcImage.SetCenter();
}

void mwTriangleTurningToolDlg::DoDataExchange(CDataExchange* pDX)
{
	if(pDX->m_bSaveAndValidate == NULL)
	{
		mw5axuiDlgsParams::GetDlgParams(*pDX);

		PaintBMPs();
		SetTexts();

		ShowControlsUpdated();
	}

	mwToolDlg::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TURNING_TOOL_TRIANGLE_DLG_IMG_STATIC, m_rcImage);

	DDX_Text(pDX, IDC_TURNING_TOOL_TRIANGLE_DLG_EDT_DIAMETER, m_diameter);
	DDX_Text(pDX, IDC_TURNING_TOOL_TRIANGLE_DLG_EDT_CORNER_RADIUS, m_cornerRadius);
	DDX_Text(pDX, IDC_TURNING_TOOL_TRIANGLE_DLG_EDT_THICKNESS, m_thickness);

	if(pDX->m_bSaveAndValidate)
	{
		mw5axuiDlgsParams::SetDlgParams(*pDX);
	}
}

BEGIN_MESSAGE_MAP(mwTriangleTurningToolDlg, mwTurningToolDlg)
	ON_EN_SETFOCUS(IDC_TURNING_TOOL_TRIANGLE_DLG_EDT_DIAMETER, OnSetfocusToolDiameter)
	ON_EN_SETFOCUS(IDC_TURNING_TOOL_TRIANGLE_DLG_EDT_CORNER_RADIUS, OnSetfocusCornerRadius)
	ON_EN_SETFOCUS(IDC_TURNING_TOOL_TRIANGLE_DLG_EDT_THICKNESS, OnSetfocusToolThickness)
END_MESSAGE_MAP()

void mwTriangleTurningToolDlg::SetTexts()
{
	SetCntrlText(
		IDC_TURNING_TOOL_TRIANGLE_DLG_LBL_DIAMETER, MSG_TURNING_TOOL_DLG_LBL_TOOL_DIAMETER);
	SetCntrlText(
		IDC_TURNING_TOOL_TRIANGLE_DLG_LBL_CORNER_RADIUS, MSG_TURNING_TOOL_DLG_LBL_TOOL_CORNER_RADIUS);
	SetCntrlText(
		IDC_TURNING_TOOL_TRIANGLE_DLG_LBL_THICKNESS, MSG_TURNING_TOOL_DLG_LBL_TOOL_THICKNESS);
}

void mwTriangleTurningToolDlg::UpdateWindowControls()
{
}

void mwTriangleTurningToolDlg::GetParams()
{
	m_cornerRadius = m_triangleTurningTool->GetCornerRadius();
	m_diameter = m_triangleTurningTool->GetInscribedCircleDiameter();
	m_thickness = m_triangleTurningTool->GetThickness();

	SetHolder(m_triangleTurningTool->GetHolder());
}

void mwTriangleTurningToolDlg::PaintBMPs()
{
	SetRCImage(IDB_TURNING_TOOL_PAGE_INSERT_TRIANGLE_DLG,
		IDC_TURNING_TOOL_TRIANGLE_DLG_IMG_STATIC,
		_T("mwTriangleTurningToolDlg"));
}

void mwTriangleTurningToolDlg::SetParams()
{
	try
	{
		*m_triangleTurningTool = cadcam::mwTriangleTurningTool(
			GetHolder(),
			m_thickness,
			m_cornerRadius,
			m_diameter);
	}
	catch(misc::mwException& e)
	{
		throw mw5axuiException(e, _T("mwTriangleTurningToolDlg::GetParams"));
	}
}

misc::mwAutoPointer<cadcam::mwTool> mwTriangleTurningToolDlg::GetToolPtr()
{
	return m_triangleTurningTool;
}

misc::mwAutoPointer<cadcam::mwTool> mwTriangleTurningToolDlg::GetCopyOfTool()
{
	return new cadcam::mwTriangleTurningTool(*m_triangleTurningTool);
}

void mwTriangleTurningToolDlg::OnSetfocusToolDiameter()
{
	SendToolComment(MSG_TURNING_TOOL_DLG_DEF_TOOL_DIAMETER);
}

void mwTriangleTurningToolDlg::OnSetfocusCornerRadius()
{
	SendToolComment(MSG_TURNING_TOOL_DLG_DEF_CORNER_RADIUS);
}

void mwTriangleTurningToolDlg::OnSetfocusToolThickness()
{
	SendToolComment(MSG_TURNING_TOOL_DLG_DEF_THICKNESS);
}

void mwTriangleTurningToolDlg::SendToolComment(const int commentID)
{
	if (GetParent() == NULL)
		return;

	GetParent()->SendMessage(WM_5AXUI_TURNING_TOOL_COMMENT_CHANGED, commentID, 0);
}

UINT mwTriangleTurningToolDlg::GetIDD()
{
	return IDD_TURNING_TOOL_TRIANGLE_DLG;
}

void mwTriangleTurningToolDlg::SetMsgIDForToolName()
{
	m_msgIDForToolName = MSG_TURNING_TOOL_PAGE_DEF_INSERT_TRIANGLE;
}
