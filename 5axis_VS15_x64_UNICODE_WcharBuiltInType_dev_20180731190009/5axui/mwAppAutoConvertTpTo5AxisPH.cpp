/******************************************************************************
*               File: mwAppAutoConvertTpTo5AxisPH.cpp                         *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  6/24/2009 3:14:42 PM Created by: Octavian Stanila                         *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/

#include "StdAfx.h"
#include "mwAppAutoConvertTpTo5AxisPH.hpp"
#include "mwExceptionHandler.hpp"
#include "mw5axuiException.hpp"
#include "mwToolDlg.h"
#include "mwToolDlgsSet.hpp"
#include "mwAppAutoConvertTpTo5AxisPage.hpp"
#include "mwLimitationsApplier.hpp"
#include "mwParamHandlerVisitor.hpp"

//#############################################################################
const unsigned long mwAppAutoConvertTpTo5AxisPH::NOT_AVAILABLE_PAGES= 
	mwParamHandler::AXUI_TOOLRATES_PAGE    
	| mwParamHandler::AXUI_SURF_PATHS_PAGE    
	| mwParamHandler::AXUI_TILT_PAGE 
	| mwParamHandler::AXUI_COLL_PAGE 
	| mwParamHandler::AXUI_LINK_PAGE
	| mwParamHandler::AXUI_ROUGHING_PAGE      
	| mwParamHandler::AXUI_UTIL_PAGE          
	| mwParamHandler::AXUI_MACH_DEF;
//#############################################################################
const short mwAppAutoConvertTpTo5AxisPH::SPECIAL_PAGES_START_INDEX = 1;
//#############################################################################
mwAppAutoConvertTpTo5AxisPH::mwAppAutoConvertTpTo5AxisPH(mw5axParams &toSet, const mwParamInteractor &pia,
														HINSTANCE resHandler,HINSTANCE neutralResHandler,
														CWnd* pParentWnd  )
 :mwParamHandler(toSet,pia,resHandler,neutralResHandler,pParentWnd),
 m_ShowPage(true),
 m_ShowToolPage(true),
 m_SpecialPagesStartIndex(SPECIAL_PAGES_START_INDEX)
{

	ApplyConstVals(mPmrs);
	mwParamHandler::RemovePages(NOT_AVAILABLE_PAGES);
}
//#############################################################################
void mwAppAutoConvertTpTo5AxisPH::RemovePages( unsigned long pages)
{
	if (pages > APP_AUTO_CONVERT_TOOLPATH25AXIS_ALL)
		throw mw5axuiException( mw5axuiException::PROPERTY_PAGE, _T("mwAppAutoConvertTpTo5AxisPH::RemovePages"));
	
	m_SpecialPagesStartIndex=SPECIAL_PAGES_START_INDEX;
	unsigned long stdPages=0;
	if (APP_AUTO_CONVERT_TOOLPATH25AXIS_TOOL_PAGE & pages)
	{
		stdPages|=mwParamHandler::AXUI_TOOL_PAGE;
		m_SpecialPagesStartIndex--;
		m_ShowToolPage=false;
	}
	else
	{
		m_ShowToolPage=true;
	}
	
	if (APP_AUTO_CONVERT_TOOLPATH25AXIS_PAGE & pages)
	{
		m_ShowPage=false;
	}
	else
	{
		m_ShowPage=true;
	}

	mwParamHandler::RemovePages(stdPages | NOT_AVAILABLE_PAGES);
}
//#############################################################################
void mwAppAutoConvertTpTo5AxisPH::SetParamsUsingToolPages(misc::mwAutoPointer<cadcam::mwTool> &curtool, mwToolDlgsSet &rToolDlgs)
{

	bool warningShownAlready=false;
	
	//remove not allowed tool pages		
	
	if (m_ShowToolPage)
	{
		for (size_t i=0;i<rToolDlgs.GetNoOfPages ();i++)
			if (rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_ENDMILL
				|| rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_BULLMILL
				|| rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_SLOTMILL
				|| rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_DOVEMILL
				|| rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_CHAMFERMILL
				|| rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_BARRELMILL
				|| rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_CONVEXTIPMILL
				|| rToolDlgs.GetPage (i)->GetIDD()== IDD_TOOL_DRILLMILL
				)
			{
				try
				{	
					rToolDlgs.RemovePage(i);
				}catch (mw5axuiException &e)
				{
					if (!warningShownAlready)
					{
						mwExceptionHandler::HandleErr(e , *iaProvider);
						warningShownAlready=true;
					}
				}
				i--;
			}
	}

	m_appAutoConvTpTo5AxPage = new mwAppAutoConvertTpTo5AxisPage(curtool,mPmrs,*iaProvider,mwParamHandler::m_uiObserver);
	//add special page if the user did not removed it already
	if (m_ShowPage)
	{
		short offset=0;
		for (unsigned short i=0;i<m_customPropPages.size();i++)
		{
			if (m_customPropPages[i].second<=(m_SpecialPagesStartIndex+offset))
			{
				offset++;
			}
		}
		mwParamHandler::AddPage (*m_appAutoConvTpTo5AxPage,m_SpecialPagesStartIndex+offset);
	}
	//call parent function
	mwParamHandler::SetParamsUsingToolPages(curtool, rToolDlgs);
}
//#############################################################################
void mwAppAutoConvertTpTo5AxisPH::LimitInterfaceOptions(const mwOptionsLimiter& opt)
{
	mwParamHandler::m_OptionsForPages  = opt; 
}
//#############################################################################
void mwAppAutoConvertTpTo5AxisPH::ApplyConstVals(mw5axParams& paramsToSet)
{
	
	mwTpCalcMethodsParams calcMethodsParams(paramsToSet.GetTpCalculationMethodsParams());
	mwToolAxisControlParams tacParams(paramsToSet.GetToolAxisControlParams());

	//relink options constant values
	mwExistingToolpathBasedTpCalcParams existingTpCalcParams(calcMethodsParams.GetExistingToolpathBasedTpCalcParams());
	existingTpCalcParams.SetConversionLinkType(mwExistingToolpathBasedTpCalcParams::USE_LINKS_FROM_INPUT_TOOLPATH);
	calcMethodsParams.SetExistingToolpathBasedTpCalcParams(existingTpCalcParams);

	//tilting parameters constant values
	tacParams.SetCurOutputType(mw5axParams::OUTPUT_TYPE_5AXIS);
	tacParams.SetTiltStrategy(mw5axParams::FIXED_ANGLE);
	tacParams.SetPointToolToRotAxisFlg(false);
	paramsToSet.SetMaxAngleChange(3.);
	tacParams.SetTiltAngleFixed(0.);
	tacParams.SetTiltAxis(mw5axParams::EXT_AXIS_Z);
	tacParams.SetAxisMeetTiltFlg(true);
	tacParams.SetReverseToolFlg(false);
	tacParams.SetRunTool(mw5axParams::RUN_AUTO);

	paramsToSet.SetToolAxisControlParams(tacParams);//set tool axis control params 

	//collision parameters constant values
	mwCollCtrlParams collParams = paramsToSet.GetCollControl();
	
	collParams.SetCheckBetweenPts(true);
	collParams.SetExtendToolToInfinityFlg(true);
	collParams.SetCheckLinkMotionsFlg(true);
	collParams.SetReportRemainingColls(false);
	collParams.SetCheckTipRadiusFlg(false);
	collParams.SetToolShaftLowerClearance(0.);
	collParams.SetToolShaftUpperClearance(0.);

	for (unsigned int i = 0 ; i < paramsToSet.GetCollControl().GetNoOfCollCtrlOperations() ; ++i)
	{
		mwCollCtrlOpParams collOpParams=paramsToSet.GetCollControl().GetCollCtrlOperations()[i];
		
		if(i==0)
		{
			collOpParams.SetStatus(true);
			collOpParams.SetCheckToolTip(true);
			collOpParams.SetCheckToolShaft(true);
			collOpParams.SetCheckArbor(true);
			collOpParams.SetCheckHolder(true);
			collOpParams.SetStrategy(mwCollCtrlOpBaseParams::CS_TILT_TOOL_AWAY);
			collOpParams.SetAvoidTiltDir(mwCollCtrlOpParams::CONVERSION_3_TO_5_AXIS);
			collOpParams.SetCheckSurfaces(true);
			collOpParams.SetCheckDriveSurfaces(false);
		}
		else
		{
			collOpParams.SetStatus(false);
		}

		collParams.SetCollCtrlOperation(collOpParams, i);
	}

	paramsToSet.SetCollControl(collParams);


	//For this App  the current calculation method must be based on existing toolpath
	calcMethodsParams.SetMethod(mwTpCalcMethodsParams::TCM_EXISTING_TOOLPATH_BASED);
	paramsToSet.SetTpCalculationMethodsParams(calcMethodsParams);
}
//#############################################################################
void mwAppAutoConvertTpTo5AxisPH::EndUIAppSpecific()
{
	if(m_appAutoConvTpTo5AxPage != NULL)
	{
		delete m_appAutoConvTpTo5AxPage ;
	}
}
//#############################################################################
void mwAppAutoConvertTpTo5AxisPH::AcceptVisitor( mwParamHandlerVisitor& visitor) const
{
	visitor.Visit(*this);
}
//#############################################################################