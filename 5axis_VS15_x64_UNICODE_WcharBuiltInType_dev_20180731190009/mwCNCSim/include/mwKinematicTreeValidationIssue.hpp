#pragma once

#include "mwCNCSimDllImportExport.hpp"
#include "mwString.hpp"
#include "mwMachineItemNode.hpp"
#include "mwCNCSimEnumTypes.hpp"
#include <vector>

namespace cncsim
{
	class MW_CNCSIM_API mwKinematicTreeValidationIssue
	{
	public:
		mwKinematicTreeValidationIssue();

		void SetIssueText(const misc::mwstring& message) { m_message = message; }
		const misc::mwstring& GetIssueText() const { return m_message; }

		void SetAffectedNodes(const std::vector<mwMachineItemNode>& node) { m_nodes = node; }
		const std::vector<mwMachineItemNode>& GetAffectedNodes() const { return m_nodes; }

		void SetIssueType(const mwIssueType issueType) { m_issueType = issueType; }
		mwIssueType GetIssueType() const { return m_issueType; }

		bool operator==(const mwKinematicTreeValidationIssue& rhs);
		bool operator!=(const mwKinematicTreeValidationIssue& rhs)
		{
			return !(*this == rhs);
		}

	private:
#pragma warning(push)
#pragma warning(disable:4251) // hide warning C4251 (class needs to have dll-interface) for included core class
		misc::mwstring m_message;
		std::vector<mwMachineItemNode> m_nodes;
#pragma warning(pop)
		mwIssueType m_issueType;
	};
}