// mwAppElectrodeMachining4Plus1AxisPage.cpp: Implementierungsdatei
//

#include "StdAfx.h"
 
#include "mwStringConversions.hpp"
#include "mwMultiCutsDlg.h"
#include "mwCustomAxisSelectionDlg.h"
#include "mwAppElectrode_Machining_4Plus1_AxisPage.h"
#include "mwLimitCutsBetween2Points.h"
#include "mwMarginsDlg.h"
#include "mwCleanupDlg.h"
#include "mwExtendTrimDlg.h"
#include "mwSplitDlg.h"
#include "mwContainmentDlg.h"
#include "mwPickPntOrDirDlg.h"
#include "mwLeadBothSetupDlg.hpp"
#include "mwToolAngleLimitsDlg.h"
#include "mwExceptionHandler.hpp"
#include "mwMessages_APP_Electrode_Machining_4Plus1_Axis.hpp"
#include "mw5axParamsEx.hpp"
#include "mwCollCtrlParams.hpp"
#include "mwStartPointDlg.h"
#include "mwTpCalcMethodsDefs.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite mwAppElectrodeMachining4Plus1AxisPage 

//IMPLEMENT_DYNCREATE(mwAppElectrodeMachining4Plus1AxisPage, CClientPropertyPage)

mwAppElectrodeMachining4Plus1AxisPage::mwAppElectrodeMachining4Plus1AxisPage( const misc::mwAutoPointer<cadcam::mwTool> &curtool,
																			  mw5axParams &r5axParams,
																			  mwNotifyParamInteractor &interActor,
																			  mw5axuiClientObserver* uiObserver)
: mw5axuiPage(curtool,r5axParams,interActor,IDD ,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS,uiObserver )

{
	//{{AFX_DATA_INIT(mwAppElectrodeMachining4Plus1AxisPage)

	//GougePage
	m_collOp1Status = FALSE;
	//m_enable5axisCollCheckSelected = FALSE;

	//TiltPage
	m_maxAngleChange =0.0;
	m_tilt_limits_selected = FALSE;

	//RoughingPage
	m_avoid_air_cuts = FALSE;
	
	m_clearanceTypeCmbIndex				 = -1;
	m_useClearance = TRUE;
	m_clearanceHeight = 0.0;
	m_clearanceRadius = 0.0;
	m_clearanceSph_X =0;
	m_clearanceSph_Y =0;
	m_clearanceSph_Z =0;
	m_clearanceCyl_X =0;
	m_clearanceCyl_Y =0;
    m_clearanceCyl_Z =0;
	m_stock_remain = 0.0;
	m_topofstock = 0.0;
	m_distanceRapid  = 0.0;
	m_entryFeedDistance   = 0.0;
	m_exitFeedDistance   = 0.0;
	m_tilt_to_axis = -1;
	m_toolaxis_meet_tilt = FALSE;
	m_output_type = TP_OUTPUT_TYPE_5AXIS;
	m_tilt_strategy = TP_FIXED_ANGLE;
	m_CurveTiltType = -1;	
	m_reverse_cuts = FALSE;		
	m_cutting_area = -1;
	m_cutType = -1;
	m_cutMethod = -1;
	m_cut_order = -1;
	m_directionForOneWay = -1;
	m_enforceClosedContour = FALSE;
	m_startPos_sw = FALSE;	
	m_maxStepOver  = 0.0;	
	m_chainingTol = 0.0;
	
	m_moveSizeAsPercentageOfStepover = 0.0;	
	//}}AFX_DATA_INIT
	
	m_cutting_area_value=static_cast<DWORD_PTR>(-1);
	m_nIDResBmp = IDB_LINK_DLG_MOVE_SIZE_PERC_OF_STEPOVER;

	//m_enable5axisCollCheckVisible = false;
	m_CutTolVisible = true;
	m_cutTol = 0.0;

	m_clearanceType = 0;
	m_clearance_axis = 0;
	m_distanceAirMove =0.;
	m_tilt_angle_fixed =0.;
}
//#############################################################################
mwAppElectrodeMachining4Plus1AxisPage::~mwAppElectrodeMachining4Plus1AxisPage()
{
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if( pDX->m_bSaveAndValidate == NULL )
	{
		GetDlgParams(*pDX);
		
		SetTexts();
		
		if( m_cutType == mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PARALLEL)
			OnParallel();
	
		ReloadCuttingAreaBox();

		m_checkCollisionBtn.SetChecked( (m_Job.GetCollControl().GetCollCtrlOperations())[mwCollCtrlParams::CCO_SECOND].GetStatus() );
//		m_enable5axisCollCheckSelected = (m_Job.GetCollControl().GetCollCtrlOperations())[mwCollCtrlParams::CCO_SECOND].GetStatus() ;
	
//		m_enable5axisCollCheckVisible = m_checkCollisionBtn.IsChecked();
		ShowControlsUpdated();

	}

	mw5axuiPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwAppElectrodeMachining4Plus1AxisPage)

	//GougePage
	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ChkCollOp1_STATUS, m_collOp1Status);
//	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENABLE_5AXIS_COLL_CHECK, m_enable5axisCollCheckSelected);

	//TiltPage
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MAX_ANGLE_CHANGE, m_maxAngleChange);
	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_LIMITS, m_tilt_limits_selected);


	//RoughingPage
	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STOCKDEF_AVOID_AIR_CUTS, m_avoid_air_cuts);
	
	//UtilPage
	

	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STOCKREMAIN_VALUE, m_stock_remain);
	DDX_Control(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_COLLISION_CHECK, m_checkCollisionBtn);
	DDX_CBIndex(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CMB_CLEARANCE_TYPE, m_clearanceTypeCmbIndex);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_HEIGHT,  m_clearanceHeight);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_RADIUS , m_clearanceRadius );
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_X,  m_clearanceSph_X);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Y,  m_clearanceSph_Y);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Z,  m_clearanceSph_Z);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_X ,m_clearanceCyl_X);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Y ,m_clearanceCyl_Y);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Z ,m_clearanceCyl_Z);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_RAPID,  m_distanceRapid);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_FEED,  m_entryFeedDistance);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_FEED_EXIT,  m_exitFeedDistance);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_AIRMOVE,  m_distanceAirMove);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_FESTERWINKEL, m_tilt_angle_fixed);
	DDX_CBIndex(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS, m_tilt_to_axis);
	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TOOLAXIS_MEETS_TILTAXIS, m_toolaxis_meet_tilt);
	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_REVERSE_CUTS, m_reverse_cuts);
	DDX_CBIndex(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA, m_cutting_area);
	DDX_CBIndex(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_METHOD, m_cutMethod);
	DDX_CBIndex(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER, m_cut_order);
	DDX_CBIndex(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR, m_directionForOneWay);
	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENFORCE_DIRECTION, m_enforceClosedContour);
	DDX_Check(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STARTPOINTUSED, m_startPos_sw);	
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTOL, m_cutTol);
 	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MAX_QUER, m_maxStepOver);
   	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CHAININGTOL, m_chainingTol);
	DDX_Text(pDX, IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MOVE_SIZE_PERC,  m_moveSizeAsPercentageOfStepover);	

	DDX_Control(pDX,IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_FINBMP,m_rcImage);
	DDX_Control(pDX,IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STATIC,m_staticImage);
	//}}AFX_DATA_MAP

	if( pDX->m_bSaveAndValidate )
	{
		SetDlgParams(*pDX);
	}
 
}


BEGIN_MESSAGE_MAP(mwAppElectrodeMachining4Plus1AxisPage, mw5axuiPage)
	//{{AFX_MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS(mwAppElectrodeMachining4Plus1AxisPage)

		
	//GougePage
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ChkCollOp1_STATUS, OnChkCollOp1Status)
//	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENABLE_5AXIS_COLL_CHECK, OnEnable5axisCollCheck)

	//SurfPathsPage
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STOCKREMAIN_VALUE, OnSetfocusStockRemain)

	//TiltPage
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MAX_ANGLE_CHANGE, OnSetfocusMaxAngleChange)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_LIMITS, OnLimits)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_SEL_LIM, OnSelLim)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_FESTERWINKEL, OnSetfocusFixedTiltAngle)

	//RoughingPage
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STOCKDEF_AVOID_AIR_CUTS, OnAvoidAirCuts)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_STOCK_DEFINITION, OnBtnStockDefinition)

	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_DRIVE_SURFACE, OnBtnDriveSurface)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_COLLISION_CHECK, OnCheckCollision)
	ON_CBN_SELCHANGE(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CMB_CLEARANCE_TYPE,OnSelchangeComboClearanceType)	
	
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_X,OnSetfocusClearanceCylX )
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Y, OnSetfocusClearanceCylY)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Z, OnSetfocusClearanceCylZ)

	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_X ,OnSetfocusClearanceSphX )
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Y ,OnSetfocusClearanceSphY)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Z, OnSetfocusClearanceSphZ)

	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_HEIGHT, OnSetfocusClearancePlaneHeight)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_RADIUS, OnSetfocusClearanceRadius)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_PLANE_HEIGHT, OnClearancePlaneHeight)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUSTOM , OnCustomCoordinates)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_RAPID,OnSetfocusDistanceRapid )
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_FEED, OnSetfocusDistanceFeed)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_FEED_EXIT, OnSetfocusDistanceFeed)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_AIRMOVE,OnSetfocusDistanceAirMove )	
	ON_CBN_SELCHANGE(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS, OnSelchangeTiltToAxis)
	ON_CBN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS, OnSetfocusTiltToAxis)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_SEL_LINE, OnSelLine)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TOOLAXIS_MEETS_TILTAXIS, OnToolaxisMeetsTiltaxis)	
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_CHECK_SURFACES, OnBtnCheckSurfaces)	
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_REVERSE_CUTS, OnStartCutFromCurve)
	ON_CBN_SELCHANGE(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA, OnSelchangeCuttingArea)
	ON_CBN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA, OnSetfocusCuttingArea)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_LIM_CUTS, OnLimCuts)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MARGINS, OnMargins)
	ON_CBN_SELCHANGE(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_METHOD, OnSelchangeCutMethod)
	ON_CBN_SELCHANGE(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER, OnSelchangeCutOrder)
	ON_CBN_SELCHANGE(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR, OnSelchangeRotationDir)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENFORCE_DIRECTION, OnEnforceDir)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STARTPOINTUSED, OnStartpointused)
	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_START_POINT,OnBtnStartPoint)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTOL, OnSetfocusCuttol)
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MAX_QUER, OnSetfocusMaxQuer)	
	ON_EN_SETFOCUS (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CHAININGTOL, OnChainingTolSetFocus)
	ON_EN_KILLFOCUS (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CHAININGTOL, OnChainingTolLostFocus)
	
	ON_EN_SETFOCUS(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MOVE_SIZE_PERC, OnSetfocusMoveSizePerc )	

	ON_BN_CLICKED(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_ENTRY_EXIT_MACRO, OnBtnEntryExitLeadSettings)

	//}}AFX_MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten mwAppElectrodeMachining4Plus1AxisPage 

void mwAppElectrodeMachining4Plus1AxisPage::PaintBMPs() 
{
	mw5axuiPage::SetRCImage(m_nIDResBmp,
							IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_FINBMP,
							_T("mwAppElectrodeMachining4Plus1AxisPage"));
}


//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::GetParams() 
{
	
	const mwToolAxisControlParams& tacParams(m_Job.GetToolAxisControlParams());
	//GougePage
	m_collOp1Status=m_Job.GetCollControl().GetCollCtrlOperations()[mwCollCtrlParams::CCO_FIRST].GetStatus();
	
	//TiltPage
	m_maxAngleChange = m_Job.GetMaxAngleChange();
	m_tilt_limits_selected=tacParams.GetLimitsFlg();
	

	//RoughingPage
	m_avoid_air_cuts    = 	m_Job.GetCollCtrlOpStockParams().GetStatus();

	try
	{
		switch( m_Job.GetCuttingAreaType() )
		{
		case mw5axParams::LIMIT_CUTS:
			m_cutting_area_value=mwAppElectrodeMachining4Plus1AxisPage::FP_LIMIT_CUTS ;
			break;

		case mw5axParams::EXACT_SURFACE:
			m_cutting_area_value=mwAppElectrodeMachining4Plus1AxisPage::FP_EXACT_SURFACE ;
			break;

		case mw5axParams::AVOID_CUTS:
			m_cutting_area_value=mwAppElectrodeMachining4Plus1AxisPage::FP_AVOID_CUTS ;
			break;

		case mw5axParams::NUMBER_OF_CUTS:
			m_cutting_area_value=mwAppElectrodeMachining4Plus1AxisPage::FP_NUMBER_OF_CUTS ;
			break;

		default:
			throw mw5axuiException( mw5axuiException::CUTTING_AREA, _T("mwAppElectrodeMachining4Plus1AxisPage::GetParams"));
		};

		const mwLinkParams& linkParams(m_Job.GetLinkParams());

		m_stock_remain = m_Job.GetStockRemain();
		
		m_clearanceType=ConvertClearanceType(linkParams.GetClearanceType());
		m_clearance_axis=ConvertClearanceAxis(linkParams.GetClearanceAxis());
		m_clearanceHeight=linkParams.GetClearancePlaneHeight();
		m_clearanceRadius=linkParams.GetClearanceRadius();
		m_clearanceCyl_X=linkParams.GetCylinderThroughPoint().x();
		m_clearanceCyl_Y=linkParams.GetCylinderThroughPoint().y();
		m_clearanceCyl_Z=linkParams.GetCylinderThroughPoint().z();
		m_clearanceSph_X=linkParams.GetSphereAroundPoint().x();
		m_clearanceSph_Y=linkParams.GetSphereAroundPoint().y();
		m_clearanceSph_Z=linkParams.GetSphereAroundPoint().z();
		m_clearanceTypeCmbIndex = static_cast<int>( (3 * m_clearanceType)  + m_clearance_axis);

		m_distanceRapid=linkParams.GetRetractPlaneIncremental();
		m_entryFeedDistance=linkParams.GetApproachFeedPlaneIncremental();
		m_exitFeedDistance=linkParams.GetRetractFeedPlaneIncremental();
		m_distanceAirMove=linkParams.GetAirMoveSafetyDistance();
		

		m_tilt_angle_fixed = tacParams.GetTiltAngleFixed();

		switch( tacParams.GetTiltAxis() )
		{
		case mw5axParams::EXT_AXIS_X:
			m_tilt_to_axis=mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_X ;
			break;

		case mw5axParams::EXT_AXIS_Y:
			m_tilt_to_axis=mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_Y ;
			break;

		case mw5axParams::EXT_AXIS_Z:
			m_tilt_to_axis=mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_Z ;
			break;

		case mw5axParams::EXT_USER_AXIS:
			m_tilt_to_axis=mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_USER_AXIS ;
			break;
		default:
			throw mw5axuiException( mw5axuiException::EXTENDED_AXIS, _T("mwAppElectrodeMachining4Plus1AxisPage::GetParams"));
		};

		m_toolaxis_meet_tilt = tacParams.GetAxisMeetTiltFlg();

		m_reverse_cuts = m_Job.GetReverseCutsFlg();
		
		switch( m_Job.GetCurCutOrder() )
		{
			case mw5axParams::ORDER_STANDARD:
				m_cut_order=mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_STANDARD ;
				break;

			case mw5axParams::ORDER_FROM_CENTER:
				m_cut_order=mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_FROM_CENTER ;
				break;

			case mw5axParams::ORDER_FROM_OUTER:
				m_cut_order=mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_FROM_OUTER ;
				break;

			default:
				m_cut_order=mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_STANDARD ;
		}

		switch( m_Job.GetMachDirForOneWay() )
		{
		case mw5axParams::DIR_COUNTER_CLOCKWISE:
			m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_COUNTER_CLOCKWISE ;
			break;

		case mw5axParams::DIR_CLOCKWISE :
			m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CLOCKWISE ;
			break;

		case mw5axParams::DIR_CLIMB:
			m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CLIMB ;
			break;

		case mw5axParams::DIR_CONVENTIONAL:
			m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CONVENTIONAL;
			break;

		default:
			m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_COUNTER_CLOCKWISE ;
		};
		m_enforceClosedContour = m_Job.GetEnforceClosedContourFlg();
		m_startPos_sw = m_Job.GetStartPosFlag();
		
		switch( m_Job.GetCurMachType() )
		{
		case  mw5axParams::MACHTYPE_ZIGZAG :
			m_cutMethod=mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ZIGZAG;
			break;

		case mw5axParams::MACHTYPE_ONEWAY :
			m_cutMethod=mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ONEWAY;
			break;

		case mw5axParams::MACHTYPE_SPIRAL:
			m_cutMethod =mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_SPIRAL;
			break;

		case mw5axParams::MACHTYPE_UP:
			m_cutMethod =mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_UP;
			break;

		case mw5axParams::MACHTYPE_DOWN:
			m_cutMethod =mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_DOWN;
			break;
		
		default:
			throw mw5axuiException( mw5axuiException::MACHINING_TYPE, _T("mwAppElectrodeMachining4Plus1AxisPage::GetParams"));
		};

		if (m_CutTolVisible )
		{
			m_cutTol = m_Job.GetCutTolerance();
			
		}
		
		m_maxStepOver = m_Job.GetMaxStepoverDistance();
		
		m_chainingTol = m_Job.GetChainingTolerance();
	
		
		m_moveSizeAsPercentageOfStepover  = linkParams.GetLinkBetweenSlices().GetGapSize().GetPercent();

	}catch (mw5axParamsEx &e)
	{
		throw mw5axuiException( e, _T("mwAppElectrodeMachining4Plus1AxisPage::GetParams"));
	}
}

/*void mwAppElectrodeMachining4Plus1AxisPage::OnEnable5axisCollCheck()
{
	if ( UpdateData(true) )
	{		
		// disable rest of coll. checking
		mwCollCtrlParams control=m_Job.GetCollControl();
		for (unsigned int i=0;i<mwCollCtrlParams::GetNoOfCollCtrlOperations();i++)
		{
			mwCollCtrlOpParams collOpParams=control.GetCollCtrlOperations()[i];
			// if enabled, then set the default values !
			if ( (i==mwCollCtrlParams::CCO_SECOND) && m_enable5axisCollCheckSelected )
			{
				collOpParams.SetStatus ( true );
			}
			else
			{
				collOpParams.SetStatus ( false );
			}

			control.SetCollCtrlOperation(collOpParams, i );
		}

	if ( UpdateData(true) )
	{		


	}
}*/
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetTexts()
{	
	//SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENABLE_5AXIS_COLL_CHECK,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ENABLE_5AXIS_COLL_CHECK);
	

    //TiltPage
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MAX_ANGLE_TEXT,  MSG_MAX_ANGLE);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_SEL_LIM,  MSG_LIMITS );

	//RoughingPage
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STOCKDEF_AVOID_AIR_CUTS,  MSG_AVOID_AIR_CUTS);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_STOCK_DEFINITION,  MSG_BTN_STOCK_DEFINITION);

	m_checkCollisionBtn.m_pressedText   = GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CHK_COLLISION_ON).c_str();
	m_checkCollisionBtn.m_unPressedText = GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CHK_COLLISION_OFF).c_str();

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_DRIVE_SURFACE,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_BTN_DRIVE_SURFACE);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STOCKREMAIN,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_STOCKREMAIN ); 
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_RAPID_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_RETRPLANE);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_FEED_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_FEEDPLANE);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_FEED_EXIT_TEXT,MSG_EXIT_FEED_DISTANCE);

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_AIRMOVE_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_AIR_MOVE_SAFE_DIST);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_DIST_FRAME,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_DISTANCES_FRAME);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_FRAME,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_RAPID_PLANE_FRAME);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_TYPE_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_LBL_RAPID_PLANE_TYPE);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_DIM_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_SHEET_TITLE);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_X_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_AXIS_X);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_Y_TEXT ,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_AXIS_Y);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_Z_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_AXIS_Z);
	//!command buttons 
	//SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_PLANE_HEIGHT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_PLANE_HEIGHT);
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CMB_CLEARANCE_TYPE ))->ResetContent();

	SetClearancePlaneComboItems();
	
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_ANGLE_FIXED,  MSG_TILT_ANGLE_FIXED );
	
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS ))->ResetContent();
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS ))->AddString( GetText(MSG_APPELECTRODE_MACHINING_4PLUS1_AXIS_IDC_TILT_TO_X_AXIS ).c_str() );
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS ))->AddString( GetText(MSG_APPELECTRODE_MACHINING_4PLUS1_AXIS_IDC_TILT_TO_Y_AXIS ).c_str() );
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS ))->AddString( GetText(MSG_APPELECTRODE_MACHINING_4PLUS1_AXIS_IDC_TILT_TO_Z_AXIS ).c_str() );
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS ))->AddString( GetText(MSG_APPELECTRODE_MACHINING_4PLUS1_AXIS_IDC_TILT_TO_LINE ).c_str() );
	
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TOOLAXIS_MEETS_TILTAXIS,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_TOOLAXIS_MEETS_TILTAXIS);

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_CHECK_SURFACES, MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_BTN_CHECK_SURFACES);
	
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_CONTROL,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_CONTROL);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_GRP_AREA,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_AREA_FRAME);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_AREA_TEXT,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_AREA);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_LIM_CUTS,MSG_SURF_BASED_AREA_BTN_LIMIT_CUTS);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MARGINS,MSG_SURF_BASED_AREA_BTN_MARGINS);

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_REVERSE_CUTS,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_REVERSE_CUTS);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_METHOD,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_METHOD);

	
	CComboBox* cutMethodCombo = (CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_METHOD );
	cutMethodCombo->ResetContent();
	cutMethodCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_METHOD_ZIGZAG ).c_str() );
	cutMethodCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_METHOD_ONE_WAY ).c_str() );
	cutMethodCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ADVANCED_SPIRAL_CUTS ).c_str() );
	
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER_TEXT, MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER );
	
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER ))->ResetContent();
	
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER ))->
		AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER_STANDARD ).c_str() );
	
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER ))->
		AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER_FROM_CENTER ).c_str() );
	
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER ))->
		AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER_TO_CENTER ).c_str() );
	
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR_TEXT, MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR );
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR ))->ResetContent();
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR ))->
		AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR_CCWISE ).c_str() );
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR ))->
		AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR_CWISE ).c_str() );
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR ))->
		AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR_CLIMB ).c_str() );
	((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR ))->
		AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR_CONVENTIONAL ).c_str() );

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENFORCE_DIRECTION,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ENFORCE_DIRECTION);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_START_POINT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_STARTPOINT);

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_SURF_QUALITY_TEXT,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_SURF_QUALITY);
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_TOLERANCE,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUT_TOLERANCE);
	
	SetCutControlVisibility();
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STEP_OVER_TEXT, MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_STEP_OVER);

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CHAINING_TOLERANCE,  MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CHAINING_TOLERANCE);
	
	
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENTRY_EXIT_ARC_BETWEEN_SLICES_TEXT,MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ENTRY_EXIT_ARC_BETWEEN_SLICES);	
	
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_ENTRY_EXIT_MACRO,
				MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_BTN_ENTRY_EXIT_MACRO);
	//
	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CHAIN_TOL_NOTE,
		         MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CHAIN_TOL_NOTE);
	
}

//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetClearancePlaneComboItems()
{
	
	CComboBox * clearanceTypeCombo = (CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CMB_CLEARANCE_TYPE );
	clearanceTypeCombo->ResetContent();
	
	clearanceTypeCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CMB_RAPID_PLANE_PL_X_CONST).c_str() );
	clearanceTypeCombo->SetItemData( (clearanceTypeCombo->GetCount()-1), LP_CLEARANCE_PLANE);
	
	clearanceTypeCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CMB_RAPID_PLANE_PL_Y_CONST).c_str() );
	clearanceTypeCombo->SetItemData( (clearanceTypeCombo->GetCount()-1), LP_CLEARANCE_PLANE);

	clearanceTypeCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CMB_RAPID_PLANE_PL_Z_CONST).c_str() );
	clearanceTypeCombo->SetItemData( (clearanceTypeCombo->GetCount()-1), LP_CLEARANCE_PLANE);
	
	clearanceTypeCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CMB_RAPID_PLANE_CYL_AROUND_X).c_str() );
	clearanceTypeCombo->SetItemData( (clearanceTypeCombo->GetCount()-1), LP_CLEARANCE_CYLINDER);

	clearanceTypeCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CMB_RAPID_PLANE_CYL_AROUND_Y).c_str() );
	clearanceTypeCombo->SetItemData( (clearanceTypeCombo->GetCount()-1), LP_CLEARANCE_CYLINDER);

	clearanceTypeCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CMB_RAPID_PLANE_CYL_AROUND_Z).c_str() );
	clearanceTypeCombo->SetItemData( (clearanceTypeCombo->GetCount()-1), LP_CLEARANCE_CYLINDER);
	
	clearanceTypeCombo->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CMB_RAPID_PLANE_SPHERE).c_str() );
	clearanceTypeCombo->SetItemData( (clearanceTypeCombo->GetCount()-1), LP_CLEARANCE_SPHERE);
	
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelchangeComboClearanceType()
{
	
	CComboBox * clearanceTypeCombo  = (CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CMB_CLEARANCE_TYPE);
	
	m_clearanceTypeCmbIndex  = clearanceTypeCombo->GetCurSel();
	m_clearanceType = clearanceTypeCombo->GetItemData(m_clearanceTypeCmbIndex);
	m_clearance_axis = (m_clearanceTypeCmbIndex % 3);
	ShowControlsUpdated();


	if (m_clearanceType==mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_PLANE)
	{
		switch(m_clearance_axis) 
		{
			case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_X : 
				m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_TYPE_PLANE_X;
				break;
			case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Y : 
				m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_TYPE_PLANE_Y;
				break;
			case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Z : 
				m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_TYPE_PLANE_Z;
				break;
			default:
				throw mw5axuiException( mw5axuiException::RAPID_PLANE_AXIS, 
					_T("Setting picture for Clearance Type  = Plane"));
				break;
		};
	}
	else
	if(m_clearanceType==mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER)
		{
		switch(m_clearance_axis) 
		{
			case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_X : 
				m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_TYPE_CYL_PAR_X;
				break;
			case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Y : 
				m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_TYPE_CYL_PAR_Y;
				break;
			case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Z : 
				m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_TYPE_CYL_PAR_Z;
				break;
			default:
				throw mw5axuiException( mw5axuiException::RAPID_PLANE_AXIS, 
					_T("Setting picture for Clearance Type  = Cylinder"));
				break;
		};
	}
	else
	if(m_clearanceType==mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_SPHERE)
	{
		
		m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_TYPE_SPHERE;
		
	}
	else
	{
		
		throw mw5axuiException( mw5axuiException::CLEARANCE_TYPE, _T("Setting picture for Clearance Type"));
	}

	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::UpdateWindowControls( )
{
	//GougeCheckPage 
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_CHECK_SURFACES)->EnableWindow(m_collOp1Status);
	//GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENABLE_5AXIS_COLL_CHECK)->ShowWindow(m_enable5axisCollCheckVisible);
	
	//TiltPage
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_SEL_LIM)->EnableWindow(m_tilt_limits_selected);
	if(m_tilt_limits_selected)
	{
		m_toolAngleLimitsDlg = new 	mwToolAngleLimitsDlg(m_Job,m_CurrentTool.GetRealTool(),
			m_interActor,this,MSG_LIMITS,GetClientObserver());
	}
	else
	{
		m_toolAngleLimitsDlg = NULL;
	}

	//RoughingPage
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_STOCK_DEFINITION)->EnableWindow( m_avoid_air_cuts ? TRUE : FALSE);

	//!Show/hide enable/disable items depending on Clearance type 
	bool selectedSpOrCyl = m_clearanceType == LP_CLEARANCE_CYLINDER ||
						   m_clearanceType == LP_CLEARANCE_SPHERE;

	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_X_TEXT)->ShowWindow(selectedSpOrCyl);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_Y_TEXT)->ShowWindow(selectedSpOrCyl);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_Z_TEXT)->ShowWindow(selectedSpOrCyl);

	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_DIM_TEXT)->ShowWindow(selectedSpOrCyl);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUSTOM)->ShowWindow(selectedSpOrCyl);

	
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_HEIGHT)->ShowWindow(m_clearanceType == LP_CLEARANCE_PLANE);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_RADIUS)->ShowWindow(selectedSpOrCyl);
	
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_X)->ShowWindow(m_clearanceType == LP_CLEARANCE_SPHERE);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Y)->ShowWindow(m_clearanceType == LP_CLEARANCE_SPHERE);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Z)->ShowWindow(m_clearanceType == LP_CLEARANCE_SPHERE);

	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_X)->ShowWindow(m_clearanceType == LP_CLEARANCE_CYLINDER);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Y)->ShowWindow(m_clearanceType == LP_CLEARANCE_CYLINDER);
	GetDlgItem (IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Z)->ShowWindow(m_clearanceType == LP_CLEARANCE_CYLINDER);

	SetCntrlText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_DIM_TEXT, (m_clearanceType==LP_CLEARANCE_CYLINDER)?
													MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_LBL_RAPID_PLANE_THROUGH:
												    MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_LBL_RAPID_PLANE_AROUND);

	(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_PLANE_HEIGHT)) ->ShowWindow(!selectedSpOrCyl);
	
	
	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_ANGLE_FIXED)->ShowWindow( SW_SHOW );	


	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_FESTERWINKEL)->
		ShowWindow( (m_tilt_strategy == mwAppElectrodeMachining4Plus1AxisPage::TP_FIXED_ANGLE || 
					( m_tilt_strategy == mwAppElectrodeMachining4Plus1AxisPage::TP_THRU_CURVE && m_CurveTiltType != mwAppElectrodeMachining4Plus1AxisPage::TP_FROM_START_TO_END)) ? SW_SHOW : SW_HIDE );	

	
	const bool isSelLineBtnAvailable = m_tilt_to_axis == mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_USER_AXIS;
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_SEL_LINE)->
		ShowWindow(isSelLineBtnAvailable );	
	if(isSelLineBtnAvailable)
	{

		m_selectLineFctAccessor = new mwLineFctsAccessorOnTheFlyUpdater(m_Job, mwLineFctsAccessorOnTheFlyUpdater::PRMS_TL_AX_CTRL_PARAMS_TILT_AXIS);
		m_selectLineDlg = new mwCustomAxisSelectionDlg (m_Job,
												m_CurrentTool.GetRealTool(),
												m_interActor, 
												m_selectLineFctAccessor,
												MSG_TILT_SEL_LINE,
												MSG_TILT_LINE_PAGE_TITLE,
												this,
												GetClientObserver());
	}
	else
	{
		m_selectLineFctAccessor = NULL;
		m_selectLineDlg = NULL;
	}

	
	/*bool showAxis = (m_tilt_strategy == mwAppElectrodeMachining4Plus1AxisPage::TP_RELATIVE_ANGLE || 
					 m_tilt_strategy == mwAppElectrodeMachining4Plus1AxisPage::TP_FIXED_ANGLE ||
					 m_tilt_strategy == mwAppElectrodeMachining4Plus1AxisPage::TP_AROUND_AXIS );*/

	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS)->EnableWindow();	

	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TOOLAXIS_MEETS_TILTAXIS)->
		EnableWindow(  );	

	bool bProjectionCurvSlct= (m_cutType == mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PROJECT_CURVES);
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_AREA_TEXT)->
	ShowWindow( bProjectionCurvSlct ? SW_HIDE : SW_SHOW);	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA)->
	ShowWindow( bProjectionCurvSlct ? SW_HIDE : SW_SHOW);
	
	const bool isLimitsCutsBtnAvailable = !bProjectionCurvSlct &&
		m_cutting_area_value== mwAppElectrodeMachining4Plus1AxisPage::FP_LIMIT_CUTS;
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_LIM_CUTS)->
	ShowWindow(isLimitsCutsBtnAvailable);	
	if(isLimitsCutsBtnAvailable)
	{
		m_limitCutsDlg = new mwLimitCutsBetween2Points (m_Job,m_CurrentTool.GetRealTool(),
			m_interActor,this,MSG_LIMIT_CUTS,GetClientObserver());
	}
	else
	{
		m_limitCutsDlg = NULL;
	}

	const bool isMarginsBtnAvailable = ((m_cutType==FP_CUT_BETWEEN_CURVES || m_cutType==FP_CUT_BETWEEN_SURFACES) 
										&& (m_cutting_area_value== FP_EXACT_SURFACE || m_cutting_area_value ==FP_NUMBER_OF_CUTS ))
										|| m_cutType == FP_CUT_PARALLEL_SURFACE 
										||(m_cutType==FP_CUT_PARALLEL_CURVE  
										   && (m_cutting_area_value== FP_EXACT_SURFACE || m_cutting_area_value== FP_NUMBER_OF_CUTS));
										

	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MARGINS)->ShowWindow(isMarginsBtnAvailable); 
	if(isMarginsBtnAvailable)
	{
		const bool displayBothMargins=(m_cutType != mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PARALLEL_SURFACE 
										&& m_cutType!=mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PARALLEL_CURVE);

		m_marginsDlg = new  mwMarginsDlg (m_Job,m_CurrentTool.GetRealTool(),
			m_interActor,displayBothMargins,this,true,MSG_START_END_MARGINS_PAGE_TITLE,GetClientObserver());
	}
	else
	{
		m_marginsDlg  = NULL;
	}
		

//*********
/*	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MARGINS)->
	ShowWindow( ((m_cutType==mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_BETWEEN_CURVES || 
					m_cutType==mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_BETWEEN_SURFACES) && 
					(m_cutting_area_value== mwAppElectrodeMachining4Plus1AxisPage::FP_EXACT_SURFACE || 
					m_cutting_area_value ==mwAppElectrodeMachining4Plus1AxisPage::FP_NUMBER_OF_CUTS ))||
					m_cutType == mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PARALLEL_SURFACE || 
					(m_cutType==mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PARALLEL_CURVE  && 
				  (m_cutting_area_value== mwAppElectrodeMachining4Plus1AxisPage::FP_EXACT_SURFACE || 
				  m_cutting_area_value== mwAppElectrodeMachining4Plus1AxisPage::FP_NUMBER_OF_CUTS))
				  ? SW_SHOW : SW_HIDE  );	
*/	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_REVERSE_CUTS)->
	ShowWindow( bProjectionCurvSlct ? SW_HIDE : SW_SHOW);	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_METHOD)->
	ShowWindow( bProjectionCurvSlct ? SW_HIDE : SW_SHOW);	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_METHOD)->
	ShowWindow( bProjectionCurvSlct ? SW_HIDE : SW_SHOW);	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER_TEXT)->
	ShowWindow( !bProjectionCurvSlct ? SW_SHOW : SW_HIDE );	

	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER)->
	ShowWindow( !bProjectionCurvSlct ? SW_SHOW : SW_HIDE );	

	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER)->
		EnableWindow(m_cutMethod != mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_SPIRAL);	



	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR_TEXT)->
	ShowWindow( !bProjectionCurvSlct &&
				(m_cutMethod == mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ONEWAY ||
				 m_cutMethod == mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_SPIRAL) ? SW_SHOW : SW_HIDE );	


	// Enable controls depending on Einweg milling
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR)->	  
	ShowWindow( (!bProjectionCurvSlct && (m_cutMethod == mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ONEWAY ||
										  m_cutMethod == mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_SPIRAL) ) ? SW_SHOW : SW_HIDE );	
	
	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ENFORCE_DIRECTION)->
	ShowWindow( !bProjectionCurvSlct &&
				m_cutMethod == mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ONEWAY &&
				( 	m_directionForOneWay == mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_COUNTER_CLOCKWISE  || 
				    m_directionForOneWay == mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CLOCKWISE )
				? SW_SHOW : SW_HIDE );	
	
	ShowStartPointItems ( !bProjectionCurvSlct );
	// Enable button depending on check box starting point
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_START_POINT)->
	EnableWindow( m_startPos_sw );	
	if(m_startPos_sw)
	{
		m_startPointDlg = new mwStartPointDlg (m_Job,
												m_CurrentTool.GetRealTool(),
												m_interActor,
												this,
												mwTpCalcMethodsDefs::TPCD_CALC_SURFACE_BASED,
												mwTpCalcMethodsDefs::TPCD_CUT_PARALLEL,
												false,
												MSG_START_POINT_PAGE_TITLE,
												GetClientObserver());
	}
	else
	{
		m_startPointDlg = NULL;
	}
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STEP_OVER_TEXT)->
	ShowWindow( ( bProjectionCurvSlct || 
				(m_cutType == FP_CUT_BETWEEN_CURVES && m_cutting_area_value == FP_NUMBER_OF_CUTS) )? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MAX_QUER)->
	ShowWindow( ( bProjectionCurvSlct || 
				(m_cutType == FP_CUT_BETWEEN_CURVES && m_cutting_area_value == FP_NUMBER_OF_CUTS) )? SW_HIDE : SW_SHOW);
	
}

//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnCustomCoordinates()
{
	double x =  m_clearanceType == mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER                  ?
				GetDlgControlTextAsNumber(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_X) )  : 
				GetDlgControlTextAsNumber(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_X) )  ;

	double y =  m_clearanceType == mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER                  ?
				GetDlgControlTextAsNumber(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Y) )  : 
				GetDlgControlTextAsNumber(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Y) )  ;


	double z =  m_clearanceType == mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER                  ?
				GetDlgControlTextAsNumber(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Z) )  : 
				GetDlgControlTextAsNumber(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Z) )  ;
							
		
	mwNotifyParamInteractor::measurablePoint3d tmpPoint(x,y,z,m_interActor.GetUnits() );
	
	if (m_interActor.EditPoint( m_interActor.GetMsg (MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ON_LIM_PNT_SELECT,TXT_SECTION), tmpPoint) )
	{		
		tmpPoint.SetUnits(m_interActor.GetUnits());
		
		if (m_clearanceType == mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER)
		{
			SetDlgItemText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_X, misc::from_value(tmpPoint.x(),5).c_str());
			SetDlgItemText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Y, misc::from_value(tmpPoint.y(),5).c_str());
			SetDlgItemText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_CYL_Z, misc::from_value(tmpPoint.z(),5).c_str());
		}
		else
		{
			SetDlgItemText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_X, misc::from_value(tmpPoint.x(),5).c_str());
			SetDlgItemText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Y, misc::from_value(tmpPoint.y(),5).c_str());
			SetDlgItemText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_SPH_Z, misc::from_value(tmpPoint.z(),5).c_str());
		}
		
	}
	
}
//#############################################################################
mwLinkParams::ClearanceAxis mwAppElectrodeMachining4Plus1AxisPage::ConvertClearanceAxis(const int vAxis)
{
	switch( vAxis )
	{
	case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_X:
		return mwLinkParams::AXIS_X;
		break;

	case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Y:
		return mwLinkParams::AXIS_Y;
		break;

	case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Z:
		return mwLinkParams::AXIS_Z;
		break;
	default:
		throw mw5axuiException( mw5axuiException::RAPID_PLANE_AXIS, _T("mwAppElectrodeMachining4Plus1AxisPage::SetParams"));
	}
}
//#############################################################################
mwLinkParams::ClearanceType mwAppElectrodeMachining4Plus1AxisPage::ConvertClearanceType(const DWORD_PTR vType)
{
	switch( vType  )
	{
	case mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_PLANE:
		return mwLinkParams::CLEARANCE_PLANE;
		break;

	case mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER:
		return mwLinkParams::CLEARANCE_CYLINDER;
		break;

	case mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_SPHERE:
		return mwLinkParams::CLEARANCE_SPHERE;
		break;
	default:
		throw mw5axuiException( mw5axuiException::CLEARANCE_TYPE, _T("mwAppElectrodeMachining4Plus1AxisPage::GetParams"));
	};
}
//#############################################################################
int mwAppElectrodeMachining4Plus1AxisPage::ConvertClearanceAxis(const mwLinkParams::ClearanceAxis vAxis)
{
	switch( vAxis )
	{
	case mwLinkParams::AXIS_X:
		return mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_X ;
		break;

	case mwLinkParams::AXIS_Y:
		return mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Y ;
		break;

	case mwLinkParams::AXIS_Z:
		return mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Z ;
		break;
	default:
		throw mw5axuiException( mw5axuiException::RAPID_PLANE_AXIS, _T("mwAppElectrodeMachining4Plus1AxisPage::GetParams"));
	};
}
//#############################################################################
int mwAppElectrodeMachining4Plus1AxisPage::ConvertClearanceType(const mwLinkParams::ClearanceType vType)
{
	switch( vType  )
	{
	case mwLinkParams::CLEARANCE_PLANE:
		return mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_PLANE ;
		break;

	case mwLinkParams::CLEARANCE_CYLINDER:
		return mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER ;
		break;

	case mwLinkParams::CLEARANCE_SPHERE:
		return  mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_SPHERE ;
		break;
	default:
		throw mw5axuiException( mw5axuiException::CLEARANCE_TYPE, _T("mwAppElectrodeMachining4Plus1AxisPage::GetParams"));
	};
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearanceCylX()
{
	
	m_nIDResBmp = IDB_LINK_DLG_CYL_THROUGH_X;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearanceCylY()
{
	
    m_nIDResBmp = IDB_LINK_DLG_CYL_THROUGH_Y;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearanceCylZ()
{
	
	
	m_nIDResBmp = IDB_LINK_DLG_CYL_THROUGH_Z;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearancePlaneHeight()
{
	m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_PLANE_HEIGHT;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearanceRadius()
{
	
	
	if (m_clearanceType == mwAppElectrodeMachining4Plus1AxisPage::LP_CLEARANCE_CYLINDER)
	{
		m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_CYL_RADIUS;
	}
	else
	{
		m_nIDResBmp = IDB_LINK_DLG_CLEARANCE_SPH_RADIUS;
	}

	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnClearancePlaneHeight()
{
	mwNotifyParamInteractor::measurableAxis mAxis(mwNotifyParamInteractor::measurableAxis::AXIS_X,m_interActor.GetUnits());
	
	switch( m_clearance_axis )
	{
	case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_X:
		mAxis.SetAxis(mwNotifyParamInteractor::measurableAxis::AXIS_X);
		break;

	case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Y:
		mAxis.SetAxis(mwNotifyParamInteractor::measurableAxis::AXIS_Y);
		break;
	
	case mwAppElectrodeMachining4Plus1AxisPage::LP_AXIS_Z:
		mAxis.SetAxis(mwNotifyParamInteractor::measurableAxis::AXIS_Z);
		break;
	default:
		 throw mw5axuiException( mw5axuiException::RAPID_PLANE_AXIS, _T("mwAppElectrodeMachining4Plus1AxisPage::OnClearancePlaneHeight"));
	}

	//set axis value
	mAxis.SetValue(GetDlgControlTextAsNumber(GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_HEIGHT)) );

	if (m_interActor.SelectCoordinate( m_interActor.GetMsg (MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_ON_SELECT_A_COORDINATE,TXT_SECTION), mAxis) )
	{		
		mAxis.SetUnits(m_interActor.GetUnits());
		SetDlgItemText(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_HEIGHT, misc::from_value(mAxis.GetValue(),5).c_str());
	}	
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetParams() 
{

	mwToolAxisControlParams& tacParams(m_Job.GetToolAxisControlParams());


	//GougePage	
		mwCollCtrlParams& control=m_Job.GetCollControl();
		mwCollCtrlOpParams& collOpParams1=control.GetCollCtrlOperations()[mwCollCtrlParams::CCO_FIRST];
		collOpParams1.SetStatus ( (m_collOp1Status==TRUE) );
		//control.SetCollCtrlOperation(collOpParams1,mwCollCtrlParams::CCO_FIRST);

		mwCollCtrlOpParams& collOpParams2 = control.GetCollCtrlOperations()[mwCollCtrlParams::CCO_SECOND];
		collOpParams2.SetStatus( m_checkCollisionBtn.IsChecked() );
		//control.SetCollCtrlOperation(collOpParams2, mwCollCtrlParams::CCO_SECOND );

		//m_Job.SetCollControl(control);

	
/*	mwCollCtrlParams control=m_Job.GetCollControl();
	mwCollCtrlOpParams collOpParams = control.GetCollCtrlOperations()[mwCollCtrlParams::CCO_SECOND];
	collOpParams.SetStatus( m_enable5axisCollCheckSelected==TRUE && m_enable5axisCollCheckVisible );
	control.SetCollCtrlOperation(collOpParams, mwCollCtrlParams::CCO_SECOND );

	collOpParams = control.GetCollCtrlOperations()[mwCollCtrlParams::CCO_FIRST];
	collOpParams.SetStatus( m_enable5axisCollCheckVisible );
	control.SetCollCtrlOperation(collOpParams, mwCollCtrlParams::CCO_FIRST );

	m_Job.SetCollControl(control);
*/
	//TiltPage
	m_Job.SetMaxAngleChange(m_maxAngleChange);

	//RoughingPage
	mwCollCtrlOpStockParams& collStockParams (m_Job.GetCollCtrlOpStockParams());
	collStockParams.SetStatus((m_avoid_air_cuts ==TRUE));
	//m_Job.SetCollCtrlOpStockParams(collStockParams);
	//
	try
	{
		
		
		
		tacParams.SetLimitsFlg(m_tilt_limits_selected == TRUE);

		switch( m_cutting_area_value)
		{
		case mwAppElectrodeMachining4Plus1AxisPage::FP_AVOID_CUTS:
			m_Job.SetCuttingAreaType(mw5axParams::AVOID_CUTS);
			break;
						
		case mwAppElectrodeMachining4Plus1AxisPage::FP_EXACT_SURFACE:
			m_Job.SetCuttingAreaType(mw5axParams::EXACT_SURFACE);

			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_NUMBER_OF_CUTS:
			m_Job.SetCuttingAreaType(mw5axParams::NUMBER_OF_CUTS);

			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_LIMIT_CUTS:
			m_Job.SetCuttingAreaType(mw5axParams::LIMIT_CUTS);

			break;
		default:	
			throw mw5axuiException( mw5axuiException::CUTTING_AREA, _T("mwAppElectrodeMachining4Plus1AxisPage::SetParams"));
		};

		m_Job.SetStockRemain (m_stock_remain );

		mwLinkParams& linkParams(m_Job.GetLinkParams());
		
		// clearance
		linkParams.SetClearanceType(ConvertClearanceType(m_clearanceType));
		linkParams.SetClearanceAxis(ConvertClearanceAxis(m_clearance_axis));
		linkParams.SetClearancePlaneHeight(m_clearanceHeight);
		linkParams.SetClearanceRadius(m_clearanceRadius);
		linkParams.SetCylinderThroughPoint(point3d(m_clearanceCyl_X,m_clearanceCyl_Y,m_clearanceCyl_Z));
		linkParams.SetSphereAroundPoint(point3d(m_clearanceSph_X,m_clearanceSph_Y,m_clearanceSph_Z));

		linkParams.SetRetractPlaneIncremental(m_distanceRapid);
		linkParams.SetApproachFeedPlaneIncremental(m_entryFeedDistance);
		linkParams.SetRetractFeedPlaneIncremental(m_exitFeedDistance);
		linkParams.SetAirMoveSafetyDistance(m_distanceAirMove);


		tacParams.SetTiltAngleFixed( m_tilt_angle_fixed );
		
		switch( m_tilt_to_axis )
		{
		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_X:
			tacParams.SetTiltAxis( mw5axParams::EXT_AXIS_X );
			break;

		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_Y:
			tacParams.SetTiltAxis( mw5axParams::EXT_AXIS_Y );
			break;

		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_Z:
			tacParams.SetTiltAxis( mw5axParams::EXT_AXIS_Z );
			break;

		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_USER_AXIS:
			tacParams.SetTiltAxis( mw5axParams::EXT_USER_AXIS );
			break;
		default:
			throw mw5axuiException( mw5axuiException::EXTENDED_AXIS, _T("mwAppElectrodeMachining4Plus1AxisPage::SetParams"));
		};
		
		tacParams.SetAxisMeetTiltFlg( (m_toolaxis_meet_tilt==TRUE));
		m_Job.SetReverseCutsFlg( (m_reverse_cuts==TRUE));
		
		switch( m_cut_order )
		{
			case mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_STANDARD:
				m_Job.SetCurCutOrder( mw5axParams::ORDER_STANDARD );
				break;

			case mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_FROM_CENTER:
				m_Job.SetCurCutOrder( mw5axParams::ORDER_FROM_CENTER );
				break;

			case mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_FROM_OUTER:
				m_Job.SetCurCutOrder( mw5axParams::ORDER_FROM_OUTER );
				break;

			default:
				throw mw5axuiException( mw5axuiException::CUT_ORDER, _T("mwAppElectrodeMachining4Plus1AxisPage::SetParams"));
		}
		switch( m_directionForOneWay )
		{
		case mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_COUNTER_CLOCKWISE:
			m_Job.SetMachDirForOneWay( mw5axParams::DIR_COUNTER_CLOCKWISE );
			break;

		case mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CLOCKWISE:
			m_Job.SetMachDirForOneWay( mw5axParams::DIR_CLOCKWISE );
			break;

		case mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CLIMB:
			m_Job.SetMachDirForOneWay( mw5axParams::DIR_CLIMB );
			break;

		case mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CONVENTIONAL:
			m_Job.SetMachDirForOneWay( mw5axParams::DIR_CONVENTIONAL );
			break;

		default:
			throw mw5axuiException( mw5axuiException::DIRECTION_FOR_ONE_WAY_CUT, _T("mwAppElectrodeMachining4Plus1AxisPage::SetParams"));
		};
		m_Job.SetEnforceClosedContourFlg( (m_enforceClosedContour==TRUE));
		m_Job.SetStartPosFlag( (m_startPos_sw==TRUE));
		
		switch( m_cutMethod )
		{
			case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ZIGZAG:
				m_Job.SetCurMachType( mw5axParams::MACHTYPE_ZIGZAG );
				break;
							
			case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ONEWAY:
				m_Job.SetCurMachType( mw5axParams::MACHTYPE_ONEWAY );
				break;

			case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_SPIRAL:
				m_Job.SetCurMachType( mw5axParams::MACHTYPE_SPIRAL );
				break;

			case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_UP:
				m_Job.SetCurMachType( mw5axParams::MACHTYPE_UP );
				break;

			case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_DOWN:
				m_Job.SetCurMachType( mw5axParams::MACHTYPE_DOWN );
				break;

			default:
				throw mw5axuiException( mw5axuiException::MACHINING_TYPE, _T("mwAppElectrodeMachining4Plus1AxisPage::SetParams"));
		};

		if (m_CutTolVisible )
		{
			m_Job.SetCutTolerance( m_cutTol );
		}

		m_Job.SetMaxStepoverDistance( m_maxStepOver);
		
		m_Job.SetChainingTolerance( m_chainingTol );

		//!Links between slices
		mwLinkBetweenSlices& linkBetweenSlices(linkParams.GetLinkBetweenSlices());
		
		mwPercentOrValueParameter& tmpMoveSize(linkBetweenSlices.GetGapSize());
		tmpMoveSize.SetPercent (m_moveSizeAsPercentageOfStepover);
		//linkBetweenSlices.SetGapSize(tmpMoveSize);
		//linkParams.SetLinkBetweenSlices(linkBetweenSlices);

		//m_Job.SetLinkParams(linkParams);

		m_Job.SetToolAxisControlParams(tacParams);

	}catch (mw5axParamsEx &e)
	{
		throw mw5axuiException( e, _T("mwAppElectrodeMachining4Plus1AxisPage::SetParams"));
	}
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusCuttingArea() 
{
	SetCuttingAreaPicture();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnBtnDriveSurface() 
{
	if (this->UpdateData(true))
		m_interActor.EditDriveSurfaces ();	
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusDistanceRapid()
{
	m_nIDResBmp = IDB_LINK_DLG_DISTANCE_RAPID;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusDistanceAirMove()
{
	m_nIDResBmp = IDB_LINK_DLG_DISTANCE_AIR_MOVE;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusDistanceFeed()
{
	m_nIDResBmp = IDB_LINK_DLG_DISTANCE_FEED;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnCheckCollision() 
{
/*	if ( UpdateData(true) )
	{		
		// disable rest of coll. checking
		mwCollCtrlParams control=m_Job.GetCollControl();
		for (unsigned int i=0;i<mwCollCtrlParams::GetNoOfCollCtrlOperations();i++)
		{
			mwCollCtrlOpParams collOpParams=control.GetCollCtrlOperations()[i];
			// if enabled, then set the default values !
			if ( (i==mwCollCtrlParams::CCO_FIRST) && m_checkCollisionBtn.IsChecked() )
			{
				collOpParams.SetStatus ( true );
			}
			else
			{
				collOpParams.SetStatus ( false );
			}

			control.SetCollCtrlOperation(collOpParams, i );
		}

		mwCollCtrlOpParams collOpParams=control.GetCollCtrlOperations()[mwCollCtrlParams::CCO_FIRST];
		collOpParams.SetStatus ( m_checkCollisionBtn.IsChecked() );
		control.SetCollCtrlOperation(collOpParams, mwCollCtrlParams::CCO_FIRST );
		m_Job.SetCollControl(control);
	}
*/
	//m_enable5axisCollCheckVisible=!m_enable5axisCollCheckVisible;
//	ShowControlsUpdated();

}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::DisplayTiltToAxisPicture()
{
	switch (m_tilt_to_axis)
	{
		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_X://axis X
			m_nIDResBmp = IDB_TILT_PAGE_TILT_FIXED_ANGLE_X_AXIS;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_Y : //axis Y
			m_nIDResBmp = IDB_TILT_PAGE_TILT_FIXED_ANGLE_Y_AXIS;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_AXIS_Z://axis Z
			m_nIDResBmp = IDB_TILT_PAGE_TILT_FIXED_ANGLE_Z_AXIS;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::TP_EXT_USER_AXIS ://line 
			m_nIDResBmp = IDB_TILT_PAGE_TILT_FIXED_ANGLE_LINE;
			break;
	};

	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelchangeTiltToAxis() 
{
	m_tilt_to_axis = ((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_TILT_TO_AXIS))->GetCurSel() ;	
	
	DisplayTiltToAxisPicture();

	ShowControlsUpdated();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelLine() 
{
	m_selectLineDlg->DisplayDialog();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnToolaxisMeetsTiltaxis() 
{
	switch(m_tilt_strategy) 
	{
		case mwAppElectrodeMachining4Plus1AxisPage::TP_RELATIVE_ANGLE : //tilted with the angle
			m_nIDResBmp = IDB_TILT_PAGE_TILT_WITH_ANGLE_TOOL_AX_CROSS_TILT_AX;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::TP_FIXED_ANGLE://tilted with fixed angle 
			m_nIDResBmp = IDB_TILT_PAGE_TILT_FIXED_ANGLE_TOOL_AX_CROSS_TILT_AX;
			break;
	};
	
	PaintBMPs();
	
	m_toolaxis_meet_tilt = !m_toolaxis_meet_tilt;
	ShowControlsUpdated();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnBtnCheckSurfaces() 
{
	// TODO: Add your control notification handler code here
	if (this->UpdateData(true))
		m_interActor.EditCheckSurfaces (mwNotifyParamInteractor::CS_FIRST);
}

//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::ReloadCuttingAreaBox( )
{
	CComboBox * cuttingArea=(CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA );
	cuttingArea->ResetContent();
	cuttingArea->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA_AVOID_CUTS ).c_str() );
	cuttingArea->SetItemData( (cuttingArea->GetCount()-1),FP_AVOID_CUTS );
	cuttingArea->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA_EXACT_SURFACE_EDGES ).c_str() );
	cuttingArea->SetItemData( (cuttingArea->GetCount()-1),FP_EXACT_SURFACE );
	if(m_cutType==mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PARALLEL_CURVE ||
	   m_cutType==mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_PARALLEL_SURFACE ||
	   m_cutType==mwAppElectrodeMachining4Plus1AxisPage::FP_CUT_BETWEEN_CURVES)
	{
		cuttingArea->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA_NUMBER_OF_CUTS ).c_str() );
		cuttingArea->SetItemData( (cuttingArea->GetCount()-1),FP_NUMBER_OF_CUTS );
	} 
	cuttingArea->AddString( GetText(MSG_APP_ELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA_POINTS ).c_str() );
	cuttingArea->SetItemData( (cuttingArea->GetCount()-1),FP_LIMIT_CUTS );
	int i=0;
	for (;i<cuttingArea->GetCount();i++)
		if (cuttingArea->GetItemData(i)==m_cutting_area_value)
		{
			cuttingArea->SetCurSel(i);
			m_cutting_area=i;
			break;
		}
	if (i==cuttingArea->GetCount())
	{
		cuttingArea->SetCurSel(0);
		m_cutting_area_value=cuttingArea->GetItemData(0);
		m_cutting_area_value=0;
		m_cutting_area = 0;
	}
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelchangeCuttingArea() 
{
	
	m_cutting_area= ((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA ))->GetCurSel();
	m_cutting_area_value=((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTING_AREA ))->GetItemData(m_cutting_area);
	
	SetCuttingAreaPicture();

	ShowControlsUpdated();
	
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetCuttingAreaPicture()
{
	switch( m_cutting_area_value )
	{
		case mwAppElectrodeMachining4Plus1AxisPage::FP_AVOID_CUTS:
			m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__AVOID_CUTS;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_EXACT_SURFACE:
			m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__CUTTING_AREA_EXACT_SURFEDGES;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_NUMBER_OF_CUTS:
			m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__CUTTING_AREA_NUMBER_OF_CUTS;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_LIMIT_CUTS:
			m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__CUTTING_AREA_LIM_BETW_PNTS;
			break;
		default:
			throw mw5axuiException( mw5axuiException::CUTTING_AREA, _T("mwAppElectrodeMachining4Plus1AxisPage::SetCuttingAreaPicture"));
	};	
	
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnLimCuts() 
{
	
	mw5axParams	tmpParams(m_Job);

	if(m_limitCutsDlg->DisplayDialog() == IDCANCEL)
	{
		m_Job=tmpParams;
	}
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnMargins() 
{
	mw5axParams	tmpParams(m_Job);

	if(m_marginsDlg->DisplayDialog() == IDCANCEL)
	{
		m_Job=tmpParams;
	}
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnStartCutFromCurve() 
{
	m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__FLIP_CUTS;
	PaintBMPs();
}
//#############################################################################

void mwAppElectrodeMachining4Plus1AxisPage::OnParallel() 
{
	m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__PARALLEL_CUTS;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelchangeCutMethod() 
{
	m_cutMethod = ( (CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_METHOD))->GetCurSel();

	switch(m_cutMethod)
	{
		case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ZIGZAG:
			m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__ZIGZAG;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_ONEWAY:
			m_nIDResBmp =  IDR_SURFACE_PATHS_PAGE__ONE_WAY; 
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_SPIRAL:
			m_nIDResBmp  = IDR_SURFACE_PATHS_PAGE__ADVANCED_SPIRAL;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_UP:
			m_nIDResBmp  = IDB_SURFACE_PATHS_PAGE__CUT_METHOD_UP;
			break;
		case mwAppElectrodeMachining4Plus1AxisPage::FP_MACHTYPE_DOWN:
			m_nIDResBmp  = IDB_SURFACE_PATHS_PAGE__CUT_METHOD_DOWN;
			break;
		default:
			throw mw5axuiException( mw5axuiException::MACHINING_TYPE,
				_T("mwAppElectrodeMachining4Plus1AxisPage::OnSelchangeCutMethod"));
	};


	PaintBMPs();
	ShowControlsUpdated();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelchangeCutOrder() 
{
	m_cut_order = ((CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_ORDER))->GetCurSel();

	if( m_cut_order == mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_STANDARD )
		m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__CUT_ORDER_STANDARD;

	if( m_cut_order == mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_FROM_CENTER )
		m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__CUT_ORDER_FROM_CENTER;
	
	if( m_cut_order == mwAppElectrodeMachining4Plus1AxisPage::FP_ORDER_FROM_OUTER )
		m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__CUT_ORDER_TO_CENTER;

	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelchangeRotationDir() 
{
	switch( (( CComboBox * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_ROTATION_DIR))->GetCurSel()  )
	{
	case 0:
		OnCCW();
		break;
	case 1:
		OnCW();
		break;
	case 2:
		OnClimb();
		break;
	case 3:
		OnConventional();
		break;
	}

}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnCCW() 
{
	m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_COUNTER_CLOCKWISE ;
	ShowControlsUpdated();

	m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__CCW;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnCW() 
{
	m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CLOCKWISE ;
	ShowControlsUpdated();

	m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__CWISE;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnClimb() 
{
	m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CLIMB ;
	ShowControlsUpdated();

	m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__CLIMB;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnConventional() 
{
	m_directionForOneWay = mwAppElectrodeMachining4Plus1AxisPage::FP_DIR_CONVENTIONAL;
	ShowControlsUpdated();

	m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__CONV;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnEnforceDir() 
{
	m_enforceClosedContour = !m_enforceClosedContour;
	ShowControlsUpdated();

	m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__ENFORCE_DIR;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnStartpointused() 
{
	m_startPos_sw = !m_startPos_sw;
	ShowControlsUpdated();

	m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__START_PNT;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::ShowStartPointItems(bool showFlag)
{
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STARTPOINTUSED)->ShowWindow(showFlag);	
	GetDlgItem(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_START_POINT)->ShowWindow(showFlag);	
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnBtnStartPoint()
{
	
	mw5axParams	tmpParams(m_Job);


	if( m_startPointDlg->DisplayDialog() == IDCANCEL)
	{
		m_Job=tmpParams;

	}
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetCutToleranceVisibility(bool isVisible)
{
	m_CutTolVisible = isVisible;
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetCutControlVisibility()
{
	if (m_CutTolVisible )
	{
		
		((CStatic * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_TOLERANCE ))->ShowWindow(SW_SHOW);
		((CEdit * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTOL ))->ShowWindow(SW_SHOW);
		
	}
	else
	{
		((CStatic * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUT_TOLERANCE ))->ShowWindow(SW_HIDE);
		((CEdit * )GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUTTOL ))->ShowWindow(SW_HIDE);
	}
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusCuttol() 
{
	m_nIDResBmp = IDR_SURFACE_PATHS_PAGE__CUT_TOL;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusMaxQuer() 
{
	m_nIDResBmp = IDB_SURFACE_PATHS_PAGE__STEPOVER;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnChainingTolSetFocus ()
{
	GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CHAIN_TOL_NOTE )->ShowWindow(SW_SHOW);
	m_nIDResBmp = IDB_ADV_OPT_FOR_SURF_QUALITY_DLG_CHAIN_TOL;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnChainingTolLostFocus ()
{
	GetDlgItem( IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CHAIN_TOL_NOTE )->ShowWindow(SW_HIDE);	
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusMoveSizePerc()
{
	//m_nIDResBmp = IDB_GAP_PAGE__GAP_BETW_CUTS;
	m_nIDResBmp = IDB_LINK_DLG_MOVE_SIZE_PERC_OF_STEPOVER;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearanceSphX()
{
	m_nIDResBmp = IDB_LINK_DLG_SPH_AROUND_X;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearanceSphY()
{
	m_nIDResBmp = IDB_LINK_DLG_SPH_AROUND_Y;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusClearanceSphZ()
{
	m_nIDResBmp = IDB_LINK_DLG_SPH_AROUND_Z;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnBtnEntryExitLeadSettings()
{

	mw5axParams	tmpParams(m_Job);

	if(m_defaultLeadsDlg->DisplayDialog() == IDCANCEL)
	{
		m_Job = tmpParams;

	}

}	
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusMaxAngleChange()
{
	m_nIDResBmp = IDB_TILT_PAGE__MAX_ANG;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnLimits()
{
	m_tilt_limits_selected=!m_tilt_limits_selected;
	ShowControlsUpdated();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSelLim()
{
	mw5axParams	tmpParams(m_Job);

	if(m_toolAngleLimitsDlg->DisplayDialog() == IDCANCEL )
	{
		m_Job=tmpParams;
	}
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusStockRemain()
{
	m_nIDResBmp  =  IDB_SURFACE_PATHS_PAGE__DRIVE_SURFACES_OFFSET;
	PaintBMPs();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusFixedTiltAngle()
{
	
	m_nIDResBmp = IDB_TILT_PAGE_TILT_FIXED_ANGLE_TILT_ANGLE;

	PaintBMPs(); 
	
	
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnAvoidAirCuts()
{
	m_avoid_air_cuts = !m_avoid_air_cuts;

	ShowControlsUpdated();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnBtnStockDefinition() 
{
	// TODO: Add your control notification handler code here
	if (this->UpdateData(true))
		m_interActor.EditStockDefinition();
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnChkCollOp1Status() 
{

	m_collOp1Status=!m_collOp1Status;
	ShowControlsUpdated();

}
//#############################################################################
BOOL mwAppElectrodeMachining4Plus1AxisPage::OnInitDialog()
{
	
	BOOL result =  mw5axuiPage::OnInitDialog();	

	if(result)
	{
		mw5axuiPage::SetRCImage(IDB_AppElectrode_Machining_4Plus1_Axis,
			IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_STATIC,
			_T("mwAppElectrodeMachining4Plus1AxisPage"));

	}
	//
	m_defaultLeadsLeadInController   =	new mwLeadController (m_Job.GetLinkParams().GetLeadInDefaultParams(),true,true);
	m_defaultLeadsLeadOutController  = 	new mwLeadController (m_Job.GetLinkParams().GetLeadOutDefaultParams(),true,true);
	
	m_defaultLeadsDlg = new mwLeadBothSetupDlg (m_Job,m_CurrentTool.GetRealTool(),m_interActor,
												true,this,mwLeadControllerHelper::LCT_DEFAULT_LEAD,
												MSG_LEADS_DEFAULT_LEADS_PAGE_TITLE,
												GetClientObserver());
	//

	return result;
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::OnSetfocusTiltToAxis()
{
	DisplayTiltToAxisPicture();
}
//#############################################################################
void  mwAppElectrodeMachining4Plus1AxisPage::SetNonCustomizableButtons()
{
	m_nonCustomizableButtons.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_DRIVE_SURFACE);
	m_nonCustomizableButtons.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_CHECK_SURFACES);
	m_nonCustomizableButtons.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_STOCK_DEFINITION);
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetControls2HideIfBntCustomizationIsActive()
{
	mw5axuiDlgsParams::m_ctrls2HideIfBtnCustomizeIsActive.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_SEL_LINE);
	mw5axuiDlgsParams::m_ctrls2HideIfBtnCustomizeIsActive.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_LIM_CUTS);
	mw5axuiDlgsParams::m_ctrls2HideIfBtnCustomizeIsActive.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_MARGINS);
	mw5axuiDlgsParams::m_ctrls2HideIfBtnCustomizeIsActive.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_BTN_ENTRY_EXIT_MACRO);
}
//#############################################################################
void mwAppElectrodeMachining4Plus1AxisPage::SetArrowImageStyleButtons()
{
	m_arrowImageStyleButtons.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CLEARANCE_PLANE_HEIGHT);
	m_arrowImageStyleButtons.push_back(IDC_APPELECTRODE_MACHINING_4PLUS1_AXIS_CUSTOM);
}
//#############################################################################