/*******************************************************************************
 (C) 2013 by ModuleWorks GmbH               
 Author:Luci Buta
******************************************************************************/
#ifndef __mwExtensionsDlg_hpp__
#define __mwExtensionsDlg_hpp__

#include "mwTpCalcBaseDlg.hpp"

class mwExtensionsDlg : public mwTpCalcBaseDlg
{
public:
	mwExtensionsDlg(mw5axParams &r5axParams,
		const misc::mwAutoPointer<cadcam::mwTool> &curtool,
		mwNotifyParamInteractor &interActor,
		mwTpCalcMethodsParams::Method method,
		CWnd* pParent = NULL,
		mw5axuiClientObserver* clientObserver  = NULL);   

	~mwExtensionsDlg();

	void UpdateWindowControls();

	// Dialog Data
	//{{AFX_DATA(mwMachiningAnglesDlg)
	enum { IDD = IDD_EXTENSIONS_DLG };
	int m_extensionType;
	int m_extensionTypeIdx;
	double m_extensionLengthAtStart;
	double m_extensionLengthAtEnd;
	double m_extensionStartAngle;
	int m_method;
	//}}AFX_DATA
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(mwMachiningAnglesDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(mwMachiningAnglesDlg)
	afx_msg void OnSelChangeExtensionType();
	afx_msg void OnSetFocusExtensionType();
	afx_msg void OnSetFocusExtensionAtStart();
	afx_msg void OnSetFocusExtensionAtEnd();
	afx_msg void OnSetFocusExtensionStartAngle();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void SetParams();
	void GetParams();
	void SetTexts();
	void SetExtensionParams( mwExtensionParams &extensionParams );
private:

	enum ExtensionType 
	{
		SMP_SET_AUTOMATIC,
		SMP_SET_ALIGN_TO_EDGES,
		SMP_SET_START_WITH_ANGLE
	};
	//!Converts extensions type from params format in format used by this dialog
	/*
		\param toConvert is extensions type in params format
		\return extensions type in format used by this dialog
	*/
	const int ConvertExtensionType(const mwExtensionParams::ExtensionType & toConvert) const;
	//#############################################################################
	//!Converts extensions type from format used by this dialog in param format
	/*
		\param toConvert is extensions type in format used by this dialog
		\return extensions type in params format
	*/
	const mwExtensionParams::ExtensionType  ConvertExtensionType(const int& toConvert) const;
	//#############################################################################
	//!Displays preview pictures for currently selected item in "Extensions Type" combobox
	void DisplayExtensionTypePicture();
	//#############################################################################
	void mwExtensionsDlg::SetCombosIndexes();
	//#############################################################################
	void FillExtensionTypeCombo();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif //__mwExtensionsDlg_hpp__
