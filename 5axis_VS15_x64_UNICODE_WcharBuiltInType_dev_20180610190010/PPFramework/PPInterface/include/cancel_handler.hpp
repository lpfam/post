#pragma once

namespace ppinterface {
	class CancelHandler {
	public:
		virtual bool ShouldCancel() = 0;
	};
}
