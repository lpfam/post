/******************************************************************************
  (C) 2003 by ModuleWorks GmbH
  Author: Sergej Nevstruyev
******************************************************************************/

#ifndef MW_MWMEMORYIOSTREAM_HPP_
#define MW_MWMEMORYIOSTREAM_HPP_
#include "mwPresetIOStream.hpp"
#include "mwDllImpExpDef.hpp"
#include "mwString.hpp"
#include "mwBasicTypes.hpp"

namespace misc
{
	class MW_5AXUTIL_API  mwMemoryIOStream : public mwPresetIOStream
	{
	public:
		typedef ::misc::uint64_t uint64_t;

		//!Create a memory stream
		/*!The default amount of memory allocated = increaseStep.
			\param increaseStep amount of memory to be allocated every time stream is 
			running out of memory
		*/
		mwMemoryIOStream( const uint64_t &increaseStep );	

		mwMemoryIOStream();
		//!Destructor
		virtual ~mwMemoryIOStream();
		//copy constructor 
		mwMemoryIOStream(const mwMemoryIOStream&);
		// =  operator 
		const mwMemoryIOStream& operator= (const mwMemoryIOStream&);
		

		//!Set increaseStep. 
		/*!Sets increase step. Doesn't reallocate the buffer.
			\param newIncreaseStep new value of increase step
		*/
		void SetIncreaseStep( const uint64_t &newIncreaseStep );


		//!Get data
		/*!
			\returns pointer to the buffer with data
		*/
		virtual const void *GetBuffer() const;

		//!Get data size
		/*!
			\returns length of the data
		*/
		virtual const uint64_t GetDataLength() const;

		//!Read data from the stream
		/*!Reads data from the internal buffer number of bytes actually read is stored in dataLen. 
			The internal pointer is moved forward by the number of bytes read.
			\param data buffer to store the read data
			\param dataLen size of the buffer
		*/
		virtual void Read( void *data, uint64_t &dataLen );

		//!Writes data to the stream
		/*!Writes data to the internal buffer. Reallocates the internal buffer automatically if needed.
			\param data buffer with data
			\param dataLen length of the buffer
			\throws mwMemoryException if memory reallocation fails
		*/
		virtual void Write( const void *data, const uint64_t &dataLen );

		//!Rewind
		/*!Sets the position of the internal pointer to the specified position.
			\param pos offset to set the pointer
			\throws mwIdxRangeException if specified position is out of range
		*/
		virtual void Rewind( const uint64_t &pos = 0 );

		//!Get data position
		/*!
			\returns Gets the position of the internal pointer relative to the data 
		*/
		virtual const uint64_t GetDataPosition() const;

		virtual void Attach(const void *data, const uint64_t &dataLen );

		void Detach();
		
		//#############################################################################
		//!Clears memory stream content
		/*!Deallocate all alocated memory for the stream and initialize to zero all member variables.
		After a call to clear, you should call SetIncreaseStep 
		*/
		void Clear();
		//#############################################################################
		
		mwMemoryIOStream( const misc::mwstring &, const bool );	

	protected:
	private:
		
		void Copy(const mwMemoryIOStream& toCopy);
		
		uint64_t 	mIncreaseStep;
		uint64_t	mDataSize;
		uint64_t	mAllocatedDataSize;
		char        *mDataPtr;
		char		*mCurDataPtr;
		bool	    m_isAttached;
	};
};
#endif	//	MW_MWMEMORYIOSTREAM_HPP_
