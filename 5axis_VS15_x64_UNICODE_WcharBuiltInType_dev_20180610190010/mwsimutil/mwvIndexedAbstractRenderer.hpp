/******************************************************************************
  (C) 2013 by ModuleWorks GmbH
  Author: Jan Theegarten
******************************************************************************/
#ifndef __mwvIndexedAbstractRenderer_hpp__
#define __mwvIndexedAbstractRenderer_hpp__

#include "mwvAbstractRendererBase.hpp"
#include "mwvAbstractRendererPrimitives.hpp"
#include "mwvAbstractRendererAttributes.hpp"


namespace VerifierUtil
{


//! Specialized renderer interface, outputting triangles and lines with shared vertices
/** 
- See mwvAbstractRendererBase about the general idea of this class.
- This class provides renderer-data as triangles and lines, which can use shared vertices.
The vertices are shared by providing them as an array together with indices, putting them
together to triangles and lines.
*/
class MW_SIMUTIL_API mwvIndexedAbstractRenderer : public mwvAbstractRendererBase
{
public:
	typedef unsigned Index;
	typedef mwvAbstractRendererPrimitives::ColorRGBA ColorRGBA;
	typedef mwvAbstractRendererPrimitives::LineVertex LineVertex;
	typedef mwvAbstractRendererPrimitives::TriangleVertex TriangleVertex;
	typedef AbstractRendererAttributes::VertexAttributes VertexAttributes;

	//! Will be deprecated soon, please use DrawTriangles(..., vertexAttributes)
	virtual inline void DrawTriangles(
		const RenderGroupID groupId, 
		const TriangleVertex* vertices, const size_t numOfVertices, 
		const bool indicesValid, const Index* indices, const size_t numOfIndices)
	{
		// Default implemented to be able to use DrawTriangles(..., vertexAttributes) only.
		MW_AVOID_WARNING_UNREFERENCED_PARAM(groupId, vertices, numOfVertices, indicesValid, indices, numOfIndices);
	};

	//! Called, if some triangles are rendered.
	/*! \param [in] groupId Identifies group for the triangles. If the values for groupId appears the first time,
	a new group was created. Consecutive calls with the same groupId will update the triangles. \sa DeleteGroup.
	\param [in] vertices C-array of vertices.
	\param [in] numOfVertices Number of vertices in array. 
	\param [in] indicesValid True, iff indices are available, see indices.
	\param [in] indices C-array of indices. Indices are positions in the vertices-array. 
	Three indices together define a triangles, that is the vertices of the first triangle are
	vertices[indices[0]], vertices[indices[1]], vertices[indices[2]]. If indices points to NULL,
	indices are not available, that is the vertices are not shared. Then each three vertices in the vertices-array define one triangle. See also indicesValid.
	\param [in] numOfIndices Number of indices in array, dividable by 3.
	\param [in] vertexAttributes Requested vertex attributes, see also VerifierUtil::RenderOptions.	*/
	virtual void DrawTriangles(
		const RenderGroupID groupId, 
		const TriangleVertex* vertices, const size_t numOfVertices, 
		const bool indicesValid, const Index* indices, const size_t numOfIndices,
		const VertexAttributes& vertexAttributes)
		MW_VIRTUAL_PURE_DEFINITION(void,groupId, vertices, numOfVertices, indicesValid, indices, numOfIndices, vertexAttributes);

	//! Called, if some lines are rendered.
	/*! \param [in] groupId Identifies group for the triangles. If the values for groupId appears the first time,
	a new group was created. Consecutive calls with the same groupId will update the triangles. \sa DeleteGroup.
	\param [in] vertices C-array of vertices.
	\param [in] numOfVertices Number of vertices in array. 
	\param [in] indicesValid True, iff indices are available, see indices.
	\param [in] indices C-array of indices. Indices are positions in the vertices-array. 
	Two indices together define a line, that is the vertices of the first line are
	vertices[indices[0]], vertices[indices[1]]. If indices points to NULL,
	indices are not available, that is the vertices are not shared. Then each three vertices in the vertices-array define one triangle. See also indicesValid.
	\param [in] numOfIndices Number of indices in array, dividable by 2.
	*/
	virtual void DrawLines(
		const RenderGroupID groupId, 
		const LineVertex* vertices, const size_t numOfVertices, 
		const bool indicesValid, const Index* indices, const size_t numOfIndices) = 0;

	//! See mwvAbstractRendererBase::SetGroupVisibility().
	virtual void SetGroupVisibility(const RenderGroupID groupId, const bool visible) MW_VIRTUAL_PURE_DEFINITION(void,groupId, visible);

	//! See mwvAbstractRendererBase::DeleteGroup().
	virtual void DeleteGroup(const RenderGroupID groupId) = 0;

	//! Part of the visitor pattern, used internally and does not have to be changed/overridden.
	inline virtual void AcceptVirtual(mwvAbstractRendererVisitor& visitor)
	{ 
		visitor.Visit(*this);
	};
	virtual ~mwvIndexedAbstractRenderer();
};

}

#endif  // __mwvIndexedAbstractRenderer_hpp__