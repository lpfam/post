/******************************************************************************
*               File: mwColorSchemaAdjustAxisPole.cpp                         *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*   22.06.2009 10:53:36 Created by: Mihai Vasilian                            *
*******************************************************************************
*               (C) 2009 by ModuleWorks GmbH                                  *
******************************************************************************/

#include "StdAfx.h"
#include "mwColorSchemaAdjustAxisPole.hpp"
#include "mwBcgLocalResource.hpp"
#include "mwMSimGuiResource.hpp"
#include "mwMachSimUtils.hpp"
#include "mwMathConstants.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// mwColorSchemaAdjustAxisPole dialog

IMPLEMENT_DYNAMIC(mwColorSchemaAdjustAxisPole, mwMSimDialog)

mwColorSchemaAdjustAxisPole::mwColorSchemaAdjustAxisPole( double mMinimAngle, 
																   double mMaximAngle, 																		   
																   CWnd* pParent )
																   : mwMSimDialog(mwColorSchemaAdjustAxisPole::IDD, pParent)
																   , m_minDefined( mMinimAngle )
																   , m_maxDefined( mMaximAngle )	
																   , m_reset(FALSE)
																   , m_minSavedValue( mMinimAngle )
																   , m_maxSavedValue( mMaximAngle )																		   
{
}

mwColorSchemaAdjustAxisPole::~mwColorSchemaAdjustAxisPole()
{
}

void mwColorSchemaAdjustAxisPole::DoDataExchange(CDataExchange* pDX)
{
	mwMSimDialog::DoDataExchange(pDX);
	//use UpdateControls method to update controls values.
	UpdateControls(pDX->m_bSaveAndValidate) ;
}


BEGIN_MESSAGE_MAP(mwColorSchemaAdjustAxisPole, mwMSimDialog)	
	ON_BN_CLICKED( IDC_RESET_CHECK, OnResetCheckBnClicked )
END_MESSAGE_MAP()


// mwColorSchemaAdjustAxisPole message handlers
BOOL mwColorSchemaAdjustAxisPole::OnInitDialog()
{
	mwMSimDialog::OnInitDialog();


	misc::mwstring sSection = _T("Toolpath");
#ifndef MCAM_INTEGRATION
	SetWindowText( mwMSimGuiResource::GetMsg( sSection,56).c_str() );
#endif
	SetCntrlText(IDC_MINIMUM_STATIC, sSection, 57);
	SetCntrlText( IDC_MAXIMUM_STATIC, sSection,58);
	SetCntrlText( IDC_RESET_CHECK, sSection,26);


	return true;
}

void mwColorSchemaAdjustAxisPole::OnOK()
{
	UpdateControls(TRUE);
	misc::mwstring mwSection(_T("Toolpath")) ;

	if( m_reset )
	{
		mwMSimDialog::OnOK() ;
		return ;
	}


	misc::mwstring text(_T("")) ;

	if( mathdef::is_lt(m_minDefined, 0.0) ||
        mathdef::is_gte(m_minDefined, 90.0) )
	{	
		text = mwMSimGuiResource::GetMsg( mwSection,59) ;						
		mwMSimGuiResource::MsgBox( text, MB_OK | MB_ICONWARNING ) ;
		return ;
	}



	if( mathdef::is_lte(m_maxDefined, 0.0) ||
        mathdef::is_gt(m_maxDefined, 90.0) )
	{		
		text = mwMSimGuiResource::GetMsg( mwSection,60) ;						
		mwMSimGuiResource::MsgBox( text, MB_OK | MB_ICONWARNING ) ;
		return ;				
	}	
	
	mwMSimDialog::OnOK() ;
	
}



void mwColorSchemaAdjustAxisPole::UpdateControls( BOOL saveAndValidate ) 
{
	if( saveAndValidate==TRUE)//retrieve data from controls
	{
		TCHAR sMin[50] , sMax[50] ;
		double dMin=0., dMax=0.;
		GetDlgItemText(IDC_MINIMUM_EDIT, sMin, 49) ;
		GetDlgItemText(IDC_MAXIMUM_EDIT, sMax, 49) ;
		m_reset = (((CButton*)GetDlgItem( IDC_RESET_CHECK))->GetCheck() == BST_CHECKED ? TRUE : FALSE ) ;

		try
		{
			misc::to_value(sMin, dMin) ;
			misc::to_value(sMax, dMax) ;
			m_minDefined = dMin ;
			m_maxDefined = dMax ;
		}
		catch(...){} ;		
	}
	else
	{
		misc::mwstring sMin(_T("0.00000")) , sMax(_T("0.00000")) ;		
		try
		{
			sMin = misc::from_value( m_minDefined, 5 ) ;
			sMax = misc::from_value( m_maxDefined, 5 ) ;			
		}
		catch(...){} ;	


		GetDlgItem(IDC_MAXIMUM_EDIT)->SetWindowText( sMax.c_str() ) ;
		GetDlgItem(IDC_MINIMUM_EDIT)->SetWindowText( sMin.c_str() ) ;		
		((CButton*)GetDlgItem(IDC_RESET_CHECK))->SetCheck( (m_reset == TRUE ? BST_CHECKED : BST_UNCHECKED) ) ;

		GetDlgItem( IDC_MINIMUM_EDIT )->EnableWindow( !m_reset ) ;
		GetDlgItem( IDC_MAXIMUM_EDIT )->EnableWindow( !m_reset ) ;

	}

}


//handlers
void mwColorSchemaAdjustAxisPole::OnResetCheckBnClicked( )
{
	m_reset = (((CButton*)GetDlgItem( IDC_RESET_CHECK))->GetCheck() == BST_CHECKED ? TRUE : FALSE ) ;

	GetDlgItem( IDC_MINIMUM_EDIT )->EnableWindow( !m_reset ) ;
	GetDlgItem( IDC_MAXIMUM_EDIT )->EnableWindow( !m_reset ) ;
};
