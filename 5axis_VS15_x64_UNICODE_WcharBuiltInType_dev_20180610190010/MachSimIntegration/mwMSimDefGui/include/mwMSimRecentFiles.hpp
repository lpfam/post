#ifndef __mwMSimRecentFiles_HPP
#define __mwMSimRecentFiles_HPP

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mwMSimGuiControl.hpp"

class mwMSimRecentFiles :
	public CBCGPRecentFilesListBox,
	public mwMSimGuiControl
{
	DECLARE_DYNAMIC(mwMSimRecentFiles)

public:

	enum FileType
	{
		MACHINE_DEF,
		SIM_FILE
	};

	mwMSimRecentFiles(FileType fileType);   // standard constructor

	virtual CRecentFileList* GetRecentList();
	virtual void OnChooseRecentFile(UINT uiCmd);
	virtual void OnChooseRecentFolder(LPCTSTR lpszFolder);
	virtual void OnChoosePinnedFile(LPCTSTR lpszFile);
	void SetMaxSize(int size);
	virtual int AddItem(const CString& strFilePath, UINT nCmd, BOOL bPin);

protected:


private:

	bool CheckFileExists(LPCTSTR lpszFile);
	bool AskFileRemove();

	FileType m_fileType;
	int m_maxSize;
};

#endif // !ifndef __mwMSimRecentFiles_HPP
