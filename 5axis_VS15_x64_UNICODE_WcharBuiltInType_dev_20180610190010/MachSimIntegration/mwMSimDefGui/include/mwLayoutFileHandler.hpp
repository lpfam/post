/******************************************************************************
  (C) 2017 by ModuleWorks GmbH
  Author: Relu Dumitru
******************************************************************************/
#ifndef __mwLayoutFileHandler_hpp__
#define __mwLayoutFileHandler_hpp__

#include "mwException.hpp"
#include "mwMSimExceptionHandler.hpp"
#include "mwStringTokenizer.hpp"

#include <algorithm>
#include <iterator>
#include <vector>

class mwLayoutFileHandler
{
public:
	struct Value
	{
		Value(misc::mwstring name = _T(""), DWORD type = 0, std::vector<BYTE> data = std::vector<BYTE>())
			: m_name(name),
			m_type(type),
			m_data(std::move(data))
		{
		}

		misc::mwstring m_name;
		DWORD m_type;
		std::vector<BYTE> m_data;
	};

	mwLayoutFileHandler();
	mwLayoutFileHandler(const CString& path);

	void ParseLayoutFile(const CString& path);

	void WriteLayoutFile(const CString& path);

	void WriteLayoutXML(const CString& path);

	void SetValue(const misc::mwstring& path, const Value& value);

	template <typename T>
	void SetValue(const misc::mwstring& path, const misc::mwstring& name, const T& value);

	template <typename T>
	void SetValue(const misc::mwstring& path, const misc::mwstring& name, const std::vector<T>& values);


	std::vector<Value> GetValues(const misc::mwstring& path);

	Value GetValue(const misc::mwstring& path, const misc::mwstring& name);

	template <typename T>
	T GetValue(const misc::mwstring& path, const misc::mwstring& name);

	template <typename T>
	std::vector<T> GetListValue(const misc::mwstring& path, const misc::mwstring& name);


	bool HasValue(const misc::mwstring& path, const misc::mwstring& name);

	bool HasNode(const misc::mwstring& path);

	bool DeleteNode(const misc::mwstring& path);

private:
	struct Node
	{
		Node(misc::mwstring name = _T(""))
			: m_name(name)
		{
		}

		misc::mwstring m_name;
		std::vector<Value> m_values;
		std::vector<Node> m_children;
	};

	misc::mwstring Convert(const CString& cStr) const;
	CString Convert(const misc::mwstring& str) const;

	misc::mwstring ConvertToString(const std::vector<BYTE>& data) const;
	std::vector<BYTE> ConvertToData(const misc::mwstring& str) const;

	Node Parse(CArchive& archive, const misc::mwstring& nodeName = _T("")) const;
	void Write(CArchive& archive, const Node& node) const;
	void WriteXml(std::ofstream& file, const Node& node, const int level = 0) const;

	Node* GetNode(Node& root, const misc::mwstring& path, const bool createPath = true) const;
	Value* GetValue(Node& node, const misc::mwstring& name) const;

	Node m_root;
};



template <typename T>
void mwLayoutFileHandler::SetValue(const misc::mwstring& path, const misc::mwstring& name, const T& value)
{
	Value val;
	val.m_name = name;
	val.m_type = 0;

	val.m_data = ConvertToData(misc::from_value(value));

	return SetValue(path, val);
}

template <typename T>
void mwLayoutFileHandler::SetValue(const misc::mwstring& path, const misc::mwstring& name, const std::vector<T>& values)
{
	if (values.empty())
	{
		return;
	}

	misc::mwstring asString;
	const auto lastIt = values.cend() - 1;
	for (auto it = values.cbegin(); it != values.cend(); ++it)
	{
		auto valueAsString = misc::from_value(*it);
		asString += valueAsString;

		if (it != lastIt)
		{
			asString += ",";
		}
	}

	Value value;
	value.m_name = name;
	value.m_type = 0;
	value.m_data = ConvertToData(asString);

	SetValue(path, value);
}


template <typename T>
T mwLayoutFileHandler::GetValue(const misc::mwstring& path, const misc::mwstring& name)
{
	T result = T();
	Value val = GetValue(path, name);
	if (val.m_name == name)
	{
		auto asString = ConvertToString(val.m_data);


		misc::to_value(asString, result);
	}

	return result;
}

template <typename T>
std::vector<T>
mwLayoutFileHandler::GetListValue(const misc::mwstring& path, const misc::mwstring& name)
{
	std::vector<T> result;
	Value val = GetValue(path, name);
	if (val.m_name != name)
	{
		return result;
	}

	auto listOfValuesAsString = ConvertToString(val.m_data);
	misc::mwStringTokenizer tokenizer(_T(","));
	tokenizer.Parse(listOfValuesAsString);
	for (auto it = tokenizer.GetTokenBegin(); it != tokenizer.GetTokenEnd(); ++it)
	{
		T value = T();
		misc::to_value(*it, value);

		result.push_back(value);
	}

	return result;
}

#endif //__mwLayoutFileHandler_hpp__