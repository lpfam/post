/******************************************************************************
  (C) 2016 by ModuleWorks GmbH
  Author: Relu Dumitru
******************************************************************************/
#ifndef __mwLayerSettingsHelper_hpp__
#define __mwLayerSettingsHelper_hpp__

#include "mwToolpathBaseManager.hpp"
#include "mwToolpathGraphicIniParams.hpp"

#include <vector>
#include <utility>

//! Helper class used to process a list of toolpath graphics and identify
//the layer from them.
class mwLayerSettingsHelper
{
public:
	typedef std::vector<machsim::mwfToolpathGraphicPtr> ToolpathGraphicPtrs;
	typedef cadcam::mwPoint3df Point3D;
	typedef machsim::mwfToolpathGraphic::BBox BoundingBox;

	//! Constructor.
	mwLayerSettingsHelper();

	//! Process the given toolpath graphics using the given normal.
	void ProcessToolpathGraphics(const ToolpathGraphicPtrs& toolpathGraphics, const Point3D& normal);

	//! Sets the layer thickness.
	void SetLayerThickness(const float thickness);

	//! Returns the layer thickness.
	const float GetLayerThickness() const;

	//! Sets the the layer offset.
	void SetLayerOffset(const float layerOffset);

	//! Returns the layer offset.
	const float GetLayerOffset() const;

	//! Returns the first layer index.
	const int GetFirstLayer() const;

	//! Returns the last layer index.
	const int GetLastLayer() const;

	//! Returns the bounding box of the processed toolpath graphics.
	const BoundingBox& GetToolpathBoundingBox() const;

	//! Returns the minimum value along the given normal.
	const float GetMinValue() const;

	//! Returns the maximum value along the given normal.
	const float GetMaxValue() const;

private:
	typedef std::pair<int, int> LayerRange;
	typedef std::pair<float, float> LayerValueRange;
	typedef std::vector<LayerValueRange> LayerValueRanges;

	void UpdateLayerRange();
	const BoundingBox ComputeBoundingBox(const ToolpathGraphicPtrs& toolpathGraphics) const;
	const LayerValueRange ComputeLayerValueRange(const ToolpathGraphicPtrs& toolpathGraphics) const;

	const LayerValueRange FindToolpathGraphicLayerValueRange(const machsim::mwfToolpathGraphicPtr& tpGraphic) const;
	const LayerValueRange FindLayerValueRange(const LayerValueRanges& ranges) const;

	float m_layerThickness;
	float m_layerOffset;
	LayerRange m_layerRange;
	LayerValueRange m_layerValueRange;
	BoundingBox m_tpBbox;
	Point3D m_sectionNormal;
};

#endif //__mwLayerSettingsHelper_hpp__