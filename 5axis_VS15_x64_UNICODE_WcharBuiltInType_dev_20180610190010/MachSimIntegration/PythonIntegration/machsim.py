import os
import array
import struct
import win32con
import win32api
import win32gui
import subprocess

_CMD_ID = 0

def AllocCMD():
    global _CMD_ID
    _CMD_ID += 1
    return _CMD_ID

APICMD = 2121
APICMD_VERSION = 1

APICMD_NOP = AllocCMD()
assert APICMD_NOP == 1

APICMD_LOAD_MACHINE_DEFINITION = AllocCMD()
APICMD_LOAD_TOOL_PATH = AllocCMD()

APICMD_RENDER_SCENE = AllocCMD()

APICMD_RUN_SIMULATION = AllocCMD()
APICMD_STOP_SIMULATION = AllocCMD()
APICMD_PAUSE_SIMULATION = AllocCMD()

APICMD_GO_TO_NEXT_OPERATION = AllocCMD()
APICMD_GO_TO_PREV_OPERATION = AllocCMD()

APICMD_GO_TO_NEXT_MOVE = AllocCMD()
APICMD_GO_TO_PREV_MOVE = AllocCMD()

APICMD_FIT_TO_SCREEN = AllocCMD()

APICMD_SET_SIMULATION_SPEED = AllocCMD()

APICMD_ENABLE_MACHINE_HOUSING = AllocCMD()
APICMD_ENABLE_STOCK = AllocCMD()
APICMD_ENABLE_TOOL_PATH = AllocCMD()
APICMD_ENABLE_WORK_AREA_FOCUS = AllocCMD()

APICMD_SET_MANUAL_MODE = AllocCMD()

APICMD_SHUT_DOWN = AllocCMD()

class MachSim(object):

    def __init__(self, exePath, attachToExisting=True, closeOnDelete=False):
        self.__closeOnDelete = closeOnDelete
        self.__machSimProcess = None
        self.__machSimWindow = 0
        if attachToExisting:
            self.__machSimWindow = win32gui.FindWindow(None, "MachSimIntegration")
        if self.__machSimWindow == 0:
            machSimExePath = os.path.abspath(exePath)
            machSimFolder = os.path.dirname(machSimExePath)
            self.__machSimProcess = subprocess.Popen(args='"%s"' % machSimExePath, executable=exePath, cwd=machSimFolder)
        while True:
            if self.__machSimProcess is not None and self.__machSimProcess.poll() is not None:
                raise RuntimeError("MachSim process exited before being able to communicate")
            if self.__machSimWindow != 0:
                if self.NOP(raiseIfFailed=False):
                    break
            else:
                self.__machSimWindow = win32gui.FindWindow(None, "MachSimIntegration")
            win32api.Sleep(10)

    def __del__(self):
        if self.__closeOnDelete:
            self.ShutDown()

    def NOP(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_NOP, raiseIfFailed=raiseIfFailed)

    def LoadMachineDefinition(self, path, raiseIfFailed=True):
        return self.__APICommand(APICMD_LOAD_MACHINE_DEFINITION, pathValue=path, raiseIfFailed=raiseIfFailed)

    def LoadToolPath(self, path, raiseIfFailed=True):
        return self.__APICommand(APICMD_LOAD_TOOL_PATH, pathValue=path, raiseIfFailed=raiseIfFailed)

    def RenderScene(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_RENDER_SCENE, raiseIfFailed=raiseIfFailed)

    def RunSimulation(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_RUN_SIMULATION, raiseIfFailed=raiseIfFailed)

    def StopSimulation(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_STOP_SIMULATION, raiseIfFailed=raiseIfFailed)

    def PauseSimulation(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_PAUSE_SIMULATION, raiseIfFailed=raiseIfFailed)

    def GoToNextOperation(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_GO_TO_NEXT_OPERATION, raiseIfFailed=raiseIfFailed)

    def GoToPrevOperation(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_GO_TO_PREV_OPERATION, raiseIfFailed=raiseIfFailed)

    def GoToNextMove(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_GO_TO_NEXT_MOVE, raiseIfFailed=raiseIfFailed)

    def GoToPrevMove(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_GO_TO_PREV_MOVE, raiseIfFailed=raiseIfFailed)

    def FitToScreen(self, raiseIfFailed=True):
        return self.__APICommand(APICMD_FIT_TO_SCREEN, raiseIfFailed=raiseIfFailed)

    def SetSimulationSpeed(self, speed, raiseIfFailed=True):
        return self.__APICommand(APICMD_SET_SIMULATION_SPEED, intValue=speed, raiseIfFailed=raiseIfFailed)

    def EnableMachineHousing(self, enabled=True, raiseIfFailed=True):
        return self.__APICommand(APICMD_ENABLE_MACHINE_HOUSING, boolValue=enabled, raiseIfFailed=raiseIfFailed)

    def EnableStock(self, enabled=True, raiseIfFailed=True):
        return self.__APICommand(APICMD_ENABLE_STOCK, boolValue=enabled, raiseIfFailed=raiseIfFailed)

    def EnableToolPath(self, enabled=True, raiseIfFailed=True):
        return self.__APICommand(APICMD_ENABLE_TOOL_PATH, boolValue=enabled, raiseIfFailed=raiseIfFailed)

    def EnableWorkAreaFocus(self, enabled=True, raiseIfFailed=True):
        return self.__APICommand(APICMD_ENABLE_WORK_AREA_FOCUS, boolValue=enabled, raiseIfFailed=raiseIfFailed)

    def SetManualMode(self, manualMode=True, raiseIfFailed=True):
        return self.__APICommand(APICMD_SET_MANUAL_MODE, boolValue=manualMode, raiseIfFailed=raiseIfFailed)

    def ShutDown(self):
        if self.__machSimWindow != 0:
            if win32gui.IsWindow(self.__machSimWindow):
                win32gui.SendMessage(self.__machSimWindow, win32con.WM_CLOSE, 0, 0)
            self.__machSimWindow = 0
        if self.__machSimProcess is not None and self.__machSimProcess.poll() is None:
            self.__machSimProcess.wait()
            self.__machSimProcess = None

    def __APICommand(self, command, intValue=0, boolValue=False, pathValue="", raiseIfFailed=False):
        if self.__machSimProcess is not None and self.__machSimProcess.poll() is not None:
            raise RuntimeError("MachSim process is no longer running")
        if self.__machSimWindow == 0 or not win32gui.IsWindow(self.__machSimWindow):
            raise RuntimeError("MachSim window is no longer valid")
        data, dataAddress, dataLength = self.__Pack("=iiiB%ds" % win32con.MAX_PATH, APICMD_VERSION, command, intValue, boolValue, pathValue)
        cds, cdsAddress, cdsLength = self.__Pack("=III", APICMD, dataLength, dataAddress)
        result = win32gui.SendMessage(self.__machSimWindow, win32con.WM_COPYDATA, 0, cdsAddress)
        if raiseIfFailed and not result:
            raise RuntimeError("MachSim command failed")
        return result

    @staticmethod
    def __Pack(format, *args):
        data = array.array("c", struct.pack(format, *args))
        dataAddress, dataLength = data.buffer_info()
        return data, dataAddress, dataLength
