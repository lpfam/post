import os
import machsim
import win32api

def Main():
    machSimExe = os.path.abspath(r"..\mwMSimStandaloneDefGUI\Release\MachSimIntegration.exe")
    machineDefinition = os.path.abspath(r"..\..\quickstart\Machines\5AxOkuma_MU400VA\5AxOkuma_MU400VA.xml")
    #toolPath = r"T.sim"
    machSim = machsim.MachSim(machSimExe, attachToExisting=True, closeOnDelete=False)
    machSim.LoadMachineDefinition(machineDefinition)
    win32api.Sleep(1000)
    #machSim.LoadToolPath(toolPath)
    #win32api.Sleep(1000)
    #machSim.RunSimulation()
    #win32api.Sleep(3000)
    #machSim.PauseSimulation()
    #win32api.Sleep(1000)
    #machSim.StopSimulation()
    #win32api.Sleep(1000)
    machSim.EnableMachineHousing(False)
    win32api.Sleep(2000)
    machSim.FitToScreen()
    win32api.Sleep(2000)
    #machSim.ShutDown()

if __name__ == "__main__":
    Main()
