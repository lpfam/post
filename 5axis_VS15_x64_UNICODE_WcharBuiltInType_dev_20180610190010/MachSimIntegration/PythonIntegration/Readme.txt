Controlling machsim from Python 2.4 or newer

The pywin32 Python package is required. Please install the one compatible with your Python version.
http://downloads.sourceforge.net/pywin32/

machsim.py is the module which you must import to control MachSim.

You can use test.py as a starting point.

MachSim is started in a different process.

!!! Note that when MachSim is started it's current folder is set to the folder
    which contains it's exacutable. So if you pass relative file names to methods
    like LoadMachineDefinition, they will be relative to the MachSim binary folder,
    not to the current folder of the script. This is why I recommend passing absolute
    filenames (you can use os.path.abspath())

Currently we can't pass exception information back to the script. We only know if the command
succedeed or failed. The exception text is however sent to the debug output. You can use this
tool to view it: http://www.microsoft.com/technet/sysinternals/utilities/debugview.mspx
