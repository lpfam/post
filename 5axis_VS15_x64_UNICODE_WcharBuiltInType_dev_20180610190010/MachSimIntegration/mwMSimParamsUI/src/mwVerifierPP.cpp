/******************************************************************************
  (C) 2007 by ModuleWorks GmbH
  Author: Tolbariu Ionut Irinel
******************************************************************************/


#include "StdAfx.h"
#include "mwVerifierPP.hpp"
#include "mwMessages.hpp"
#include "mwResource.hpp"
#include "mwFileSystem.hpp"
#include "mwStockCreator.hpp"

//#############################################################################
// mwVerifierPP dialog
//IMPLEMENT_DYNAMIC(mwVerifierPP, CPropertyPage)
mwVerifierPP::mwVerifierPP( 
						   mwMachSimExtendedStartUpParams& msStartUpParam, 
						   mwStockParam& stockParam,
						   mwMachsimVerificationParams& machsimVerificationParams,
						   const misc::mwAutoPointer<mwGeoLib>& pGeoLib,
						   const mwMachSimParamInteractor& pInteractor)
	: mwMachSimUIDlgParams(msStartUpParam, pInteractor) 
	, m_pGeoLib(pGeoLib)
	, m_stockParam(stockParam)
	, m_machsimVerificationParams(machsimVerificationParams)
	, CPropertyPage(IDD)
	, m_adjustStockMeshQualityDuringRun(FALSE)
	, m_adjustStockMeshSlider(-1)
	, m_autoAdjustPrecision(FALSE)
	, m_autoQualityDraw(FALSE)
	, m_samples(DEFAULT)
	, m_min3(0)
	, m_med3(0)
	, m_max3(0)
	, m_min32(0)
	, m_med32(0)
	, m_max32(0)
	, m_min3Man(0)
	, m_med3Man(0)
	, m_max3Man(0)
	, m_min32Man(0)
	, m_med32Man(0)
	, m_max32Man(0)
	, m_3AxisDataMod(FALSE)
	, m_32AxisDataMod(FALSE)
	, m_3AxisDataModMan(FALSE)
	, m_32AxisDataModMan(FALSE)
	, m_3axSlider(-1)
	, m_32axSlider(-1)
	, m_includeAlsoCheckSurfaces(FALSE)
	, m_choice(-1)
	, m_offsettingX(0)
	, m_offsettingY(0)
	, m_offsettingZ(0)
	, m_offsettingRadius(0)
	, m_offsettingLength(0)
	, m_offsettingValue(0)
	, m_cylinderAxisGoesThroghOrigin(FALSE)
	, m_showAllGouges(TRUE)
	, m_gouges(0)
	, m_showAllExcesses(TRUE)
	, m_excesses(0)
	, m_makeGaugeReport3(FALSE)
	, m_makeGaugeReport32(FALSE)
	, m_3axisResetPush(false)
	, m_32axisResetPush(false)
	, m_biggestStockEdge(0.0)
	, m_enableVerifier(TRUE)
{
	mTitle = GetText(MSG_VERIFIER_PP_TITLE);
	m_psp.pszTitle = _tcsdup(mTitle.c_str());
	m_psp.dwFlags |= PSP_USETITLE;
	m_psp.dwFlags |= PSP_HASHELP;

	m_adjustStockMeshQualityDuringRunTT=MW_NULL;
	m_adjustStockMeshQualityDuringRunSliderTT=MW_NULL;
	m_autoAdjustPrecisionTT=MW_NULL;
	m_autoQualityDrawTT=MW_NULL;
	m_makeGaugeReport3TT=MW_NULL;
	m_makeGaugeReport32TT=MW_NULL;
	m_3axMinTT=MW_NULL;
	m_3axMedTT=MW_NULL;
	m_3axMaxTT=MW_NULL;
	m_32axMinTT=MW_NULL;
	m_32axMedTT=MW_NULL;
	m_32axMaxTT=MW_NULL;
	m_gougesTT=MW_NULL;
	m_excessesTT=MW_NULL;
	m_verifierResourceFileTT = MW_NULL;
	m_enableVerifierTT = MW_NULL;
}
//#############################################################################
mwVerifierPP::~mwVerifierPP()
{
	free( (LPVOID)m_psp.pszTitle );
	m_psp.pszTitle = MW_NULL;

	if(m_adjustStockMeshQualityDuringRunTT)
		delete m_adjustStockMeshQualityDuringRunTT;
	if(m_adjustStockMeshQualityDuringRunSliderTT)
		delete m_adjustStockMeshQualityDuringRunSliderTT;
	if(m_autoAdjustPrecisionTT)
		delete m_autoAdjustPrecisionTT;
	if(m_autoQualityDrawTT)
		delete m_autoQualityDrawTT;
	if(m_makeGaugeReport3TT)
		delete m_makeGaugeReport3TT;
	if(m_makeGaugeReport32TT)
		delete m_makeGaugeReport32TT;
	if(m_3axMinTT)
		delete m_3axMinTT;
	if(m_3axMedTT)
		delete m_3axMedTT;
	if(m_3axMaxTT)
		delete m_3axMaxTT;
	if(m_32axMinTT)
		delete m_32axMinTT;
	if(m_32axMedTT)
		delete m_32axMedTT;
	if(m_32axMaxTT)
		delete m_32axMaxTT;
	if(m_gougesTT)
		delete m_gougesTT;
	if(m_excessesTT)
		delete m_excessesTT;
	if(m_verifierResourceFileTT)
		delete m_verifierResourceFileTT;
	if (m_enableVerifierTT)
		delete m_enableVerifierTT;
}
//#############################################################################
BOOL mwVerifierPP::OnInitDialog()
{
	CDialog::OnInitDialog();

	CreateToolTips();
	OnClickedEnableMaterialRemovalMode();

	return TRUE;  // return TRUE  unless you set the focus to a control
}
//#############################################################################
void mwVerifierPP::CreateToolTips()
{
	//Create the ToolTip controls
	
	m_verifierResourceFileTT = new CToolTipCtrl;
	if(!m_verifierResourceFileTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_verifierResourceFileTT->AddTool(&*GetDlgItem(IDC_EDT_CUTSIM_RESOURCE_FILE), _T("resourcefile"));
		m_verifierResourceFileTT->AddTool(&*GetDlgItem(IDC_BUT_CUTSIM_RESOURCE_FILE), _T("resourcefile"));
		m_verifierResourceFileTT->Activate(TRUE);
	}

	m_adjustStockMeshQualityDuringRunTT = new CToolTipCtrl;
	if( !m_adjustStockMeshQualityDuringRunTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_adjustStockMeshQualityDuringRunTT->AddTool( &*GetDlgItem(IDC_CHECK_MISCELLANEOUS_ADJUST), _T("meshruntimeadjustmentenabled"));
		m_adjustStockMeshQualityDuringRunTT->Activate(TRUE);
	}

	m_adjustStockMeshQualityDuringRunSliderTT = new CToolTipCtrl;
	if( !m_adjustStockMeshQualityDuringRunSliderTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_adjustStockMeshQualityDuringRunSliderTT->AddTool( &*GetDlgItem(IDC_SLIDER_MISCELLANEOUS), _T("meshruntimeadjustmentlevel"));
		m_adjustStockMeshQualityDuringRunSliderTT->Activate(TRUE);
	}

	m_autoAdjustPrecisionTT = new CToolTipCtrl;
	if( !m_autoAdjustPrecisionTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_autoAdjustPrecisionTT->AddTool( &*GetDlgItem(IDC_CHECK_AUTO_ADJUST_PRECISION), _T("precautoadjustmentenable"));
		m_autoAdjustPrecisionTT->Activate(TRUE);
	}

	m_autoQualityDrawTT = new CToolTipCtrl;
	if( !m_autoQualityDrawTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_autoQualityDrawTT->AddTool( &*GetDlgItem(IDC_CHECK_AUTO_QUALITY_DRAW), _T("autoQualityDrawEnable"));
		m_autoQualityDrawTT->Activate(TRUE);
	}

	m_makeGaugeReport3TT = new CToolTipCtrl;
	if( !m_makeGaugeReport3TT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_makeGaugeReport3TT->AddTool( &*GetDlgItem(IDC_CHECK_MAKE_GOUGE_REPORT_3AX), _T("enableGetGougesAfterFFin3X"));
		m_makeGaugeReport3TT->Activate(TRUE);
	}

	m_makeGaugeReport32TT = new CToolTipCtrl;
	if( !m_makeGaugeReport32TT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_makeGaugeReport32TT->AddTool( &*GetDlgItem(IDC_CHECK_MAKE_GOUGE_REPORT_5AX), _T("enableGetGougesAfterFFin3+2X"));
		m_makeGaugeReport32TT->Activate(TRUE);
	}

	m_3axMinTT = new CToolTipCtrl;
	if( !m_3axMinTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_3axMinTT->AddTool( &*GetDlgItem(IDC_EDT_3_AXIS_MIN), _T("minsamples"));
		m_3axMinTT->AddTool( &*GetDlgItem(IDC_EDT_3_AXIS_MIN_MAN), _T("minsamples"));
		m_3axMinTT->Activate(TRUE);
	}

	m_3axMedTT = new CToolTipCtrl;
	if( !m_3axMedTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_3axMedTT->AddTool( &*GetDlgItem(IDC_EDT_3_AXIS_MED), _T("medsamples"));
		m_3axMedTT->AddTool( &*GetDlgItem(IDC_EDT_3_AXIS_MED_MAN), _T("medsamples"));
		m_3axMedTT->Activate(TRUE);
	}

	m_3axMaxTT = new CToolTipCtrl;
	if( !m_3axMaxTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_3axMaxTT->AddTool( &*GetDlgItem(IDC_EDT_3_AXIS_MAX), _T("maxsamples"));
		m_3axMaxTT->AddTool( &*GetDlgItem(IDC_EDT_3_AXIS_MAX_MAN), _T("maxsamples"));
		m_3axMaxTT->Activate(TRUE);
	}

	m_32axMinTT = new CToolTipCtrl;
	if( !m_32axMinTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_32axMinTT->AddTool( &*GetDlgItem(IDC_EDT_5_AXIS_MIN), _T("minsamples3+2axisDM"));
		m_32axMinTT->AddTool( &*GetDlgItem(IDC_EDT_5_AXIS_MIN_MAN), _T("minsamples3+2axisDM"));
		m_32axMinTT->Activate(TRUE);
	}

	m_32axMedTT = new CToolTipCtrl;
	if( !m_32axMedTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_32axMedTT->AddTool( &*GetDlgItem(IDC_EDT_5_AXIS_MED), _T("medsamples3+2axisDM"));
		m_32axMedTT->AddTool( &*GetDlgItem(IDC_EDT_5_AXIS_MED_MAN), _T("medsamples3+2axisDM"));
		m_32axMedTT->Activate(TRUE);
	}

	m_32axMaxTT = new CToolTipCtrl;
	if( !m_32axMaxTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_32axMaxTT->AddTool( &*GetDlgItem(IDC_EDT_5_AXIS_MAX), _T("maxsamples3+2axisDM"));
		m_32axMaxTT->AddTool( &*GetDlgItem(IDC_EDT_5_AXIS_MAX_MAN), _T("maxsamples3+2axisDM"));
		m_32axMaxTT->Activate(TRUE);
	}

	m_gougesTT = new CToolTipCtrl;
	if( !m_gougesTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_gougesTT->AddTool( &*GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_GOUGES), _T("maxNoOfDisplayedGouges"));
		m_gougesTT->AddTool( &*GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_GOUGES), _T("maxNoOfDisplayedGouges"));
		m_gougesTT->AddTool( &*GetDlgItem(IDC_RADIO_SHOW_ALL_GOUGES), _T("maxNoOfDisplayedGouges"));
		m_gougesTT->Activate(TRUE);
	}

	m_excessesTT = new CToolTipCtrl;
	if( !m_excessesTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_excessesTT->AddTool( &*GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_EXCESSES), _T("maxNoOfDisplayedExcesses"));
		m_excessesTT->AddTool( &*GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_EXCESSES), _T("maxNoOfDisplayedExcesses"));
		m_excessesTT->AddTool( &*GetDlgItem(IDC_RADIO_SHOW_ALL_EXCESSES), _T("maxNoOfDisplayedExcesses"));
		m_excessesTT->Activate(TRUE);
	}
	
	m_enableVerifierTT = new CToolTipCtrl;
	if (!m_enableVerifierTT->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_enableVerifierTT->AddTool(&*GetDlgItem(IDC_CHECK_ENABLE_MATERIAL_REMOVAL_MODE), _T("enableVerifier"));
		m_enableVerifierTT->Activate(TRUE);
	}
}
//#############################################################################
void mwVerifierPP::DoDataExchange(CDataExchange* pDX)
{
	if( pDX->m_bSaveAndValidate == NULL )
	{
		GetDlgParams(*pDX);		
		//UpdateControls();
		SetTexts();
		((CButton*)GetDlgItem(IDC_RADIO_DISCRETE_COLLISION))->SetCheck(BST_CHECKED); // for now, untill this future will be suported...
	}

	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX,IDC_EDT_CUTSIM_RESOURCE_FILE, m_verifierResourceFileEDTBox);

	DDX_Check(pDX, IDC_CHECK_MISCELLANEOUS_ADJUST, m_adjustStockMeshQualityDuringRun);
	DDX_Slider(pDX, IDC_SLIDER_MISCELLANEOUS, m_adjustStockMeshSlider);
	DDV_MinMaxInt(pDX, m_adjustStockMeshSlider, 1, 5);

	DDX_Check(pDX, IDC_CHECK_AUTO_ADJUST_PRECISION, m_autoAdjustPrecision);
	DDX_Check(pDX, IDC_CHECK_AUTO_QUALITY_DRAW, m_autoQualityDraw);

	DDX_Text(pDX, IDC_EDT_3_AXIS_MIN, m_min3);
	DDX_Text(pDX, IDC_EDT_3_AXIS_MED, m_med3);
	DDX_Text(pDX, IDC_EDT_3_AXIS_MAX, m_max3);

	DDX_Text(pDX, IDC_EDT_5_AXIS_MIN, m_min32);
	DDX_Text(pDX, IDC_EDT_5_AXIS_MED, m_med32);
	DDX_Text(pDX, IDC_EDT_5_AXIS_MAX, m_max32);

	DDX_Text(pDX, IDC_EDT_3_AXIS_MIN_MAN, m_min3Man);
	
	DDX_Text(pDX, IDC_EDT_3_AXIS_MED_MAN, m_med3Man);
	
	DDX_Text(pDX, IDC_EDT_3_AXIS_MAX_MAN, m_max3Man);
	
	if(m_3AxisDataModMan)
	{
		DDV_MinMaxInt( pDX, m_min3Man, 1, 99999 );
		DDV_MinMaxInt( pDX, m_med3Man, 1, 99999 );
		DDV_MinMaxInt( pDX, m_max3Man, 1, 99999 );
	}

	DDX_Text(pDX, IDC_EDT_5_AXIS_MIN_MAN, m_min32Man);
	
	DDX_Text(pDX, IDC_EDT_5_AXIS_MED_MAN, m_med32Man);
	
	DDX_Text(pDX, IDC_EDT_5_AXIS_MAX_MAN, m_max32Man);
	

	if(m_32AxisDataModMan)
	{
		DDV_MinMaxInt( pDX, m_min32Man, 1, 99999 );
		DDV_MinMaxInt( pDX, m_med32Man, 1, 99999 );
		DDV_MinMaxInt( pDX, m_max32Man, 1, 99999 );
	}

	DDX_Slider(pDX, IDC_SLIDER_3_AXIS, m_3axSlider);
	DDV_MinMaxInt(pDX, m_3axSlider, 1, 9);

	DDX_Slider(pDX, IDC_SLIDER_5_AXIS, m_32axSlider);
	DDV_MinMaxInt(pDX, m_32axSlider, 1, 5);

	DDX_Check(pDX, IDC_CHECK_SURFACES, m_includeAlsoCheckSurfaces);
	DDX_CBIndex(pDX, IDC_COMBO_CHOICE, m_choice);
	DDX_Text(pDX, IDC_EDT_X, m_offsettingX) ;
	DDV_MinMaxDouble( pDX, m_offsettingX, 0, 99999.0 );
	DDX_Text(pDX, IDC_EDT_Y, m_offsettingY) ;
	DDV_MinMaxDouble( pDX, m_offsettingY, 0, 99999.0 );
	DDX_Text(pDX, IDC_EDT_Z, m_offsettingZ) ;
	DDV_MinMaxDouble( pDX, m_offsettingZ, 0, 99999.0 );
	DDX_Text(pDX, IDC_EDT_RADIUS, m_offsettingRadius) ;
	DDV_MinMaxDouble( pDX, m_offsettingRadius, 0, 99999.0 );
	DDX_Text(pDX, IDC_EDT_LENGTH, m_offsettingLength) ;
	DDV_MinMaxDouble( pDX, m_offsettingLength, 0, 99999.0 );
	DDX_Text(pDX, IDC_EDT_OFFSET_VALUE, m_offsettingValue) ;
	DDV_MinMaxDouble( pDX, m_offsettingValue, static_cast<double>( mathdef::MW_MATH_TOL_FLOAT ), 99999.0 );
	DDX_Check(pDX, IDC_CHECK_CYLINDER_THROUGH_ORIGIN, m_cylinderAxisGoesThroghOrigin);

	DDX_Text(pDX, IDC_EDT_SHOW_ONLY_BIGGEST_GOUGES, m_gouges);
	DDV_MinMaxInt( pDX, m_gouges, 0, 100 );
	DDX_Text(pDX, IDC_EDT_SHOW_ONLY_BIGGEST_EXCESSES, m_excesses);
	DDV_MinMaxInt( pDX, m_excesses, 0, 100 );

	DDX_Check(pDX, IDC_CHECK_MAKE_GOUGE_REPORT_3AX, m_makeGaugeReport3);
	DDX_Check(pDX, IDC_CHECK_MAKE_GOUGE_REPORT_5AX, m_makeGaugeReport32);
	DDX_Check(pDX, IDC_CHECK_ENABLE_MATERIAL_REMOVAL_MODE, m_enableVerifier);

	if( pDX->m_bSaveAndValidate )
	{
		SetDlgParams(*pDX);
	}
}
//#############################################################################
BEGIN_MESSAGE_MAP(mwVerifierPP, CPropertyPage)
	ON_WM_HELPINFO()
	ON_WM_CTLCOLOR()
	
	ON_BN_CLICKED(IDC_CHECK_MISCELLANEOUS_ADJUST, OnClickedButMiscellaneousAdjust)

	ON_CBN_SELCHANGE(IDC_COMBO_CHOICE, OnSelchangeComboChoice)
	ON_BN_CLICKED(IDC_BTN_LOAD, OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_RADIO_X, OnRadioAroundX)
	ON_BN_CLICKED(IDC_RADIO_Y, OnRadioAroundY)
	ON_BN_CLICKED(IDC_RADIO_Z, OnRadioAroundZ)
	ON_BN_CLICKED(MSG_CHECK_SURFACES, OnClikedButIncludeAlsoCheckSurfaces)
	ON_BN_CLICKED(IDC_CHECK_CYLINDER_THROUGH_ORIGIN, OnClikedButCylinderThroughOrigin)

	ON_BN_CLICKED(IDC_RADIO_DEFAULT, OnRadioDefault)
	ON_BN_CLICKED(IDC_RADIO_ADJUST_SAMPLES, OnRadioAdjustSamplesByStockDim)
	ON_BN_CLICKED(IDC_RADIO_ADJUST_SAMPLES_MANUALLY, OnRadioAdjustSamlesManually)

	ON_BN_CLICKED(IDC_CHECK_3_AXIS_DATA_MODEL, OnClickedBut3AxisDataMod)
	ON_BN_CLICKED(IDC_CHECK_5_AXIS_DATA_MODEL, OnClickedBut32AxisDataMod)
	ON_BN_CLICKED(IDC_CHECK_3_AXIS_DATA_MODEL_MAN, OnClickedBut3AxisDataModMan)
	ON_BN_CLICKED(IDC_CHECK_5_AXIS_DATA_MODEL_MAN, OnClickedBut32AxisDataModMan)

	ON_BN_CLICKED(IDC_BUT_RESET_3_AXIS, OnClickedButReset3)
	ON_BN_CLICKED(IDC_BUT_RESET_5_AXIS, OnClickedButReset32)

	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_3_AXIS, &mwVerifierPP::OnNMReleasedcapture3axSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_5_AXIS, &mwVerifierPP::OnNMReleasedcapture32axSlider)

	ON_BN_CLICKED(IDC_RADIO_SHOW_ONLY_BIGGEST_GOUGES, OnClickedButShowOnlyBiggestGauges)
	ON_BN_CLICKED(IDC_RADIO_SHOW_ONLY_BIGGEST_EXCESSES, OnClickedButShowOnlyBiggestExcesses)
	ON_BN_CLICKED(IDC_RADIO_SHOW_ALL_GOUGES, OnClickedButShowAllGauges)
	ON_BN_CLICKED(IDC_RADIO_SHOW_ALL_EXCESSES, OnClickedButShowAllExcesses)

	ON_EN_CHANGE(IDC_EDT_X, &mwVerifierPP::OnEnChangeEditX)
	ON_EN_CHANGE(IDC_EDT_Y, &mwVerifierPP::OnEnChangeEditY)
	ON_EN_CHANGE(IDC_EDT_Z, &mwVerifierPP::OnEnChangeEditZ)
	ON_EN_CHANGE(IDC_EDT_OFFSET_VALUE, &mwVerifierPP::OnEnChangeEditOffset)
	ON_EN_CHANGE(IDC_EDT_RADIUS, &mwVerifierPP::OnEnChangeEditRadius)
	ON_EN_CHANGE(IDC_EDT_LENGTH, &mwVerifierPP::OnEnChangeEditLength)

	ON_BN_CLICKED(IDC_BUT_CUTSIM_RESOURCE_FILE, OnClickedButCutsimResourceFile)
	ON_BN_CLICKED(IDC_CHECK_ENABLE_MATERIAL_REMOVAL_MODE, OnClickedEnableMaterialRemovalMode)
END_MESSAGE_MAP()
//#############################################################################
// mwVerifierPP message handlers
BOOL mwVerifierPP::OnHelpInfo(HELPINFO* pHelpInfo)
{
	// TODO: Add your message handler code here and/or call default
	if (pHelpInfo==NULL)
		AfxGetApp()->WinHelp(IDD)	;
	else
		AfxGetApp()->WinHelp(pHelpInfo->iCtrlId, HELP_CONTEXTPOPUP);
	return TRUE;	
}
//#############################################################################
BOOL mwVerifierPP::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	// Check for enter key (return) press.
	if(m_adjustStockMeshQualityDuringRunTT)
		m_adjustStockMeshQualityDuringRunTT->RelayEvent(pMsg);
	if(m_adjustStockMeshQualityDuringRunSliderTT)
		m_adjustStockMeshQualityDuringRunSliderTT->RelayEvent(pMsg);
	if(m_makeGaugeReport3TT)
		m_makeGaugeReport3TT->RelayEvent(pMsg);
	if(m_makeGaugeReport32TT)
		m_makeGaugeReport32TT->RelayEvent(pMsg);
	if(m_autoAdjustPrecisionTT)
		m_autoAdjustPrecisionTT->RelayEvent(pMsg);
	if(m_autoQualityDrawTT)
		m_autoQualityDrawTT->RelayEvent(pMsg);
	if(m_3axMinTT)
		m_3axMinTT->RelayEvent(pMsg);
	if(m_3axMedTT)
		m_3axMedTT->RelayEvent(pMsg);
	if(m_3axMaxTT)
		m_3axMaxTT->RelayEvent(pMsg);
	if(m_32axMinTT)
		m_32axMinTT->RelayEvent(pMsg);
	if(m_32axMedTT)
		m_32axMedTT->RelayEvent(pMsg);
	if(m_32axMaxTT)
		m_32axMaxTT->RelayEvent(pMsg);
	if(m_gougesTT)
		m_gougesTT->RelayEvent(pMsg);
	if(m_excessesTT)
		m_excessesTT->RelayEvent(pMsg);
	if(m_verifierResourceFileTT)
		m_verifierResourceFileTT->RelayEvent(pMsg);

	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		CPropertyPage::NextDlgCtrl(); // Pass focus to next control
		return TRUE;			// Don't translate further
	}else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		static_cast <CPropertySheet *> (CPropertyPage::GetParent())->EndDialog(IDCANCEL);
		return TRUE;
	}
	else
		return CPropertyPage::PreTranslateMessage(pMsg);
}
//#############################################################################
HBRUSH mwVerifierPP::OnCtlColor(CDC* pDC, CWnd *pWnd, UINT nCtlColor)
{
	if(nCtlColor == CTLCOLOR_STATIC && pWnd->GetSafeHwnd() == GetDlgItem(IDC_LBL_INFO)->GetSafeHwnd())
	{
		pDC->SetTextColor(RGB(255, 0, 0));
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	}
	else
	{
		return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	}
}
//#############################################################################
void mwVerifierPP::SetTexts() 
{
	SetCntrlText(IDC_GRPBOX_MISCELLANEOUS, MSG_GRPBOX_MISCELLANEOUS);
	SetCntrlText(IDC_CHECK_MISCELLANEOUS_ADJUST, MSG_CHECK_MISCELLANEOUS_ADJUST);
	SetCntrlText(IDC_LBL_HIGH, MSG_LBL_HIGH);
	SetCntrlText(IDC_LBL_LOW, MSG_LBL_LOW);

	SetCntrlText(IDC_GRPBOX_SAMPLES, MSG_GRPBOX_SAMPLES);
	SetCntrlText(IDC_CHECK_AUTO_ADJUST_PRECISION, MSG_CHECK_AUTO_ADJUST_PRECISION);
	SetCntrlText(IDC_CHECK_AUTO_QUALITY_DRAW, MSG_CHECK_AUTO_QUALITY_DRAW);
	
	SetCntrlText(IDC_LBL_INFO, MSG_LBL_INFO);
	SetCntrlText(IDC_RADIO_DEFAULT, MSG_RADIO_DEFAULT);
	SetCntrlText(IDC_RADIO_ADJUST_SAMPLES, MSG_RADIO_ADJUST_SAMPLES);
	SetCntrlText(IDC_RADIO_ADJUST_SAMPLES_MANUALLY, MSG_RADIO_ADJUST_SAMPLES_MANUALLY);

	if( m_extendedStartUpParams.GetUnitsPtr().IsNotNull() )
	{
		if(*m_extendedStartUpParams.GetUnitsPtr() == measures::mwUnitsFactory::METRIC)
		{
			SetCntrlText(IDC_LBL_3_AX_SAMPLES_MM, MSG_LBL_SAMPLES_MM);
			SetCntrlText(IDC_LBL_5_AX_SAMPLES_MM, MSG_LBL_SAMPLES_MM);
		}
		else
		{
			SetCntrlText(IDC_LBL_3_AX_SAMPLES_MM, MSG_LBL_SAMPLES_INCH);
			SetCntrlText(IDC_LBL_5_AX_SAMPLES_MM, MSG_LBL_SAMPLES_INCH);
		}
	}
	else
	{
		SetCntrlText(IDC_LBL_3_AX_SAMPLES_MM, MSG_LBL_SAMPLES_UNKNOWN);
		SetCntrlText(IDC_LBL_5_AX_SAMPLES_MM, MSG_LBL_SAMPLES_UNKNOWN);
	}


	SetCntrlText(IDC_BUT_RESET_3_AXIS, MAG_BUT_RESET_3_AXIS);
	SetCntrlText(IDC_CHECK_3_AXIS_DATA_MODEL, MSG_CHECK_3_AXIS_DATA_MODEL);

	SetCntrlText(IDC_LBL_3_AXIS_MIN, MSG_LBL_3_AXIS_MIN);
	SetCntrlText(IDC_LBL_3_AXIS_MED, MSG_LBL_3_AXIS_MED);
	SetCntrlText(IDC_LBL_3_AXIS_MAX, MSG_LBL_3_AXIS_MAX);
	SetCntrlText(IDC_CHECK_3_AXIS_DATA_MODEL_MAN, MSG_CHECK_3_AXIS_DATA_MODEL_MAN);
	SetCntrlText(IDC_LBL_3_AXIS_MIN_MAN, MSG_LBL_3_AXIS_MIN_MAN);
	SetCntrlText(IDC_LBL_3_AXIS_MED_MAN, MSG_LBL_3_AXIS_MED_MAN);
	SetCntrlText(IDC_LBL_3_AXIS_MAX_MAN, MSG_LBL_3_AXIS_MAX_MAN);
                                       
	SetCntrlText(IDC_BUT_RESET_5_AXIS, MSG_BUT_RESET_5_AXIS);
	SetCntrlText(IDC_CHECK_5_AXIS_DATA_MODEL, MSG_CHECK_5_AXIS_DATA_MODEL);

	SetCntrlText(IDC_LBL_5_AXIS_MIN, MSG_LBL_5_AXIS_MIN);
	SetCntrlText(IDC_LBL_5_AXIS_MED, MSG_LBL_5_AXIS_MED);
	SetCntrlText(IDC_LBL_5_AXIS_MAX, MSG_LBL_5_AXIS_MAX);
	SetCntrlText(IDC_CHECK_5_AXIS_DATA_MODEL_MAN, MSG_CHECK_5_AXIS_DATA_MODEL_MAN);
	SetCntrlText(IDC_LBL_5_AXIS_MIN_MAN, MSG_LBL_5_AXIS_MIN_MAN);
	SetCntrlText(IDC_LBL_5_AXIS_MED_MAN, MSG_LBL_5_AXIS_MED_MAN);
	SetCntrlText(IDC_LBL_5_AXIS_MAX_MAN, MSG_LBL_5_AXIS_MAX_MAN);

	SetCntrlText(IDC_GRPBOX_MESH_COLLISIONS, MSG_GRPBOX_MESH_COLLISIONS);
	SetCntrlText(IDC_CHECK_CUTSIM_MESH_VS_STOCK, MSG_CHECK_CUTSIM_MESH_VS_STOCK);
	SetCntrlText(IDC_RADIO_DISCRETE_COLLISION, MSG_RADIO_DISCRETE_COLLISION);
	SetCntrlText(IDC_RADIO_CONTINOUS_COLLISION, MSG_RADIO_CONTINOUS_COLLISION);

	SetCntrlText(IDC_GRPBOX_STOCK_MATERIAL, MSG_GRPBOX_STOCK_MATERIAL);
	SetCntrlText(IDC_LBL_CHOICES, MSG_LBL_CHOICES);
	CComboBox* cmb = (CComboBox*) GetDlgItem(IDC_COMBO_CHOICE);
	cmb->ResetContent();
	if (!m_stockParam.IsMXP())
	{
		if (m_stockParam.IsStock())
		{
			cmb->AddString( GetText(MSG_STOCK_COMBO_C1_YES).c_str() );
		}
		cmb->AddString( GetText(MSG_STOCK_COMBO_C2).c_str() );
		cmb->AddString( GetText(MSG_STOCK_COMBO_C3).c_str() );
		cmb->AddString( GetText(MSG_STOCK_COMBO_C4).c_str() );
	}
	cmb->AddString( GetText(MSG_STOCK_COMBO_C5).c_str() );
	SetCntrlText(IDC_BTN_LOAD, MSG_BTN_LOAD);
	SetCntrlText(IDC_CHECK_SURFACES, MSG_CHECK_SURFACES);
	SetCntrlText(IDC_GRPBOX_OFFSET, MSG_GRPBOX_OFFSET);
	SetCntrlText(IDC_LBL_OFFSET_VALUE, MSG_LBL_OFFSET_VALUE);
	SetCntrlText(IDC_LBL_LENGTH, MSG_LBL_LENGTH);
	SetCntrlText(IDC_LBL_RADIUS, MSG_LBL_RADIUS);
	SetCntrlText(IDC_LBL_X, MSG_LBL_X);
	SetCntrlText(IDC_LBL_Y, MSG_LBL_Y);
	SetCntrlText(IDC_LBL_Z, MSG_LBL_Z);
	SetCntrlText(IDC_GRPBOX_AROUND, MSG_GRPBOX_AROUND);
	SetCntrlText(IDC_CHECK_CYLINDER_THROUGH_ORIGIN, MSG_CHECK_CYLINDER_THROUGH_ORIGIN);
	SetCntrlText(IDC_RADIO_X, MSG_LBL_X);
	SetCntrlText(IDC_RADIO_Y, MSG_LBL_Y);
	SetCntrlText(IDC_RADIO_Z, MSG_LBL_Z);

	SetCntrlText(IDC_GRPBOX_GOUGE_AND_EXCESS, MSG_GRPBOX_GOUGE_AND_EXCESS);
	SetCntrlText(IDC_RADIO_SHOW_ONLY_BIGGEST_GOUGES, MSG_RADIO_SHOW_ONLY_BIGGEST_GOUGES);
	SetCntrlText(IDC_RADIO_SHOW_ONLY_BIGGEST_EXCESSES, MSG_RADIO_SHOW_ONLY_BIGGEST_EXCESSES);
	SetCntrlText(IDC_LBL_GOUGES, MSG_LBL_GOUGES);
	SetCntrlText(IDC_LBL_EXCESSES, MSG_LBL_EXCESSES);
	SetCntrlText(IDC_RADIO_SHOW_ALL_GOUGES, MSG_RADIO_SHOW_ALL_GOUGES);
	SetCntrlText(IDC_RADIO_SHOW_ALL_EXCESSES, MSG_RADIO_SHOW_ALL_EXCESSES);
	SetCntrlText(IDC_CHECK_MAKE_GOUGE_REPORT_3AX, MSG_CHECK_MAKE_GOUGE_REPORT_3AX);
	SetCntrlText(IDC_CHECK_MAKE_GOUGE_REPORT_5AX, MSG_CHECK_MAKE_GOUGE_REPORT_5AX);
	SetCntrlText(IDC_CHECK_ENABLE_MATERIAL_REMOVAL_MODE, MSG_CHECK_ENABLE_MATERIAL_REMOVAL_MODE);
}
//#############################################################################
void mwVerifierPP::GetParams() 
{
	//////////Miscellaneous
	m_adjustStockMeshQualityDuringRun = m_extendedStartUpParams.IsMeshRuntimeAdjustmentEnabled();
	m_adjustStockMeshSlider = m_extendedStartUpParams.GetMeshRuntimeAdjustmentLevel();
	((CSliderCtrl*)GetDlgItem(IDC_SLIDER_MISCELLANEOUS))->SetRange(1, 5);
	//////////Samples
	m_autoAdjustPrecision = m_machsimVerificationParams.IsAutoAdjustPrecisionEnabled();
	m_autoQualityDraw = m_machsimVerificationParams.IsAutoQualityDrawEnabled();

	m_min3 = m_min3Man = m_machsimVerificationParams.GetAccuracySizeMin3();
	m_med3 = m_med3Man = m_machsimVerificationParams.GetAccuracySizeMed3();
	m_max3 = m_max3Man = m_machsimVerificationParams.GetAccuracySizeMax3();

	m_min32 = m_min32Man = m_machsimVerificationParams.GetAccuracySizeMin32();
	m_med32 = m_med32Man = m_machsimVerificationParams.GetAccuracySizeMed32();
	m_max32 = m_max32Man = m_machsimVerificationParams.GetAccuracySizeMax32();

	if(m_min3 != mwMachsimVerificationParams::GetDefaultAccuracySizeMin3() ||
		m_med3 != mwMachsimVerificationParams::GetDefaultAccuracySizeMed3() || 
		m_max3 != mwMachsimVerificationParams::GetDefaultAccuracySizeMax3() ||
		m_min32 != mwMachsimVerificationParams::GetDefaultAccuracySizeMin32() ||
		m_med32 != mwMachsimVerificationParams::GetDefaultAccuracySizeMed32() ||
		m_max32 != mwMachsimVerificationParams::GetDefaultAccuracySizeMax32())
	{
		m_samples = ADJUST_SAMPLES_MANUALLY;
	}

	((CSliderCtrl*)GetDlgItem(IDC_SLIDER_3_AXIS))->SetRange(1, 9);

	((CSliderCtrl*)GetDlgItem(IDC_SLIDER_5_AXIS))->SetRange(1, 5);

	//////////Stock Material Selection
	m_includeAlsoCheckSurfaces = m_stockParam.GetCheckSurfaces();
	m_choice = static_cast<int>(m_stockParam.GetChoice());
	if(m_choice == -1)
	{
		m_choice = 0;
	}
	else
		if(m_stockParam.IsMXP())
		{
			m_choice -= 4;
		}
		else
			if(!m_stockParam.IsStock())
			{
				m_choice -= 1;
			}
	m_offsettingX = m_stockParam.GetX();
	m_offsettingY = m_stockParam.GetY();
	m_offsettingZ = m_stockParam.GetZ();
	m_offsettingRadius = m_stockParam.GetRadius();
	m_offsettingLength = m_stockParam.GetLength();
	m_offsettingValue = m_stockParam.GetOffsetValue();
	m_cylinderAxisGoesThroghOrigin = m_stockParam.IsThroughOrigin();
	m_around = m_stockParam.GetAroundAxis();
	//////////Gouge & Excess
	m_gouges = m_machsimVerificationParams.GetMaxNoOfGougesToDisplay();
	if(m_gouges == 0)
	{
		m_showAllGouges = TRUE;
	}
	else
	{
		m_showAllGouges = FALSE;
	}

	m_excesses = m_machsimVerificationParams.GetMaxNoOfExcessesToDisplay();
	if(m_excesses == 0)
	{
		m_showAllExcesses = TRUE;
	}
	else
	{
		m_showAllExcesses = FALSE;
	}
	m_makeGaugeReport3 = m_machsimVerificationParams.GetEnableGetGougesAfterFFin3X();
	m_makeGaugeReport32 = m_machsimVerificationParams.GetEnableGetGougesAfterFFin32X();
	//////////
	AdjustSlidersPosAndTexts(m_stockParam);

	EnableMiscellaneous();
	EnableSamples();
	EnableMeshCollisions();
	EnableStockMaterialSelection();
	EnableGougeAndExcess();

	m_verifierResourceFile = m_extendedStartUpParams.GetVerifierGUIResourceFileName();
	GetDlgItem(IDC_EDT_CUTSIM_RESOURCE_FILE)->SetWindowText(m_verifierResourceFile.c_str());
}
//#############################################################################
void mwVerifierPP::SetParams()
{
	//////////Miscellaneous
	m_extendedStartUpParams.SetMeshRuntimeAdjustmentEnabled(m_adjustStockMeshQualityDuringRun ? true : false);
	m_extendedStartUpParams.SetMeshRuntimeAdjustmentLevel(static_cast<unsigned int>(m_adjustStockMeshSlider));
	//////////Samples
	m_machsimVerificationParams.SetAutoAdjustPrecisionEnable(m_autoAdjustPrecision ? true : false);
	m_machsimVerificationParams.SetAutoQualityDrawEnabled(m_autoQualityDraw ? true : false);

	if(m_samples == ADJUST_SAMPLES_BY_STOCK || m_3axisResetPush || m_32axisResetPush)
	{
		if(m_3AxisDataMod || m_3axisResetPush)
		{
			m_machsimVerificationParams.SetAccuracySizeMin3(static_cast<int>(m_min3));
			m_machsimVerificationParams.SetAccuracySizeMed3(static_cast<int>(m_med3));
			m_machsimVerificationParams.SetAccuracySizeMax3(static_cast<int>(m_max3));
		}

		if(m_32AxisDataMod || m_32axisResetPush)
		{
			m_machsimVerificationParams.SetAccuracySizeMin32(static_cast<int>(m_min32));
			m_machsimVerificationParams.SetAccuracySizeMed32(static_cast<int>(m_med32));
			m_machsimVerificationParams.SetAccuracySizeMax32(static_cast<int>(m_max32));
		}
		
	}
	else
		if(m_samples == ADJUST_SAMPLES_MANUALLY || m_3axisResetPush || m_32axisResetPush)
		{
			if(m_3AxisDataModMan || m_3axisResetPush)
			{
				m_machsimVerificationParams.SetAccuracySizeMin3(static_cast<int>(m_min3Man));
				m_machsimVerificationParams.SetAccuracySizeMed3(static_cast<int>(m_med3Man));
				m_machsimVerificationParams.SetAccuracySizeMax3(static_cast<int>(m_max3Man));
			}
			
			if(m_32AxisDataModMan || m_32axisResetPush)
			{
				m_machsimVerificationParams.SetAccuracySizeMin32(static_cast<int>(m_min32Man));
				m_machsimVerificationParams.SetAccuracySizeMed32(static_cast<int>(m_med32Man));
				m_machsimVerificationParams.SetAccuracySizeMax32(static_cast<int>(m_max32Man));
			}
		}
	//////////Stock Material Selection
	m_stockParam.SetCheckSurfaces(m_includeAlsoCheckSurfaces ? true : false);
	m_stockParam.SetChoice(ConvertCheckBoxToChoice(m_choice));
	m_stockParam.SetX(m_offsettingX);
	m_stockParam.SetY(m_offsettingY);
	m_stockParam.SetZ(m_offsettingZ);
	m_stockParam.SetRadius(m_offsettingRadius);
	m_stockParam.SetLength(m_offsettingLength);
	m_stockParam.SetOffsetValue(m_offsettingValue);
	m_stockParam.SetThroughOrigin(m_cylinderAxisGoesThroghOrigin ? true : false);
	m_stockParam.SetAroundAxis(m_around);
	//////////Gouge & Excess
	m_machsimVerificationParams.SetMaxNoOfGougesToDisplay(m_gouges);
	m_machsimVerificationParams.SetMaxNoOfExcessesToDisplay(m_excesses);

	m_machsimVerificationParams.SetEnableGetGougesAfterFFin3X(m_makeGaugeReport3 ? true : false);
	m_machsimVerificationParams.SetEnableGetGougesAfterFFin32X(m_makeGaugeReport32 ? true : false);
	//////////

	TCHAR tmpPath[MAX_PATH];

	GetDlgItem(IDC_EDT_CUTSIM_RESOURCE_FILE)->GetWindowText(tmpPath, MAX_PATH);
	m_verifierResourceFile = tmpPath;
	if(!m_verifierResourceFile.empty() && !misc::mwFileSystem::FileExists(m_verifierResourceFile))
	{
		throw mwMachSimParamsUIException(mwMachSimParamsUIException::VERIFIER_GUI_PATH_MISSING, _T("mwMachSimPP::SetParams()"));
	}
	m_extendedStartUpParams.SetVerifierGUIResourceFileName(m_verifierResourceFile);
}
//#############################################################################
const mwStockParam::choice mwVerifierPP::ConvertCheckBoxToChoice(int choice)
{
	if(m_stockParam.IsMXP())
	{
		choice += 4;
	}
	else
	{
		if(!m_stockParam.IsStock())
		{
			choice += 1;
		}
	}
	return static_cast<mwStockParam::choice>(choice);
}

//#############################################################################
void mwVerifierPP::EnableMiscellaneous()
{
	GetDlgItem(IDC_CHECK_MISCELLANEOUS_ADJUST)->EnableWindow(TRUE);
	GetDlgItem(IDC_SLIDER_MISCELLANEOUS)->EnableWindow(m_adjustStockMeshQualityDuringRun);
	GetDlgItem(IDC_LBL_HEIGH)->EnableWindow(m_adjustStockMeshQualityDuringRun);
	GetDlgItem(IDC_LBL_LOW)->EnableWindow(m_adjustStockMeshQualityDuringRun);
}

//#############################################################################
void mwVerifierPP::EnableStockMaterialSelection()
{
	int choice = m_choice;
	if(m_stockParam.IsMXP())
	{
		choice += 4;
	}
	else
	{
		if(!m_stockParam.IsStock())
		{
			choice += 1;
		}
	}
	//  STOCK_USED_FOR_CALC_TOOLPATH = 0,
	//  BOUNDING_BOX_FOR_DRIVE_SURFACE = 1,
	//	BOUNDING_CYLINDER_FOR_DRIVE_SURFACE = 2,
	//	THE_PART_OFFSET = 3,
	//	STL_FILE = 4

	GetDlgItem(IDC_LBL_CHOICES)->EnableWindow(TRUE);
	GetDlgItem(IDC_COMBO_CHOICE)->EnableWindow(TRUE);
	GetDlgItem(IDC_BTN_LOAD)->EnableWindow(choice==4);
	GetDlgItem(IDC_CHECK_SURFACES)->EnableWindow( choice==1 || choice==2 || choice==3 );
	GetDlgItem(IDC_LBL_X)->EnableWindow(choice==1);
	GetDlgItem(IDC_EDT_X)->EnableWindow(choice==1);
	GetDlgItem(IDC_LBL_Y)->EnableWindow(choice==1);
	GetDlgItem(IDC_EDT_Y)->EnableWindow(choice==1);
	GetDlgItem(IDC_LBL_Z)->EnableWindow(choice==1);
	GetDlgItem(IDC_EDT_Z)->EnableWindow(choice==1);
	GetDlgItem(IDC_LBL_RADIUS)->EnableWindow(choice==2);
	GetDlgItem(IDC_EDT_RADIUS)->EnableWindow(choice==2);
	GetDlgItem(IDC_LBL_LENGTH)->EnableWindow(choice==2);
	GetDlgItem(IDC_EDT_LENGTH)->EnableWindow(choice==2);
	GetDlgItem(IDC_LBL_OFFSET_VALUE)->EnableWindow(choice==3);
	GetDlgItem(IDC_EDT_OFFSET_VALUE)->EnableWindow(choice==3);
	GetDlgItem(IDC_CHECK_CYLINDER_THROUGH_ORIGIN)->EnableWindow(choice==2);
	GetDlgItem(IDC_RADIO_X)->EnableWindow(choice==2);
	GetDlgItem(IDC_RADIO_Y)->EnableWindow(choice==2);
	GetDlgItem(IDC_RADIO_Z)->EnableWindow(choice==2);

	GetDlgItem(IDC_STATIC_IMAGE_2)->ShowWindow(choice==1);
	GetDlgItem(IDC_STATIC_IMAGE_1)->ShowWindow(choice==2);	

	((CButton*)GetDlgItem(IDC_RADIO_X))->SetCheck(m_around == mwStockParam::X_AXIS ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RADIO_Y))->SetCheck(m_around == mwStockParam::Y_AXIS ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RADIO_Z))->SetCheck(m_around == mwStockParam::Z_AXIS ? BST_CHECKED : BST_UNCHECKED);
}
//#############################################################################
void mwVerifierPP::EnableSamples()
{
	GetDlgItem(IDC_CHECK_AUTO_ADJUST_PRECISION)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_AUTO_QUALITY_DRAW)->EnableWindow(TRUE);

	bool knownUnits = m_extendedStartUpParams.GetUnitsPtr().IsNotNull();
	
	GetDlgItem(IDC_RADIO_DEFAULT)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO_ADJUST_SAMPLES)->EnableWindow(knownUnits);
	GetDlgItem(IDC_RADIO_ADJUST_SAMPLES_MANUALLY)->EnableWindow(TRUE);

	GetDlgItem(IDC_LBL_INFO)->EnableWindow(TRUE);

	GetDlgItem(IDC_BUT_RESET_3_AXIS)->EnableWindow(m_samples == DEFAULT);
	GetDlgItem(IDC_CHECK_3_AXIS_DATA_MODEL)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && knownUnits);
	GetDlgItem(IDC_LBL_3_AX_SAMPLES_MM)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_SCALE_3_AXIS)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_SLIDER_3_AXIS)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_3_AXIS_MIN)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_EDT_3_AXIS_MIN)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_3_AXIS_MED)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_EDT_3_AXIS_MED)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_3_AXIS_MAX)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_EDT_3_AXIS_MAX)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_3AxisDataMod && knownUnits);
	GetDlgItem(IDC_CHECK_3_AXIS_DATA_MODEL_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY);
	GetDlgItem(IDC_LBL_3_AXIS_MIN_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_3AxisDataModMan);
	GetDlgItem(IDC_EDT_3_AXIS_MIN_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_3AxisDataModMan);
	GetDlgItem(IDC_LBL_3_AXIS_MED_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_3AxisDataModMan);
	GetDlgItem(IDC_EDT_3_AXIS_MED_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_3AxisDataModMan);
	GetDlgItem(IDC_LBL_3_AXIS_MAX_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_3AxisDataModMan);
	GetDlgItem(IDC_EDT_3_AXIS_MAX_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_3AxisDataModMan);
	
	GetDlgItem(IDC_BUT_RESET_5_AXIS)->EnableWindow(m_samples == DEFAULT);
	GetDlgItem(IDC_CHECK_5_AXIS_DATA_MODEL)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && knownUnits);
	GetDlgItem(IDC_LBL_5_AX_SAMPLES_MM)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_SCALE_5_AXIS)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_SLIDER_5_AXIS)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_5_AXIS_MIN)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_EDT_5_AXIS_MIN)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_5_AXIS_MED)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_EDT_5_AXIS_MED)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_LBL_5_AXIS_MAX)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_EDT_5_AXIS_MAX)->EnableWindow(m_samples == ADJUST_SAMPLES_BY_STOCK && m_32AxisDataMod && knownUnits);
	GetDlgItem(IDC_CHECK_5_AXIS_DATA_MODEL_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY);
	GetDlgItem(IDC_LBL_5_AXIS_MIN_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_32AxisDataModMan);
	GetDlgItem(IDC_EDT_5_AXIS_MIN_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_32AxisDataModMan);
	GetDlgItem(IDC_LBL_5_AXIS_MED_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_32AxisDataModMan);
	GetDlgItem(IDC_EDT_5_AXIS_MED_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_32AxisDataModMan);
	GetDlgItem(IDC_LBL_5_AXIS_MAX_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_32AxisDataModMan);
	GetDlgItem(IDC_EDT_5_AXIS_MAX_MAN)->EnableWindow(m_samples == ADJUST_SAMPLES_MANUALLY && m_32AxisDataModMan);

	((CButton*)GetDlgItem(IDC_RADIO_DEFAULT))->SetCheck(m_samples == DEFAULT ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RADIO_ADJUST_SAMPLES))->SetCheck(m_samples == ADJUST_SAMPLES_BY_STOCK ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RADIO_ADJUST_SAMPLES_MANUALLY))->SetCheck(m_samples == ADJUST_SAMPLES_MANUALLY ? BST_CHECKED : BST_UNCHECKED);

	((CButton*)GetDlgItem(IDC_CHECK_3_AXIS_DATA_MODEL))->SetCheck(m_3AxisDataMod ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_5_AXIS_DATA_MODEL))->SetCheck(m_32AxisDataMod ? BST_CHECKED : BST_UNCHECKED);

	((CButton*)GetDlgItem(IDC_CHECK_3_AXIS_DATA_MODEL_MAN))->SetCheck(m_3AxisDataModMan ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_CHECK_5_AXIS_DATA_MODEL_MAN))->SetCheck(m_32AxisDataModMan ? BST_CHECKED : BST_UNCHECKED);
}
//#############################################################################
void mwVerifierPP::EnableMeshCollisions()
{
	GetDlgItem(IDC_RADIO_DISCRETE_COLLISION)->EnableWindow(TRUE);
}
//#############################################################################
void mwVerifierPP::EnableGougeAndExcess()
{
	GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_GOUGES)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_GOUGES)->EnableWindow(m_showAllGouges == FALSE);
	GetDlgItem(IDC_LBL_GOUGES)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO_SHOW_ALL_GOUGES)->EnableWindow(TRUE);

	GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_EXCESSES)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_EXCESSES)->EnableWindow(m_showAllExcesses == FALSE);
	GetDlgItem(IDC_LBL_EXCESSES)->EnableWindow(TRUE);
	GetDlgItem(IDC_RADIO_SHOW_ALL_EXCESSES)->EnableWindow(TRUE);

	GetDlgItem(IDC_CHECK_MAKE_GOUGE_REPORT_3AX)->EnableWindow(TRUE);
	GetDlgItem(IDC_CHECK_MAKE_GOUGE_REPORT_5AX)->EnableWindow(TRUE);

	((CButton*)GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_GOUGES))->SetCheck(m_showAllGouges == FALSE ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RADIO_SHOW_ALL_GOUGES))->SetCheck(m_showAllGouges == TRUE ? BST_CHECKED : BST_UNCHECKED);

	((CButton*)GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_EXCESSES))->SetCheck(m_showAllExcesses == FALSE ? BST_CHECKED : BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RADIO_SHOW_ALL_EXCESSES))->SetCheck(m_showAllExcesses == TRUE ? BST_CHECKED : BST_UNCHECKED);
}
//#############################################################################
void mwVerifierPP::OnClickedButCutsimResourceFile()
{
	LPTSTR  tmp = _T("") ;
	// TODO: Add your control notification handler code here
	misc::mwstring filter = m_Interactor.GetMsg( 8, _T("MachSim-Files") );
	filter += _T(" (*.dll)|*.dll| All files (*.*)|*.*||");
	CFileDialog dlg( TRUE, _T("dll"), tmp, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, 
		filter.c_str() );

	if( dlg.DoModal() == IDOK )
	{
		m_verifierResourceFile = dlg.m_ofn.lpstrFile ;
		GetDlgItem(IDC_EDT_CUTSIM_RESOURCE_FILE)->SetWindowText( m_verifierResourceFile.c_str() ) ;
	}
}
//#############################################################################
void mwVerifierPP::OnSelchangeComboChoice()
{
	m_choice = static_cast<CComboBox * >(GetDlgItem(IDC_COMBO_CHOICE))->GetCurSel();

	mwStockParam stockParam = m_stockParam;
	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	UpdateOffsetX(stockParam);
	UpdateOffsetY(stockParam);
	UpdateOffsetZ(stockParam);
	UpdateOffsetRadius(stockParam);
	UpdateOffsetLength(stockParam);
	UpdateOffsetValue(stockParam);

	stockParam.SetCheckSurfaces(m_includeAlsoCheckSurfaces ? true : false);
	stockParam.SetAroundAxis(m_around);
	stockParam.SetThroughOrigin(m_cylinderAxisGoesThroghOrigin ? true : false);

	AdjustSlidersPosAndTexts(stockParam);

	EnableStockMaterialSelection();
}
//#############################################################################
void mwVerifierPP::OnBnClickedButtonLoad()
{
	LPTSTR  mStockPath = _T("") ;
	misc::mwstring filter = GetText( MSG_DLG_LOAD_STL );
	filter += _T(" (*.stl)|*.stl| All files (*.*)|*.*||");
	CFileDialog dlg( TRUE, _T("stl"), mStockPath, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, 
		filter.c_str() );

	if( dlg.DoModal() == IDOK )
	{
		m_stockParam.SetStockFile(dlg.m_ofn.lpstrFile);
		m_stockParam.SetStockFileLoad(true);
	}
	else
	{
		m_stockParam.SetStockFileLoad(false);
	}

	mwStockParam stockParam = m_stockParam;
	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnRadioAroundX()
{
	m_around = mwStockParam::X_AXIS;

	mwStockParam stockParam = m_stockParam;
	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	UpdateOffsetRadius(stockParam);
	UpdateOffsetLength(stockParam);
	stockParam.SetThroughOrigin(m_cylinderAxisGoesThroghOrigin ? true : false);
	stockParam.SetAroundAxis(m_around);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnRadioAroundY()
{
	m_around = mwStockParam::Y_AXIS;

	mwStockParam stockParam = m_stockParam;
	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	UpdateOffsetRadius(stockParam);
	UpdateOffsetLength(stockParam);
	stockParam.SetThroughOrigin(m_cylinderAxisGoesThroghOrigin ? true : false);
	stockParam.SetAroundAxis(m_around);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnRadioAroundZ()
{
	m_around = mwStockParam::Z_AXIS;

	mwStockParam stockParam = m_stockParam;
	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	UpdateOffsetRadius(stockParam);
	UpdateOffsetLength(stockParam);
	stockParam.SetThroughOrigin(m_cylinderAxisGoesThroghOrigin ? true : false);
	stockParam.SetAroundAxis(m_around);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnRadioDefault()
{
	m_samples = DEFAULT;

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnClikedButIncludeAlsoCheckSurfaces()
{
	m_includeAlsoCheckSurfaces = !m_includeAlsoCheckSurfaces;

	mwStockParam stockParam = m_stockParam;
	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	stockParam.SetCheckSurfaces(m_includeAlsoCheckSurfaces ? true : false);

	switch(m_choice)
	{
	case mwStockParam::BOUNDING_BOX_FOR_DRIVE_SURFACE:
		UpdateOffsetX(stockParam);
		UpdateOffsetY(stockParam);
		UpdateOffsetZ(stockParam);
		break;
	case mwStockParam::BOUNDING_CYLINDER_FOR_DRIVE_SURFACE:
		UpdateOffsetRadius(stockParam);
		UpdateOffsetLength(stockParam);
		stockParam.SetThroughOrigin(m_cylinderAxisGoesThroghOrigin ? true : false);
		stockParam.SetAroundAxis(m_around);
		break;
	case mwStockParam::THE_PART_OFFSET:
		UpdateOffsetValue(stockParam);
		break;
	}

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnClikedButCylinderThroughOrigin()
{
	m_cylinderAxisGoesThroghOrigin = !m_cylinderAxisGoesThroghOrigin;

	mwStockParam stockParam = m_stockParam;
	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	UpdateOffsetRadius(stockParam);
	UpdateOffsetLength(stockParam);
	stockParam.SetThroughOrigin(m_cylinderAxisGoesThroghOrigin ? true : false);
	stockParam.SetAroundAxis(m_around);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnRadioAdjustSamplesByStockDim()
{
	m_3axisResetPush = false;
	m_32axisResetPush = false;

	m_samples = ADJUST_SAMPLES_BY_STOCK;

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnRadioAdjustSamlesManually()
{
	m_3axisResetPush = false;
	m_32axisResetPush = false;

	m_samples = ADJUST_SAMPLES_MANUALLY;

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnClickedButReset3()
{
	m_3axisResetPush = true;

	m_min3 = m_min3Man = m_machsimVerificationParams.GetDefaultAccuracySizeMin3();
	m_med3 = m_med3Man = m_machsimVerificationParams.GetDefaultAccuracySizeMed3();
	m_max3 = m_max3Man = m_machsimVerificationParams.GetDefaultAccuracySizeMax3();

	GetDlgItem(IDC_EDT_3_AXIS_MIN)->SetWindowText( misc::from_value(m_min3).c_str() ) ;
	GetDlgItem(IDC_EDT_3_AXIS_MIN_MAN)->SetWindowText( misc::from_value(m_min3).c_str() ) ;

	GetDlgItem(IDC_EDT_3_AXIS_MED)->SetWindowText( misc::from_value(m_med3).c_str() ) ;
	GetDlgItem(IDC_EDT_3_AXIS_MED_MAN)->SetWindowText( misc::from_value(m_med3).c_str() ) ;

	GetDlgItem(IDC_EDT_3_AXIS_MAX)->SetWindowText( misc::from_value(m_max3).c_str() ) ;
	GetDlgItem(IDC_EDT_3_AXIS_MAX_MAN)->SetWindowText( misc::from_value(m_max3).c_str() ) ;

	m_3AxisDataMod = m_32AxisDataMod = m_3AxisDataModMan = m_32AxisDataModMan = FALSE;

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnClickedButReset32()
{
	m_32axisResetPush = true;

	m_min32 = m_min32Man = m_machsimVerificationParams.GetDefaultAccuracySizeMin32();
	m_med32 = m_med32Man = m_machsimVerificationParams.GetDefaultAccuracySizeMed32();
	m_max32 = m_max32Man = m_machsimVerificationParams.GetDefaultAccuracySizeMax32(); 

	GetDlgItem(IDC_EDT_5_AXIS_MIN)->SetWindowText( misc::from_value(m_min32).c_str() ) ;
	GetDlgItem(IDC_EDT_5_AXIS_MIN_MAN)->SetWindowText( misc::from_value(m_min32).c_str() ) ;

	GetDlgItem(IDC_EDT_5_AXIS_MED)->SetWindowText( misc::from_value(m_med32).c_str() ) ;
	GetDlgItem(IDC_EDT_5_AXIS_MED_MAN)->SetWindowText( misc::from_value(m_med32).c_str() ) ;

	GetDlgItem(IDC_EDT_5_AXIS_MAX)->SetWindowText( misc::from_value(m_max32).c_str() ) ;
	GetDlgItem(IDC_EDT_5_AXIS_MAX_MAN)->SetWindowText( misc::from_value(m_max32).c_str() ) ;

	m_3AxisDataMod = m_32AxisDataMod = m_3AxisDataModMan = m_32AxisDataModMan = FALSE;

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnNMReleasedcapture3axSlider(NMHDR * /*pNMHDR*/, LRESULT *pResult)
{
	m_3axSlider = ((CSliderCtrl*)GetDlgItem(IDC_SLIDER_3_AXIS))->GetPos();

	double poz = m_3axSlider;

	poz *= m_biggestStockEdge;
	
	poz = ::ceil(poz);

	m_med3 = static_cast<int>(poz); 
	m_min3 = m_med3 / 2;
	m_max3 = m_med3 * 2;

	GetDlgItem(IDC_EDT_3_AXIS_MIN)->SetWindowText( misc::from_value(m_min3).c_str() ) ;
	GetDlgItem(IDC_EDT_3_AXIS_MED)->SetWindowText( misc::from_value(m_med3).c_str() ) ;
	GetDlgItem(IDC_EDT_3_AXIS_MAX)->SetWindowText( misc::from_value(m_max3).c_str() ) ;

	*pResult = 0;
}
//#############################################################################
const double mwVerifierPP::Convert5axDataModSliderPosToValue(const int pos) const
{
	double value = 0;

	if(pos == 1)
		value = 0.5;
	else
		if(pos == 2)
			value = 1;
		else
			if(pos == 3)
				value = 2;
			else
				if(pos == 4)
					value = 4;
				else
					if(pos == 5)
						value = 6;

	return value;
}
//#############################################################################
const int mwVerifierPP::Convert5axDataModSliderValueToPos(const double value) const
{
	int pos = 0;

	if(value < 1)
		pos = 1;
	else
		if(value > 1 && value < 2)
			pos = 2;
		else
			if(value > 2 && value < 4)
				pos = 3;
			else
				if(value > 4 && value < 6)
					pos = 4;
				else
					pos = 5;

	return pos;
}
//#############################################################################
void mwVerifierPP::OnNMReleasedcapture32axSlider(NMHDR * /*pNMHDR*/, LRESULT *pResult)
{
	m_32axSlider = ((CSliderCtrl*)GetDlgItem(IDC_SLIDER_5_AXIS))->GetPos();

	double poz = Convert5axDataModSliderPosToValue(m_32axSlider);

	poz *= m_biggestStockEdge;

	poz = ::ceil(poz);

	m_med32 = static_cast<int>(poz);
	m_min32 = m_med32 / 2;
	m_max32 = m_med32 * 2;

	GetDlgItem(IDC_EDT_5_AXIS_MIN)->SetWindowText( misc::from_value(m_min32).c_str() ) ;
	GetDlgItem(IDC_EDT_5_AXIS_MED)->SetWindowText( misc::from_value(m_med32).c_str() ) ;
	GetDlgItem(IDC_EDT_5_AXIS_MAX)->SetWindowText( misc::from_value(m_max32).c_str() ) ;

	*pResult = 0;
}
//#############################################################################
void mwVerifierPP::AdjustSlidersPosAndTexts(mwStockParam stockParam)
{
	if(stockParam.GetChoice() == mwStockParam::STL_FILE && ( stockParam.GetStockFile().empty() || !misc::mwFileSystem::FileExists(stockParam.GetStockFile())) )
		return;

	if(m_pGeoLib.IsNull() && !stockParam.IsMXP())
		return;

	// IF THERE IS STILL A PROBLEM WITH TIME, this function can be improved , if we compute only one the BB for the workpiece, here or outside
	// of the DLG, and we only shift the X,Y or Z component...

	double aditionalShift = 0.0;
	if(stockParam.GetChoice() == mwStockParam::THE_PART_OFFSET)
	{
		stockParam.SetX(0);
		stockParam.SetY(0);
		stockParam.SetZ(0);
		stockParam.SetChoice(mwStockParam::BOUNDING_BOX_FOR_DRIVE_SURFACE);
		aditionalShift = stockParam.GetOffsetValue();
	}
	
	const mwStockCreator::MeshPtr meshPtr = mwStockCreator::GetStockMeshBaseOnStockParamSettings(
		m_pGeoLib,
		stockParam,
		stockParam.IsMXP());

	if(meshPtr.IsNull())
		return;

	m_biggestStockEdge = mwStockCreator::GetMaxSizeAlongAxisForBoudingBox(
		meshPtr,
		stockParam);

	m_biggestStockEdge += 2 * aditionalShift;

	if(m_biggestStockEdge>0)
	{
		
		if(m_3AxisDataMod)
		{
			m_med3 = static_cast<int>(::ceil(m_3axSlider * m_biggestStockEdge));
			m_min3 = m_med3 / 2;
			m_max3 = m_med3 * 2;

			GetDlgItem(IDC_EDT_3_AXIS_MIN)->SetWindowText( misc::from_value(m_min3).c_str() ) ;
			GetDlgItem(IDC_EDT_3_AXIS_MED)->SetWindowText( misc::from_value(m_med3).c_str() ) ;
			GetDlgItem(IDC_EDT_3_AXIS_MAX)->SetWindowText( misc::from_value(m_max3).c_str() ) ;
		}
		else
		{
			m_3axSlider = static_cast<int>(m_med3 / m_biggestStockEdge);
			if(m_3axSlider > 9)
				m_3axSlider = 9;
			((CSliderCtrl*)GetDlgItem(IDC_SLIDER_3_AXIS))->SetPos(m_3axSlider);
		}

		if(m_32AxisDataMod)
		{
			m_med32 = static_cast<int>(::ceil(Convert5axDataModSliderPosToValue(m_32axSlider) * m_biggestStockEdge));
			m_min32 = m_med32 / 2;
			m_max32 = m_med32 * 2;

			GetDlgItem(IDC_EDT_5_AXIS_MIN)->SetWindowText( misc::from_value(m_min32).c_str() ) ;
			GetDlgItem(IDC_EDT_5_AXIS_MED)->SetWindowText( misc::from_value(m_med32).c_str() ) ;
			GetDlgItem(IDC_EDT_5_AXIS_MAX)->SetWindowText( misc::from_value(m_max32).c_str() ) ;
		}
		else
		{
			double _32axSlider = m_med32 / m_biggestStockEdge;

			m_32axSlider = Convert5axDataModSliderValueToPos(_32axSlider);

			((CSliderCtrl*)GetDlgItem(IDC_SLIDER_5_AXIS))->SetPos(m_32axSlider);
		}
	}
}
//#############################################################################
void mwVerifierPP::OnClickedButShowOnlyBiggestGauges()
{
	m_showAllGouges = FALSE;

	EnableGougeAndExcess();
}
//#############################################################################
void mwVerifierPP::OnClickedButShowAllGauges()
{
	m_showAllGouges = TRUE;

	m_gouges = 0;
	GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_GOUGES)->SetWindowText( misc::from_value(m_gouges).c_str() ) ;

	EnableGougeAndExcess();
}
//#############################################################################
void mwVerifierPP::OnClickedButShowOnlyBiggestExcesses()
{
	m_showAllExcesses = FALSE;

	EnableGougeAndExcess();
}
//#############################################################################
void mwVerifierPP::OnClickedButShowAllExcesses()
{
	m_showAllExcesses = TRUE;

	m_excesses = 0;
	GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_EXCESSES)->SetWindowText( misc::from_value(m_excesses).c_str() ) ;

	EnableGougeAndExcess();
}
//#############################################################################
void mwVerifierPP::OnClickedBut3AxisDataMod()
{
	m_3AxisDataMod = !m_3AxisDataMod;

	if(m_3AxisDataMod)
	{
		mwStockParam stockParam = m_stockParam;
		mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
		stockParam.SetChoice(choice);

		AdjustSlidersPosAndTexts(stockParam);
	}
	
	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnClickedBut32AxisDataMod()
{
	m_32AxisDataMod = !m_32AxisDataMod;

	if(m_32AxisDataMod)
	{
		mwStockParam stockParam = m_stockParam;
		mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
		stockParam.SetChoice(choice);

		AdjustSlidersPosAndTexts(stockParam);
	}
	

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnClickedBut3AxisDataModMan()
{
	m_3AxisDataModMan = !m_3AxisDataModMan;

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::OnClickedBut32AxisDataModMan()
{
	m_32AxisDataModMan = !m_32AxisDataModMan;

	EnableSamples();
}
//#############################################################################
void mwVerifierPP::UpdateOffsetX(mwStockParam& stockParam)
{
	TCHAR  tmpPath[MAX_PATH] ;

	GetDlgItem(IDC_EDT_X)->GetWindowText( tmpPath, MAX_PATH ) ;

	try
	{
		misc::to_value(tmpPath, m_offsettingX);
	}
	catch(...)
	{
		AfxMessageBox(_T("Please use a number..."));
		m_offsettingX = 0;
		GetDlgItem(IDC_EDT_X)->SetWindowText( _T("0") ) ;
		return;
	}

	stockParam.SetX(m_offsettingX);
}
//#############################################################################
void mwVerifierPP::UpdateOffsetY(mwStockParam& stockParam)
{
	TCHAR  tmpPath[MAX_PATH] ;

	GetDlgItem(IDC_EDT_Y)->GetWindowText( tmpPath, MAX_PATH ) ;

	try
	{
		misc::to_value(tmpPath, m_offsettingY);
	}
	catch(...)
	{
		AfxMessageBox(_T("Please use a number..."));
		m_offsettingY = 0;
		GetDlgItem(IDC_EDT_Y)->SetWindowText( _T("0") ) ;
		return;
	}

	stockParam.SetY(m_offsettingY);
}
//#############################################################################
void mwVerifierPP::UpdateOffsetZ(mwStockParam& stockParam)
{
	TCHAR  tmpPath[MAX_PATH] ;

	GetDlgItem(IDC_EDT_Z)->GetWindowText( tmpPath, MAX_PATH ) ;

	try
	{
		misc::to_value(tmpPath, m_offsettingZ);
	}
	catch(...)
	{
		AfxMessageBox(_T("Please use a number..."));
		m_offsettingZ = 0;
		GetDlgItem(IDC_EDT_Z)->SetWindowText( _T("0") ) ;
		return;
	}

	stockParam.SetZ(m_offsettingZ);
}
//#############################################################################
void mwVerifierPP::UpdateOffsetValue(mwStockParam& stockParam)
{
	TCHAR  tmpPath[MAX_PATH] ;

	GetDlgItem(IDC_EDT_OFFSET_VALUE)->GetWindowText( tmpPath, MAX_PATH ) ;

	try
	{
		misc::to_value(tmpPath, m_offsettingValue);
	}
	catch(...)
	{
		AfxMessageBox(_T("Please use a number..."));
		m_offsettingValue = 0;
		GetDlgItem(IDC_EDT_OFFSET_VALUE)->SetWindowText( _T("0") ) ;
		return;
	}

	stockParam.SetOffsetValue(m_offsettingValue);
}
//#############################################################################
void mwVerifierPP::UpdateOffsetRadius(mwStockParam& stockParam)
{
	TCHAR  tmpPath[MAX_PATH] ;

	GetDlgItem(IDC_EDT_RADIUS)->GetWindowText( tmpPath, MAX_PATH ) ;

	try
	{
		misc::to_value(tmpPath, m_offsettingRadius);
	}
	catch(...)
	{
		AfxMessageBox(_T("Please use a number..."));
		m_offsettingRadius = 0;
		GetDlgItem(IDC_EDT_RADIUS)->SetWindowText( _T("0") ) ;
		return;
	}

	stockParam.SetRadius(m_offsettingRadius);
}
//#############################################################################
void mwVerifierPP::UpdateOffsetLength(mwStockParam& stockParam)
{
	TCHAR  tmpPath[MAX_PATH] ;

	GetDlgItem(IDC_EDT_LENGTH)->GetWindowText( tmpPath, MAX_PATH ) ;

	try
	{
		misc::to_value(tmpPath, m_offsettingLength);
	}
	catch(...)
	{
		AfxMessageBox(_T("Please use a number..."));
		m_offsettingLength = 0;
		GetDlgItem(IDC_EDT_LENGTH)->SetWindowText( _T("0") ) ;
		return;
	}

	stockParam.SetLength(m_offsettingLength);
}
//#############################################################################
void mwVerifierPP::OnEnChangeEditX()
{
	mwStockParam stockParam = m_stockParam;
	UpdateOffsetX(stockParam);
	UpdateOffsetY(stockParam);
	UpdateOffsetZ(stockParam);

	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnEnChangeEditY()
{
	mwStockParam stockParam = m_stockParam;
	UpdateOffsetX(stockParam);
	UpdateOffsetY(stockParam);
	UpdateOffsetZ(stockParam);

	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnEnChangeEditZ()
{
	mwStockParam stockParam = m_stockParam;
	UpdateOffsetX(stockParam);
	UpdateOffsetY(stockParam);
	UpdateOffsetZ(stockParam);

	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnEnChangeEditOffset()
{
	//seems to be very slow...
	mwStockParam stockParam = m_stockParam;
	UpdateOffsetValue(stockParam);

	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnEnChangeEditRadius()
{
	mwStockParam stockParam = m_stockParam;
	UpdateOffsetRadius(stockParam);

	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnEnChangeEditLength()
{
	mwStockParam stockParam = m_stockParam;
	UpdateOffsetLength(stockParam);

	mwStockParam::choice choice = ConvertCheckBoxToChoice(m_choice);
	stockParam.SetChoice(choice);

	AdjustSlidersPosAndTexts(stockParam);
}
//#############################################################################
void mwVerifierPP::OnClickedButMiscellaneousAdjust()
{
	m_adjustStockMeshQualityDuringRun = !m_adjustStockMeshQualityDuringRun;

	EnableMiscellaneous();
}
//#############################################################################
void mwVerifierPP::EnableVerifier(const bool toSet)
{
	m_enableVerifier = toSet ? TRUE : FALSE;
}
//#############################################################################
const bool mwVerifierPP::IsVerifierEnabled() const
{
	return m_enableVerifier == TRUE ? true : false;
}
//#############################################################################
void mwVerifierPP::OnClickedEnableMaterialRemovalMode()
{
	if (((CButton*)GetDlgItem(IDC_CHECK_ENABLE_MATERIAL_REMOVAL_MODE))->GetCheck() == BST_CHECKED)
	{
		EnableMiscellaneous();
		EnableSamples();
		EnableMeshCollisions();
		EnableStockMaterialSelection();
		EnableGougeAndExcess();
		EnableCutSimResourceFile();
	}
	else
	{
		DisableWindowControls();
	}
}
//#############################################################################
void mwVerifierPP::DisableWindowControls()
{
	GetDlgItem(IDC_LBL_CUTSIM_RESOURCE_FILE)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_CUTSIM_RESOURCE_FILE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_CUTSIM_RESOURCE_FILE)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_DISCRETE_COLLISION)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_CONTINOUS_COLLISION)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_MISCELLANEOUS_ADJUST)->EnableWindow(FALSE);
	GetDlgItem(IDC_SLIDER_MISCELLANEOUS)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_CHOICES)->EnableWindow(FALSE);
	GetDlgItem(IDC_COMBO_CHOICE)->EnableWindow(FALSE);
	GetDlgItem(IDC_BTN_LOAD)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_SURFACES)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_AUTO_ADJUST_PRECISION)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_AUTO_QUALITY_DRAW)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_X)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_X)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_Y)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_Y)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_Z)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_Z)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_OFFSET_VALUE)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_OFFSET_VALUE)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_LENGTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_LENGTH)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_RADIUS)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_RADIUS)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_CYLINDER_THROUGH_ORIGIN)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_X)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_Y)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_Z)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_DEFAULT)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_RESET_3_AXIS)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_RESET_5_AXIS)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_ADJUST_SAMPLES)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_3_AXIS_DATA_MODEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_5_AXIS_DATA_MODEL)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_3_AX_SAMPLES_MM)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_5_AX_SAMPLES_MM)->EnableWindow(FALSE);
	GetDlgItem(IDC_SLIDER_3_AXIS)->EnableWindow(FALSE);
	GetDlgItem(IDC_SLIDER_5_AXIS)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_3_AXIS_MIN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_3_AXIS_MIN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_5_AXIS_MIN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_5_AXIS_MIN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_3_AXIS_MED)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_3_AXIS_MED)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_5_AXIS_MED)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_5_AXIS_MED)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_3_AXIS_MAX)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_3_AXIS_MAX)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_5_AXIS_MAX)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_5_AXIS_MAX)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_ADJUST_SAMPLES_MANUALLY)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_3_AXIS_DATA_MODEL_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_5_AXIS_DATA_MODEL_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_3_AXIS_MIN_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_3_AXIS_MIN_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_5_AXIS_MIN_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_5_AXIS_MIN_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_3_AXIS_MED_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_3_AXIS_MED_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_5_AXIS_MED_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_5_AXIS_MED_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_3_AXIS_MAX_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_3_AXIS_MAX_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_5_AXIS_MAX_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_5_AXIS_MAX_MAN)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_GOUGES)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_GOUGES)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_GOUGES)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_SHOW_ALL_GOUGES)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_SHOW_ONLY_BIGGEST_EXCESSES)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_SHOW_ONLY_BIGGEST_EXCESSES)->EnableWindow(FALSE);
	GetDlgItem(IDC_LBL_EXCESSES)->EnableWindow(FALSE);
	GetDlgItem(IDC_RADIO_SHOW_ALL_EXCESSES)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_MAKE_GOUGE_REPORT_3AX)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_MAKE_GOUGE_REPORT_5AX)->EnableWindow(FALSE);
}

void mwVerifierPP::EnableCutSimResourceFile()
{
	GetDlgItem(IDC_LBL_CUTSIM_RESOURCE_FILE)->EnableWindow(TRUE);
	GetDlgItem(IDC_EDT_CUTSIM_RESOURCE_FILE)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUT_CUTSIM_RESOURCE_FILE)->EnableWindow(TRUE);
}
