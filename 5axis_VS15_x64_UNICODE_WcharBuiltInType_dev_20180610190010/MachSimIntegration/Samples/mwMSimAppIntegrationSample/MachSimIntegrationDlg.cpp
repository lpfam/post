/******************************************************************************
*               file: 	MachSimIntegrationDlg.cpp						      *
*******************************************************************************
*               Description:                                                  *
*	Implementation of CMachSimIntegrationDlg class							  *
*******************************************************************************
*               History:                                                      *
*  2006/12/23 23:14:54		Created by Eugene Hodzhaev						  *
*  2007/04/06 10:57:00		Updated by Mihai Vasilian						  *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/


//! local header files
#include "StdAfx.h"
#include "mwMachSimIntegrationException.hpp"
#include "MachSimIntegrationSample.h"
#include "MachSimIntegrationDlg.h"
#include "GLWrapperDlg.hpp"
//! MachSim header files
#include "mwMachSimIniParams.hpp"
#include "mwMachSimFacade.hpp"
#include "mwMachSimStartUpParams.hpp"
#include "mwMachSimKeyboardBinding.hpp"
#include "mwMachSimMouseBindings.hpp"
#include "mwMachSimStateException.hpp"
#include "mwMachSimInitializationException.hpp"
#include "mwFileAccessException.hpp"
#include "mwMachSimMachDefSemanticException.hpp"
#include "mwMachSimToolPathFormatSyntaxException.hpp"
#include "mwMachSimToolPathFormatSemanticException.hpp"
#include "mwMachSimUIBindingsException.hpp"
#include "mwMachSimConfigMismatchException.hpp"
#include "mwMachSimDefaultExceptionHandler.hpp"
#include "mwMSimOperationBuilder.hpp"
#include "mwIMachSimVerification.hpp"
//! c header files
#include <assert.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//! custom messages that mwMSimApp (MachSim) know about
#define WM_SIZECHANGE	WM_USER + 101
#define WM_PAINT_EX		WM_USER + 103

//#############################################################################
// CMachSimIntegrationDlg dialog
CMachSimIntegrationDlg::CMachSimIntegrationDlg(CWnd* pParent /*=MW_NULL*/)
	: CDialog(CMachSimIntegrationDlg::IDD, pParent)
	, m_machSimFacade( MW_NULL )
	, m_machSimWndHandle( MW_NULL )
	, m_glFrame( MW_NULL )
	, m_viewCaptureMade(false)
{
	//{{AFX_DATA_INIT(CMachSimIntegrationDlg)
	m_bgred=0;
	m_bggreen=0;
	m_bgblue=0;
	m_showtp=FALSE;
	m_showmh=FALSE;
	m_viewName=_T("");
	m_value=0;
	m_manual=FALSE;
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDI_MACHSIMINTEGRATIONSAMPLE);
	m_glFrame = new GLWrapperDlg( this );
}
//#############################################################################
void CMachSimIntegrationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMachSimIntegrationDlg)
		DDX_Text(pDX, IDC_BGR, m_bgred);
		DDV_MinMaxDouble(pDX, m_bgred, 0, 1);
		DDX_Text(pDX, IDC_BGG, m_bggreen);
		DDV_MinMaxDouble(pDX, m_bggreen, 0, 1);
		DDX_Text(pDX, IDC_BGB, m_bgblue);
		DDV_MinMaxDouble(pDX, m_bgblue, 0, 1);
		DDX_Check(pDX, IDC_SHOW_TP, m_showtp);
		DDX_Check(pDX, IDC_SHOW_MH, m_showmh);
		DDX_CBString(pDX, IDC_VIEW, m_viewName);
		DDX_Control(pDX, IDC_SIMULATION_SPEED, m_speedCtrl);
		DDX_Control(pDX, IDC_AXES, m_axis);
		DDX_Text(pDX, IDC_AXIS_VALUE, m_value);
		DDX_Check(pDX, IDC_CHECK1, m_manual);
	//}}AFX_DATA_MAP
}
//#############################################################################
BEGIN_MESSAGE_MAP(CMachSimIntegrationDlg, CDialog)
	//{{AFX_MSG_MAP(CMachSimIntegrationDlg)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(ID_INIT_MACHSIM, OnBnClickedInitMachsim)
	ON_BN_CLICKED(ID_SHUTDOWN_MACHSIM, OnBnClickedShutdownMachsim)
	ON_BN_CLICKED(IDC_LOAD_MACHINE, OnBnClickedLoadMachine)
	ON_BN_CLICKED(IDC_LOADNC, OnBnClickedLoadNC)
	ON_BN_CLICKED(IDC_SAVE_MD, OnBnClickedSaveMd)
	ON_BN_CLICKED(IDC_SAVE_EXE, OnBnClickedSaveExe)
	ON_BN_CLICKED(IDC_PLAY, OnBnClickedPlay)
	ON_BN_CLICKED(IDC_STOP, OnBnClickedStop)
	ON_BN_CLICKED(IDC_PAUSE, OnBnClickedPause)
	ON_BN_CLICKED(IDC_NEXT_OP, OnBnClickedNextOp)
	ON_BN_CLICKED(IDC_NEXT_MOVE, OnBnClickedNextMove)
	ON_BN_CLICKED(IDC_PREV_MOVE, OnBnClickedPrevMove)
	ON_BN_CLICKED(IDC_PREV_OP, OnBnClickedPrevOp)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SIMULATION_SPEED, OnNMReleasedcaptureSimulationSpeed)
	ON_BN_CLICKED(IDC_APPLY_BG_COLOR, OnBnClickedApplyBgColor)
	ON_BN_CLICKED(IDC_SHOW_TP, OnBnClickedShowTp)
	ON_BN_CLICKED(IDC_SHOW_MH, OnBnClickedShowMh)
	ON_BN_CLICKED(IDC_FIT, OnBnClickedFit)
	ON_CBN_SELCHANGE(IDC_VIEW, OnCbnSelchangeView)
	ON_BN_CLICKED(IDC_CAPTURE_VIEW, OnBnClickedCaptureView)
	ON_BN_CLICKED(IDC_MODIFY_KBB, OnBnClickedModifyKbb)
	ON_BN_CLICKED(IDC_MODIFY_MB, OnBnClickedModifyMb)
	ON_BN_CLICKED(IDC_CHECK1, OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_GETAXES, OnBnClickedGetaxes)
	ON_BN_CLICKED(IDC_APPLY_VALUE, OnBnClickedApplyValue)
	ON_CBN_SELCHANGE(IDC_AXES, OnCbnSelchangeAxis)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//#############################################################################
// CMachSimIntegrationDlg message handlers
void CMachSimIntegrationDlg::CreateWrapperDialogs()
{
	m_glFrame->Create( IDD_GLVIEW, this );

	m_glFrame->ShowWindow(SW_SHOW);
}

//#############################################################################
BOOL CMachSimIntegrationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	m_speedCtrl.SetRange( -5, 5 );


	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.


//#############################################################################
void CMachSimIntegrationDlg::OnGLSceneResize( unsigned int cx, unsigned int cy )
{
	if (m_machSimFacade != MW_NULL )
	{
		m_machSimFacade->GetViewService().SetSceneSize( cx, cy );
	}	
}

//#############################################################################
void CMachSimIntegrationDlg::OnGLRepaint()
{
	if (m_machSimFacade != MW_NULL )
	{
		m_machSimFacade->GetViewService().RenderScene();
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
//#############################################################################
HCURSOR CMachSimIntegrationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//#############################################################################
int CMachSimIntegrationDlg::OnCreate( LPCREATESTRUCT /*lpCreateStruct*/ )
{
	//necessary to avoid flicker of the MachSim Facade window
	//(when resizing the application window, repainting occurs only for the 
	// area excluding the discussed window; only after that is done, refresh of 
	// MSF window follows -> there is no double redraw and thus, no flicker)
	ModifyStyle(m_hWnd,0,WS_CLIPCHILDREN,0);
	return true;
}

//#############################################################################
afx_msg void CMachSimIntegrationDlg::OnDestroy()
{
	CDialog::OnDestroy();
	try
	{
		//Destroy machine simulation if not destroyed yet
		if ( m_machSimFacade != 0) 
		{
			m_machSimFacade->Destroy();
			m_machSimFacade = MW_NULL;
		}
	}
	catch( exceptions::mwMachSimStateException &ex )
	{
		AfxMessageBox(
				exceptions::mwMachSimDefaultExceptionHandler::BuildCompleteErrorMessage( ex ).c_str()
				);
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox(
				exceptions::mwMachSimDefaultExceptionHandler::BuildCompleteErrorMessage( ex ).c_str()
				);
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::Destroy call") );
	}

}

//#############################################################################
afx_msg void CMachSimIntegrationDlg::OnClose()
{
	CDialog::OnClose();
	try
	{
		//Destroy machine simulation if not destroyed yet
		if ( m_machSimFacade != 0) 
		{
			m_machSimFacade->Destroy();
			m_machSimFacade = MW_NULL;
		}
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::Destroy call") );
	}

}

//#############################################################################
void CMachSimIntegrationDlg::OnOK()
{
	OnClose();
	CDialog::OnOK();
}

//#############################################################################
void CMachSimIntegrationDlg::OnCancel()
{
	OnClose();
	CDialog::OnCancel();
}

//#############################################################################
void CMachSimIntegrationDlg::CheckFacadePointer()
{
	if ( m_machSimFacade == MW_NULL )
	{
		throw exceptions::mwMachSimIntegrationException( 
			exceptions::mwMachSimIntegrationException::NULL_POINTER_USE);
	}
}
//MACH SIM FACADE INITIALIZATION AND SHUTDOWN
//#############################################################################
//#############################################################################

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedInitMachsim()
{
	if ( m_machSimFacade )
	{
		OnBnClickedShutdownMachsim();
	}

	mwMachSimStartUpParams machsimFacadeStartupParams = FetchStartupParams();

	try
	{
		CreateWrapperDialogs();
		m_machSimFacade = mwMachSimFacade::Create( m_glFrame->GetSafeHwnd(),
					machsimFacadeStartupParams);

		//added verification noftifier
		m_myHandler = new VerifierCustomizationsApplyer(m_machSimFacade, GetSafeHwnd());
		m_machSimFacade->RegisterNotificationHandler(m_myHandler);

		m_machSimFacade->GetViewService().RenderScene();
		if (m_machSimFacade->IsVerifierAvailable())
		{
			m_machSimFacade->EnableVerification(true);
		}

		LoadCustomKeyBindings();

		ApplyAutoloadParameters();
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
		OnBnClickedShutdownMachsim();
	}
	catch( ... )
	{
		AfxMessageBox( _T( "Unexpected exception during mwMachSimFacade::Create call" ) );
		return;
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedShutdownMachsim()
{
	try
	{
		if (m_machSimFacade == MW_NULL)
		{
			//Destroy machine simulation
			mwMachSimFacade::Destroy();
		}
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::Destroy call") );
	}

	m_glFrame->DestroyWindow();
	
	m_machSimFacade = MW_NULL;
	m_machSimWndHandle = MW_NULL;
}

//STARTUP PARAMETERS RETRIEVAL,HANDLING
//#############################################################################
//#############################################################################

//#############################################################################
misc::mwFileName CMachSimIntegrationDlg::GetIniFileName()
{
	_TCHAR exeFileName[MAX_PATH];
	::GetModuleFileName( 0, exeFileName, MAX_PATH );

	misc::mwFileName  inifn(exeFileName);
	inifn.SetFileExtention( _T("ini"));
	
	return inifn;
}

//#############################################################################
misc::mwFileName CMachSimIntegrationDlg::GetResourceFileName()
{
	misc::mwFileName iniFileName = GetIniFileName();
	mwMachSimIniParams crtIniParams(iniFileName);
	misc::mwstring machSimResourceFileFullName = crtIniParams.GetResourceFileName();
	if ( machSimResourceFileFullName == _T("")) 
		{
			misc::mwstring defaultFile = crtIniParams.GetExecutablesDirectory() + _T("mwMachSim_res.dll");
			misc::mwFileName tempResourceFileName(defaultFile);
			machSimResourceFileFullName = tempResourceFileName.GetFilePath();
		}

	return misc::mwFileName(machSimResourceFileFullName);
}

//#############################################################################
mwMachSimStartUpParams CMachSimIntegrationDlg::FetchStartupParams()
{
	mwMachSimCollisionDetectionParameters collisionDetectionParameters;
	double  targetTolerance = 0.7;
	double  stlTolerance = 0;
	double  targetOffset = 0;
	bool	disableMachineConfig = false;
	int		multiSample = 4;
	double	meshSmoothingAngle = 20.;
	mmedia::mwOpenGLBackgroundImage::bgType bgType = mmedia::mwOpenGLBackgroundImage::BG_GRADIENT;
	misc::mwstring bgImage = _T("");
	mmedia::mwScreenPosition::position bgImagePosition = mmedia::mwScreenPosition::FILL_CENTER;

	misc::mwAutoPointer< mwMachSimStartUpParams::Units > displayUnitSystem;
	
	misc::mwColor bgColor( 0 , 0 , 0 );
	mmedia::mwColorGradient bgGradientColor( mmedia::mwColorGradient::horizontal, 
		misc::mwColor( 0 , 0 , 0.1f ),
		misc::mwColor( 0.4f , 0.1f , 0.2f ) );

	if ( misc::mwFileSystem::FileExists( GetIniFileName().GetFilePath() ) )
	{
		mwMachSimIniParams currentIniParams( GetIniFileName().GetFilePath() );
		if ( currentIniParams.HasDisplayUnitSystem() )
		{
			displayUnitSystem =  
				new mwMachSimStartUpParams::Units( currentIniParams.GetDisplayUnitSystem() );
		}
		collisionDetectionParameters = currentIniParams.GetCollisionDetectionParameters();
		targetTolerance = currentIniParams.GetTargetTolerance();
		targetOffset = currentIniParams.GetTargetOffset();
		stlTolerance = currentIniParams.GetSTLTolerance();
		disableMachineConfig = currentIniParams.GetDisableMachineConfig();
		bgColor =  currentIniParams.GetBgColor();
		bgGradientColor = currentIniParams.GetBgGradient();
		multiSample = currentIniParams.GetMultisample();
		meshSmoothingAngle = currentIniParams.GetMeshSmoothAngle();
		bgType = currentIniParams.GetBgType();
		bgImagePosition = currentIniParams.GetBgImagePos();
		bgImage = currentIniParams.GetBgImage();
	}

	mwMachSimStartUpParams startupParams( GetResourceFileName().GetFilePath() );

	if ( displayUnitSystem.IsNotNull() )
	{
		startupParams.SetDisplayUnitSystem( *displayUnitSystem );
	}
	startupParams.SetCollisionDetectionParameters( collisionDetectionParameters );
	startupParams.SetTargetTolerance( targetTolerance );
	startupParams.SetTargetOffset( targetOffset );
	startupParams.SetSTLTolerance( stlTolerance );
	startupParams.SetDisableMachineConfig( disableMachineConfig );
	startupParams.SetBgColor( bgColor );
	startupParams.SetBgGradient( bgGradientColor );
	startupParams.SetMultisample( multiSample );
	startupParams.SetMeshSmoothAngle( meshSmoothingAngle );
	startupParams.SetBgType( bgType );
	startupParams.SetBgImage( bgImage );
	startupParams.SetBgImagePos( bgImagePosition );

	return startupParams;
}

//#############################################################################
void CMachSimIntegrationDlg::LoadCustomKeyBindings()
{
	misc::mwFileName tempKeyBindingsFileName = GetIniFileName();
	tempKeyBindingsFileName.SetFileExtention(_T("keymap"));
	
	tempKeyBindingsFileName.SetFileCoreName(_T("default"));
	misc::mwstring defaultKeyBindingsFileName = tempKeyBindingsFileName.GetFilePath();

	tempKeyBindingsFileName.SetFileCoreName(_T("current"));
	misc::mwstring currentKeyBindingsFileName = tempKeyBindingsFileName.GetFilePath();
	
	misc::mwFileName iniFileName = GetIniFileName();
	if ( misc::mwFileSystem::FileExists( iniFileName.GetFilePath() ) )
	{	
		mwMachSimIniParams currentIniParams( iniFileName.GetFilePath() );
	
		if ( currentIniParams.GetDefKeyFileName() != _T("") )
		{
			defaultKeyBindingsFileName = currentIniParams.GetDefKeyFileName();
		}
	
		if ( currentIniParams.GetCrtKeyFileName() != _T("") )
		{
			currentKeyBindingsFileName = currentIniParams.GetCrtKeyFileName();
		}
	}	

	m_machSimFacade->GetViewService().SetGraphPluginDefaultKeyBindingsFromStdFile( defaultKeyBindingsFileName );
	m_machSimFacade->GetViewService().SetGraphPluginCurrentKeyBindingsFromStdFile( currentKeyBindingsFileName );
}

//#############################################################################
void CMachSimIntegrationDlg::ApplyAutoloadParameters()
{
	if ( misc::mwFileSystem::FileExists( GetIniFileName().GetFilePath() ) )
	{
	
	mwMachSimIniParams currentParams( GetIniFileName().GetFilePath() );

		if ( currentParams.GetAutoLoad() )
		{
			m_machSimFacade->LoadMachineDefinition( currentParams.GetMachineDefinitionFileName() );
			m_machSimFacade->LoadOperations(mwMSimOperationBuilder::GetOperationsFromSimFile(currentParams.GetNCFileName()), MW_NULL, MW_NULL);
			m_machSimFacade->GetViewService().SetBgColor( currentParams.GetBgColor() );
			if (currentParams.GetAutoStart() )
			{
				m_machSimFacade->RunSimulation();
			}
		}
	}
}

//LOADING ROUTINES FOR MACHINE DEFINITION AND TOOLPATH DATA
//#############################################################################
//#############################################################################

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedLoadMachine()
{
	misc::mwstring filter = _T("Machine definition files (*.xml)|*.xml| All files (*.*)|*.*||");
	
	CFileDialog ofn(TRUE, _T("sim"), MW_NULL, OFN_FILEMUSTEXIST, 
		filter.c_str(),this); 

	if( ofn.DoModal() == IDOK )
	{
		misc::mwstring fileName = ofn.GetPathName();
	
		try
		{
			CheckFacadePointer();

			//Load machine definition from the selected XML file
			m_machSimFacade->LoadMachineDefinition( fileName );
		}
		catch (exceptions::mwMachSimIntegrationException& ex)
		{
			MachSimIntegrationExceptionMessage( ex );
		}
		catch ( misc::mwException& ex )
		{
			AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
		}
		catch( ... )
		{
			AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::LoadMachineDefinition call") );
		}	
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedLoadNC()
{
	misc::mwstring filter = _T("CL files (*.cl)|*.cl| Sim files (*.sim)|*.sim| All files (*.*)|*.*||");
	
	CFileDialog ofn(TRUE,_T("cl"), MW_NULL, OFN_FILEMUSTEXIST, 
		filter.c_str(),this); 

	if( ofn.DoModal() == IDOK )
	{
		misc::mwFileName fileName (ofn.GetPathName().GetString());
		
		try
		{
			CheckFacadePointer();

			if (fileName.GetFileExtention()=="sim")
			{
				//Load tool path from a specified file in SIM format
				m_machSimFacade->LoadOperations(mwMSimOperationBuilder::GetOperationsFromSimFile(fileName.GetFilePath()), MW_NULL, MW_NULL);
			}else
			{
				cadcam::mwTPoint3d<double> origin(0, 0, 0);
				post::mwMXPParam::MachineLimits machineLimits = post::mwMXPParam::NO_LIMITS;
				mwMachSimOperationsProgrammePtr operationProgrammePtr;
				mwMSimOperationBuilder::GetOperationsProgrammeFromCLFile(operationProgrammePtr, fileName.GetFilePath(), m_machSimFacade->GetMachineFileName(), origin, machineLimits, m_machSimFacade->GetTargetTolerance()*0.1);
				m_machSimFacade->LoadOperationsProgramme(*operationProgrammePtr);
			}
			
			if (m_machSimFacade->IsVerifierAvailable())
			{
				if (m_myHandler->m_textureHandler.IsNotNull())
				{//Apply Texture
					misc::mwColor white(1.0f,1.0f,1.0f);
					m_machSimFacade->GetVerificationService()->SetAlternateStockColor(true,&white,0);
					m_machSimFacade->GetVerificationService()->SetDrawMode(0,mwMachSimVerifier::WDM_TEXTURE_PLUS_COLLISIONS);
				}
				m_machSimFacade->GetViewService().EnableInitialStock(false);
			}
		}
		catch (exceptions::mwMachSimIntegrationException& ex)
		{
			MachSimIntegrationExceptionMessage( ex );
		}
		catch ( misc::mwException& ex)
		{
			AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
		}
		catch( ... )
		{
			AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::LoadToolPath call") );
		}
	}
}

//SAVE MACHINE DEFINITION, CREATE PRESENTATION
//#############################################################################
//#############################################################################

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedSaveMd()
{
	misc::mwstring filter = _T("Machine definition files (*.xml)|*.xml| All files (*.*)|*.*||");
	
	CFileDialog dlg(FALSE, _T("xml"), MW_NULL, OFN_OVERWRITEPROMPT, 
		filter.c_str(),this); 

	if( dlg.DoModal() == IDOK )
	{
		try
		{
			CheckFacadePointer();

			//Save current machine definition to an XML file
			m_machSimFacade->SaveMachineDefinition( (const _TCHAR *)dlg.GetPathName() );
		}
		catch ( misc::mwException& ex )
		{
			AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
 		}
		catch( ... )
		{
			AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SaveMachineDefinition call") );
		}

	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedSaveExe()
{
	misc::mwstring filter = _T("Presentation files (*.exe)|*.exe| All files (*.*)|*.*||");
	
	CFileDialog sfn(FALSE, _T("exe"), MW_NULL, OFN_OVERWRITEPROMPT, 
		filter.c_str(),this); 

	if( sfn.DoModal() == IDOK )
	{
		misc::mwstring fileName = sfn.GetPathName();
		try
		{
			CheckFacadePointer();

			//Save current set up as an executable
			m_machSimFacade->SaveAsExe( (const _TCHAR *)sfn.GetPathName() );
		}
		catch (exceptions::mwMachSimIntegrationException& ex)
		{
			MachSimIntegrationExceptionMessage( ex );
		}
		catch ( misc::mwException& ex )
		{
			AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
		}
		catch( ... )
		{
			AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SaveAsExe call") );
		}
	}
}

//SIMULATION CONTROL
//#############################################################################
//#############################################################################

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedPlay()
{
	try
	{
		CheckFacadePointer();
		
		//Start automatic simulation
		m_machSimFacade->RunSimulation();
	}
	catch (exceptions::mwMachSimIntegrationException& ex)
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::RunSimulation call") );
	}

}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedStop()
{
	try
	{
		CheckFacadePointer();

		//Stop automatic simulation
		m_machSimFacade->StopSimulation();
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::StopSimulation call") );
	}

}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedPause()
{
	try
	{
		CheckFacadePointer();

		//Pause simulation
		m_machSimFacade->PauseSimulation();
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::PauseSimulation call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedNextOp()
{
	try
	{
		CheckFacadePointer();

		//Go to the next operation
		m_machSimFacade->GoToNextOperation();
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::GoToNextOperation call") );
	}

}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedNextMove()
{
	try
	{
		CheckFacadePointer();

		//Go to the next move
		m_machSimFacade->GoToNextMove();
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::GoToNextMove call") );
	}

}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedPrevMove()
{
	try
	{
		CheckFacadePointer();

		//Go to the previous move
		m_machSimFacade->GoToPrevMove();
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::GoToPrevMove call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedPrevOp()
{
	try
	{
		CheckFacadePointer();

		//Go to the previous operation
		m_machSimFacade->GoToPrevOperation();
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::GoToPrevOperation call") );
	}

}

//#############################################################################
void CMachSimIntegrationDlg::OnNMReleasedcaptureSimulationSpeed(NMHDR * /*pNMHDR*/, LRESULT *pResult)
{
	UpdateData();

	try
	{
		CheckFacadePointer();

		//Change simulation speed
		m_machSimFacade->SetSimulationSpeed( static_cast<short>( m_speedCtrl.GetPos() ) );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SetSimulationSpeed call") );
	}

	*pResult = 0;
}


//GRAPHIC REPRESENTATION
//#############################################################################
//#############################################################################

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedApplyBgColor()
{
	UpdateData();
	try
	{
		CheckFacadePointer();
		//Set background color
		m_machSimFacade->GetViewService().SetBgColor( misc::mwColor( static_cast<float>(m_bgred), 
													  static_cast<float>(m_bggreen),
													  static_cast<float>(m_bgblue) ) );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SetBGColor call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedShowTp()
{
	UpdateData();
	try
	{
		CheckFacadePointer();
		//Enable/disable graphical tool path representation
		m_machSimFacade->EnableToolpath( m_showtp == TRUE );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::EnableToolpath call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedShowMh()
{
	UpdateData();
	try
	{
		CheckFacadePointer();
		//Show/hide machine housing
		m_machSimFacade->GetViewService().EnableMachineHousing( m_showmh == TRUE );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::EnableMachineHousing call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedFit()
{
	try
	{
		CheckFacadePointer();
		//Fit to screen
		m_machSimFacade->GetViewService().FitToScreen();
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::FitToScreen call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnCbnSelchangeView()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	((CComboBox *)GetDlgItem( IDC_VIEW ))->
		GetLBText( ((CComboBox *)GetDlgItem( IDC_VIEW ))->GetCurSel(), m_viewName );
	
	//Switch to one of the standard views or to a previously captured view
	try
	{
		CheckFacadePointer();

		if ( m_viewName == "Iso" )
		{
			m_machSimFacade->GetViewService().SetCurrentView( m_machSimFacade->GetViewService().GetCurrentView().CreateIso() );
		}
		else if( m_viewName == "Top" )
		{
			m_machSimFacade->GetViewService().SetCurrentView( m_machSimFacade->GetViewService().GetCurrentView().CreateTop() );
		}
		else if( m_viewName == "Bottom" )
		{
			m_machSimFacade->GetViewService().SetCurrentView( m_machSimFacade->GetViewService().GetCurrentView().CreateBottom() );
		}
		else if( m_viewName == "Left" )
		{
			m_machSimFacade->GetViewService().SetCurrentView( m_machSimFacade->GetViewService().GetCurrentView().CreateLeft() );
		}
		else if( m_viewName == "Right" )
		{
			m_machSimFacade->GetViewService().SetCurrentView( m_machSimFacade->GetViewService().GetCurrentView().CreateRight() );
		}
		else if( m_viewName == "Front" )
		{
			m_machSimFacade->GetViewService().SetCurrentView( m_machSimFacade->GetViewService().GetCurrentView().CreateFront() );
		}
		else if( m_viewName == "Back" )
		{
			m_machSimFacade->GetViewService().SetCurrentView( m_machSimFacade->GetViewService().GetCurrentView().CreateBack() );
		}
		else
		{
			if (m_viewCaptureMade)
			{
				m_machSimFacade->GetViewService().SetCurrentView( m_capturedView );
			}
			
		}
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SetCurrentView call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedCaptureView()
{
	try
	{
		CheckFacadePointer();
		//Get current view and save it for later use
		m_capturedView = m_machSimFacade->GetViewService().GetCurrentView();
		m_viewCaptureMade = true;
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::GetCurrentView call") );
	}
}

// MOUSE AND KEYBOARD BINDINGS
//#############################################################################
//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedModifyKbb()
{
	try
	{
		CheckFacadePointer();

		//Sample keyboard binding modification
		mwMachSimKeyboardBindings currentBinds( m_machSimFacade->GetViewService().GetKeyboardBindings() );

		//Assign zoom to K and unzoom to M keys
		currentBinds.SetZoomInBinding( misc::mwActionKeyBinding( 
			'K', misc::mwActionKeyBinding::NONE ) );
		currentBinds.SetZoomOutBinding( misc::mwActionKeyBinding( 
			'M', misc::mwActionKeyBinding::NONE ) );
		
		m_machSimFacade->GetViewService().SetKeyboardBindings( currentBinds );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SetKeyboardBindings call") );
	}

}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedModifyMb()
{
	try
	{
		CheckFacadePointer();

		//Modify mouse bindings
		mwMachSimMouseBindings currentBinds( m_machSimFacade->GetViewService().GetMouseBindings() );
		
		currentBinds.SetPositionZoomInBinding( misc::mwActionKeyBinding( 256, misc::mwActionKeyBinding::ALT |
			misc::mwActionKeyBinding::CTRL ) );
		currentBinds.SetPositionZoomOutBinding( misc::mwActionKeyBinding( 256, misc::mwActionKeyBinding::ALT |
			misc::mwActionKeyBinding::SHIFT ) );

		m_machSimFacade->GetViewService().SetMouseBindings( currentBinds );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SetMouseBindings call") );
	}
}

//MANUAL AXIS CONTROL
//#############################################################################
//#############################################################################

void CMachSimIntegrationDlg::OnBnClickedCheck1()
{
	UpdateData();
	try
	{
		CheckFacadePointer();

		//Switch to the manual mode
		m_machSimFacade->SetManualMode( m_manual == TRUE );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SetManualMode call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedGetaxes()
{
	try
	{
		CheckFacadePointer();

		//List available axis
		axesCollection axis( m_machSimFacade->GetAvailableAxes() );

		acConstIterator It;
		acConstIterator End( axis.end() );
		int index = 0;
		m_axis.ResetContent();

		for( It = axis.begin(); It != End; It++, index++ )
		{
			m_axis.AddString( It->GetAxisName().c_str() );
		}
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception ") );
	}

	UpdateData( FALSE );
}

//#############################################################################
void CMachSimIntegrationDlg::OnCbnSelchangeAxis()
{
	CString currText;
	m_axis.GetLBText( m_axis.GetCurSel(), currText );
	
	try
	{
		CheckFacadePointer();

		//Retrieve a value for an exis
		m_value = m_machSimFacade->GetAxisValue( (const _TCHAR *)currText );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::GetAxisValue call") );
	}

	UpdateData( FALSE );
}

//#############################################################################
void CMachSimIntegrationDlg::OnBnClickedApplyValue()
{
	UpdateData();

	CString currText;
	if (m_axis.GetCurSel() == CB_ERR)
		return;
	m_axis.GetLBText( m_axis.GetCurSel(), currText );
	try
	{
		CheckFacadePointer();

		//Set a value of an axis
		m_machSimFacade->SetAxisValue( m_value, (const _TCHAR *)currText );
	}
	catch ( exceptions::mwMachSimIntegrationException& ex )
	{
		MachSimIntegrationExceptionMessage( ex );
	}
	catch ( misc::mwException& ex )
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch( ... )
	{
		AfxMessageBox( _T("Unexpected exception during mwMachSimFacade::SetAxisValue call") );
	}
}

//#############################################################################
void CMachSimIntegrationDlg::MachSimIntegrationExceptionMessage(
							const exceptions::mwMachSimIntegrationException& ex)
{
	misc::mwstring errorMessage(_T(""));
	
	switch( ex.GetCause() )
	{
	
	case exceptions::mwMachSimIntegrationException::NULL_POINTER_USE:
		
		errorMessage += _T("The facade is not initialized!");

		break;
	}

	AfxMessageBox( errorMessage.c_str() );
}
//#############################################################################
