/******************************************************************************
*               File: mwMachSimIntegrationException.cpp                       *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  14.09.2005 16:25:53 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2005 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "mwMachSimIntegrationException.hpp"

namespace exceptions
{

//#############################################################################
	mwMachSimIntegrationException::mwMachSimIntegrationException( 
		const cause& exceptionCause, const misc::mwstring& addInfo )
		:mwException( 0, _T("") ), m_cause( exceptionCause )
	{
		misc::mwstring errorMessage;
		switch( m_cause )
		{
		case NULL_POINTER_USE:
			errorMessage = _T("A null pointer was used!");
			break;
		};
	
		if( addInfo != _T("") )
		{
			errorMessage += _T(" (");
			errorMessage += addInfo;
			errorMessage += _T(")");
		}
		
		SetErrorMessage( errorMessage );
	}

//#############################################################################
	const mwMachSimIntegrationException::cause& 
						mwMachSimIntegrationException::GetCause() const
	{
		return m_cause;
	}
};
