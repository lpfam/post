/******************************************************************************
*               file: 	MachSimIntegrationSample.h 						      *
*******************************************************************************
*               Description:                                                  *
*	Description of CMachSimIntegrationApp class								  *
*******************************************************************************
*               History:                                                      *
*  2006/12/23 23:14:54		Created by Eugene Hodzhaev						  *
*  2007/04/06 10:57:00		Updated by Mihai Vasilian						  *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/


#ifndef		__MACHSIMINTEGRATIONSAMPLE_H__
#define		__MACHSIMINTEGRATIONSAMPLE_H__

#ifndef __AFXWIN_H__
	#error include 'StdAfx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMachSimIntegrationApp:
// See MachSimIntegrationSample.cpp for the implementation of this class
//

class CMachSimIntegrationApp : public CWinApp
{
public:
	CMachSimIntegrationApp();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMachSimIntegrationApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMachSimIntegrationApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

extern CMachSimIntegrationApp theApp;

#endif // !defined(__MACHSIMINTEGRATIONSAMPLE_H__)
