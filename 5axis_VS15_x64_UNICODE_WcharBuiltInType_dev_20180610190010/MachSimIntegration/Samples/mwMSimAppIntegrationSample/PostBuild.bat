SET CONFIGURATION_TYPE=%1

if "%CONFIGURATION_TYPE%" == "Debug" (SET DEBUGMODE=TRUE)
if "%CONFIGURATION_TYPE%" == "DebugUnicode" (SET DEBUGMODE=TRUE)

if "%DEBUGMODE%" == "TRUE" GOTO DEBUG_SECTION

GOTO RELEASE_SECTION 

:RELEASE_SECTION
SET QUICKSTART_FOLDER=quickstart
GOTO COMMON_SECTION

:DEBUG_SECTION
SET QUICKSTART_FOLDER=quickstart_debug
GOTO COMMON_SECTION

:COMMON_SECTION
copy                        .\%CONFIGURATION_TYPE%\mwMSimAppIntegrationSample.exe                        ..\..\..\%QUICKSTART_FOLDER%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwsimutil.dll                        .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwMSimApp.dll                        .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwMachSim_res.dll                        .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwMSimOperationBuilder.dll  .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\multixpost.dll    .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwSupport.dll    .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwMSimCollisionChecker.dll    .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwVerifier.dll    .\%CONFIGURATION_TYPE%\
copy                        ..\..\..\%QUICKSTART_FOLDER%\mwJobSetup.dll    .\%CONFIGURATION_TYPE%\


