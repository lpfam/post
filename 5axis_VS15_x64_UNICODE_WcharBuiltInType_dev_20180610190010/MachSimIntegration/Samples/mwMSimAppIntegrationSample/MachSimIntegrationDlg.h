/******************************************************************************
*               file: 	MachSimIntegrationDlg.h 						      *
*******************************************************************************
*               Description:                                                  *
*	Description of CMachSimIntegrationDlg class								  *
*******************************************************************************
*               History:                                                      *
*  2006/12/23 23:14:54		Created by Eugene Hodzhaev						  *
*  2007/04/06 10:57:00		Updated by Mihai Vasilian						  *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/


#ifndef		__MACHSIMINTEGRATIONDLG_H__
#define		__MACHSIMINTEGRATIONDLG_H__

//! local header files
#include "GLWrapperDlg.hpp"
//! MachSim header files
#include "mwMachSimIniParams.hpp"
#include "mwMachSimStartUpParams.hpp"
#include "mwMachSimAxis.hpp"
#include "mwMachSimView.hpp"
//! misc header files
#include "mwActionKeyBinding.hpp"
#include "mwFileName.hpp"
#include "mwFileSystem.hpp"
#include "mwAutoPointer.hpp"
#include "mwMachSimOperationsProgrammeBuilder.hpp"
//! mfc header files
#include "afxcmn.h"
#include "afxwin.h"
#include "VerifierCustomizationsApplyer.h"

//! MachSim facade class (access to MachSim core functionality)
class mwMachSimFacade;

//Predefines
namespace exceptions
{
	//! local project exception class (derives from misc::mwException)
	class mwMachSimIntegrationException;
}

/////////////////////////////////////////////////////////////////////////////
// CMachSimIntegrationDlg dialog
class CMachSimIntegrationDlg : public CDialog
{
// Construction
public:
	//! list type with axis representation elements
	typedef std::list< mwMachSimAxis >			axesCollection;
	//! iterator of axis list
	typedef axesCollection::iterator			acIterator;
	//! const iterator of axis list
	typedef axesCollection::const_iterator		acConstIterator;

	//! dialog constructor
	CMachSimIntegrationDlg(CWnd* pParent = MW_NULL);	// standard constructor
	//! resizes MachSim GL view
	void OnGLSceneResize( unsigned int cx, unsigned int cy );
	//! redraw MachSim GL view
	void OnGLRepaint();

// Dialog Data
	//{{AFX_DATA(CMachSimIntegrationDlg)
	enum { IDD = IDD_MACHSIMINTEGRATIONSAMPLE_DIALOG };
	//! mapped values to dialog controls
	//! = RGB(red value)/255.0 
	float		m_bgred;
	//! = RGB(green value)/255.0
	float		m_bggreen;
	//! = RGB(blue value)/255.0
	float		m_bgblue;
	//! shows/hides toolpath
	BOOL		m_showtp;
	//! shows/hides machine housing
	BOOL		m_showmh;
	//! view name Iso/Top/Bottom/...
	CString		m_viewName;
	//! axis combobox
	CComboBox	m_axis;
	//! speed slider control
	CSliderCtrl m_speedCtrl;
	//! axis value
	double		m_value;
	//! toggles manual mode on/off
	BOOL		m_manual;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMachSimIntegrationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

protected:
	HICON m_hIcon;
	// Generated message map functions
	//{{AFX_MSG(CMachSimIntegrationDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnOK();
	afx_msg	void OnCancel();
	//!On button clicked initialize MachSim
	/*!  
		The MachSim Facade is built using:
			- a graphics parent window handle;
			- a machine definition dialog parent window handle;
			- a notification handling class derived from mwMachSimNotificationEventHandler;
			- a mwMachSimStartUpParams object, obtained through
			FetchStartupParams function.
		After construction, customization continues with calls to 
		LoadCustomKeyBindings and ApplyAutoLoadParameters functions.
	*/
	afx_msg void OnBnClickedInitMachsim();

	//!On button clicked shutdown MachSim
	/*!  
		The MachSim Facade is destroyed through its destroy member function.
	*/
	afx_msg void OnBnClickedShutdownMachsim();

	//!On button clicked load machine
	/*!  
		A new machine definition is loaded from a .xml file specified through a modal dialog.
		The Facade's LoadMachineDefinition(const misc::mwstring& fileName) call is used.
		/remark the following exceptions are handled:
		mwMachSimStateException, mwFileAccessException, mwMachSimMachDefException
	*/
	afx_msg void OnBnClickedLoadMachine();

	//!On button clicked load numerical control 
	/*!  
		A new .sim file, specified through a modal dialog, is loaded.
		The Facade's LoadToolPathDefinition(const misc::mwstring& fileName) call is used.
		/remark the following exceptions are handled:
		mwMachSimStateException, mwMachSimToolPathFormatException, mwFileAccessException
	*/
	afx_msg void OnBnClickedLoadNC();

	//!On button clicked save machine definition
	/*!  
		The current machine definition is saved to a .xml file, specified through a modal dialog.
		The Facade's SaveMachineDefinition(const misc::mwstring& fileName) call is used.
		/remark the following exceptions are handled:
		mwMachSimStateException, mwFileAccessException
	*/
	afx_msg void OnBnClickedSaveMd();

	//!On button clicked save as EXE
	/*! 
		A new presentation is created and packed into a self extracting .exe,
		through Facade's SaveAsExe(const misc::mwstring& fileName) call.
		/remark the following exception is handled:
			mwMachSimStateException
			
	*/
	afx_msg void OnBnClickedSaveExe();

	//!On button clicked play
	/*!  
		Starts the simulation through Facade's RunSimulation() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedPlay();

	//!On button clicked stop
	/*!  
		Stops the simulation through Facade's StopSimulation() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedStop();

	//!On button clicked pause
	/*!  
		Pauses the simulation through Facade's PauseSimulation() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedPause();

	//!On button clicked next operation
	/*!  
		Moves to next operation through Facade's GoToNextOperation() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedNextOp();

	//!On button clicked next move
	/*!  
		Goes to next move through Facade's GoToNextMove() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedNextMove();

	//!On button clicked previous move
	/*!
		Goes to previous move through Facade's GoToPrevMove() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedPrevMove();

	//!On button clicked previous operation
	/*!  
		Moves to previous operation through Facade's GoToPrevOperation() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedPrevOp();

	//!On NM released capture simulation speed
	/*!  
		Sets simulation speed according to the new active position of the simulation speed slider
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnNMReleasedcaptureSimulationSpeed(NMHDR *pNMHDR, LRESULT *pResult);
	
	//!On button clicked apply background color
	/*!  
		Sets a new background color through Facade's SetBGColor() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedApplyBgColor();
	
	//!On button clicked show toolpath
	/*!  
		Toggles display of the toolpath on/off through 
		Facade's EnableToolpath() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedShowTp();
	
	//!On button clicked show machine housing
	/*!  
		Toggles display of the machine housing on/off through 
		Facade's EnableMachineHousing() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedShowMh();
	
	//!On button clicked fit 
	/*!  
		Fits the current machine to the graphic representation window
		through Facade's FitToScreen() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedFit();
	
	//!On button clicked selection change view
	/*!  
		Sets a new view according with the changed selection through
		Facade's SetCurrentView() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnCbnSelchangeView();
	
	//!On button clicked capture view
	/*!  
		Captures the current view for subsequent retrieval using the
		view list box. Facade's GetCurrentView() call is used.
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedCaptureView();
	
	//!On button clicked modify keyboard bindings
	/*!  
		Modifies the graphical representation's keyboard bindings.
		This is achieved through two consecutive Facade calls:
			a) mwMachSimKeyboardBindings GetKeyboardBindings()
				- provides the scaffold to build the modifications on;
			b) SetKeyboardBindings(mwMachSimKeyboardBindings& newBindings)
				- makes the update.
		/remark the following exceptions are handled:
			mwMachSimStateException,mwMachSimUIBindingsException
	*/
	afx_msg void OnBnClickedModifyKbb();
	
	//!On button clicked modify mouse bindings
	/*!  
		Modifies the graphical representation's mouse bindings.
		This is achieved through two consecutive Facade calls:
			a) mwMachSimMouseBindings GetMouseBindings()
				- provides the scaffold to build the modifications on;
			b) SetMouseBindings(mwMachSimMouseBindings& newBindings)
				- makes the update.
		/remark the following exceptions are handled:
			mwMachSimStateException,mwMachSimUIBindingsException
	*/
	afx_msg void OnBnClickedModifyMb();
	
	//!On button clicked toggle manual axis control
	/*!  
		 Toggles manual axis control on/off through Facade's 
		 SetManualMode(const bool manualMode=true) call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedCheck1();
	
	//!On button clicked get axis
	/*!
		Gets available axis to feed the axis list box.
		This is achieved through Facade's GetAvailableAxes() call
		/remark the following exception is handled:
			mwMachSimStateException 
	*/
	afx_msg void OnBnClickedGetaxes();
	
	//!On button clicked apply axis value
	/*!  
		Sets a new value for the current selection in the axis list box,using
		Facade's SetAxisValue(const double newValue,const misc::mwstring& axisName) call
		/remark the following exceptions are handled:
			mwMachSimStateException, mwMachSimConfigMismatchException
	*/
	afx_msg void OnBnClickedApplyValue();
	
	//!On button clicked selection change axis
	/*!  
		Gets value for the current selection in the axis list box,
		using Facade's GetAxesValue(const misc::mwstring& axisName) call
		/remark the following exceptions are handled:
			mwMachSimStateException,mwMachSimConfigMismatchException
	*/
	afx_msg void OnCbnSelchangeAxis();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
private:
	//! CreateWrapperDialogs
	/*! Creates GL dialog and Machine Definition dialog, and makes them visible
	*/
	void CreateWrapperDialogs();

	//!MachSim integration exception message
	/*! Displays a message depending on the specific cause retrieved from
		the mwMachSimIntegrationException class object
		\param ex of type mwMachSimIntegrationException
	*/
	void MachSimIntegrationExceptionMessage(const exceptions::mwMachSimIntegrationException& ex);
	
	//!Get .ini filename
	/*! For retrieval the current .exe's filename is used  
			\returns .ini filename of type misc::mwFileName
	*/
	misc::mwFileName GetIniFileName();

	//!Get text resource filename
	/*! If the .ini file doesn't specify it, mwMachSim_res.dll is used instead
			\returns text resource filename of type misc::mwFileName
	*/
	misc::mwFileName GetResourceFileName();

	//!Fetch startup parameters
	/*! The function returns a mwMachSimStartUpParams class object (which will
		be used in the initialization call for the MachSimFacade) constructed
		using data from the MachSim's .ini file.
		If this file is not found, some default values will be used instead.
		\returns startup parameters of type mwMachSimStartUpParams
	*/
	mwMachSimStartUpParams FetchStartupParams();

	//!Load user interface hot key bindings
	/*! Loads custom mouse and keyboard bindings for MachSim's graphical interface,
		in two sets:default and current.
			\remark the keylist filenames are retrieved using the .ini file.
				If it's not found, the following names will be used instead:
				default.keymap, current.keymap
	*/
	void LoadCustomKeyBindings();
	
	//!Apply autoload parameters
	/*!
		This is called after the MachSimFacade has been built.
		If the .ini file exists and its autoload parameter is set to 1, the
		following actions will take place:
			- machine definition is loaded from the .ini specified .xml file
			- simulation is loaded from the .ini specified .sim file
			- if autostart .ini flag is set to 1, the simulation is started
	*/
	void ApplyAutoloadParameters();
	
	//!Check facade pointer
	/*!
		If the member pointer to facade is MW_NULL, a mwMachSimIntegrationException
		will be thrown
	*/
	void CheckFacadePointer() ;

	//! MachSim core facade
	mwMachSimFacade*					m_machSimFacade;
	//! MachSim main frame window
	HWND								m_machSimWndHandle;
	//! MachSim view window
	mwMachSimView						m_capturedView;
	//! Was a view capture made
	bool								m_viewCaptureMade;
	//! Autopointer to OpenGL dialog class
	misc::mwAutoPointer< GLWrapperDlg > m_glFrame;

	misc::mwAutoPointer<cadcam::mwTMesh<float>>   m_workpieceMesh;
	misc::mwAutoPointer<cadcam::mwTMesh<float>>   m_stockMesh;

	misc::mwAutoPointer<VerifierCustomizationsApplyer> m_myHandler;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__MACHSIMINTEGRATIONDLG_H__)
