/******************************************************************************
*               File: GLWrapperDlg.cpp                                        *
*******************************************************************************
*               Description: implementation of GLWrapperDlg class             *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  2/3/2006 5:59:25 PM Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2005 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "GLWrapperDlg.hpp"
#include "MachSimIntegrationDlg.h"

BEGIN_MESSAGE_MAP(GLWrapperDlg, CDialog)
//{{AFX_MSG_MAP(GLWrapperDlg)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//#############################################################################
GLWrapperDlg::GLWrapperDlg( CMachSimIntegrationDlg* pParent /*=MW_NULL*/ )
:CDialog( GLWrapperDlg::IDD, (CWnd*) pParent )
,m_pParentDlg( pParent )
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_MACHSIMINTEGRATIONSAMPLE);

}

//#############################################################################
BOOL GLWrapperDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

//#############################################################################
int GLWrapperDlg::OnCreate( LPCREATESTRUCT /*lpCreateStruct*/ )
{
	return true;
}

//#############################################################################
HCURSOR GLWrapperDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//#############################################################################
void GLWrapperDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	m_pParentDlg->OnGLSceneResize( cx, cy );

	m_pParentDlg->OnGLRepaint();
}

//#############################################################################
void GLWrapperDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		m_pParentDlg->OnGLRepaint();

		CDialog::OnPaint();
	}

}

//#############################################################################
void GLWrapperDlg::OnDestroy()
{

}