/******************************************************************************
  (C) 2016 by ModuleWorks GmbH
  Author: Costin Calisov
******************************************************************************/
#ifndef __VerifierCustomizationsApplyer_hpp__
#define __VerifierCustomizationsApplyer_hpp__

#include "mwMachSimPluginNotificationHandler.hpp"
#include "mwCutMaterialTextureHandlerParametersDecl.hpp"

class mwMachSimFacade;

#pragma warning(push)
#pragma warning(disable: 4996)
class VerifierCustomizationsApplyer : public mwMachSimPluginNotificationHandler
{
public:

	VerifierCustomizationsApplyer(mwMachSimFacade* machSimFacade, HWND controlWindow) 
		:m_machSimFacade(machSimFacade), m_controlWindow(controlWindow){};


	virtual void OnVerificationStarted(mwMachSimVerifier* verifier, size_t verifierId);

	misc::mwAutoPointer <VerifierUtil::mwCutMaterialTextureHandlerParameters> m_textureHandler;
private:
	void SetupNumberOfNails(mwMachSimVerifier* verifier);
	void SetupWoodTextureHandler(mwMachSimVerifier* verifier);
	void SetupAutoQualityImprovmentOn(mwMachSimVerifier* verifier);

	mwMachSimFacade*	m_machSimFacade;
	HWND				m_controlWindow;

};
#pragma warning(pop)

#endif
