//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by mwMSimAppIntegrationSample.rc
//
#define ID_INIT_MACHSIM                 3
#define ID_SHUTDOWN_MACHSIM             5
#define IDD_MACHSIMINTEGRATIONSAMPLE_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME2                  129
#define IDI_ICON1                       132
#define IDI_MACHSIMINTEGRATIONSAMPLE    132
#define IDD_GLVIEW                      133
#define ID_MACHSIM                      1000
#define IDC_LOAD_MACHINE                1001
#define IDC_LOADNC                      1002
#define IDC_ENABLE_GUI                  1003
#define IDC_PREV_OP                     1004
#define IDC_BGR                         1005
#define IDC_PREV_MOVE                   1006
#define IDC_NEXT_MOVE                   1007
#define IDC_NEXT_OP                     1008
#define IDC_PAUSE                       1009
#define IDC_STOP                        1010
#define IDC_PLAY                        1011
#define IDC_BGG                         1012
#define IDC_BGB                         1013
#define IDC_APPLY_BG_COLOR              1014
#define IDC_FIT                         1015
#define IDC_BUTTON3                     1016
#define IDC_SAVE_MD                     1016
#define IDC_SHOW_TP                     1017
#define IDC_SHOW_MH                     1018
#define IDC_VIEW                        1019
#define IDC_CAPTURE_VIEW                1020
#define IDC_MODIFY_KBB                  1021
#define IDC_MODIFY_MB                   1022
#define IDC_SIMULATION_SPEED            1023
#define IDC_GETAXES                     1024
#define IDC_AXES                        1025
#define IDC_AXIS_VALUE                  1026
#define IDC_APPLY_VALUE                 1027
#define IDC_CHECK1                      1028
#define IDC_BUTTON1                     1029
#define IDC_SAVE_EXE                    1029
#define IDC_TEST1                       1030
#define IDC_CADRU                       1031
#define IDC_PROGRESS_TEXT               1032

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1034
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
