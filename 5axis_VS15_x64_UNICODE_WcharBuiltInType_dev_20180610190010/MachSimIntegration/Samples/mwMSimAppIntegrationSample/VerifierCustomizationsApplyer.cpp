#include "stdafx.h"
#include "Resource.h"
#include "VerifierCustomizationsApplyer.h"
#include "mwIMachSimVerification.hpp"
#include "mwMachSimFacade.hpp"

class WindowsRefineNotifier : public mwMachSimVerifier::RefineNotifier
{
public:

	WindowsRefineNotifier(const HWND windowHandle,HWND controlWindow)
		:m_windowHandle(windowHandle),m_controlWindow(controlWindow)
	{  }

	void OnRedrawNeeded()
	{
		// this is how it is done in VAS (winapi is thread safe):
		if (m_windowHandle != 0)
		{
			::RedrawWindow(m_windowHandle, 0, 0, RDW_ALLCHILDREN | RDW_INVALIDATE);
		}
	}

	virtual void OnRefineStarted(const bool ) 
	{
		misc::mwstring text (_T("AutoQualityImprove 0%)"));
		SetDlgItemText(m_controlWindow,IDC_PROGRESS_TEXT , text.c_str());
	}

	virtual void OnRefineCompleted() 
	{
		misc::mwstring text (_T(""));
		SetDlgItemText(m_controlWindow,IDC_PROGRESS_TEXT , text.c_str());
	}


	virtual void OnProgressUpdate(const float progress)
	{
		misc::mwstring text (_T("AutoQualityImprove "));
		text += misc::from_value(progress*100,1);
		text += _T("%");
		SetDlgItemText(m_controlWindow,IDC_PROGRESS_TEXT , text.c_str());
	}


	virtual void OnRefineAborted()
	{
		misc::mwstring text (_T(""));
		SetDlgItemText(m_controlWindow,IDC_PROGRESS_TEXT , text.c_str());
	}

private:
	HWND m_windowHandle;
	HWND m_controlWindow;
};

void VerifierCustomizationsApplyer::OnVerificationStarted(mwMachSimVerifier* verifier, size_t /*verifierId*/)
{
	SetupNumberOfNails(verifier);
	SetupWoodTextureHandler(verifier);
	SetupAutoQualityImprovmentOn(verifier);
}

void VerifierCustomizationsApplyer::SetupNumberOfNails(mwMachSimVerifier* verifier)
{
	verifier->SetPrecision(0.1f);
}

void VerifierCustomizationsApplyer::SetupWoodTextureHandler(mwMachSimVerifier* verifier)
{
	/*this just setup the handler.
		To really activate wood you have to call after load simulation is done the:
		mwIMachSimVerification::SetAlternateStockColor and mwIMachSimVerification::SetDrawMode
		see CMachSimIntegrationDlg::OnBnClickedLoadNC for a sample
	*/
	if (m_textureHandler.IsNull())
	{
		misc::mwstring machinePath = m_machSimFacade->GetMachineFileName();
		size_t pathIndex = machinePath.rfind("\\");
		machinePath = (pathIndex>0) ? machinePath.substr(0,pathIndex+1): "";

		misc::mwstring textureNameCut="";
		misc::mwstring textureNameUnCut="";
		if (misc::mwFileSystem::FileExists(machinePath+"wood_cut.jpg"))
		{
			textureNameCut = machinePath+"wood_cut.jpg";
		}
		if (misc::mwFileSystem::FileExists(machinePath+"wood_uncut.jpg"))
		{
			textureNameUnCut = machinePath+"wood_uncut.jpg";
		}
		if (!textureNameCut.empty() && !textureNameUnCut.empty())
		{
			const float textureCutWidth = 16.0;
			const float textureCutHeight = 16.0;
			const float textureUnCutWidth = 80.0;
			const float textureUnCutHeight = 80.0;
			m_textureHandler = new VerifierUtil::mwCutMaterialTextureHandlerParameters(textureNameCut,textureCutWidth,textureCutHeight, 
				textureNameUnCut, textureUnCutWidth, textureUnCutHeight);
			verifier->SetTextureHandler(&*m_textureHandler);
		}
	}
}

void VerifierCustomizationsApplyer::SetupAutoQualityImprovmentOn(mwMachSimVerifier* verifier)
{
	const HWND windowHandle = m_machSimFacade->GetViewService().GetViewHWND();
	misc::mwAutoPointer<mwMachSimVerifier::RefineNotifier> notifier =
		new WindowsRefineNotifier(windowHandle, m_controlWindow);
	verifier->SetRefineNotifier(notifier);
	verifier->StartAutoRefine(1000);
	VerifierUtil::mwRefineHelpers::RefineLayout layout= VerifierUtil::mwRefineHelpers::ComputeFeatureLayout();
	verifier->UpdateAutoRefineLayout(layout);
}
