/******************************************************************************
*               file: 	MachSimIntegrationSample.cpp					      *
*******************************************************************************
*               Description:                                                  *
*	Implementation of CMachSimIntegrationApp class							  *
*******************************************************************************
*               History:                                                      *
*  2006/12/23 23:14:54		Created by Eugene Hodzhaev						  *
*  2007/04/06 10:57:00		Updated by Mihai Vasilian						  *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/



#include "StdAfx.h"
#include "MachSimIntegrationSample.h"
#include "MachSimIntegrationDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static _TCHAR THIS_FILE[] = _T(__FILE__);
#endif

//#############################################################################
// CMachSimIntegrationApp

BEGIN_MESSAGE_MAP(CMachSimIntegrationApp, CWinApp)
	//{{AFX_MSG_MAP(CMachSimIntegrationApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


//#############################################################################
// CMachSimIntegrationApp construction
CMachSimIntegrationApp::CMachSimIntegrationApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

//#############################################################################
// The one and only CMachSimIntegrationApp object

CMachSimIntegrationApp theApp;

//#############################################################################
// CMachSimIntegrationApp initialization
BOOL CMachSimIntegrationApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	InitCommonControls();

	CWinApp::InitInstance();

	AfxEnableControlContainer();


	CMachSimIntegrationDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
//#############################################################################



//#############################################################################