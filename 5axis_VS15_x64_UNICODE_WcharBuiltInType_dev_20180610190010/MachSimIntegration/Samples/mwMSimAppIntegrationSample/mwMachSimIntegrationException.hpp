/******************************************************************************
*               File: mwMachSimIntegrationException.hpp	                      *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  14.09.2005 16:25:08 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2005 by ModuleWorks GmbH                                  *
******************************************************************************/

#ifndef __mwMachSimIntegrationException_hpp__
#define __mwMachSimIntegrationException_hpp__

#include <mwException.hpp>

namespace exceptions
{
	class mwMachSimIntegrationException : public misc::mwException
	{
	public:

		enum cause
		{
			NULL_POINTER_USE
		};

		mwMachSimIntegrationException( const cause &what, 
			const misc::mwstring &addInfo  = _T("") );

		const cause &GetCause() const;
	protected:
	private:
		cause m_cause;
	};

};

#endif //__mwMachSimIntegrationException_hpp__
