/******************************************************************************
*               File: GLWrapperDlg.hpp                                        *
*******************************************************************************
*               Description: definition of GLWrapperDlg class                 *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  2/3/2006 5:59:11 PM Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __GLWrapperDlg_hpp__
#define __GLWrapperDlg_hpp__

#include "resource.h"
class CMachSimMoveApplierIntegrationDlg;

class GLWrapperDlg : public CDialog
{
public:
	GLWrapperDlg( CMachSimMoveApplierIntegrationDlg* pParent = MW_NULL );
	enum { IDD = IDD_GLVIEW };
		
protected:
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	afx_msg int  OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	CMachSimMoveApplierIntegrationDlg*		m_pParentDlg;
};


#endif //__GLWrapperDlg_hpp__

