#include "StdAfx.h"
#include "mw5AxMoveGenerator.h"
#include "mwFileStream.hpp"
#include "mwStringTokenizer.hpp"
#include "mwStringOps.hpp"



#define MAX_POSTED_MOVES			16
#define MAX_AXIS					6

mw5AxMoveGeneratorFromList::mw5AxMoveGeneratorFromList(misc::mwFileName const & moveDefFile, const size_t numAxis)
{
	misc::mwIFStream str( moveDefFile.GetFilePath().ToAscii().c_str() );
	// TODO:
		
	for (;;)
	{
		// read line
		misc::mwstring line;
		getline( str, line );
		if (str.fail())
		{
			break;
		}
			
		// skip empty lines and comments
		if (line.size() == 0 || line[0] == _T('#'))
		{
			continue;
		}
		misc::mwStringTokenizer tok(_T(" \t"));
		tok.Parse(line);

		misc::mwStringTokenizer::tokenListIterator iter = tok.GetTokenBegin(),
			iterEnd = tok.GetTokenEnd();

		AxesValues av;
		av.reserve( numAxis );
		for (;iter != iterEnd; ++iter)
		{
			float toPush;
			misc::to_value(*iter, toPush);
			av.push_back(toPush);
		}
		if (numAxis != av.size()) {
			throw misc::mwException(0, _T("Move does not a position for each axis"));
		}
		m_allPositions.push_back(av);

		
	}

#if 0

	float matrix[][MAX_AXIS]=
	{   //  X           Y			Z            C			B
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 400.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
		{ 470.0f,		800.0f,		800.0f,		0.00000f,	0.00000f, 0.000f },//
	};


	float matrix[][MAX_AXIS]=
	{   //  X           Y			Z            C			B
		{ -30.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -25.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ -20.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ -15.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ -10.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ -5.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 0.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 5.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 10.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 15.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 20.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 25.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 30.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 35.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 40.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ 45.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ -30.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ -30.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},
		{ -30.0f,		-13.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		-11.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		-9.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		-7.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		-5.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		-3.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		-1.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		1.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		3.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		5.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		7.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ -30.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ 30.0f,		9.0f,		170.0f,		0.00000f,	-45.00000f},//
		{ 30.0f,		9.0f,		145.0f,		0.00000f,	-45.00000f},//
		{ 30.0f,		9.0f,		140.0f,		0.00000f,	-45.00000f},//
		{ 30.0f,		9.0f,		120.0f,		0.00000f,	-45.00000f},//
	};
#endif

#if 0
	float matrix[MAX_POSTED_MOVES][MAX_AXIS]=
	{   //  X           Y			Z            C        B
		{ -70.93674f, 44.90300f,	230.97273f,   0.00000f,   -45.00000f},//
		{ -70.93674f, 44.90300f,	190.97273f,   0.00000f,   -45.00000f},//
		{ -70.93674f, 44.90300f,	182.97173f,   0.00000f,   -45.00000f},//
		{ -72.93374f, 44.90300f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -72.93374f, 59.78000f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -73.32974f, 61.73018f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -74.45501f, 64.44383f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -77.93374f, 64.78000f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -85.36374f, 64.78000f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -87.31391f, 64.38400f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -88.95518f, 63.25873f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -90.02756f, 61.58242f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -90.36374f, 59.78000f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -90.36374f, 30.64500f,	170.97173f,   0.00000f,   -45.00000f},//
		{ -89.96774f, 28.69482f,	170.97173f,   0.00000f,   -45.00000f},//
		{ 0.00000f,	 0.00000f,	100.00000f,	 0.00000f,   -45.00000f}//
	};


	m_allPositions.reserve(MAX_POSTED_MOVES);
	for ( int mIdx = 0; mIdx < MAX_POSTED_MOVES; ++mIdx )
	{
		AxesValues av;
		av.reserve(MAX_AXIS) ;
		for ( int a = 0; a < MAX_AXIS; ++a )
		{
			av.push_back(matrix[mIdx][a]) ;
		}
		m_allPositions.push_back(av);
	}

#endif
}

mw5axMoveGeneratorFromFunction::mw5axMoveGeneratorFromFunction(misc::mwFileName const & moveDefFile, const size_t numAxis)
{
	misc::mwIFStream str( moveDefFile.GetFilePath().ToAscii().c_str() );
	// TODO:

	int index = 0;
	for (;;)
	{
		// read line
		misc::mwstring line;
		getline( str, line );
		if (str.fail())
		{
			break;
		}

		// skip empty lines and comments
		if (line.size() == 0 || line[0] == _T('#'))
		{
			continue;
		}
		misc::mwStringTokenizer tok(_T(" \t"));
		tok.Parse(line);

		misc::mwStringTokenizer::tokenListIterator iter = tok.GetTokenBegin(),
			iterEnd = tok.GetTokenEnd();

		if (index == 0)
		{
			AxesValues av;
			av.reserve( numAxis );
			for (;iter != iterEnd; ++iter)
			{
				float toPush;
				misc::to_value(*iter, toPush);
				av.push_back(toPush);
			}
			if (numAxis != av.size()) {
				throw misc::mwException(0, _T("Move does not a position for each axis"));
			}

			m_absStartPos = av;
		}
		else if (index == 1)
		{
			AxesValues av;
			av.reserve( numAxis );
			for (;iter != iterEnd; ++iter)
			{
				float toPush;
				misc::to_value(*iter, toPush);
				av.push_back(toPush);
			}
			if (numAxis != av.size()) {
				throw misc::mwException(0, _T("Move does not a position for each axis"));
			}

			m_absEndPos = av;
		}
		else if (index == 2)
		{
			if (tok.GetSize() != 3)
			{
				throw misc::mwException(0, _T("Please specify axis indices correctly"));
			}

			misc::to_value(*(iter++), m_axis1idx);
			misc::to_value(*(iter++), m_axis2idx);
			misc::to_value(*(iter++), m_axis3idx);
		}
		else if (index == 3)
		{
			if (tok.GetSize() != 1)
			{
				throw misc::mwException(0, _T("Please specify removal correctly"));
			}

			misc::to_value(*(iter++), m_removal);
		}
		else if (index == 4)
		{
			if (tok.GetSize() != 1)
			{
				throw misc::mwException(0, _T("Please specify depth step correctly"));
			}

			misc::to_value(*(iter++), m_deltaAxis3);
		}

		index++;
	}

	CheckSignOfDeltas();
	MoveToStart();

}

void mw5AxMoveGeneratorCornedSpiral::MoveNextMove()
{
	float deltaAxis1 = m_deltaAxis1;
	float deltaAxis2 = m_deltaAxis2;
	float deltaAxis3 = m_deltaAxis3;
	float removal = m_removal;
	switch (m_moveDir)
	{
	case PlusAxis1:
		{
			double remainingDeltaAxis1 = m_relEndPos[m_axis1idx] - m_curPos[m_axis1idx];
			if (fabs(remainingDeltaAxis1) > fabs(deltaAxis1))
			{
				m_curPos[m_axis1idx] += deltaAxis1;
			}
			else
			{
				m_curPos[m_axis1idx] = m_relEndPos[m_axis1idx];
				m_moveDir = PlusAxis2;
			}
		}
		break;
	case PlusAxis2:
		{
			double remainingDeltaAxis2 = m_relEndPos[m_axis2idx] - m_curPos[m_axis2idx];
			if (fabs(remainingDeltaAxis2) > fabs(deltaAxis2))
			{
				m_curPos[m_axis2idx] += deltaAxis2;
			}
			else
			{
				m_curPos[m_axis2idx] = m_relEndPos[m_axis2idx];
				m_moveDir = MinusAxis1;
			}
		}
		break;
	case MinusAxis1:
		{
			double remainingDeltaAxis1 = m_curPos[m_axis1idx] - m_relStartPos[m_axis1idx];
			if (fabs(remainingDeltaAxis1) > fabs(deltaAxis1))
			{
				m_curPos[m_axis1idx] -= deltaAxis1;
			}
			else
			{
				m_curPos[m_axis1idx] = m_relStartPos[m_axis1idx];
				m_moveDir = MinusAxis2;
			}
		}
		break;
	case MinusAxis2:
		{
			double remainingDeltaAxis2 = m_curPos[m_axis2idx] - m_relStartPos[m_axis2idx];
			if (fabs(remainingDeltaAxis2) > fabs(deltaAxis2))
			{
				m_curPos[m_axis2idx] -= deltaAxis2;
			}
			else
			{
				m_curPos[m_axis2idx] = m_relStartPos[m_axis2idx];
				m_moveDir = ToCenter;
			}
		}
		break;
	case ToCenter:
		{
			double remainingDeltaAxis1 = m_relEndPos[m_axis1idx] - m_relStartPos[m_axis1idx];
			double remainingDeltaAxis2 = m_relEndPos[m_axis2idx] - m_relStartPos[m_axis2idx];
			if (fabs(remainingDeltaAxis1) < fabs(2 * removal)
				|| fabs(remainingDeltaAxis2) < fabs(2 * removal) )
			{
				if (fabs(remainingDeltaAxis1) >= fabs(2 * removal) )
				{
					m_curPos[m_axis1idx] += removal;
					m_curPos[m_axis2idx] = (m_relEndPos[m_axis2idx] + m_relStartPos[m_axis2idx]) / 2;
				}
				else if (fabs(remainingDeltaAxis2) >= fabs( 2 * removal) )
				{
					m_curPos[m_axis1idx] = (m_relEndPos[m_axis1idx] + m_relStartPos[m_axis1idx]) / 2;
					m_curPos[m_axis2idx] += removal; 
				}
				else
				{
					m_curPos[m_axis1idx] = (m_relEndPos[m_axis1idx] + m_relStartPos[m_axis1idx]) / 2;
					m_curPos[m_axis2idx] = (m_relEndPos[m_axis2idx] + m_relStartPos[m_axis2idx]) / 2;
				}

				m_moveDir = ToStart;
			}
			else
			{
				m_relStartPos[m_axis1idx] += removal;
				m_relStartPos[m_axis2idx] += m_removal;

				m_relEndPos[m_axis1idx] -= removal;
				m_relEndPos[m_axis2idx] -= removal;

				m_curPos = m_relStartPos;
				m_moveDir = PlusAxis1;
			}
		}
		break;
	case ToStart:
		{
			double remainingDeltaAxis1 = m_curPos[m_axis1idx] - m_absStartPos[m_axis1idx];
			double remainingDeltaAxis2 = m_curPos[m_axis2idx] - m_absStartPos[m_axis2idx];
			if (fabs(remainingDeltaAxis1) < fabs(removal)
				|| fabs(remainingDeltaAxis2) < fabs(removal) )
			{
				if (fabs(remainingDeltaAxis1) >= fabs(removal))
				{
					m_curPos[m_axis1idx] -= removal;
					m_curPos[m_axis2idx] = m_absStartPos[m_axis2idx];
				}
				else if (fabs(remainingDeltaAxis2) >= fabs(removal))
				{
					m_curPos[m_axis1idx] = m_absStartPos[m_axis1idx];
					m_curPos[m_axis2idx] -= removal;;
				}
				else
				{
					m_curPos[m_axis1idx] = m_absStartPos[m_axis1idx];
					m_curPos[m_axis2idx] = m_absStartPos[m_axis2idx];

					m_relStartPos[m_axis1idx] = m_absStartPos[m_axis1idx];
					m_relStartPos[m_axis2idx] = m_absStartPos[m_axis2idx];
					m_relEndPos[m_axis1idx] = m_absEndPos[m_axis1idx];
					m_relEndPos[m_axis2idx] = m_absEndPos[m_axis2idx];

					m_moveDir = Down;
				}
			}
			else
			{
				m_curPos[m_axis1idx] -= removal;
				m_curPos[m_axis2idx] -= removal;
			}
		}
		break;
	case Down:
		{
			double remainingDeltaAxis3 = m_curPos[m_axis3idx] - m_absEndPos	[m_axis3idx];
			if (fabs(remainingDeltaAxis3) < 1e-5)
			{
				m_reachedEnd = true;
			}
			else if (fabs(remainingDeltaAxis3) < fabs(deltaAxis3) )
			{
				m_curPos[m_axis3idx] = m_absEndPos[m_axis3idx];
				m_relStartPos[m_axis3idx] = m_absEndPos[m_axis3idx];
				m_relEndPos[m_axis3idx] = m_absEndPos[m_axis3idx];

				m_moveDir = PlusAxis1;
			}
			else
			{
				m_curPos[m_axis3idx] += deltaAxis3;
				m_relStartPos[m_axis3idx] += deltaAxis3;
				m_relEndPos[m_axis3idx] += deltaAxis3;

				m_moveDir = PlusAxis1;
			}
		}
		break;
	default:
		m_reachedEnd = true;
	}
	m_currentIdx++;
};

void mw5AxMoveGeneratorParellelLines::MoveNextMove()
{
	float deltaAxis1 = m_deltaAxis1;
	float deltaAxis3 = m_deltaAxis3;
	float removal = m_removal;

	switch (m_moveDir)
	{
	case PlusAxis1:
		{
			double remainingDeltaAxis1 = m_relEndPos[m_axis1idx] - m_curPos[m_axis1idx];
			if (fabs(remainingDeltaAxis1) > fabs(deltaAxis1))
			{
				m_curPos[m_axis1idx] += deltaAxis1;
			}
			else
			{
				m_curPos[m_axis1idx] = m_relEndPos[m_axis1idx];
				m_moveDir = PlusAxis2;
			}
		}
		break;
	case MinusAxis1:
		{
			double remainingDeltaAxis1 = m_curPos[m_axis1idx] - m_relStartPos[m_axis1idx];
			if (fabs(remainingDeltaAxis1) > fabs(deltaAxis1))
			{
				m_curPos[m_axis1idx] -= deltaAxis1;
			}
			else
			{
				m_curPos[m_axis1idx] = m_relStartPos[m_axis1idx];
				m_moveDir = PlusAxis2;
			}
		}
		break;
	case PlusAxis2:
		{
			double remainingDeltaAxis1 = m_relEndPos[m_axis1idx] - m_curPos[m_axis1idx];
			double remainingDeltaAxis2 = m_relEndPos[m_axis2idx] - m_curPos[m_axis2idx];
			if (fabs(remainingDeltaAxis2) < 1e-5)
			{
				m_curPos[m_axis2idx] = m_relEndPos[m_axis2idx];
				m_moveDir = ToStart;
			}
			else 
			{
				if (fabs(remainingDeltaAxis2) > fabs(removal))
				{
					m_curPos[m_axis2idx] += removal;
				}
				else
				{
					m_curPos[m_axis2idx] = m_relEndPos[m_axis2idx];
				}

				if (fabs(remainingDeltaAxis1) < 1e-5)
				{
					m_moveDir = MinusAxis1;
				}
				else
				{
					m_moveDir = PlusAxis1;
				}
			}
		}
		break;
	case ToStart:
		{
			double remainingDeltaAxis1 = m_curPos[m_axis1idx] - m_absStartPos[m_axis1idx];
			double remainingDeltaAxis2 = m_curPos[m_axis2idx] - m_absStartPos[m_axis2idx];
			if (fabs(remainingDeltaAxis1) < fabs(removal)
				|| fabs(remainingDeltaAxis2) < fabs(removal) )
			{
				if (fabs(remainingDeltaAxis1) >= fabs(removal))
				{
					m_curPos[m_axis1idx] -= removal;
					m_curPos[m_axis2idx] = m_absStartPos[m_axis2idx];
				}
				else if (fabs(remainingDeltaAxis2) >= fabs(removal))
				{
					m_curPos[m_axis1idx] = m_absStartPos[m_axis1idx];
					m_curPos[m_axis2idx] -= removal;;
				}
				else
				{
					m_curPos[m_axis1idx] = m_absStartPos[m_axis1idx];
					m_curPos[m_axis2idx] = m_absStartPos[m_axis2idx];

					m_relStartPos[m_axis1idx] = m_absStartPos[m_axis1idx];
					m_relStartPos[m_axis2idx] = m_absStartPos[m_axis2idx];
					m_relEndPos[m_axis1idx] = m_absEndPos[m_axis1idx];
					m_relEndPos[m_axis2idx] = m_absEndPos[m_axis2idx];

					m_moveDir = Down;
				}
			}
			else
			{
				m_curPos[m_axis1idx] -= removal;
				m_curPos[m_axis2idx] -= removal;
			}
		}
		break;
	case Down:
		{
			double remainingDeltaAxis3 = m_curPos[m_axis3idx] - m_absEndPos	[m_axis3idx];
			if (fabs(remainingDeltaAxis3) < 1e-5)
			{
				m_reachedEnd = true;
			}
			else if (fabs(remainingDeltaAxis3) < fabs(deltaAxis3) )
			{
				m_curPos[m_axis3idx] = m_absEndPos[m_axis3idx];
				m_relStartPos[m_axis3idx] = m_absEndPos[m_axis3idx];
				m_relEndPos[m_axis3idx] = m_absEndPos[m_axis3idx];

				m_moveDir = PlusAxis1;
			}
			else
			{
				m_curPos[m_axis3idx] += deltaAxis3;
				m_relStartPos[m_axis3idx] += deltaAxis3;
				m_relEndPos[m_axis3idx] += deltaAxis3;

				m_moveDir = PlusAxis1;
			}
		}
		break;
	default:
		m_reachedEnd = true;
	}
	m_currentIdx++;
};
