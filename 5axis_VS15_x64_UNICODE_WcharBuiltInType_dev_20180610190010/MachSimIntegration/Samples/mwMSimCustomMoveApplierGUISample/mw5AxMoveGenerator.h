#ifndef __5AXMOVEGENERATOR
#define __5AXMOVEGENERATOR

#include "mwAutoPointer.hpp"
#include "mwCNCMove.hpp"
#include "mw5axMove.hpp"
#include "mwFileName.hpp"
#include "mwCriticalSection.hpp"

class mw5AxMoveGenerator
{
public:
	typedef std::vector<float> AxesValues;

public:
	virtual ~mw5AxMoveGenerator()
	{}

	virtual bool			ReachedEndOfMoveList() const = 0;
	virtual size_t			GetMoveIdx() const = 0;

	virtual void			MoveToStart() = 0;
	virtual void			MoveNextMove() = 0;
	virtual void			SetMaxMoveLength(float /* maxLength */) 
	{} ;
	
	virtual AxesValues const & GetAxesValues() const = 0;
};

class mw5AxMoveGeneratorFromList : public mw5AxMoveGenerator
{
private:
	typedef std::vector<AxesValues> AxesValuesList;
	AxesValuesList	m_allPositions;
	size_t m_currentIdx;

public:
	mw5AxMoveGeneratorFromList(misc::mwFileName const & moveDefFile, const size_t numAxis);
	virtual ~mw5AxMoveGeneratorFromList()
	{}

	virtual bool			ReachedEndOfMoveList() const 
	{
		return m_currentIdx >= m_allPositions.size();
	};
	virtual size_t			GetMoveIdx() const
	{
		return m_currentIdx;
	};

	virtual void			MoveToStart()
	{
		m_currentIdx = 0;
	}
	virtual void			MoveNextMove()
	{
		m_currentIdx++;
	};

	virtual AxesValues const & GetAxesValues() const
	{
		return m_allPositions[m_currentIdx];
	}
};

class mw5axMoveGeneratorFromFunction : public mw5AxMoveGenerator
{
public:
	mw5axMoveGeneratorFromFunction(
		misc::mwFileName const & moveDefFile, 
		const size_t numAxis);

	virtual ~mw5axMoveGeneratorFromFunction()
	{}

protected:
	AxesValues m_absStartPos;
	AxesValues m_absEndPos;
	AxesValues m_curPos;
	size_t m_currentIdx;

	size_t m_axis1idx;
	size_t m_axis2idx;
	size_t m_axis3idx;

	float m_deltaAxis1;
	float m_deltaAxis2;
	float m_deltaAxis3;
	float m_removal;
	bool m_reachedEnd;

public:
	virtual bool			ReachedEndOfMoveList() const 
	{
		return m_reachedEnd;
	};
	virtual size_t			GetMoveIdx() const
	{
		return m_currentIdx;
	};

	virtual void			MoveToStart()
	{
		m_curPos = m_absStartPos;
		m_currentIdx = 0;
		m_reachedEnd = false;
	}
	virtual void SetMaxMoveLength(float maxLength)
	{
		m_deltaAxis1 = m_deltaAxis2 = maxLength;
		CheckSignOfDeltas();
	}
	virtual AxesValues const & GetAxesValues() const
	{
		return m_curPos;
	}

protected:
	void CheckSignOfDeltas() 
	{
		if (m_absStartPos[m_axis1idx] < m_absEndPos[m_axis1idx])
		{
			m_deltaAxis1 = +fabs(m_deltaAxis1);
		}
		else
		{
			m_deltaAxis1 = -fabs(m_deltaAxis1);
		}

		if (m_absStartPos[m_axis2idx] < m_absEndPos[m_axis2idx])
		{
			m_deltaAxis2 = +fabs(m_deltaAxis2);
		}
		else
		{
			m_deltaAxis2 = -fabs(m_deltaAxis2);
		}

		if (m_absStartPos[m_axis3idx] < m_absEndPos[m_axis3idx])
		{
			m_deltaAxis3 = +fabs(m_deltaAxis3);
		}
		else
		{
			m_deltaAxis3 = -fabs(m_deltaAxis3);
		}
	}
};

class mw5AxMoveGeneratorCornedSpiral : public mw5axMoveGeneratorFromFunction
{
private:
	enum MoveDir
	{
		PlusAxis1,
		PlusAxis2,
		MinusAxis1,
		MinusAxis2,

		ToCenter,
		ToStart,
		Down,
	};

	MoveDir m_moveDir;
	AxesValues m_relStartPos;
	AxesValues m_relEndPos;


public:
	mw5AxMoveGeneratorCornedSpiral(misc::mwFileName const & moveDefFile, const size_t numAxis)
		: mw5axMoveGeneratorFromFunction(moveDefFile, numAxis)
	{
		MoveToStart();
	}

	virtual ~mw5AxMoveGeneratorCornedSpiral()
	{}

	virtual void MoveToStart()
	{
		mw5axMoveGeneratorFromFunction::MoveToStart();
		m_relStartPos = m_absStartPos;
		m_relEndPos = m_absEndPos;
		m_moveDir = Down;
	}

	virtual void MoveNextMove();
};

class mw5AxMoveGeneratorParellelLines : public mw5axMoveGeneratorFromFunction
{
private:
	enum MoveDir
	{
		PlusAxis1,
		PlusAxis2,
		MinusAxis1,
		ToStart,
		Down,
	};

	MoveDir m_moveDir;
	AxesValues m_relStartPos;
	AxesValues m_relEndPos;

public:
	mw5AxMoveGeneratorParellelLines(misc::mwFileName const & moveDefFile, const size_t numAxis)
		: mw5axMoveGeneratorFromFunction(moveDefFile, numAxis)
	{
		MoveToStart();
	}

	virtual ~mw5AxMoveGeneratorParellelLines()
	{}

	virtual void MoveToStart()
	{
		mw5axMoveGeneratorFromFunction::MoveToStart();
		m_relStartPos = m_absStartPos;
		m_relEndPos = m_absEndPos;
		m_moveDir = Down;
	}

	virtual void MoveNextMove();
};

#endif // __5AXMOVEGENERATOR