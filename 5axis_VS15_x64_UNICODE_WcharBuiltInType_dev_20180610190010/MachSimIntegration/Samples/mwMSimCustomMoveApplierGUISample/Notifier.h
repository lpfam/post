#ifndef __NOTIFIER_HPP__
#define __NOTIFIER_HPP__



#include "mwMachSimPluginNotificationHandler.hpp"
#include "mwMachSimFacade.hpp"

namespace samples
{

	class collision_notifier : public mwMachSimPluginNotificationHandler
	{
	public:
		collision_notifier(mwMachSimFacade& mMachSimFacade)
			: mwMachSimPluginNotificationHandler(),
			m_MachSimFacade(mMachSimFacade),
			m_MachSimCollisionStatus(false) ,
			m_MachSimCollisions(0L) 
		{
			
		};

		~collision_notifier(){} ;

		void OnCollisionEvent( 	
			const mwMSimCollisionEvent& collisionInformation)
		{
			collisionInformation;
			m_MachSimCollisionStatus = true ;
			++m_MachSimCollisions ;
		}

		void GetCollisionReport(/*out*/long &collisions, /*out*/ bool &status)
		{
			collisions = m_MachSimCollisions ;
			status = m_MachSimCollisionStatus ;
		} ;

		void ResetCollisionComplete()
		{
			m_MachSimCollisionStatus= false ;
			m_MachSimCollisions = 0L ;
		}

	private:
		mwMachSimFacade &m_MachSimFacade ;
		bool m_MachSimCollisionStatus ;
		long m_MachSimCollisions ;
	};
};//namespace samples
#endif
