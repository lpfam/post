// NSSimInvoker.cpp: implementation of the NSSimInvoker class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "NSSimInvoker.h"
#include "resource.h"
#include "CimMachineSimDlg.h"
#include "NcMachineSimInitData.h"
#include "mwFileSystem.hpp"
#include "mwFileName.hpp"
#include "mwCimMachSimResource.hpp"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void ShowDlg()
{
	CCimMachineSimDlg Dlg;
	Dlg.DoModal();	
}

void NSSimInvoker::Invoke( const mwCimMachSimCoreStartupParams& coreStartupParams,
						  const misc::mwstring& iRegPath )
{
	//AFX_MANAGE_STATE(AfxGetStaticModuleState());
	// Check license
   
	// Init parameters
	try
	{
		misc::mwstring resourceFileName = coreStartupParams.GetResourceFileName();
		if (resourceFileName==_T(""))
		{
			_TCHAR exeFileName[MAX_PATH];
			::GetModuleFileName( 0, exeFileName, MAX_PATH );
			misc::mwFileName defaultResourceFileName(exeFileName) ;
			const misc::mwstring defaultName(_T("CimMachineSim_res.dll"));
			defaultResourceFileName.SetFileName(defaultName);

			if( misc::mwFileSystem::FileExists( defaultResourceFileName.GetFilePath() ) )
			{
				resourceFileName = defaultResourceFileName;
			}
		}
		
		mwCimMachSimResource::handle.SetFile( resourceFileName );	
	    CNcMachineSimInitData::Instance()->Init( coreStartupParams, iRegPath );
        ShowDlg();
    }
	catch (misc::mwException& ex)
	{
		AfxMessageBox( ex.GetCompleteErrorMessage().c_str() );
	}
	catch (...)
	{
		AfxMessageBox(_T("An unknown exception has occured"));
	}
	
}
