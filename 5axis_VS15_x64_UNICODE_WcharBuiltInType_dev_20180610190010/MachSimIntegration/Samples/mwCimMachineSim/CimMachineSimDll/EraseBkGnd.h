/******************************************************************************
*               File: EraseBkGnd.h		    								  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  21.10.2004 12:00:00 Created by: Alex Curutiu                               *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "stdafx.h"

#ifndef __EraseBkGnd_hpp__
#define __EraseBkGnd_hpp__

class CEraseBkGnd
{
public:
	static void Erase(CWnd *parent,CWnd *child,CBrush *br,CDC *dc);
	static void Erase(HWND parent, HWND child,HBRUSH br,HDC dc);
private:
	//! Constructor not allowed since it's a static class
	CEraseBkGnd();
};

#endif