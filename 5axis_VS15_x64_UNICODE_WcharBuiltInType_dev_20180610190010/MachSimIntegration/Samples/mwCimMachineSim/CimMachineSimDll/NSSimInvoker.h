// NSSimulatorInvoker.h: interface for the NSSimulatorInvoker class.
//
//////////////////////////////////////////////////////////////////////

#ifndef NSSimInvoker_h
#define NSSimInvoker_h

#include <string>
#include "mwCimMachSimCoreStartupParams.hpp"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef _NSSimInvoker
#define EXPORT_CimMachineSimDll __declspec(dllexport)
#else
#define EXPORT_CimMachineSimDll __declspec(dllimport)
#endif


namespace NSSimInvoker  
{
	EXPORT_CimMachineSimDll void Invoke ( 
		const mwCimMachSimCoreStartupParams& coreStartupParams,
		const misc::mwstring& iRegPath
		 );
};

#endif // NSSimInvoker_h
