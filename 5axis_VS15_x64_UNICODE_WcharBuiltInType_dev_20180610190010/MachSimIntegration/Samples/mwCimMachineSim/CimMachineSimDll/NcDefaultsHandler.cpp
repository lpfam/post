// NcDefaultsHandler.cpp: implementation of the NsNcDefaultsHandler class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "NcDefaultsHandler.h"
#include "NcMachineSimInitData.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//defaults assignements:

const misc::mwstring cMachineDef (_T("5AxHeadHead"));

const misc::mwstring cDefaultX (_T("0.0"));
const misc::mwstring cDefaultY (_T("0.0"));
const misc::mwstring cDefaultZ (_T("0.0"));
const misc::mwstring cDefaultRed(_T("0.0"));
const misc::mwstring cDefaultGreen (_T("0.0"));
const misc::mwstring cDefaultBlue(_T("0.0"));


const HKEY cCurrentUserKey = HKEY_CURRENT_USER;

///////////////////////////////////////////////////////////////////////////////
// General Registry services:
///////////////////////////////////////////////////////////////////////////////
bool GetKey(const misc::mwstring& iName, misc::mwstring& oValue)
{
	HKEY   aKey;
    LONG   aFoundKey, aFoundVal;
    DWORD  aType       =REG_MULTI_SZ;
    DWORD  aBufferSize = 512;
    unsigned char* aBuffer = new unsigned char[512];
    aBuffer[0] = '\0';
	misc::mwstring  aRegistryPath  = CNcMachineSimInitData::Instance()->GetRegPath();
    // open the key
    aFoundKey = RegOpenKeyEx(cCurrentUserKey, aRegistryPath.c_str(), 0, KEY_READ, &aKey);
    // if we opened the key then get the neede value
    if (ERROR_SUCCESS == aFoundKey)
        aFoundVal = RegQueryValueEx( aKey, iName.c_str(), MW_NULL, &aType, aBuffer, &aBufferSize);
    
    RegCloseKey(aKey);
	
	/*if (REG_MULTI_SZ!=aType)
	{
		delete [] aBuffer;
		return false;
	}*/

    if (ERROR_SUCCESS != aFoundKey || ERROR_SUCCESS != aFoundVal )
	{
		delete [] aBuffer;
        return false;
	}

	// if we ok so far then copy the buffer to the string
	oValue = (LPCTSTR)aBuffer;

    delete [] aBuffer;
	if(oValue == _T(""))
		return false;
    return true;
}


bool SetKey(const misc::mwstring& iName, const misc::mwstring& iValue)
{
    HKEY  aNewKey;
    DWORD aOutcome;
	misc::mwstring aRegistryPath  = CNcMachineSimInitData::Instance()->GetRegPath();
    // Create the key
    if (ERROR_SUCCESS == RegCreateKeyEx(cCurrentUserKey,
		aRegistryPath.c_str(), 0, MW_NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS, MW_NULL,
		&aNewKey, &aOutcome))
    {
		//const unsigned char* buf=(const unsigned char*)((LPCTSTR)iValue);
		const unsigned char* buf= (const unsigned char*) iValue.c_str();
        // Create the values for the key
        RegSetValueEx(aNewKey, iName.c_str(), 0, REG_MULTI_SZ , 
            buf, static_cast<DWORD>((iValue.size() + 1) * sizeof(TCHAR)));
	
		RegCloseKey(aNewKey);
		return (true);
	}
	else
		return (false);
}



/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Accessors:
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

// Last X, Y, Z used. (0, 0, 0) for the first usage
void NsNcDefaultsHandler::GetXYZ (misc::mwstring& oX, misc::mwstring& oY, misc::mwstring& oZ)
{
	// Set X
	misc::mwstring aAxis(_T(""));
	GetKey (_T("X"), aAxis);

	// When success: return value
	if (!aAxis.empty())
		oX = aAxis;
	else
	{
		// When Failure: Set default and return value
		SetKey (_T("X"), cDefaultX);
		oX = cDefaultX;
	}
	// End Set X

	// Set Y
	aAxis = _T("");
	GetKey (_T("Y"), aAxis);

	// When success: return value
	if (!aAxis.empty())
		oY = aAxis;
	else
	{
		// When Failure: Set default and return value
		SetKey (_T("Y"), cDefaultY);
		oY = cDefaultY;
	}
	// End Set Y

	// Set Z
	aAxis = _T("");
	GetKey (_T("Z"), aAxis);

	// When success: return value
	if (!aAxis.empty())
		oZ = aAxis;
	else
	{
		// When Failure: Set default and return value
		SetKey (_T("Z"), cDefaultZ);
		oZ = cDefaultZ;
	}
	// End Set Z
}
void NsNcDefaultsHandler::GetRGB ( misc::mwstring& oRed,  misc::mwstring& oGreen,  misc::mwstring& oBlue)
{
	misc::mwstring aRed(_T(""));
	misc::mwstring aGreen(_T(""));
	misc::mwstring aBlue(_T(""));
	GetKey (_T("MachSim_Back_Color_Red"), aRed);
	// When success: return value
	if (!aRed.empty())
		oRed = aRed;
	else
	{
		SetKey (_T("MachSim_Back_Color_Red"), cDefaultRed);	
		oRed = cDefaultRed;
	}
	GetKey (_T("MachSim_Back_Color_Green"), aGreen);
	// When success: return value
	if (!aGreen.empty())
		oGreen = aGreen;
	else
	{
		SetKey (_T("MachSim_Back_Color_Green"), cDefaultGreen);	
		oGreen = cDefaultGreen;
	}
	GetKey (_T("MachSim_Back_Color_Blue"), aBlue);
	// When success: return value
	if (!aBlue.empty())
		oBlue = aBlue;
	else
	{
		SetKey (_T("MachSim_Back_Color_Blue"), cDefaultBlue);	
		oBlue = cDefaultBlue;
	}
}


void NsNcDefaultsHandler::SetXYZ (const misc::mwstring& iX, const misc::mwstring& iY, const misc::mwstring& iZ)
{

	SetKey (_T("X"), iX);
	SetKey (_T("Y"), iY);
	SetKey (_T("Z"), iZ);
}


void NsNcDefaultsHandler::SetRGB (const misc::mwstring& iRed, const misc::mwstring& iGreen, const misc::mwstring& iBlue)
{
	SetKey (_T("MachSim_Back_Color_Red"), iRed);
	SetKey (_T("MachSim_Back_Color_Green"), iGreen);
	SetKey (_T("MachSim_Back_Color_Blue"), iBlue);

}

// Last Machine used, "Head Head" for first usgae
misc::mwstring NsNcDefaultsHandler::GetMachineDef ( const misc::mwstring& defaultMachineName )
{
	misc::mwstring aMachineDef(_T(""));
	GetKey (_T("MachineDef"), aMachineDef);

	// When success: return value
	if (!aMachineDef.empty())
		return aMachineDef;
	
	// When Failure: Set default and return value
	misc::mwstring rtrnMachineName;
	if ( defaultMachineName != _T("") )
	{
		rtrnMachineName = defaultMachineName;
	}
	else
	{
		rtrnMachineName = cMachineDef;
	}
	SetKey (_T("MachineDef"), rtrnMachineName);
	
	return rtrnMachineName;
}

void NsNcDefaultsHandler::SetMachineDef (const misc::mwstring& iMachineDef)
{
	SetKey (_T("MachineDef"), iMachineDef);
}

//
misc::mwstring NsNcDefaultsHandler::GetMxp ()
{
	misc::mwstring aMxp(_T(""));
	GetKey (_T("mxp"), aMxp);

	// When success: return value
	if (!aMxp.empty())
		return aMxp;

	// When Failure: Set default and return value
	SetKey (_T("mxp"), _T(""));
	return aMxp;
}

void NsNcDefaultsHandler::SetMxp (misc::mwstring& iMxp)
{
	SetKey (_T("mxp"), iMxp);
}

	


