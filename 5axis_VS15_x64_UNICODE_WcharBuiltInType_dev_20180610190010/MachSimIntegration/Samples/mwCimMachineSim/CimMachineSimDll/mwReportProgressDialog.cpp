/******************************************************************************
*               File: mwMSimDialogProgress.cpp                                *
*******************************************************************************
*               Description: Implementation of                                *
*                     mwMSimDialogProgress                                    *
*******************************************************************************
*               History:                                                      *
*   04.01.2008 13:19:25 Created by: Eugene Hodzhaev                           *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "resource.h"
#include "mwReportProgressDialog.hpp"
#include "mwMachSimUtils.hpp"
#include "mwCimExceptionHandler.hpp"

#define MW_WM_START         WM_USER + 100
#define MW_WM_FINISH		WM_USER + 101

mwMSimDialogProgress::mwMSimDialogProgress( CWnd* pParent /*=MW_NULL*/, 
										   misc::mwAutoPointer<mwIProgressProcess> process,
										   CCimMachineSimCore* cimMachineCore,
										   const bool bOnTop )
: CDialog(IDD_REPORT_DIALOG, pParent)
, CimCommandBasedSimControllerDisabler(cimMachineCore)
, m_process(process)
, m_stepCount(0)
, m_step(0)
, m_range(0)
, m_cancelled(false)
, m_bOnTop( bOnTop )
{
	DoModal();
}

void mwMSimDialogProgress::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TIME_REMAINING, m_timeRemaining);
	DDX_Control(pDX, IDC_TIME_ELAPSED, m_timeElapsed);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
}

BEGIN_MESSAGE_MAP(mwMSimDialogProgress, CDialog)
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(MW_WM_START, OnStart)
	ON_MESSAGE(MW_WM_FINISH, OnFinish)
END_MESSAGE_MAP()

void mwMSimDialogProgress::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	if (bShow)
	{
		CRect rect;
		m_progress.GetWindowRect(rect);
		m_range = rect.Width();
		m_progress.SetRange32(0, m_range);
		m_progress.SetPos( 0 );
		m_startTime = CTime::GetCurrentTime();
		m_startTick = m_lastTick = GetTickCount();
		if (m_process!=MW_NULL)
		{
			PostMessage( MW_WM_START, 0, 0);
		}
		if( !m_bOnTop )
		{
			SetWindowPos( &CWnd::wndBottom, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE );
		}
	}
}

LRESULT mwMSimDialogProgress::OnStart(WPARAM wp, LPARAM lp)
{
	try
	{
		m_process->Run(this);
	}
	catch ( exceptions::mwMachSimException& ex)
	{
		mwCimExceptionHandler::HandleMachSimException(ex);
	}
	catch ( misc::mwException& ex)
	{
		mwCimExceptionHandler::HandleMiscException(ex);
	}
	catch(std::exception& ex)
	{
		mwCimExceptionHandler::HandleStdException( ex );
	}
	catch (...)
	{
		mwCimExceptionHandler::HandleUnknownException();
	}
	OnFinish(0,0);
	return 1;
}

LRESULT mwMSimDialogProgress::OnFinish(WPARAM wp, LPARAM lp)
{
	DoEvents(false);
	CDialog::OnOK();
	return 1;
}

void mwMSimDialogProgress::OnCancel() 
{
	if ( CDialog::MessageBox( _T("Do you really want to break the operation ?")
		, MW_NULL, MB_YESNO | MB_ICONWARNING ) != IDYES )
	{
		return;
	}
	CDialog::OnCancel();

	m_cancelled = true;
}

BOOL mwMSimDialogProgress::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetIcon(MW_NULL, FALSE);
	SetIcon(MW_NULL, TRUE);

	m_progress.SetStep( 1 );

	return TRUE;
}

bool mwMSimDialogProgress::UpdateTime() 
{
	// update every 0.5 sec
	const DWORD updateFreq = 500;
	DWORD curtick = GetTickCount();
	if (curtick - m_lastTick < updateFreq)
	{
		return true;
	}
	m_lastTick = curtick;

	// Update time info
	CTimeSpan time1 = CTime::GetCurrentTime() - m_startTime;

	unsigned long u, p;
	u = m_stepCount;
	p = m_step;
	double m = (double)(curtick-m_startTick) / (double)( p );
	int isecs = (int)( m * 0.001 * (double)( u - p ) );
	unsigned long secs = isecs < 0 ? 0 : (unsigned long)( m * 0.001 * (double)( u - p ) );
	CTimeSpan time2( secs / 86400, ((unsigned long)( secs / 3600 ))%24, ((unsigned long)( secs / 60 ))%60, secs % 60 );
	m_timeElapsed.SetWindowText( time1.Format("%D:%H:%M:%S") );
	m_timeRemaining.SetWindowText( time2.Format("%D:%H:%M:%S") );
	// Update caption
	CString caption;
	unsigned long proc = (unsigned long)(100.0*((double)( p )/(double)( u )));
	caption.Format( _T("%s (%i%%)"), (LPCTSTR)m_caption, proc );
	CDialog::SetWindowText(caption);
	return false;
}

unsigned long mwMSimDialogProgress::StepIt(unsigned long number)
{
	m_step += number;
	DoEvents(true);

	return 0;
}

unsigned long mwMSimDialogProgress::SetPos( unsigned long value )
{
	
	m_step = value;
	DoEvents(true);

	return 0;
}

void mwMSimDialogProgress::SetStepCount( unsigned long stepCount )
{
	m_stepCount = stepCount;
}

void mwMSimDialogProgress::Close()
{
	if ( IsWindow( m_hWnd ) )
	{
		PostMessage( MW_WM_FINISH, 0, 0);
		DoEvents(false);
	}
}

void mwMSimDialogProgress::Reset()
{
	SetPos(0);
}

void mwMSimDialogProgress::SetCaption( const misc::mwstring &info )
{
	m_caption = info.c_str();
	SetWindowText(m_caption);
	DoEvents(false);
}

bool mwMSimDialogProgress::IsAborted()
{
	return m_cancelled;
}

void mwMSimDialogProgress::SetAborted(bool abort)
{
	m_cancelled = abort;
}


void mwMSimDialogProgress::DoEvents(bool updateTime)
{	
	// check the time
	if (updateTime && !m_cancelled && UpdateTime())
	{
		return;
	}

	// update progress bar
	m_progress.SetPos( (int)(double(m_step)*double(m_range)/double(m_stepCount)) );

	// pump messages
	MSG msg;
	while (!m_cancelled && ::PeekMessage(&msg, MW_NULL,0,0,PM_NOREMOVE)) 
	{
		AfxGetApp()->PumpMessage();
	}
}


void mwMSimDialogProgress::SetInfo( const misc::mwstring & )
{
	//SetDlgItemText(IDC_INFO_EDIT, info);
	DoEvents(false);
}

int SimulationReportProcess::Run(mwIProgressDialogHandler* progress)
{
	progress->SetCaption(_T("Simulating all and generating report"));
	m_machSimCore->SimulateAllAndWriteReport( m_xmlFileName, *progress );
	return 0;
}

int ExternalToolProcess::Run(mwIProgressDialogHandler* progress)
{
	progress->SetCaption(_T("Executing tool"));
	m_machSimCore->m_machSimCoreFacade->ExecuteExternalTool( m_toolSection, m_toolName, m_toolParameter, progress );
	return 0;
}

