//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CimMachineSimDll.rc
//
#define IDD_UIMACHSIM                   2000
#define IDC_COMBO1                      2000
#define IDC_EDIT1                       2001
#define IDD_UISETUP                     2001
#define IDC_EDIT2                       2002
#define IDR_MENU1                       2002
#define IDC_EDIT3                       2003
#define IDC_MACHSIM_WND                 2004
#define IDD_DIALOG1                     2004
#define IDD_REPORT_DIALOG               2004
#define IDC_PROGRESS1                   2005
#define IDI_MACHSIM                     2005
#define IDC_TIME_ELAPSED                2006
#define IDC_TIME_REMAINING              2007
#define IDC_TIME_ELAPSED_CAPTION        2008
#define IDC_TIME_REMAINING__CAPTION     2009
#define IDC_STATIC_PART_ORIGIN_CAPTION  2010
#define IDC_STATIC_MACHINE_CAPTION      2011
#define IDC_STATIC_MACHINE_AND_POST_CAPTION 2012
#define ID_FILE_EXIT                    32771
#define ID_SIMULATIONSETUP              32772
#define ID_HELP32773                    32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2006
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         2013
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
