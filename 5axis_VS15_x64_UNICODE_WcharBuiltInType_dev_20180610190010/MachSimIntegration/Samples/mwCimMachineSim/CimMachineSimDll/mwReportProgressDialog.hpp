/******************************************************************************
*               File: mwMSimDialogProgress.hpp                                *
*******************************************************************************
*               Description: Implementation of                                *
*                     mwMSimDialogProgress                                    *
*******************************************************************************
*               History:                                                      *
*   04.01.2008 13:19:25 Created by: Eugene Hodzhaev                           *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMSimDialogProgress_hpp__
#define __mwMSimDialogProgress_hpp__

#include "mwIProgressHandler.hpp"
#include "CCimMachineSimCore.h"
#include "CimCommandBasedSimControllerDisabler.hpp"
#include "afxcmn.h"

class mwMSimDialogProgress : public CDialog, public mwIProgressDialogHandler, public CimCommandBasedSimControllerDisabler
{

public:
	//! standard constructor
	mwMSimDialogProgress(CWnd* pParent /* = MW_NULL */, 
		misc::mwAutoPointer<mwIProgressProcess> process = MW_NULL,
		CCimMachineSimCore* cimMachineCore = MW_NULL,
		const bool bOnTop = true );

	virtual bool IsAborted();
	virtual void SetAborted(bool abort);
	virtual void SetStepCount( unsigned long stepCount );
	virtual unsigned long SetPos( unsigned long value );
	virtual unsigned long StepIt( unsigned long stepCount );
	virtual void SetCaption( const misc::mwstring &info );
	virtual void SetInfo( const misc::mwstring &info );
	virtual void Show() {}
	virtual void Close();
	virtual void SetStyles(unsigned int styles) {}
	virtual void AddStyles(unsigned int styles) {}

	const CString& GetTitle() const;

	void SetTitle(const CString& title);
	void SetThread(CWinThread * thread);
	void Reset();

protected:

	//! DDX/DDV support
	virtual void DoDataExchange(CDataExchange* pDX);

	afx_msg LRESULT OnFinish      (WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnStart      (WPARAM wp, LPARAM lp);

	virtual void OnCancel();

	//// Generated message map functions
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	bool UpdateTime();
	DECLARE_MESSAGE_MAP()

private:

	//afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	void DoEvents(bool updateTime);

	CString									m_caption;
	CTime									m_startTime;
	DWORD									m_startTick;
	DWORD									m_lastTick;
	unsigned long							m_stepCount;
	unsigned long							m_step;
	int										m_range;
	CStatic									m_timeRemaining;
	CStatic									m_timeElapsed;
	bool									m_cancelled;
	misc::mwAutoPointer<mwIProgressProcess>	m_process;
	CProgressCtrl							m_progress;

	bool									m_bOnTop;
};

class SimulationReportProcess : public mwIProgressProcess
{
public:

	SimulationReportProcess( CCimMachineSimCore* machSimCore, const misc::mwstring& xmlFileName )
		: m_machSimCore(machSimCore), 
		m_xmlFileName(xmlFileName)
	{
	}

	virtual int Run(mwIProgressDialogHandler* progress);

	CCimMachineSimCore*		m_machSimCore;
	const misc::mwstring&	m_xmlFileName;
};

class ExternalToolProcess : public mwIProgressProcess
{
public:

	ExternalToolProcess( CCimMachineSimCore* machSimCore, const misc::mwstring& toolSection, 
		const misc::mwstring &toolName, const misc::mwstring &toolParameter )
		: m_machSimCore(machSimCore), m_toolSection( toolSection ), 
		m_toolName( toolName ), m_toolParameter( toolParameter )
	{
	}

	virtual int Run(mwIProgressDialogHandler* progress);

	CCimMachineSimCore*		m_machSimCore;
	const misc::mwstring&	m_toolSection;
	const misc::mwstring&	m_toolName;
	const misc::mwstring&	m_toolParameter;
};

#endif //__mwMSimDialogProgress_hpp__