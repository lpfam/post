#ifndef __MWMACHINESIMDLG_H__
#define __MWMACHINESIMDLG_H__
//
#include "afxpriv.h"
#include "mwAutoPointer.hpp"

#define WM_ENDSIM				WM_USER + 102

/////////////////////////////////////////////////////////////////////////////
// CCimMachineSimDlg dialog
class CCimMachineSimCore;

class CCimMachineSimDlg : public CDialog
{
// Construction
public:
	CCimMachineSimDlg(CWnd* pParent = MW_NULL);   // standard constructor
	~CCimMachineSimDlg();
// Dialog Data
	//{{AFX_DATA(CCimMachineSimDlg)
	enum { IDD = IDD_UIMACHSIM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	void StartMachSim();
	void ShutDownMachSim() ;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCimMachineSimDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CCimMachineSimCore*			 mMachineCore;
	
	// Generated message map functions
	//{{AFX_MSG(CCimMachineSimDlg)
	//! initializes MachSim
	LRESULT OnReportTrigger( WPARAM wParam, LPARAM lParam );

	afx_msg void OnSimulationsetup();
	afx_msg void OnFileExit();	
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd( CDC* pDC );
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnEndSim(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void LoadWindowPlacement();
	void SaveWindowPlacement();

	bool					m_firstRun;
	static const _TCHAR*	m_windowSection;
	static const UINT		m_machSimWindowOffset = 22;
public:
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	void LoadUI();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__MWMACHINESIMDLG_H__)
