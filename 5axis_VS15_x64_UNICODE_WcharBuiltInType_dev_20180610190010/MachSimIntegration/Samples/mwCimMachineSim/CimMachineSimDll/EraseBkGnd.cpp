/******************************************************************************
*               File: EraseBkGnd.cpp	    								  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  21.10.2004 12:00:00 Created by: Alex Curutiu                               *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "EraseBkGnd.h"

void CEraseBkGnd::Erase(CWnd *parent,CWnd *child,CBrush *br,CDC *dc)
{
	CRgn reg1,reg2,reg;
	WINDOWPLACEMENT wpl;
	RECT r;

	parent->GetClientRect(&r);
	reg1.CreateRectRgn(r.left,r.top,r.right,r.bottom);
	child->GetWindowPlacement(&wpl);	
	reg2.CreateRectRgn(wpl.rcNormalPosition.left,wpl.rcNormalPosition.top,wpl.rcNormalPosition.right,wpl.rcNormalPosition.bottom);
	reg.CreateRectRgn(0,0,10,10);
	int nCombineResult = reg.CombineRgn(&reg1,&reg2,RGN_DIFF);

	dc->FillRgn(&reg,br);	
}
void CEraseBkGnd::Erase(HWND parent, HWND child,HBRUSH br,HDC dc)
{
	CEraseBkGnd::Erase(CWnd::FromHandle(parent),CWnd::FromHandle(child),CBrush::FromHandle(br),CDC::FromHandle(dc));
}
