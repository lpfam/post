// SetupDlg.cpp : implementation file
//

#include "StdAfx.h"
#include "resource.h"
#include "SetupDlg.h"
#include "mwCimMachSimResource.hpp"
#include "NcMachineSimInitData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetupDlg dialog


CSetupDlg::CSetupDlg(const std::vector<misc::mwstring>& machineList,const misc::mwstring &defaultMach,
					 const misc::mwstring &PartOrigX,const misc::mwstring &PartOrigY,const misc::mwstring &PartOrigZ,
					 const measures::mwUnitsFactory::Units units,
					 CWnd* pParent /*=MW_NULL*/)
	: CDialog(CSetupDlg::IDD, pParent)	
	, mMachineList(machineList)
	, mDefaultMach(defaultMach)
	, m_units(units)
{
	//{{AFX_DATA_INIT(CSetupDlg)
	misc::to_value(PartOrigX,mPartOrigX);
	misc::to_value(PartOrigY,mPartOrigY);
	misc::to_value(PartOrigZ,mPartOrigZ);
	//}}AFX_DATA_INIT
	if (m_units==measures::mwUnitsFactory::INCH)
	{
		mPartOrigX/=25.4;
		mPartOrigY/=25.4;
		mPartOrigZ/=25.4;
	}
}


void CSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	setlocale(LC_ALL,"");
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetupDlg)
	DDX_Control(pDX, IDC_COMBO1, m_UIMachineDef);
	DDX_Text(pDX, IDC_EDIT1, mPartOrigX);
	DDX_Text(pDX, IDC_EDIT2, mPartOrigY);
	DDX_Text(pDX, IDC_EDIT3, mPartOrigZ);
	setlocale(LC_ALL,"C");
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetupDlg, CDialog)
	//{{AFX_MSG_MAP(CSetupDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetupDlg message handlers
void CSetupDlg::UpdateMachineDefList()
{
	std::vector<misc::mwstring>::const_iterator it,ite;
	it=mMachineList.begin();
	ite=mMachineList.end();
	for (;it!=ite;it++)
	{
		m_UIMachineDef.AddString( (*it).c_str() );
	}
	// try to select default machine , if not found try the headhead machine ... 
	int nindex=m_UIMachineDef.FindStringExact( -1, mDefaultMach.c_str() );
	if (nindex!=CB_ERR)
	{
		m_UIMachineDef.SetCurSel(nindex);
	}else
	{		
		if (m_UIMachineDef.GetCount()>0)
			m_UIMachineDef.SetCurSel(0);
	}

}
BOOL CSetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	mwCimMachSimCoreStartupParams startupParams = 
		CNcMachineSimInitData::Instance()->GetCoreStartupParams();

	UpdateMachineDefList();

	const misc::mwstring setupDlgResourceSection(_T("SimulationSetup"));
	SetDlgItemText( IDC_STATIC_MACHINE_AND_POST_CAPTION, mwCimMachSimResource::handle.GetMsg(2, setupDlgResourceSection).c_str() );
	SetDlgItemText( IDC_STATIC_PART_ORIGIN_CAPTION, mwCimMachSimResource::handle.GetMsg(4, setupDlgResourceSection).c_str() );
	SetDlgItemText( IDC_STATIC_MACHINE_CAPTION, mwCimMachSimResource::handle.GetMsg(3, setupDlgResourceSection).c_str() );

	SetDlgItemText( IDOK, mwCimMachSimResource::handle.GetMsg(2, _T("CimMachineSim")).c_str() );
	SetDlgItemText( IDCANCEL, mwCimMachSimResource::handle.GetMsg(3, _T("CimMachineSim")).c_str() );

	SetWindowText( mwCimMachSimResource::handle.GetMsg(1, setupDlgResourceSection).c_str() );
	
	mIndexMach=0;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSetupDlg::OnOK() 
{
	UpdateData();
	
	mIndexMach=m_UIMachineDef.GetCurSel();

	CDialog::OnOK();

	if (m_units==measures::mwUnitsFactory::INCH)
	{
		mPartOrigX*=25.4;
		mPartOrigY*=25.4;
		mPartOrigZ*=25.4;
	}
}
