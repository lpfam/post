// CimMachineSimDlg.cpp : implementation file
//

#include "StdAfx.h"
#include "afxpriv.h"
#include "mwAvoid_Warnings.hpp"
#include "resource.h"
#include "CimMachineSimDlg.h"
#include "CCimMachineSimCore.h"
#include "SetupDlg.h"
#include "EraseBkGnd.h"
#include "NcDefaultsHandler.h"
#include "NcMachineSimInitData.h"
#include <algorithm>
#include ".\cimmachinesimdlg.h"
#include "mwReportProgressDialog.hpp"
#include "mwCimExceptionHandler.hpp"
#include "mwCimMachSimResource.hpp"
#include "mwFileSystem.hpp"
#include "mwMachSimIniParams.hpp"
#include "mwCLParserException.hpp"
#include "mwLocale.hpp"

/////////////////////////////////////////////////////////////////////////////
// CCimMachineSimDlg dialog

#define WM_REPORT_TRIGGER WM_USER + 150

CCimMachineSimDlg::CCimMachineSimDlg(CWnd* pParent /*=MW_NULL*/)
	: CDialog(CCimMachineSimDlg::IDD, pParent),
	mMachineCore(MW_NULL),
	m_firstRun(true)
{			
}

CCimMachineSimDlg::~CCimMachineSimDlg()
{
	ShutDownMachSim();
}
void CCimMachineSimDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCimMachineSimDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCimMachineSimDlg, CDialog)
	//{{AFX_MSG_MAP(CCimMachineSimDlg)
	ON_COMMAND(ID_SIMULATIONSETUP, OnSimulationsetup)
	ON_COMMAND(ID_FILE_EXIT, OnFileExit)	
	ON_MESSAGE( WM_REPORT_TRIGGER, OnReportTrigger )
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()	
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_HELPINFO()
	ON_MESSAGE(WM_ENDSIM, OnEndSim)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCimMachineSimDlg message handlers

BOOL CCimMachineSimDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_hIcon = AfxGetApp()->LoadIcon(IDI_MACHSIM);
	//SetIcon(m_hIcon, FALSE); // Set small icon
	SetIcon(m_hIcon, TRUE); // Set small icon
	
	const misc::mwstring generalResourceSection(_T("CimMachineSim"));
	SetWindowText( mwCimMachSimResource::handle.GetMsg(1, generalResourceSection).c_str() );

	
	CMenu * pMainMenu = this->GetMenu();
	// File menu
	pMainMenu->ModifyMenu(0, MF_STRING | MF_BYPOSITION, 0, 
		mwCimMachSimResource::handle.GetMsg(4, generalResourceSection).c_str() );
	pMainMenu->ModifyMenu(ID_FILE_EXIT, MF_STRING | MF_BYCOMMAND, ID_FILE_EXIT, 
		mwCimMachSimResource::handle.GetMsg(5, generalResourceSection).c_str() );
	pMainMenu->ModifyMenu(ID_SIMULATIONSETUP, MF_STRING | MF_BYCOMMAND, ID_SIMULATIONSETUP, 
		mwCimMachSimResource::handle.GetMsg(6, generalResourceSection).c_str() );

	DrawMenuBar();

	if ( ( CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetWindowStyle() == _T("0") )
		&& ( CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileName() == _T("") ) )
	{
		LoadWindowPlacement();
	}
	else if( CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileName() != _T("") )
	{
		ShowWindow(SW_HIDE);
	}
	else
	{
		ShowWindow(SW_MAXIMIZE);
	}
	
	CRect windowRect;
	GetClientRect( &windowRect );

	GetDlgItem( IDC_MACHSIM_WND )->SetWindowPos( MW_NULL, 0, 0, windowRect.Width() - m_machSimWindowOffset, 
													windowRect.Height() - m_machSimWindowOffset, SWP_NOMOVE | SWP_NOZORDER );
	GetDlgItem( IDC_MACHSIM_WND )->Invalidate();
	GetDlgItem( IDC_MACHSIM_WND )->RedrawWindow();
	
	try
	{
		StartMachSim();
	}
	catch (mwCLParserException& exc)
	{
		mwCimExceptionHandler::HandleParserException( exc );
	}
	catch (exceptions::mwMachSimException& exc)
	{
		mwCimExceptionHandler::HandleMachSimException( exc );
	}
	catch (post::mwPostException& exc)
	{
		mwCimExceptionHandler::HandlePostException( exc );
	}
	catch (misc::mwException& exc)
	{
		mwCimExceptionHandler::HandleMiscException( exc );
	}
	catch(std::exception& exc)
	{
		mwCimExceptionHandler::HandleStdException( exc );
	}
	catch(...) 
	{
		mwCimExceptionHandler::HandleUnknownException();
	}

	if ( CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileName() != _T("") )
	{
		PostMessage( WM_REPORT_TRIGGER );
	}
	
	m_firstRun = false;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

LRESULT CCimMachineSimDlg::OnReportTrigger( WPARAM wParam, LPARAM lParam )
{
	misc::mwFileName lcReportFileName( CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileName() );
	
	if( lcReportFileName.GetFileExtention() == _T("xls") )
	{
		if( !mMachineCore->m_machSimCoreFacade->IsExternalToolAvailable( _T("saveas"), _T("xlssetupsheet")))
		{
			mMachineCore->m_machSimCoreFacade->GetLocalizationHandler()->MsgBox( _T("Requested tool saveas/xlssetupsheet not available"));
		}
		else
		{
			mwMSimDialogProgress xlsSheet( this, new ExternalToolProcess( mMachineCore, 
				_T("saveas"), _T("xlssetupsheet"), lcReportFileName ), mMachineCore,
				CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileName().empty() ||
				CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileNotOnTop().empty() );
		}
	}
	else
	{
		mwMSimDialogProgress report(this,
			new SimulationReportProcess(mMachineCore, 
			lcReportFileName )
			, mMachineCore
			, CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileName().empty() ||
			CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileNotOnTop().empty() );
	}
	
	mMachineCore->ForceMachSIMGLViewRedraw();
	PostQuitMessage(0);
	return TRUE;
}

void CCimMachineSimDlg::StartMachSim()
{
	mwCimMachSimCoreStartupParams startupParams = 
		CNcMachineSimInitData::Instance()->GetCoreStartupParams();

	bool firstRun = true ;
	//
	try
	{		
		if(mMachineCore)
		{
			firstRun = false ;
			
			if(mMachineCore->CanQuit())
				mMachineCore->ShutdownMachsim() ;
			else
				return;

			delete mMachineCore ;
		}
		mMachineCore=new CCimMachineSimCore( startupParams );
	}
	catch (exceptions::mwMachSimException& exc)
	{
		mwCimExceptionHandler::HandleMachSimException( exc );
		exit(1);
	}
	catch (misc::mwException& exc)
	{
		mwCimExceptionHandler::HandleMiscException( exc );
		exit(1);
	}
	catch(std::exception& exc)
	{
		mwCimExceptionHandler::HandleStdException( exc );
		exit(1);
	}
	catch(...)
	{
		mwCimExceptionHandler::HandleUnknownException();
		exit(1) ;
	}
										
	BOOL startMachsim;					
	mwMachSimIniParams localIniParams( startupParams.GetMachSimFolder()+_T("machsim.ini"));
	if( localIniParams.GetIntegratorsSampleMenuDisabled() )
	{
		SetMenu(NULL);
	}
	// get the values from registers
	misc::mwstring machineName;
	if ((CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetInitialMachineName()!=_T(""))&&
		m_firstRun)
	{
		machineName =  CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetInitialMachineName();
		NsNcDefaultsHandler::SetMachineDef(machineName);
	}
	else
	{
		misc::mwstring defaultMachineName;
		if ( CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetMachineReplacementOption() ==
			_T("only_3_axis"))
		{
			const misc::mwstring default3axisMachineName(_T("3AxGeneric"));
			defaultMachineName = default3axisMachineName;
		}
		machineName = NsNcDefaultsHandler::GetMachineDef(defaultMachineName);
	}
	
	misc::mwstring CStrX,CStrY,CStrZ;
	NsNcDefaultsHandler::GetXYZ(CStrX, CStrY, CStrZ);
	//get the Machine Sim color background RGB;
	misc::mwstring aRed,aGreen,aBlue;
	NsNcDefaultsHandler::GetRGB(aRed, aGreen, aBlue);
	mMachineCore->SetMachineSimColor(aRed, aGreen, aBlue);
	mMachineCore->SetMachineDefName(machineName);
	
    // convert PartX, PartY and PartZ
	double PartX,PartY,PartZ;

	misc::to_value(CStrX, PartX);
	misc::to_value(CStrY, PartY);
	misc::to_value(CStrZ, PartZ);
	
	//set part Origin
	mMachineCore->SetPartOrigin(PartX,PartY,PartZ);
	//set the parent window
	CWnd *msWnd = GetDlgItem( IDC_MACHSIM_WND );
	mMachineCore->SetParentWindow(msWnd->GetSafeHwnd() );
	msWnd->ModifyStyle(0, WS_CLIPCHILDREN);
		
	// start machsim from mMachsimCore
	startMachsim = mMachineCore->RunMachSim();

	CString crtWndTitle;
	GetWindowText(crtWndTitle);

	misc::mwstring crtWndTitleStr(crtWndTitle);
	misc::mwstring newWndTitle = crtWndTitleStr + mMachineCore->GetAppTitleSuffix();
	SetWindowText( newWndTitle.c_str() );
	
	if(!startMachsim)
	{
		AfxMessageBox(_T("Machsim could not be started"));
	}
}
void CCimMachineSimDlg::OnSimulationsetup() 
{
	misc::mwstring cx;
	misc::mwstring cy;
	misc::mwstring cz;
	NsNcDefaultsHandler::GetXYZ(cx, cy, cz);
	misc::mwstring machDef = NsNcDefaultsHandler::GetMachineDef();
	
	std::vector<misc::mwstring> machineList = mMachineCore->GetMachineDefinitionNames();

	CSetupDlg setupDlg(machineList, machDef, cx, cy, cz,mMachineCore->GetUnits());
	if (setupDlg.DoModal()==IDOK)
	{		
		unsigned int machineIndex=setupDlg.GetIndexMach();

		//allow app start with no machine and sim -34763
		if( !machineList.empty() && 
			machineIndex < machineList.size())
		{
			misc::mwstring machineName=machineList[machineIndex];
			cx = misc::from_value (setupDlg.GetPartOrigX());
			cy = misc::from_value (setupDlg.GetPartOrigY());
			cz = misc::from_value (setupDlg.GetPartOrigZ());

			NsNcDefaultsHandler::SetMachineDef(machineName);
			NsNcDefaultsHandler::SetXYZ(cx, cy, cz);
		}

	    try
		{
			StartMachSim();
		}
		//temporary
		/*catch (mwCimCLParserException& exc)
		{
			mwCimExceptionHandler::HandleParserException( exc );
		}
		*/catch (exceptions::mwMachSimException& exc)
		{
			mwCimExceptionHandler::HandleMachSimException( exc );
		}
		catch (misc::mwException& exc)
		{
			mwCimExceptionHandler::HandleMiscException( exc );
		}
		catch(std::exception& exc)
		{
			mwCimExceptionHandler::HandleStdException( exc );
		}
		catch(...) 
		{
			mwCimExceptionHandler::HandleUnknownException();
		}
	}
}

void CCimMachineSimDlg::OnFileExit() 
{
	if(mMachineCore->CanQuit())
	{
		OnCancel();	
	}
}

void CCimMachineSimDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	if( nType != SIZE_MINIMIZED )
	{	
		CWnd *temp = MW_NULL;
		temp = GetDlgItem( IDC_MACHSIM_WND );
		if( temp )						
			temp->SetWindowPos( MW_NULL, 0, 0, cx - m_machSimWindowOffset, cy - m_machSimWindowOffset, SWP_NOZORDER | SWP_NOMOVE ) ; 
		if(mMachineCore)
			mMachineCore->OnSize(nType, cx, cy) ;
	}	
}

#define WM_SIZECHANGE			WM_USER + 101
#define WM_PAINT_EX				WM_USER + 103

BOOL CCimMachineSimDlg::OnEraseBkgnd( CDC* pDC )
{
	CWnd *temp = MW_NULL;
	temp = GetDlgItem( IDC_MACHSIM_WND );
	CBrush br;
	br.CreateSolidBrush(GetSysColor(COLOR_3DFACE));
	if (temp!=MW_NULL)
		CEraseBkGnd::Erase(this,temp,&br,pDC);
	
	br.DeleteObject();

	return TRUE;
}

void CCimMachineSimDlg::OnPaint() 
{
	PAINTSTRUCT ps;
	CDC* pDC = BeginPaint(&ps) ;

	//case 19930-MachSimEval kit have some refresh problems on Vista systems	
	if (mMachineCore!=MW_NULL)
	{
		HWND machsimWnd = mMachineCore->GetViewHWND() ;
		if (machsimWnd!=MW_NULL)
		{
			CWnd* pWnd = FromHandle(machsimWnd) ;
			pWnd->PostMessage(WM_PAINT_EX, 0, 0) ;
		}
	}

	EndPaint(&ps);	
}

void CCimMachineSimDlg::OnClose()
{
	if(mMachineCore && !mMachineCore->CanQuit())
		return;
	PostQuitMessage(0);
}

void CCimMachineSimDlg::OnDestroy()
{		
	if ( CNcMachineSimInitData::Instance()->GetCoreStartupParams().GetReportFileName() == _T("") )
	{
		SaveWindowPlacement();
	}
	CDialog::OnDestroy();
	// TODO: Add your message handler code here
}

LRESULT CCimMachineSimDlg::OnEndSim(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	OnFileExit();

	return 0;
}

void CCimMachineSimDlg::ShutDownMachSim()
{
	if(mMachineCore)
	{
		mMachineCore->ShutdownMachsim();
		delete mMachineCore;
		mMachineCore = MW_NULL;
	}
}

BOOL CCimMachineSimDlg::OnHelpInfo(HELPINFO* pHelpInfo)
{
	try
	{
		TCHAR buffer[MAX_PATH];
		GetModuleFileName(AfxGetApp()->m_hInstance, buffer, MAX_PATH);
		misc::mwFileName fn(buffer);
		fn.SetFileName(_T("machsim.chm"));
		if (misc::mwFileSystem::FileExists(fn.GetFilePath()))
		{
			::ShellExecute(m_hWnd, _T("open"), fn.GetFilePath().c_str(), 0, 0, SW_SHOW);
		}
	}
	catch (misc::mwException& exc)
	{
		mwCimExceptionHandler::HandleMiscException( exc );
	}
	catch(std::exception& exc)
	{
		mwCimExceptionHandler::HandleStdException( exc );
	}
	catch(...) 
	{
		mwCimExceptionHandler::HandleUnknownException();
	}

	return CDialog::OnHelpInfo(pHelpInfo);
}

const _TCHAR* CCimMachineSimDlg::m_windowSection = _T("Window");

void CCimMachineSimDlg::LoadWindowPlacement()
{
	WINDOWPLACEMENT wp, newWP;

	CWinApp* theApp(AfxGetApp());

	newWP.length=sizeof(newWP);
	newWP.showCmd=theApp->GetProfileInt(m_windowSection,_T("show"),-1);

	//If window placement information not available
	if( newWP.showCmd == -1 )
	{
		ShowWindow(SW_MAXIMIZE);
	}

	wp.length=sizeof(wp);
	GetWindowPlacement(&wp);

	newWP.length=sizeof(newWP);
	newWP.showCmd=theApp->GetProfileInt(m_windowSection,_T("show"),wp.showCmd);
	newWP.flags=theApp->GetProfileInt(m_windowSection,_T("flags"),wp.flags);
	newWP.ptMinPosition.x=theApp->GetProfileInt(m_windowSection,_T("minposx"),			wp.ptMinPosition.x);
	newWP.ptMinPosition.y=theApp->GetProfileInt(m_windowSection,_T("minposy"),			wp.ptMinPosition.y);
	newWP.ptMaxPosition.x=theApp->GetProfileInt(m_windowSection,_T("maxposx"),			wp.ptMaxPosition.x);
	newWP.ptMaxPosition.y=theApp->GetProfileInt(m_windowSection,_T("maxposy"),			wp.ptMaxPosition.y);
	newWP.rcNormalPosition.left=theApp->GetProfileInt(m_windowSection,_T("left"),		wp.rcNormalPosition.left);
	newWP.rcNormalPosition.top=theApp->GetProfileInt(m_windowSection,_T("top"),		wp.rcNormalPosition.top);
	newWP.rcNormalPosition.right=theApp->GetProfileInt(m_windowSection,_T("right"),	wp.rcNormalPosition.right);
	newWP.rcNormalPosition.bottom=theApp->GetProfileInt(m_windowSection,_T("bottom"),	wp.rcNormalPosition.bottom);

	CRect workArea;
	// Get a handle to the monitor
	HMONITOR hMonitor = ::MonitorFromPoint( CPoint(newWP.rcNormalPosition.left, newWP.rcNormalPosition.top), MONITOR_DEFAULTTONEAREST );

	// Get the monitor info
	MONITORINFO monInfo;
	monInfo.cbSize = sizeof(MONITORINFO);
	if(::GetMonitorInfo( hMonitor, &monInfo )==0)
	{
		workArea = CRect(0,0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN));

		::SystemParametersInfo(SPI_GETWORKAREA,0,&workArea,0);
	}
	else
	{
		workArea = monInfo.rcWork;
	}

	CRect rcTemp=newWP.rcNormalPosition;
	// See if any part of the window is on the screen

	if(::IntersectRect(&newWP.rcNormalPosition, &rcTemp, &workArea))
	{
		if(rcTemp!=newWP.rcNormalPosition)
		{	// some part of the window is off the screen
			if(rcTemp.left<workArea.left) 		// off the left edge
				rcTemp.OffsetRect(workArea.left-rcTemp.left,0);
			if(rcTemp.right>workArea.right) 	// off the right edge
				rcTemp.OffsetRect(workArea.right-rcTemp.right,0);
			if(rcTemp.left<workArea.left) 		// won't fit, shrink
				rcTemp.left=workArea.left;
			if(rcTemp.top<workArea.top) 		// off the top edge
				rcTemp.OffsetRect(0,workArea.top-rcTemp.top);
			if(rcTemp.bottom>workArea.bottom) 	// off the bottom edge
				rcTemp.OffsetRect(0,workArea.bottom-rcTemp.bottom);
			if(rcTemp.top<workArea.top) 		// won't fit, shrink
				rcTemp.top=workArea.top;
			newWP.rcNormalPosition=rcTemp;
		}
	}
	else
	{	// entire window is offscreen
		newWP=wp;
	}

	if(newWP.showCmd==SW_SHOWMINIMIZED || newWP.showCmd==SW_MINIMIZE)
	{	// check default to see if this was requested
		newWP.showCmd=SW_RESTORE;		// set restore flag
	}

	if(newWP.showCmd!=SW_MAXIMIZE && wp.showCmd==SW_MAXIMIZE)
	{	// check default to see if this was requested
		newWP.showCmd=SW_MAXIMIZE;		// set maximize flag
	}

	SetWindowPlacement(&newWP);
}

void CCimMachineSimDlg::SaveWindowPlacement()
{
	WINDOWPLACEMENT wp;
	this->GetWindowPlacement(&wp);

	CWinApp* theApp(AfxGetApp());

	//Registry key gets sometimes reset by the submodules
	theApp->m_pszRegistryKey = _tcsdup(_T("MW"));	

	theApp->WriteProfileInt(m_windowSection,_T("show"),wp.showCmd);
	theApp->WriteProfileInt(m_windowSection,_T("flags"),wp.flags);
	theApp->WriteProfileInt(m_windowSection,_T("minposx"),wp.ptMinPosition.x);
	theApp->WriteProfileInt(m_windowSection,_T("minposy"),wp.ptMinPosition.y);
	theApp->WriteProfileInt(m_windowSection,_T("maxposx"),wp.ptMaxPosition.x);
	theApp->WriteProfileInt(m_windowSection,_T("maxposy"),wp.ptMaxPosition.y);
	theApp->WriteProfileInt(m_windowSection,_T("left"),wp.rcNormalPosition.left);
	theApp->WriteProfileInt(m_windowSection,_T("top"),wp.rcNormalPosition.top);
	theApp->WriteProfileInt(m_windowSection,_T("right"),wp.rcNormalPosition.right);
	theApp->WriteProfileInt(m_windowSection,_T("bottom"),wp.rcNormalPosition.bottom);
}
