// NcDefaultsHandler.h: interface for the NsNcDefaultsHandler class.
//
//////////////////////////////////////////////////////////////////////

#ifndef NsNcDefaultsHandler_h
#define NsNcDefaultsHandler_h

#include "mwStringConversions.hpp"
namespace NsNcDefaultsHandler  
{
	// Last X, Y, Z used. (0, 0, 0) for the first usage
	void GetXYZ (misc::mwstring& oX, misc::mwstring& oY, misc::mwstring& oZ);
	void SetXYZ (const misc::mwstring& iX, const misc::mwstring& iY, const misc::mwstring& iZ);
	//Get the RGB for the background
	 void GetRGB ( misc::mwstring& oRed,  misc::mwstring& oGreen,  misc::mwstring& oBlue);
	 void SetRGB (const misc::mwstring& iRed, const misc::mwstring& iGreen, const misc::mwstring& iBlue);

	// Last Machine used, "Head Head" for first usgae
	misc::mwstring GetMachineDef(const misc::mwstring& defaultMachineName = _T(""));
	void SetMachineDef (const misc::mwstring& iMachineDef);

	misc::mwstring GetMxp ();
	void SetMxp (misc::mwstring& iMxp);
};

#endif // NsNcDefaultsHandler_h
