// NcMachineSimInitData.h: interface for the CNcMachineSimInitData class.
//
//////////////////////////////////////////////////////////////////////

#ifndef CNcMachineSimInitData_h
#define CNcMachineSimInitData_h
#include <string>
#include "mwStringConversions.hpp"
#include "mwCimMachSimCoreStartupParams.hpp"
//				<<Singleton>>

// CNcMachineSimInitData
class CNcMachineSimInitData  
{
private:
	CNcMachineSimInitData (){}
	~CNcMachineSimInitData(){}
	CNcMachineSimInitData (const CNcMachineSimInitData&){}

public:
	
	static CNcMachineSimInitData *Instance()
	{
		if (!stInstance)
			stInstance = new CNcMachineSimInitData;
		return stInstance;
	}

	bool Init ( const mwCimMachSimCoreStartupParams& coreStartupParams,
							const misc::mwstring& iRegPath  );

	// Get the core's startup params
	const mwCimMachSimCoreStartupParams& GetCoreStartupParams() { return m_coreStartupParams; }
	misc::mwstring GetRegPath() { return m_regPath; }
	
	void SetResourceHandle(HMODULE iResourceHandle){mResourceHandle = iResourceHandle;}
	HMODULE GetResourceHandle() {return mResourceHandle;}

private:
	mwCimMachSimCoreStartupParams	m_coreStartupParams;
	misc::mwstring					m_regPath;
	// resources handler
	HMODULE							mResourceHandle;

	static CNcMachineSimInitData*	stInstance;
};

#endif // of ifndef CNcMachineSimInitData_h
