#ifndef __SETUPDLG_H__
#define __SETUPDLG_H__
// SetupDlg.h : header file
//
#include "afxpriv.h"
#include "mwAvoid_Warnings.hpp"
#include "mwStringConversions.hpp"
#include "mwUnitsFactory.hpp"
#include <string>
#include <vector>

/////////////////////////////////////////////////////////////////////////////
// CSetupDlg dialog

class AFX_EXT_CLASS CSetupDlg : public CDialog
{
// Construction
public:
	CSetupDlg(const std::vector<misc::mwstring>& machineList,const misc::mwstring &defaultMach,
		const misc::mwstring &PartOrigX,const misc::mwstring &PartOrigY,const misc::mwstring &PartOrigZ,
		const measures::mwUnitsFactory::Units units,
		CWnd* pParent = MW_NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetupDlg)
	enum { IDD = IDD_UISETUP };
	CComboBox	m_UIMachineDef;
	double	mPartOrigX;
	double	mPartOrigY;
	double	mPartOrigZ;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
public:
// Implementation
	const double GetPartOrigX() const
	{
		return mPartOrigX;
	}
	const double GetPartOrigY() const
	{
		return mPartOrigY;
	}
	const double GetPartOrigZ() const
	{
		return mPartOrigZ;
	}
	const int GetIndexMach() const
	{
		return mIndexMach;
	}	

protected:
	void UpdateMachineDefList();
	const std::vector<misc::mwstring> mMachineList;
	int mIndexMach;
	misc::mwstring mDefaultMach;
	measures::mwUnitsFactory::Units m_units;
	// Generated message map functions
	//{{AFX_MSG(CSetupDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__SETUPDLG_H__)
