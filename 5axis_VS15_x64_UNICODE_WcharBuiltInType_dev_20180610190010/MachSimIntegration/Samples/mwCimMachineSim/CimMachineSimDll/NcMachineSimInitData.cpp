// NcMachineSimInitData.cpp: implementation of the CNcMachineSimInitData class.
//
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "NcMachineSimInitData.h"

CNcMachineSimInitData *CNcMachineSimInitData::stInstance = MW_NULL;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bool CNcMachineSimInitData::Init (const mwCimMachSimCoreStartupParams& coreStartupParams,
								  const misc::mwstring& iRegPath )
{ 
	m_coreStartupParams = coreStartupParams;
	m_regPath = iRegPath;	

	return true;
}