@echo off
if "%1"=="" (
Set LANGUAGE=en
goto Commands
)
Set LANGUAGE=%1

:Commands
set RC_FILE_GEN=..\..\..\..\..\..\libraries\rc_file_gen\rc_file_gen.exe
if not exist %RC_FILE_GEN% set RC_FILE_GEN=..\..\..\..\..\..\libraries\rc_file_gen\ReleaseUnicode\rc_file_gen.exe
if not exist %RC_FILE_GEN% set RC_FILE_GEN=..\..\..\..\..\..\libraries\rc_file_gen\Release\rc_file_gen.exe
if not exist %RC_FILE_GEN% set RC_FILE_GEN=..\..\..\..\..\..\libraries\rc_file_gen\DebugUnicode\rc_file_gen.exe
if not exist %RC_FILE_GEN% set RC_FILE_GEN=..\..\..\..\..\..\libraries\rc_file_gen\Debug\rc_file_gen.exe


REM %RC_FILE_GEN% -%Language% -c ..\CimMachineSim_res\cvs_msgmap.bin ..\
%RC_FILE_GEN% -%Language%  ..\CimMachineSimExe\
if ERRORLEVEL 1 exit /B 1

if not exist .\res md .\res
copy out_res.rc    .\res\CimMachineSim_res.rc2


rem "cleaning section has been moved to postbuild.bat, because some files are needed to create _res.dll"

@echo Copied resource files for CimMachineSim_res