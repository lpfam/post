message(STATUS "Generating a dummy CimMachineSimRes. The original file is located at CimMachineSimExe/English/. CimMachineSimExe copy the CimMachineSim_res.dll.")

mw_target(

	MW_RESOURCE:
		CimMachineSim_res en	#has only engl resources

	RESOURCE_LANGUAGE_SOURCES:
		CimMachineSimExe

	UNICODE: off
	
)

# this command do not fit for resources
#add_custom_command(TARGET CimMachineSim_res
#	POST_BUILD
#		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/../CimMachineSimExe/English/CimMachineSim_res.dll   ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CMAKE_CFG_INTDIR}/.
#	)




	
