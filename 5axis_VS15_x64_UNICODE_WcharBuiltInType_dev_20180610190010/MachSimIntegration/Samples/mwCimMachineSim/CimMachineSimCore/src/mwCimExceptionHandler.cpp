/******************************************************************************
*               File: mwCimExceptionHandler.hpp								  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  05.09.2008 12:00:00 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "mwCimExceptionHandler.hpp"
#include "mwResourceDll.hpp"
#include "mwCim5axResource.hpp"
#include "mwCimMachSimCoreResource.hpp"
#include "mwCLParserException.hpp"

//#############################################################################
void mwCimExceptionHandler::HandleDefaultException(misc::mwException& e)
{
	misc::mwstring strError ( _T("Error occured in program(using default error message): "));
	strError +=e.GetErrorMessage();	
	const misc::mwException::exceptionStack &stack = 
		e.GetExceptionStack();	
	for(unsigned int i = 0; i < stack.size(); i++ )
	{
		strError += _T("( ");
		strError += stack[i].second;
		strError += _T(" )");
	}
	AfxMessageBox( strError.c_str() );
}

//#############################################################################
void mwCimExceptionHandler::HandleParserException(mwCLParserException& e)
{
	AfxMessageBox( e.GetCompleteErrorMessage().c_str() );
}

//#############################################################################
void mwCimExceptionHandler::HandleMiscException(misc::mwException& e)
{
	try
	{
		if (e.GetErrorCode() == 0 )
		{
			AfxMessageBox( e.GetCompleteErrorMessage().c_str() );
			return;
		}
		misc::mwstring strError( mwCim5axResource::handle.GetMsg(e.GetErrorCode(), _T("exceptions")) );
		const misc::mwException::exceptionStack &stack = 
			e.GetExceptionStack();	
		for(unsigned int i = 0; i < stack.size(); i++ )
		{
			strError += _T("( ");
			strError += mwCim5axResource::handle.GetMsg( stack[i].first, _T("exceptions"));
			strError += _T(" )");
		}

		AfxMessageBox( strError.c_str() );
	}
	catch (...)
	{
		HandleDefaultException(e);
	}
}

//#############################################################################
void mwCimExceptionHandler::HandlePostException(post::mwPostException& e)
{
	AfxMessageBox( e.GetCompleteErrorMessage().c_str() );
}

//#############################################################################
void mwCimExceptionHandler::HandleMachSimException( exceptions::mwMachSimException& e )
{
	AfxMessageBox( e.GetCompleteErrorMessage().c_str() );
}

//#############################################################################
void mwCimExceptionHandler::HandleStdException(std::exception& e)
{
	AfxMessageBox( misc::from_ascii(e.what()).c_str() );
}
//#############################################################################
void mwCimExceptionHandler::HandleUnknownException()
{
	try
	{
		AfxMessageBox( mwCim5axResource::handle.GetMsg(0, _T("exceptions")).c_str() );
	}
	catch (...)
	{
		AfxMessageBox(_T("An unknown exception has occured"));
	}
}
//#############################################################################