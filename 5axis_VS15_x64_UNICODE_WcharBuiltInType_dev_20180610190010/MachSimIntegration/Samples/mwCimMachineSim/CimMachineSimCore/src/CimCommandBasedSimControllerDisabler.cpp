/********************************************************************
	created:	03 Feb 2012 16:45
	filename: 	CimCommandBasedSimControllerDisabler.cpp
	author:		Mihai Vasilian
	purpose:	implementation of CimCommandBasedSimControllerDisabler class
	copyright (c) 2012 by ModuleWorks GmbH	
*********************************************************************/

#include "StdAfx.h"
#include "CimCommandBasedSimControllerDisabler.hpp"



CimCommandBasedSimControllerDisabler::CimCommandBasedSimControllerDisabler(CCimMachineSimCore* cimMachineCore)
{
	m_cimMachineCore = cimMachineCore;
	if(m_cimMachineCore)
	{
		m_cimMachineCore->EnableCommandBasedSimulationController(false);
	}
};


CimCommandBasedSimControllerDisabler::~CimCommandBasedSimControllerDisabler()
{
	if(m_cimMachineCore)
	{
		m_cimMachineCore->EnableCommandBasedSimulationController(true);
	}
};