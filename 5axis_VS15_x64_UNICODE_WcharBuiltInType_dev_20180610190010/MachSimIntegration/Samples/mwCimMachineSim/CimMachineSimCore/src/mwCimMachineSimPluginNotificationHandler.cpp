/******************************************************************************
*               File: mwCimMachineSimPluginNotificationHandler.cpp			  * 
*******************************************************************************
*               Description: mwCimMachineSimPluginNotificationHandler class   *
*                            definition                                       *
*******************************************************************************
*               History:                                                      *
*  30.03.2010 13:14:00 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2010 by ModuleWorks GmbH                                  *
******************************************************************************/
//! local header files
#include "StdAfx.h"
#include "mwCimMachineSimPluginNotificationHandler.hpp"
#include "mwMachSimSimulationStatus.hpp"

//#############################################################################
void mwCimMachineSimPluginNotificationHandler::OnChangedToolPathPosition()
{
	if (!m_stopPositions.empty())
	{
		double completionFactor;
		if (m_coreFacade->GetSimulationStatus().IsRunning())
		{
			opCollection::mwMachSimComplexMoveIterator crtMove = m_coreFacade->GetCurrentMove(completionFactor);
			if (completionFactor == 1)
			{
				unsigned int currentNciNr = crtMove->GetNciNr();

				std::vector<unsigned int>::iterator i = find(m_stopPositions.begin(), m_stopPositions.end(), currentNciNr);

				if (i != m_stopPositions.end())
				{
					m_coreFacade->PauseSimulation();				
				}

			}
		}
	}
}