// CimMachineSimCore.cpp : Defines the entry point for the DLL application.
//

#include "StdAfx.h"
//#############################################################################
#include "CCimMachineSimCore.h"
#include "mwFileFinder.hpp"
#include "mwFileName.hpp"
#include "mwFileSystem.hpp"
#include "mwMPSDef.hpp"
#include "mwMPSInput.hpp"
#include "mwCommandLineInfo.hpp"
#include "mw5axMachDef.hpp"
#include "mwSTLTranslator.hpp"
#include "mwCimMachSimCoreResource.hpp"
#include "mwCimMachSimResource.hpp"
#include "mwCim5axResource.hpp"
#include "mwBIBasicTypes.hpp"
#include "mwStdInputStream.hpp"
#include "mwCimMachineSimPluginNotificationHandler.hpp"
#include "mwMachDefReader.hpp"
#include "mwMSimOperationBuilder.hpp"
#include "mwIniFileService.hpp"
#include "mwGLRenderingContextPreservingProgressHandlerDecorator.hpp"
#include "CimCommandBasedSimControllerDisabler.hpp"
#include "mwSTLReaderSrv.hpp"
#include "mwMachSimUtils.hpp"
#include "mwPostDefXMLReader.hpp"
#include "mwV2XMLReader.hpp"


//#############################################################################
//#############################################################################
//#############################################################################

#define WM_SIZECHANGE			WM_USER + 101
#define WM_PAINT_EX				WM_USER + 103

// Global definitions:
const double cInchFactor = 25.4;

#define LARGE_SIM_THRESHOLD			2000000
#define LARGE_SIM_THRESHOLD_WIN_64	1000000000

//#############################################################################
//#############################################################################
//#############################################################################

// This is the constructor of a class that has been exported.
// see CCimMachineSimCore.h for the class definition

CCimMachineSimCore::CCimMachineSimCore(const mwCimMachSimCoreStartupParams& machSimFilesLocation)
	:m_startupParams(machSimFilesLocation),
	m_iniParams ( GetIniFileName().GetFilePath() ),
	m_machSimWndHandle( MW_NULL ),
	m_machsimGuiDll(MW_NULL),
	m_machSimGUIFacade(MW_NULL),
	m_machSimCoreFacade(MW_NULL)
{	
	// Preprocess startup params

	if (misc::mwIS64Build())
	{
		m_moveNumberLimitForDisablingCollisionCheckingAndToolPathGraphic = LARGE_SIM_THRESHOLD_WIN_64;
	}
	else
	{																		
		m_moveNumberLimitForDisablingCollisionCheckingAndToolPathGraphic = 	misc::mwIniFileService::GetInt(_T("startup"), _T("large_toolpath_threshold"),0, m_iniParams.GetIniFilePath());

		if (!m_moveNumberLimitForDisablingCollisionCheckingAndToolPathGraphic)
		{
			m_moveNumberLimitForDisablingCollisionCheckingAndToolPathGraphic = LARGE_SIM_THRESHOLD;
		}
	}
}

//#############################################################################
CCimMachineSimCore::~CCimMachineSimCore()
{	
}

//#############################################################################
void CCimMachineSimCore::SetMachineSimColor(misc::mwstring iRed, misc::mwstring iGreen, misc::mwstring iBlue)
{
	MachineSimColorArray[eRed] = iRed;
	MachineSimColorArray[eGreen] = iGreen;
	MachineSimColorArray[eBlue] = iBlue;
}
//#############################################################################
void CCimMachineSimCore::NormalRGBValue()
{
	double aRedVal, aGreenVal, aBlueVal;
	misc::to_value(MachineSimColorArray[eRed],aRedVal);
	misc::to_value(MachineSimColorArray[eGreen],aGreenVal);
	misc::to_value(MachineSimColorArray[eBlue],aBlueVal);
	if ( ((aRedVal <= 255) && (aRedVal >= 0 )) && ((aGreenVal <= 255) && (aGreenVal >= 0 )) && ((aBlueVal <= 255) && (aBlueVal >= 0 ))) 
	{
		aRedVal /=  255;
		aGreenVal /=255;
		aBlueVal /= 255;
	}
	else
	{

		double aSumRGB = ((aRedVal * aRedVal) + (aGreenVal * aGreenVal) + (aBlueVal *aBlueVal));
		aRedVal =sqrt((aRedVal * aRedVal) / aSumRGB);
		aGreenVal =sqrt((aGreenVal * aGreenVal) / aSumRGB);
		aBlueVal =sqrt((aBlueVal *aBlueVal) / aSumRGB);
	}
	
	MachineSimColorArray[eRed] = misc::from_value(aRedVal);
	MachineSimColorArray[eGreen] = misc::from_value(aGreenVal);
	MachineSimColorArray[eBlue] = misc::from_value(aBlueVal);
}

//#############################################################################
BOOL CCimMachineSimCore::RunMachSim()
{
	misc::mwstring mdFileName = misc::mwFileSystem::CombinePath(m_currentMachineFolder, m_machineDefName + _T(".xml"));
	misc::mwstring ncFileName = misc::mwFileSystem::CombinePath(m_currentMachineFolder, _T("outputForMachSim.sim"));	

	m_iniParams.ReadMachSimIniParams();

	m_iniParams.SetMachineDefinitionFileName(mdFileName) ;
	m_iniParams.SetNCFileName(ncFileName) ;
	m_iniParams.SetAutoLoad(false);
	m_iniParams.SetParentHandle( m_ParentWindow ) ;
	
	if( !m_iniParams.HasDisplayUnitSystem()
		|| m_iniParams.GetDisplayUnitSystem() == measures::mwUnitsFactory::METRIC )
		m_Metric = true;
	else
		m_Metric = false;

	double aPartTol = m_iniParams.GetTargetTolerance();
	const misc::mwstring& startupPartTol = m_startupParams.GetPartTolerance();
	if(startupPartTol != _T(""))
	{
		aPartTol = ConvertToDoubleUsingLocale(startupPartTol);
	}

	double aPartOffset = m_iniParams.GetTargetOffset();
	const misc::mwstring& aPartOffsetString = m_startupParams.GetPartOffset();
	if ( aPartOffsetString!=_T("") )
	{
		aPartOffset = ConvertToDoubleUsingLocale(aPartOffsetString);
	}


	// Set the right units
	if(!m_Metric)
	{
		aPartTol /= cInchFactor;
		aPartOffset /= cInchFactor;
	}
	
	m_iniParams.SetTargetTolerance(aPartTol) ;
	m_iniParams.SetTargetOffset(aPartOffset) ;
	m_iniParams.SetSTLTolerance(aPartTol) ;

	////get the bg RGB it is have to be between 0 - 1
	float aRedVal, aGreenVal, aBlueVal;
	misc::to_value(MachineSimColorArray[eRed],aRedVal);
	misc::to_value(MachineSimColorArray[eGreen],aGreenVal);
	misc::to_value(MachineSimColorArray[eBlue],aBlueVal);
	if (!(aRedVal <= 1 && aRedVal >= 0 ) && (aGreenVal <= 1 && aGreenVal >= 0 ) && (aBlueVal <= 1 &&  aBlueVal >= 0 )) 
	{
		NormalRGBValue();
	}

	m_iniParams.SetBgColor(misc::mwColor(aRedVal, aGreenVal, aBlueVal)) ;
	
	mwMachSimCollisionDetectionParameters collisionDetectionParameters = m_iniParams.GetCollisionDetectionParameters();

	if ( m_startupParams.GetBetweenMovesCollCheck() !=_T("") )
	{
		if(m_startupParams.GetBetweenMovesCollCheck() == _T("0"))
			collisionDetectionParameters.SetMode( mwMachSimCollisionDetectionParameters::CC_DISCRETE_CHECKING );
		else 
			collisionDetectionParameters.SetMode( mwMachSimCollisionDetectionParameters::CC_CONTINUOUS_CHECKING_FOR_TOOL_CONTAINING_PAIRS);
		
		m_iniParams.SetCollisionDetectionParameters( collisionDetectionParameters );
	}

	if (m_startupParams.GetVerifierFileName()!=_T(""))
	{
		m_iniParams.SetVerifierFileName(m_startupParams.GetVerifierFileName()) ;
	}
	if (m_startupParams.GetVerifierGUIFileName()!=_T(""))
	{
		m_iniParams.SetVerifierGUIFileName(m_startupParams.GetVerifierGUIFileName()) ;
	}
	if (m_startupParams.GetVerifierGUIResourceFileName()!=_T(""))
	{
		m_iniParams.SetVerifierGUIResourceFileName(m_startupParams.GetVerifierGUIResourceFileName() );
	}

	if (m_iniParams.GetVerifierGUIIniFileName() == _T(""))
	{
		misc::mwstring fullWorkingDir = m_iniParams.GetFullPathRelativeToCurrentDirectory(m_startupParams.GetMachSimFolder());
		misc::mwstring fullIniPath = misc::mwFileSystem::CombinePath(fullWorkingDir, "mwVerifierGui.ini");
		misc::mwstring relativeToExeIniPath = m_iniParams.GetRelativeFilePath(
			m_iniParams.GetExecutablesDirectory(),true,fullIniPath,false);
		m_iniParams.SetVerifierGUIIniFileName(relativeToExeIniPath);
	}

	if (m_startupParams.GetMachSimResourceFileName()!=_T(""))
	{
		m_iniParams.SetResourceFileName( m_startupParams.GetMachSimResourceFileName() );
	}

	if( m_startupParams.GetSimFile() != _T(""))
	{
		m_iniParams.SetNCFileName( m_startupParams.GetSimFile());
	}
	else if( m_startupParams.GetSimulateBinFile() != _T(""))
	{
		m_iniParams.SetNCFileName( m_startupParams.GetSimulateBinFile() );
	}
		
	m_iniParams.WriteMachSimIniParams() ;

	if (m_startupParams.GetGeomCollCheckWhenVerifierEnabled() != _T(""))
	{
		int geomCollCheckWhenVerifierEnabled = 1;

		if (m_startupParams.GetGeomCollCheckWhenVerifierEnabled() == _T("0"))
		{
			geomCollCheckWhenVerifierEnabled = 0;	
		}

	}
	return  InitializeMachSim() ;

}

//#############################################################################
void CCimMachineSimCore::SetMachineDefName(const misc::mwstring machineDefName)
{
	try
	{
		misc::mwstring rootdir=m_startupParams.GetMachDefFolder();
		if (rootdir == _T(""))
		{
			m_xmlPath = machineDefName;
		}
		else
		{
			//next lines are for quicker debugging in ModuleWorks office, please leave them on...
			if( !misc::mwFileSystem::IsDirectory( rootdir ) )
			{
				misc::string_down( rootdir );
				misc::mwstring::size_type subPos( rootdir.rfind( _T("program\\xml\\") ) );
				if( subPos != misc::mwstring::npos )
				{
					rootdir.erase( subPos );
					rootdir += _T("program\\nc\\ModuleWorks\\xml\\");
				}
			}
			//end of lines for quicker ModuleWorks debugging

			m_xmlPath = rootdir + machineDefName + _T("\\") + machineDefName + _T(".xml");
		}

		misc::mwFileName targetXMLFile(m_xmlPath);
		m_currentMachineFolder = targetXMLFile.GetFileDirectory();
		m_machineDefName = targetXMLFile.GetFileCoreName();
	}
	catch (...)
	{
		misc::mwstring error(_T("An unknown error occurred while retrieving the "));
		error += machineDefName;
		error += _T(" machine definition data: ");
		throw misc::mwException(0, error);
	}
}
//#############################################################################
const misc::mwstring CCimMachineSimCore::GetMachineDefName() const
{
	return m_machineDefName;
}
//#############################################################################
void CCimMachineSimCore::CopyAscFile()
{
	misc::mwstring ts_source,ts_dest;
	ts_source = m_startupParams.GetAscFile();
	const misc::mwstring stdAscFileName(_T("toolpath.asc"));
	ts_dest = m_currentMachineFolder + stdAscFileName;
	CopyFile(ts_source.c_str(),ts_dest.c_str(),false);
}

//#############################################################################
const std::vector<misc::mwstring> CCimMachineSimCore::GetMachineDefinitionNames()
{
	std::vector<misc::mwstring> rtrnMachines;

	if (m_startupParams.GetMachineReplacementOption() == _T("none"))
	{
		rtrnMachines.push_back(m_machineDefName);
		return rtrnMachines;
	}

	misc::mwstring rootdir=m_startupParams.GetMachDefFolder();
	//find all directories in rootDir
	misc::mwFileFinder filefinder(rootdir,_T("*.*"));
	filefinder.QueryDirectory(false);
	//iterate over cadcam::mw5axMachDefcadcam::mw5axMachDefthem 
	misc::mwFileFinder::const_iterator it=filefinder.GetDirListBegin();	
	misc::mwFileFinder::const_iterator ite=filefinder.GetDirListEnd();	
	misc::mwstring dirname;
	for (;it!=ite;it++)
	{		
		try
		{
			dirname=(*it);		
			dirname.erase(0,rootdir.length());

			//check if directory contains the corresponding files: (.xml) 
			misc::mwFileFinder filefinder2(rootdir+dirname+_T("\\"),dirname+_T(".xml"));filefinder2.QueryFile(false);
			
			bool f1=filefinder2.GetFileListBegin()!=filefinder2.GetFileListEnd();
			
			if(f1)
			{
				const misc::mwstring filePath = rootdir+dirname+_T("\\")+dirname+_T(".xml");
				mwMachDefReader reader(filePath);
				post::mwMachDefBuilder machDefBlt = reader.GetMachDefBuilder(); // just for test, we make sure xml is real and there exist a machine definition
				
				bool _3Axis = false;
			
				if(m_startupParams.GetMachineReplacementOption() == _T("only_3_axis") &&
					machDefBlt.GetMachDef()->GetNumberOfAxes() != post::mwMachDef::MACHINE_3AXIS)
				{
					_3Axis = true;
				}

				if(_3Axis == false)
					rtrnMachines.push_back(dirname);// add xml path name

			}
		}
		catch (misc::mwException& ex)
		{
			misc::mwstring error(_T("An error occurred while retrieving the "));
			error+=dirname;
			error+=_T(" machine definition data: ");
			error+=ex.GetCompleteErrorMessage();
			AfxMessageBox( error.c_str() );
		}
		catch (...)
		{
			misc::mwstring error(_T("An unknown error occurred while retrieving the "));
			error+=dirname;
			error+=_T(" machine definition data");

			AfxMessageBox( error.c_str() );
		}
	}
	if (rtrnMachines.size()==0)		
	{
		misc::mwstring err(_T("No machine definitions available"));
		AfxMessageBox( err.c_str() );
	}
	return rtrnMachines;
}

//#############################################################################
const bool CCimMachineSimCore::DoesUserSpecifiedAscFileExist() const
{
	return misc::mwFileSystem::FileExists( m_startupParams.GetAscFile() );
}

//#############################################################################
//#############################################################################

//#############################################################################
misc::mwFileName CCimMachineSimCore::GetIniFileName()
{
	if ( !(m_startupParams.GetMachSimFolder().empty()) )
	{
		misc::mwstring ini = m_startupParams.GetMachSimFolder()+_T("machsim.ini") ;
		return misc::mwFileName( ini );
	}
	else
	{
		_TCHAR iniFileName[MAX_PATH];
		::GetModuleFileName( 0, iniFileName, MAX_PATH );
		misc::mwFileName	machsim_ini_file(iniFileName);
		machsim_ini_file.SetFileName(_T("machsim.ini"));
		return machsim_ini_file;
	}
}

//#############################################################################
LRESULT CCimMachineSimCore::OnInitializeMachSim( WPARAM wParam, LPARAM lParam )
{
	if ( InitializeMachSim() == 1 ) return 1;
	return 0;
}

//#############################################################################
void CCimMachineSimCore::OnSize(UINT nType, int cx, int cy)
{	
	if(m_machSimWndHandle)
	{
		CWnd* pMSWnd = CWnd::FromHandle( m_machSimWndHandle );		
		pMSWnd->PostMessage( WM_SIZECHANGE, NULL, 0) ;
		pMSWnd->PostMessage( WM_PAINT_EX, NULL, 0 ) ;
	}

}

//#############################################################################
BOOL CCimMachineSimCore::InitializeMachSim()
{	
	misc::mwFileName iniFileName = GetIniFileName();
	if ( ! misc::mwFileSystem::FileExists( iniFileName.GetFilePath() ) )
	{	
		AfxMessageBox( _T("INI file not found, program execution will be aborted") );
		PostQuitMessage(1);
		return FALSE;
	}
	
	const bool only3Axis = m_startupParams.GetMachineReplacementOption() == _T("only_3_axis");

	if (only3Axis)
	{
		mwMachDefReader xmlReader(m_xmlPath);
		if(xmlReader.GetMachDefBuilder().GetMachDef()->GetNumberOfAxes() != post::mwMachDef::MACHINE_3AXIS)
		{
			throw misc::mwException(0,_T("Only 3 axis machines can be used"));
		}
	}

	mwMachSimIniParams currentIniParams( iniFileName.GetFilePath() );

	if( !LoadResourceDLLs(currentIniParams, iniFileName) )
		return FALSE;


	misc::mwstring exeDir = currentIniParams.GetExecutablesDirectory();
	misc::mwstring winDir = currentIniParams.GetWorkingDirectory();

	if(m_startupParams.GetSimFile()!=_T(""))
	{
		misc::mwstring m_clPath = currentIniParams.GetFullPathRelativeToGivenDirectory(m_startupParams.GetSimFile(),winDir);
		misc::mwstring m_clPathOut = currentIniParams.GetRelativeFilePath(exeDir,true,m_clPath,false);
		currentIniParams.SetNCFileName(m_clPathOut);
	}

	if (m_startupParams.GetVerifierFileName()!=_T(""))
	{
		misc::mwstring m_verifierPath = currentIniParams.GetFullPathRelativeToGivenDirectory(m_startupParams.GetVerifierFileName(),winDir);
		misc::mwstring m_verifierPathOut = currentIniParams.GetRelativeFilePath(exeDir,true,m_verifierPath,false);
		currentIniParams.SetVerifierFileName(m_verifierPathOut);
	}

	if (m_startupParams.GetVerifierGUIFileName()!=_T(""))
	{
		misc::mwstring m_verifierGuiPath = currentIniParams.GetFullPathRelativeToGivenDirectory(m_startupParams.GetVerifierGUIFileName(),winDir);
		misc::mwstring m_verifierGuiPathOut = currentIniParams.GetRelativeFilePath(exeDir,true,m_verifierGuiPath,false);
		currentIniParams.SetVerifierGUIFileName(m_verifierGuiPathOut);
	}

	if (m_startupParams.GetVerifierGUIResourceFileName()!=_T(""))
	{
		misc::mwstring m_verifierGuiResPath = currentIniParams.GetFullPathRelativeToGivenDirectory(m_startupParams.GetVerifierGUIResourceFileName(),winDir);
		misc::mwstring m_verifierGuiResPathOut = currentIniParams.GetRelativeFilePath(exeDir,true,m_verifierGuiResPath,false);
		currentIniParams.SetVerifierGUIResourceFileName(m_verifierGuiResPathOut);
	}

	if (m_xmlPath!=_T(""))
	{
		misc::mwstring m_machinePath = currentIniParams.GetFullPathRelativeToGivenDirectory(m_xmlPath,winDir);
		misc::mwstring m_machinePathOut = currentIniParams.GetRelativeFilePath(exeDir,true,m_machinePath,false);
		currentIniParams.SetMachineDefinitionFileName(m_machinePathOut);
	}

	if (m_startupParams.GetResourceFileName()!=_T(""))
	{
		misc::mwstring m_resPath = currentIniParams.GetFullPathRelativeToGivenDirectory(currentIniParams.GetResourceFileName(),winDir);
		misc::mwstring m_resPathOut = currentIniParams.GetRelativeFilePath(exeDir,true,m_resPath,false);
		currentIniParams.SetResourceFileName(m_resPathOut);
	}

	currentIniParams.WriteMachSimIniParams();

	LoadUI( currentIniParams );

	m_machSimCoreFacade = m_machSimGUIFacade->GetCoreFacade();
	m_machSimWndHandle = m_machSimGUIFacade->GetViewHWND();
	
	// workpiece, stock and fixture .stl updates
	
	if ( DoesUserSpecifiedAscFileExist() )
	{
		CopyAscFile();
	}

	if( !misc::mwFileSystem::FileExists( currentIniParams.GetMachineDefinitionFileName() ))
		return true;
	else
	{
		mwIProgressDialogHandler* progress = m_machSimGUIFacade->CreateProgressDialog();
		if( !m_startupParams.GetReportFileName().empty() && !m_startupParams.GetReportFileNotOnTop().empty() )
			progress->SetStyles( mwIProgressDialogHandler::STYLE_NOT_ON_TOP );
		m_machSimGUIFacade->LoadMachineDefinition( currentIniParams.GetMachineDefinitionFileName(), progress );
	}

	CimCommandBasedSimControllerDisabler disableTimer(this);

	if( !misc::mwFileSystem::FileExists( currentIniParams.GetNCFileName() ) )
		return TRUE;
	else
	{
		misc::mwstring simCmdFileNameString( currentIniParams.GetNCFileName() );
		misc::mwFileSystem::NormalizeFile( simCmdFileNameString );
		misc::mwFileName simCmdFileName( simCmdFileNameString );

		//allow start app without parameters -34763
		const misc::mwstring& startupPartTol = m_startupParams.GetPartTolerance();	
		double partTolerance = m_iniParams.GetTargetTolerance();
		if( startupPartTol != _T("") )
		{
			partTolerance = ConvertToDoubleUsingLocale(startupPartTol);
		}

		// Set the right units
		if(!m_Metric)
		{
			partTolerance /= cInchFactor;
		}

		mwIProgressDialogHandler *progressHandler = m_machSimGUIFacade->CreateProgressDialog();
		if( !m_startupParams.GetReportFileName().empty() && !m_startupParams.GetReportFileNotOnTop().empty() )
			progressHandler->SetStyles( mwIProgressDialogHandler::STYLE_NOT_ON_TOP );

		mwIMachineStream iStream(m_xmlPath);
		machsim::mwV2XMLReader reader(&iStream, MW_NULL);
		m_machineDefinition = reader.CreateMachineDefinition(MW_NULL, false, false, false, true);

		mwPostDefXMLReader rdr;
		post::mwMXPParamPtr mxpParam = new post::mwMXPParam(mwPostDefXMLReader::GenerateInitialMultiXPostParams(m_machineDefinition->GetUnits(),
			post::mwMXPParam::ALL_LIMITS, MW_NULL, false, MW_NULL, MW_NULL));
		rdr.SetDefaultMXPParam(mxpParam);

		m_machineDefinition->SetMachinePostDef(rdr.ReadAllDefinitions(m_xmlPath));


		if( simCmdFileName.GetFileExtention() == _T("cl"))
		{
			m_operationsProgramme = 
				new mwMachSimOperationsProgramme(mwMachSimOperationsProgramme::operations());
			
			mwMSimOperationBuilder::GetOperationsProgrammeFromCLFile( m_operationsProgramme,
				m_startupParams.GetSimFile(), m_machineDefinition,
				cadcam::mwTPoint3d<double>(m_X,m_Y,m_Z), 
				GetMachineLimitsFromString(m_startupParams.GetEnforceMachineLimits()), 
				partTolerance, progressHandler);

			misc::mwAutoPointer<mwMachSimSimulation> simulation = new mwMachSimSimulation();
			simulation->SetOperationsProgramme(*m_operationsProgramme);

			SetMeshesFromStartupParamsReferencedInputFiles(simulation);

			m_machSimGUIFacade->LoadSimulation( *simulation, progressHandler ); 
		}
		else if( simCmdFileName.GetFileExtention() == _T("bin") )
		{
			m_operationsProgramme = mwMSimOperationBuilder::GetOperationsProgrammeFromBinFile(
				m_startupParams.GetSimulateBinFile(), m_machineDefinition,
				cadcam::mwTPoint3d<double>(m_X,m_Y,m_Z), 
				GetMachineLimitsFromString(m_startupParams.GetEnforceMachineLimits()),
				partTolerance );

			misc::mwAutoPointer<mwMachSimSimulation> simulation = new mwMachSimSimulation();
			simulation->SetOperationsProgramme(*m_operationsProgramme);

			SetMeshesFromStartupParamsReferencedInputFiles(simulation);

			m_machSimGUIFacade->LoadSimulation( *simulation, progressHandler ); 
		}

		delete progressHandler;
	}

	if ( currentIniParams.GetAutoStart() )
	{
		m_machSimCoreFacade->RunSimulation(false);
	}

	return TRUE ;
}	

void CCimMachineSimCore::SetMeshesFromStartupParamsReferencedInputFiles( misc::mwAutoPointer<mwMachSimSimulation> simulation )
{
	if( misc::mwFileSystem::FileExists( m_startupParams.GetStockFile() ) )
	{
		machsim::mkdPolygonalGeometry::polyDataRef stock = 
			machsim::STLReaderSrv::ReadStl( m_startupParams.GetStockFile(), GetUnits() );
		mwMachSimSimulation::NamedMeshes stockMeshes;
		mwMachSimSimulation::NamedMesh stockMesh( _T("stock"), stock );
		stockMeshes.push_back( stockMesh );
		simulation->SetStocks( stockMeshes );
	}

	if ( misc::mwFileSystem::FileExists( m_startupParams.GetStlFile() ) )
	{
		machsim::mkdPolygonalGeometry::polyDataRef workpiece = 
			machsim::STLReaderSrv::ReadStl( m_startupParams.GetStlFile(), GetUnits() );
		mwMachSimSimulation::NamedMeshes workpieceMeshes;
		mwMachSimSimulation::NamedMesh workpieceMesh( _T("workpiece"), workpiece );
		workpieceMeshes.push_back( workpieceMesh );
		simulation->SetWorkpieces( workpieceMeshes );
	}

	if ( misc::mwFileSystem::FileExists( m_startupParams.GetFixtureFile() ) )
	{
		machsim::mkdPolygonalGeometry::polyDataRef fixture = 
			machsim::STLReaderSrv::ReadStl( m_startupParams.GetFixtureFile(), GetUnits() );
		mwMachSimSimulation::NamedMeshes fixtureMeshes;
		mwMachSimSimulation::NamedMesh fixtureMesh( _T("fixture"), fixture );
		fixtureMeshes.push_back( fixtureMesh );
		simulation->SetFixtures( fixtureMeshes );
	}
}

//#############################################################################
void CCimMachineSimCore::ShutdownMachsim()
{
	if(m_machSimGUIFacade != MW_NULL)
	{
		mwMachSimGuiFree machsimGuiFree = reinterpret_cast<mwMachSimGuiFree>
		(::GetProcAddress(m_machsimGuiDll, "FreeMachSimGui"));
		if (machsimGuiFree != 0)
		{
			machsimGuiFree(m_machSimGUIFacade);
		}

		::AfxFreeLibrary(m_machsimGuiDll);
	}
		
	m_machsimGuiDll = MW_NULL;
	m_machSimCoreFacade = MW_NULL;	
	m_machSimGUIFacade = MW_NULL;
	m_machSimWndHandle = MW_NULL;
}

//#############################################################################
const measures::mwUnitsFactory::Units CCimMachineSimCore::GetUnits()
{
//temporary
//	if (m_simFile.IsNotNull())
//		return m_simFile->GetUnits();
	return measures::mwUnitsFactory::METRIC;
}

void CCimMachineSimCore::EnableCommandBasedSimulationController(bool enable)
{
	if(m_machSimGUIFacade == MW_NULL) return;

	m_machSimGUIFacade->EnableCommandBasedSimulationController(enable);
	
};

bool CCimMachineSimCore::CanQuit() const
{
	if(m_machSimWndHandle)
	{
		CWnd* guiFrame = CWnd::FromHandle(m_machSimWndHandle);
		if(guiFrame != NULL && guiFrame->IsWindowEnabled())
			return true;
	}
	return false;
}


//#############################################################################
const double CCimMachineSimCore::ConvertToDoubleUsingLocale(const misc::mwstring& toConvert)
{
	double rtrnValue;
	setlocale(LC_ALL,"");
	misc::to_value(toConvert,rtrnValue);
	setlocale(LC_ALL,"C");
	if (mathdef::is_eq(rtrnValue, 0))
	{
		misc::to_value(toConvert,rtrnValue);
	}
	return rtrnValue;
}

//#############################################################################
void CCimMachineSimCore::PostProcessOperations(std::list<mwMachSimCADCAMInputOperation>& operations)
{
	const misc::mwstring betweenMovesCollCheckForRapidMoves(m_startupParams.GetRapidMovesCollCheck());
	
	if ( betweenMovesCollCheckForRapidMoves== _T("never"))
	{
		SetAllRapidsAsPhantoms( operations );
	}
	else
	if ( betweenMovesCollCheckForRapidMoves== _T("only_if_no_orientation_change"))
	{
		SetOrientationChangingRapidsAsPhantoms( operations );
	}
}

//#############################################################################
void CCimMachineSimCore::SetAllRapidsAsPhantoms(std::list<mwMachSimCADCAMInputOperation>& /*operations*/)
{
	//temporary
	/*std::list<mwMachSimCADCAMInputOperation>::iterator opIt(operations.begin());
	for ( ; opIt != operations.end(); ++opIt )
	{
		cadcam::mwEnrichedPostedTP::Iterator itEnd( (*(opIt->GetEnrichedPostedTP())).GetMovesEnd());
		cadcam::mwEnrichedPostedTP::Iterator it;

		for ( it = enrichedPostedToolPath.GetMovesBegin(); it != itEnd; ++it )
		{
			if ( it->IsRapid() )
			{
				it->SetIsPhantom( true );
			}
		}
	}*/
}

//#############################################################################
void CCimMachineSimCore::SetOrientationChangingRapidsAsPhantoms(std::list<mwMachSimCADCAMInputOperation>& /*operations*/)
{
	//temporary
	/*if ( enrichedPostedToolPath.GetMovesBegin()->GetNrOfAxes() != 5 )
	{
		return;
	}

	cadcam::mwEnrichedPostedTP::Iterator itEnd( enrichedPostedToolPath.GetMovesEnd());
	cadcam::mwEnrichedPostedTP::Iterator it = enrichedPostedToolPath.GetMovesBegin();

	double lastValueForFirstRotationalAxis((*it)[3]);
	double lastValueForSecondRotationalAxis((*it)[4]);
	++it;
	for ( ; it != itEnd; ++ it )
	{
		if ( it->IsRapid() )
		{
			if ( ( (*it)[3] != lastValueForFirstRotationalAxis ) ||
				( (*it)[4] != lastValueForSecondRotationalAxis ) )
			{
				it->SetIsPhantom( true );
			}
		}
		lastValueForFirstRotationalAxis = (*it)[3];
		lastValueForSecondRotationalAxis = (*it)[4];
	}*/
}

void CCimMachineSimCore::LoadUI( const mwMachSimIniParams &prms )
{
	HWND m_samplePictWndHandle = m_iniParams.GetParentHandle() ;

	if (m_samplePictWndHandle != MW_NULL && !::IsWindow(m_samplePictWndHandle)) {
		::MessageBox(0, _T("MACHSIM Ini Parameters: Invalid window handle"), _T("warning"), MB_OK | MB_ICONWARNING);
		m_samplePictWndHandle = MW_NULL;
	}

	if (m_machsimGuiDll == 0)
	{
		m_machsimGuiDll = ::AfxLoadLibrary(_T("mwMSimDefGUI.dll"));
	}
	if (m_machsimGuiDll == 0)
	{
		throw misc::mwException(0, _T("MachSim: Could not load GUI dll"));
	}

	// Gets an exported methods
	mwMachSimGuiCreate machsimGuiCreator = reinterpret_cast<mwMachSimGuiCreate>
		(::GetProcAddress(m_machsimGuiDll, "CreateMachSimGui"));

	if (machsimGuiCreator == 0)
	{
		m_machsimGuiDll = 0;
		throw misc::mwException(0, _T("MachSim: Could not retrieve GUI creator"));
	}

	

	// Creates verifier instance
	m_machSimGUIFacade = machsimGuiCreator();
	if (m_machSimGUIFacade == 0)
	{
		m_machsimGuiDll = 0;
		throw misc::mwException(0, _T("MachSim: Could not create GUI instance"));
	}

	// Initialize
	m_machSimGUIFacade->Initialize( m_samplePictWndHandle, prms.GetIniFilePath()); 
}

BOOL CCimMachineSimCore::LoadResourceDLLs( mwMachSimIniParams &currentIniParams, misc::mwFileName &iniFileName )
{
	misc::mwstring machsim_res = currentIniParams.GetResourceFileName() ;
	//try to detect mwMachSim_res.dll
	if( machsim_res != _T("") ) 
	{
		if ( misc::mwFileSystem::FileExists(machsim_res) )
		{
			mwCimMachSimCoreResource::handle.SetFile( machsim_res );
		}
		else
		{
			AfxMessageBox( _T("Simulator core resource dll could be found, program execution will be aborted") );
			PostQuitMessage(1);
			return FALSE ;
		}
	}
	else
	{

		_TCHAR exeFileName[MAX_PATH];
		::GetModuleFileName( 0, exeFileName, MAX_PATH );
		misc::mwFileName	machsim_res_file(exeFileName) ;
		machsim_res_file.SetFileName(_T("mwMachSim_res.dll")) ;

		if( misc::mwFileSystem::FileExists( machsim_res_file.GetFilePath() ) )
		{
			mwCimMachSimCoreResource::handle.SetFile( machsim_res_file.GetFilePath() );
			currentIniParams.SetResourceFileName( machsim_res_file.GetFilePath() ) ;
		}
		else
		{			
			machsim_res_file = misc::mwFileName( iniFileName.GetFilePath() ) ;
			machsim_res_file.SetFileName(_T("mwMachSim_res.dll")) ;
			if( misc::mwFileSystem::FileExists( machsim_res_file.GetFilePath() ) )
			{
				mwCimMachSimCoreResource::handle.SetFile( machsim_res_file.GetFilePath() );
				currentIniParams.SetResourceFileName( machsim_res_file.GetFilePath() ) ;
			}
			else
			{
				AfxMessageBox( _T("No resource dll could be found, program execution will be aborted") );
				PostQuitMessage(1);
				return FALSE ;
			}
		}
	}	

	if ( m_startupParams.Get5AxisResourceFileName() != _T("") )
	{
		if ( misc::mwFileSystem::FileExists(m_startupParams.Get5AxisResourceFileName()) )
		{
			mwCim5axResource::handle.SetFile( m_startupParams.Get5AxisResourceFileName() );
		}
		else
		{
			AfxMessageBox( _T("5ax ui resource dll could be found, program execution will be aborted") );
			PostQuitMessage(1);
			return FALSE ;
		}
	}
	else
	{
		_TCHAR exeFileName[MAX_PATH];
		::GetModuleFileName( 0, exeFileName, MAX_PATH );
		misc::mwFileName tempResourceDLLFileName(exeFileName) ;
		const misc::mwstring default5axResourceDLLFileName(_T("mwExceptions_res.dll"));
		tempResourceDLLFileName.SetFileName( default5axResourceDLLFileName );
		if (misc::mwFileSystem::FileExists( tempResourceDLLFileName))
		{
			mwCim5axResource::handle.SetFile( tempResourceDLLFileName );
		}
		else
		{
			AfxMessageBox( _T("5ax ui resource dll could be found, program execution will be aborted") );
			PostQuitMessage(1);
			return FALSE ;
		}
	}

	return TRUE;
}
