/******************************************************************************
*               File: mwMPlusWriter.cpp											  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  16.06.2003 14:13:38 Created by: Yavuz Murtezaoglu                          *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/

#include "StdAfx.h"
#include "mwStringConversions.hpp"
#include "mwStringConversions.hpp"
#include "mwAvoid_warnings.hpp"
#include "mwExtendedASCIIWriter.hpp"

mwExtendedASCIIWriter::mwMoveGrabber::mwMoveGrabber( cadcam::mw6axMove &toFill )
: mFilledMove( toFill )
{
}

void mwExtendedASCIIWriter::mwMoveGrabber::Visit( const cadcam::mwCNCMove &mw ) 
{
	throw misc::mwException( 5303, _T("Use of a member function of an abstract class.") );
}

void mwExtendedASCIIWriter::mwMoveGrabber::Visit( const cadcam::mw3axMove &mw ) 
{
	throw misc::mwException( 5303, _T("Use of a member function of an abstract class.") );
}

void mwExtendedASCIIWriter::mwMoveGrabber::Visit( const cadcam::mw5axMove &mw ) 
{
	mFilledMove = cadcam::mw6axMove ( mw, cadcam::mwVector3d() );
}

void mwExtendedASCIIWriter::mwMoveGrabber::Visit(const cadcam::mw5axMarkedMove &mw)
{
	mFilledMove = cadcam::mw6axMove ( mw, cadcam::mwVector3d() );
}

void mwExtendedASCIIWriter::mwMoveGrabber::Visit( const cadcam::mw6axMove &mw ) 
{
	mFilledMove = mw;
}

mwExtendedASCIIWriter::mwExtendedASCIIWriter( misc::mwOFStream					&outFile )
		: mOutFile( outFile )
{

}

void mwExtendedASCIIWriter::WriteTP( const cadcam::toolpath			&toolp )
{
	//mOutFile.imbue(std::locale(_T("C")));  // Estab locale.

	// set 3 digits precision if metric, other wise 5
	std::ios_base::_Fmtflags flags =  std::ios_base::fixed;
	int precision = 5;
	mOutFile.precision( precision);
	mOutFile.setf( flags );

	// write header
	mOutFile 
	<< _T("RGB 0 0 255") << std::endl;

	cadcam::mw6axMove curMove;
	cadcam::mw6axMove prevMove;
	mwMoveGrabber grabber( curMove );


	for(cadcam::mwToolPath::ConstOverMovesIterator  move=toolp.GetMovesBegin();
		move != toolp.GetMovesEnd(); move++ )
	{
		(*move)->AcceptVisitor( grabber );

		bool firstPt = (move==toolp.GetMovesBegin() );

		bool startNewContour = false;

		if( firstPt )
			startNewContour = true;

		misc::mwstring color = _T("RGB 0 255 255"); // defaut color is blue

		if( prevMove.IsRapidMove() && !curMove.IsRapidMove() )
		{
			color = _T("RGB 0 255 255"); // blue
			startNewContour = true;
		}

		if( !prevMove.IsRapidMove() && curMove.IsRapidMove() )
		{
			color = _T("RGB 255 255 0"); // yellow
			startNewContour = true;
		}

		// set the color for new contour
		if( startNewContour )
			WriteALine( color );

		// we need to start a new contour using prev point
		if( startNewContour && !firstPt )
		{
			misc::mwstring curLine = (_T("PA ") ) +
							misc::from_value( prevMove.GetPosition().x(), precision) + _T(" ") +
							misc::from_value( prevMove.GetPosition().y(), precision) + _T(" ") +
							misc::from_value( prevMove.GetPosition().z(), precision) + _T(" ");
			WriteALine( curLine );
		}

		misc::mwstring curLine = (firstPt ? _T("PA ") : _T("PE ") ) +
						misc::from_value( curMove.GetPosition().x(), precision) + _T(" ") +
						misc::from_value( curMove.GetPosition().y(), precision) + _T(" ") +
						misc::from_value( curMove.GetPosition().z(), precision) + _T(" ");

		prevMove = curMove;

		WriteALine( curLine );
	}

}

void mwExtendedASCIIWriter::WriteALine( const misc::mwstring &lineStr )
{
	mOutFile 
	<< lineStr.c_str() << std::endl;

}
