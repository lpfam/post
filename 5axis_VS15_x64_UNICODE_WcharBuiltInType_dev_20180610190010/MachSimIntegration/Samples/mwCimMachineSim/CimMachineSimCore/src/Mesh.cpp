/******************************************************************************
*               File: Mesh.cpp											      *
*******************************************************************************
*               Description: implementation of Mesh class                     *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  17.05.2008 12:00:00 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "Mesh.hpp"

//#############################################################################
Mesh::Mesh():m_mesh( measures::mwUnitsFactory::METRIC )
{
}


//#############################################################################
const cadcam::mwMesh& Mesh::GetMesh() const
{
	return m_mesh;
}

//#############################################################################
void Mesh::SaveToStream( misc::mwBinOutputStream& rOutputStream )
{
	misc::mwBOBasicTypes basicTypesWriter(rOutputStream, false );
	basicTypesWriter << static_cast<unsigned int>(m_mesh.GetUnits()); 
	SaveVectorToStream(basicTypesWriter, m_mesh.GetPoints());
	SaveVectorToStream(basicTypesWriter, m_mesh.GetTriangles());
};

//#############################################################################
//saves vector of anything
template< class vector_type >
void Mesh::SaveVectorToStream(misc::mwBOBasicTypes& basicTypesWriter,const std::vector< vector_type > &value )
{
	basicTypesWriter << misc::mwStreamableSize_t(value.size());
	for (unsigned int i = 0; i < value.size(); i++)
		SaveToStream(basicTypesWriter, value[i]);
}

//#############################################################################
//saves triangle
void Mesh::SaveToStream(misc::mwBOBasicTypes& basicTypesWriter,  const cadcam::mwMesh::Triangle &value )
{
	basicTypesWriter << misc::mwStreamableSize_t(value.GetFirstPointIndex());
	basicTypesWriter << misc::mwStreamableSize_t(value.GetSecondPointIndex());
	basicTypesWriter << misc::mwStreamableSize_t(value.GetThirdPointIndex());
	SaveToStream(basicTypesWriter, value.GetData());
}
//#############################################################################
//saves point
void Mesh::SaveToStream(misc::mwBOBasicTypes& basicTypesWriter,  const cadcam::mwMesh::point3d &value )
{
	basicTypesWriter << value.x();
	basicTypesWriter << value.y();
	basicTypesWriter << value.z();
}

//#############################################################################
//load mesh
void Mesh::UpdateFromStream( misc::mwBinInputStream& rInputStream )
{
	misc::mwBIBasicTypes basicTypesReader( rInputStream, false, true );

	unsigned int units;
	basicTypesReader >> units ;
	m_mesh.SetUnits(static_cast<measures::mwUnitsFactory::Units>(units));

	misc::mwAutoPointer<cadcam::mwMesh::pointArray> pointArray = new cadcam::mwMesh::pointArray;
	UpdateVectorFromStream(basicTypesReader,*pointArray);
	// 
	misc::mwAutoPointer<cadcam::mwMesh::TriangleArray> triangleArray = new cadcam::mwMesh::TriangleArray;
	UpdateVectorFromStream(basicTypesReader,*triangleArray);
	m_mesh.SetTriangles(pointArray,triangleArray);
}
//#############################################################################
//load vector of anything
template< class vector_type >
void Mesh::UpdateVectorFromStream(misc::mwBIBasicTypes& basicTypesReader,std::vector< vector_type > &value )
{
	size_t uitmpValue=basicTypesReader.GetSize_t();
	value.resize(uitmpValue);
	for (unsigned int i = 0; i < uitmpValue; i++)
	{
		UpdateFromStream(basicTypesReader,value[i]);
	}
}
//#############################################################################
//load trinagle
void Mesh::UpdateFromStream(misc::mwBIBasicTypes& basicTypesReader,cadcam::mwMesh::Triangle &value )
{
	size_t firstIndex = basicTypesReader.GetSize_t();
	value.SetFirstPointIndex(firstIndex);

	size_t secondIndex = basicTypesReader.GetSize_t();
	value.SetSecondPointIndex(secondIndex);

	size_t thirdIndex = basicTypesReader.GetSize_t();
	value.SetThirdPointIndex(thirdIndex);

	cadcam::mwMesh::TFaceNormal normal; 
	UpdateFromStream(basicTypesReader, normal);
	value.SetNormal(normal);
}
//#############################################################################
//load point
void Mesh::UpdateFromStream(misc::mwBIBasicTypes& basicTypesReader,cadcam::mwMesh::point3d &value )
{
	double tempValue;

	basicTypesReader >> tempValue;
	value.x( tempValue );

	basicTypesReader >> tempValue;
	value.y( tempValue );

	basicTypesReader >> tempValue;
	value.z( tempValue );
}
