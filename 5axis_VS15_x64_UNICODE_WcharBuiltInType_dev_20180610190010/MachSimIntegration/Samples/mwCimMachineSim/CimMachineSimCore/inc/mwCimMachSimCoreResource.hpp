/******************************************************************************
*               File: mwCimMachSimCoreResource.hpp                            *
*******************************************************************************
*               Description: declaration of mwCimMachSimCoreResource class    *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  03.09.2008 15:13:08 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __MWCIMMACHSIMCORERESOURCE_HPP__
#define __MWCIMMACHSIMCORERESOURCE_HPP__
#include "mwResourceDll.hpp"

class mwCimMachSimCoreResource
{
	public:
		static misc::mwResourceDll handle;
	protected:
	private:
};

#endif // __MWCIMMACHSIMCORERESOURCE_HPP__