/******************************************************************************
*               File: mwMPSDef.hpp											  *
*******************************************************************************
*               Description:this module describe the mwMPSDef class			  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  11/30/2004 4:10:24 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMPSDef_hpp__
#define __mwMPSDef_hpp__
//#############################################################################
#include "mwMachDef.hpp"
//#############################################################################
	//! input short description
	/*! input detailed description	
	*/
class mwMPSDef
{
public:
	struct mpsDef
	{
		misc::mwstring xmlPath;
		misc::mwstring folder;
		misc::mwstring name;
		bool isDefault;
	};
	//#############################################################################
	void AddDef(const mwMPSDef::mpsDef& rDef )
	{
		m_definitions.push_back(rDef);
	}
	//#############################################################################
	const mwMPSDef::mpsDef& GetDef(const unsigned int vIndex)
	{
		return m_definitions[vIndex];
	}
	//#############################################################################
	const unsigned int GetSize()
	{
		return static_cast<unsigned int>(m_definitions.size());
	}
	const bool IsEmpty()
	{
		return m_definitions.empty();
	}
private:
	std::vector <mpsDef> m_definitions;
};
#endif //__mwMPSDef_hpp__
