/******************************************************************************
*               File: mwCimExceptionHandler.hpp								  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  05.09.2008 12:00:00 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __MW_CIMEXCEPTIONHANDLER_HPP__
#define __MW_CIMEXCEPTIONHANDLER_HPP__

#include "mwMachSimException.hpp"
#include "mwCimMachineSimCoreDllDef.hpp"
#include "mwPostException.hpp"

class mwCLParserException;

class CIMMACHINESIMCORE_API mwCimExceptionHandler
{
public:
	static void HandleDefaultException(misc::mwException& e);
	static void HandlePostException(post::mwPostException& e);
	static void HandleMiscException(misc::mwException& e);
	static void HandleMachSimException( exceptions::mwMachSimException& e );
	static void HandleParserException(mwCLParserException& e);
	static void HandleStdException(std::exception& e);
	static void HandleUnknownException();
};
#endif //__MW_CIMEXCEPTIONHANDLER_HPP__
