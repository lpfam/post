/******************************************************************************
*               File: mwExtendedASCIIWriter.hpp											  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  07.08.2003 11:18:19 Created by: Yavuz Murtezaoglu                          *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/

#ifndef __MW_EXTENDED_ASCII_WRITER_HPP__
#define __MW_EXTENDED_ASCII_WRITER_HPP__

#include "mwFileStream.hpp"
#include <vector>
#include <mw6axMove.hpp>
#include <mwToolPath.hpp>
#include <mwException.hpp>

namespace cadcam
{
	class mwTool;
	class mwPostedTP;
};

class mwExtendedASCIIWriter
{
public:
	mwExtendedASCIIWriter( misc::mwOFStream	&outFile );

	void WriteTP(const cadcam::toolpath			&toolp);

					
private:
	void WriteALine( const misc::mwstring &lineStr );

	misc::mwOFStream	&mOutFile;


	class mwMoveGrabber : public cadcam::mwCNCMoveVisitor  
	{
	public:
		mwMoveGrabber( cadcam::mw6axMove &toFill );
		//virtual ~mwMoveGrabber();

		virtual void Visit( const cadcam::mwCNCMove &mw ) 
			;

		virtual void Visit( const cadcam::mw3axMove &mw ) 
			;

		virtual void Visit( const cadcam::mw5axMove &mw ) 
			;

		virtual void Visit(const cadcam::mw5axMarkedMove &mw)
			;

		virtual void Visit( const cadcam::mw6axMove &mw ) 
			;

	private:
		cadcam::mw6axMove &mFilledMove;

	};



};


#endif
