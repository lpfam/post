/******************************************************************************
*               File: Mesh.hpp										          *
*******************************************************************************
*               Description: declaration of Mesh class                        *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  17.05.2008 12:00:00 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef _MESH_HPP__
#define _MESH_HPP__
#include "Mesh.hpp"
#include "mwAutoPointer.hpp"
#include "mwBinOutputStream.hpp"
#include "mwBinInputStream.hpp"
#include "mwBOBasicTypes.hpp"
#include "mwBIBasicTypes.hpp"
#include "mwMesh.hpp"

class Mesh
{
public:
	typedef misc::mwAutoPointer< Mesh > Ptr;
	//#############################################################################
	Mesh();
	void  SaveToStream( misc::mwBinOutputStream& rOutputStream );
	//#############################################################################
	//load mesh
	void UpdateFromStream( misc::mwBinInputStream& rInputStream );
	//#############################################################################		
	const cadcam::mwMesh& GetMesh() const;

	void SetMesh( cadcam::mwMesh& toSet )
	{
		m_mesh =toSet;
	}
protected:
	//#############################################################################
private:
	//#############################################################################
	//saves vector of anything
	template <class T>
	void SaveVectorToStream(misc::mwBOBasicTypes& basicTypesWriter,const std::vector< T > &value );
	
	//#############################################################################
	//saves triangle
	void SaveToStream(misc::mwBOBasicTypes& basicTypesWriter,  const cadcam::mwMesh::Triangle &value );
	
	//#############################################################################
	//saves point
	void SaveToStream(misc::mwBOBasicTypes& basicTypesWriter,  const cadcam::mwMesh::point3d &value );
		
	//#############################################################################
	//load vector of anything
	template< class vector_type >
	void UpdateVectorFromStream(misc::mwBIBasicTypes& basicTypesReader,std::vector< vector_type > &value );
	//#############################################################################
	//load triangle
	void UpdateFromStream(misc::mwBIBasicTypes& basicTypesReader,cadcam::mwMesh::Triangle &value );
	//#############################################################################
	//load point
	void UpdateFromStream(misc::mwBIBasicTypes& basicTypesReader,cadcam::mwMesh::point3d &value );
	
	cadcam::mwMesh m_mesh;
};

#endif