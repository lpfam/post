/********************************************************************
	created:	03 Feb 2012 16:45
	filename: 	CimCommandBasedSimControllerDisabler.hpp
	author:		Mihai Vasilian
	purpose:	declaration of CimCommandBasedSimControllerDisabler class
	copyright (c) 2012 by ModuleWorks GmbH	
*********************************************************************/


#ifndef __CimCommandBasedSimControllerDisabler_hpp__
#define __CimCommandBasedSimControllerDisabler_hpp__

#include "CCimMachineSimCore.h"


class CIMMACHINESIMCORE_API CimCommandBasedSimControllerDisabler
{
public:
	CimCommandBasedSimControllerDisabler(CCimMachineSimCore* cimMachineCore);
	~CimCommandBasedSimControllerDisabler();

private:
	CCimMachineSimCore* m_cimMachineCore;
};



#endif//__CimCommandBasedSimControllerDisabler_hpp__


