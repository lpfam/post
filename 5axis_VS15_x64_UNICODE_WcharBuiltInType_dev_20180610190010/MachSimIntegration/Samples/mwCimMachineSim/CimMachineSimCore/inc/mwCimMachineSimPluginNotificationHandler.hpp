/******************************************************************************
*               File: mwCimMachineSimPluginNotificationHandler.hpp			  *
*******************************************************************************
*               Description: mwCimMachineSimPluginNotificationHandler class   *
*                            declaration                                      *
*******************************************************************************
*               History:                                                      *
*  30.03.2010 13:14:00 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2010 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __MW_CIMMACHINESIMPLUGINNOTIFICATIONHANDLER_HPP__
#define __MW_CIMMACHINESIMPLUGINNOTIFICATIONHANDLER_HPP__

#include "mwMachSimPluginNotificationHandler.hpp"
#include "mwMachSimFacade.hpp"

//! mwMSimGuiNotificationHandler class
class mwCimMachineSimPluginNotificationHandler: public mwMachSimPluginNotificationHandler
{
public:
	//! constructor
	mwCimMachineSimPluginNotificationHandler(mwMachSimFacade* coreFacade,const std::vector<unsigned int>& stopPositions)
		:
	m_coreFacade(coreFacade),
	m_stopPositions(stopPositions)	
	{

	};
	//! called when MachSim changed the toolpath position
	virtual void OnChangedToolPathPosition();
protected:
private:
	mwMachSimFacade*								m_coreFacade;
	std::vector<unsigned int>						m_stopPositions;
};

#endif // __MW_CIMMACHINESIMPLUGINNOTIFICATIONHANDLER_HPP__
