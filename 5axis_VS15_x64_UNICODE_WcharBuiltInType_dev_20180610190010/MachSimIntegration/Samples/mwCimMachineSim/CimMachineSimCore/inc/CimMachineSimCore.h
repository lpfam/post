#ifndef __CimMachineSimCore_h__
#define __CimMachineSimCore_h__

//#############################################################################
#include "mwStringConversions.hpp"
#include <vector>
#include "mwCimMachSimCoreStartupParams.hpp"
#include "mwResource.hpp"
//#############################################################################
class mwMPSDef;
class mwCLParserException;
namespace misc
{
	class mwException;
}
//#############################################################################
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CIMMACHINESIMCORE_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CIMMACHINESIMCORE_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef CIMMACHINESIMCORE_EXPORTS
#define CIMMACHINESIMCORE_API __declspec(dllexport)
#else
#define CIMMACHINESIMCORE_API __declspec(dllimport)
#endif
//#############################################################################

// This class is exported from the CimMachineSimCore.dll
class CIMMACHINESIMCORE_API CCimMachineSimCore {
public:

//#############################################################################
	CCimMachineSimCore(const mwCimMachSimCoreStartupParams& machSimFilesLocation);

//#############################################################################
	//! destructor
	~CCimMachineSimCore();
//#############################################################################
	BOOL StartSimulation(const int vMachineIndex);
//#############################################################################
	inline const std::vector <misc::mwstring>& GetMachineList()
	{
		return m_MachineList;
	}
//#############################################################################
	//! gets MachDefsFolder
	/*! 			
			\returns the MachDefsFolder
	*/
	inline const misc::mwstring& GetMachDefsFolder() const
	{
		return m_startupParams.GetMachDefFolder();
	};
//#############################################################################
	//! sets MachDefsFolder
	/*! 			
			\param rMachDefsFolder the new MachDefsFolder
	*/
	inline void SetMachDefsFolder(const misc::mwstring& rMachDefsFolder)
	{
		m_startupParams.SetMachDefFolder(rMachDefsFolder);
	};
//#############################################################################
	//! sets PartOrigin
	/*! 			
			\param vX the x coordinate of the new part origin
			\param vY the y coordinate of the new part origin
			\param vZ the z coordinate of the new part origin
	*/
	inline void SetPartOrigin(const double vX,const double vY, const double vZ)
	{
		m_X=vX;
		m_Y=vY;
		m_Z=vZ;
	};
//#############################################################################
	//! gets PartOrigin
	/*! 			
			\param vX will receive the x coordinate of the part origin
			\param vY will receive the y coordinate of the part origin
			\param vZ will receive the z coordinate of the part origin
	*/
	inline void GetPartOrigin(double& rX,double& rY, double& rZ)
	{
		rX=m_X;
		rY=m_Y;
		rZ=m_Z;
	};
//#############################################################################
	//! gets MWSIMFile
	/*! 			
			\returns the MWSIMFile
	*/
	inline const misc::mwstring& GetMWSIMFile() const
	{
		return m_startupParams.GetSimFile();
	};
//#############################################################################
	//! sets MWSIMFile
	/*! 			
			\param rMWSIMFile the new MWSIMFile
	*/
	inline void SetMWSIMFile(const misc::mwstring& rMWSIMFile)
	{
		m_startupParams.SetSimFile(rMWSIMFile);
	};
//#############################################################################
	//! gets SimulationFile
	/*! 			
	\returns the SimulationFile
	*/
	inline const misc::mwstring& GetSimulateBinFile() const
	{
		return m_startupParams.GetSimulateBinFile();
	};
//#############################################################################
	//! sets SimulationBinFile
	/*! 			
	\param rSimulateBinFile the new SimulationBinFile
	*/
	inline void SetSimulateBinFile(const misc::mwstring& rSimulateBinFile)
	{
		m_startupParams.SetSimulateBinFile(rSimulateBinFile);
	};
//#############################################################################
	//! gets STLFile
	/*! 			
			\returns the STLFile
	*/
	inline const misc::mwstring& GetStlFile() const
	{
		return m_startupParams.GetStlFile();
	};
//#############################################################################
	//! sets STLFile
	/*! 			
			\param rSTLFile the new STLFile
	*/
	inline void SetStlFile(const misc::mwstring& rSTLFile)
	{
		m_startupParams.SetStlFile(rSTLFile);
	};
//#############################################################################
	//! gets MachSimFolder
	/*! 			
			\returns the MachSimFolder
	*/
	inline const misc::mwstring& GetMachSimFolder() const
	{
		return m_startupParams.GetMachSimFolder();
	};
//#############################################################################
	//! sets MachSimFolder
	/*! 			
			\param rMachSimFolder the new MachSimFolder
	*/
	inline void SetMachSimFolder(const misc::mwstring& rMachSimFolder)
	{
		m_startupParams.SetMachSimFolder(rMachSimFolder);		
	};
//#############################################################################
		//! gets PartTolerance
	/*! 			
			\returns the PartTolerance
	*/
	inline const misc::mwstring& PartTolerance() const
	{
		return m_startupParams.GetPartTolerance();
	};
//#############################################################################
//! sets ParTolerance
	/*! 			
			\param rPartTolerance the new PartTolerance
	*/
	inline void SetParTolerance(const misc::mwstring& rPartTolerance)
	{
		m_startupParams.SetPartTolerance(rPartTolerance);		
	};
//#############################################################################
	//! gets ParentWindow
	/*! 			
			\returns the ParentWindow
	*/
	inline const HWND& GetParentWindow() const
	{ 
		return m_ParentWindow;
	};
//#############################################################################
	//! sets ParentWindow
	/*! 			
			\param rParentWindow the new ParentWindow
	*/
	inline void SetParentWindow(const HWND& rParentWindow)
	{
		m_ParentWindow=rParentWindow;
	};
//#############################################################################
	//! gets ErrorCode
	/*! 			
			\returns the ErrorCode
	*/
	inline const int GetErrorCode() const
	{ 
		return m_ErrorCode;
	};
//#############################################################################
	//! gets ErrorMessage
	/*! 			
			\returns the ErrorMessage
	*/
	inline const misc::mwstring& GetErrorMessage() const
	{
		return m_ErrorMessage;
	};
//#############################################################################
	// set the MachinSimulator background color
	void SetMachineSimColor(misc::mwstring iRed, misc::mwstring iGreen, misc::mwstring iBlue);
//#############################################################################
private:
//#############################################################################
	enum
	{
		eRed =0,
		eGreen,
		eBlue,

		eRGBSize

	};
	misc::mwstring MachineSimColorArray[eRGBSize];
	const bool DoesUserSpecifiedAscFileExist() const;
	void CopySTLFile( const int vMachineIndex );
	void CopyAscFile( const int vMachineIndex );
	void CreateIniFile();
	void HandleParserException(mwCLParserException &e);
	void HandleMiscException(misc::mwException &e);
	void HandleDefaultException(misc::mwException &e);
	const misc::mwstring GetMsg( unsigned long msgNr, const misc::mwstring &section ) const;
	void LoadMachDefs();
	void HandleUnknownException();
	BOOL RunMachSim(const int vMachineIndex);
	//to avoid that RGB will be out of the 0->1 and negative
	void NormalRGBValue();

//#############################################################################
	bool m_Metric;
	mwCimMachSimCoreStartupParams m_startupParams;
	std::vector <misc::mwstring> m_MachineList;
	double m_X;
	double m_Y;
	double m_Z;
	HWND m_ParentWindow;
	mwMPSDef* m_machDefs;
	PROCESS_INFORMATION msPI;
	int m_ErrorCode;
	misc::mwstring m_ErrorMessage;
	
};

#endif //__CimMachineSimCore_h__