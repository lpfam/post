#ifndef __CimMachineSimCore_h__
#define __CimMachineSimCore_h__

//#############################################################################
#include "mwStringConversions.hpp"
#include <vector>
#include "mwCimMachSimCoreStartupParams.hpp"
#include "mwResource.hpp"
//#############################################################################
#include "mwMachSimIniParams.hpp"
#include "mwMSimGUIFacade.hpp"
#include "mwMachSimFacade.hpp"
#include "mwMXPParam.hpp"
#include "mwPostException.hpp"
#include "mwCimMachineSimCoreDllDef.hpp"
#include "mwMPSDef.hpp"

class mwMPSDef;
class mwCimCLParserException;
namespace misc
{
	class mwException;
}

// This class is exported from the CimMachineSimCore.dll
class CIMMACHINESIMCORE_API CCimMachineSimCore {
public:

	//typedef mwMPSDef::mpsDef				machineDefinition;

//#############################################################################
	CCimMachineSimCore(const mwCimMachSimCoreStartupParams& machSimFilesLocation);
	
//#############################################################################
	//! destructor
	~CCimMachineSimCore();
//#############################################################################
	//machineDefinition FetchMachDef( const misc::mwstring& machineName );
//#############################################################################
	const std::vector<misc::mwstring> GetMachineDefinitionNames();
//#############################################################################
	//const machineDefinition& GetCurrentMachineDefinition() const;
//#############################################################################
	//void SetCurrentMachineDefinition( const machineDefinition& machineDefinition );
//#############################################################################
	void SetMachineDefName(const misc::mwstring machineDefName);
//#############################################################################
	const misc::mwstring GetMachineDefName() const;
//#############################################################################
	BOOL RunMachSim();
//#############################################################################
	//! gets MachDefsFolder
	/*! 			
			\returns the MachDefsFolder
	*/
	inline const misc::mwstring& GetMachDefsFolder() const
	{
		return m_startupParams.GetMachDefFolder();
	};
//#############################################################################
	//! sets MachDefsFolder
	/*! 			
			\param rMachDefsFolder the new MachDefsFolder
	*/
	inline void SetMachDefsFolder(const misc::mwstring& rMachDefsFolder)
	{
		m_startupParams.SetMachDefFolder(rMachDefsFolder);
	};
//#############################################################################
	//! sets PartOrigin
	/*! 			
			\param vX the x coordinate of the new part origin
			\param vY the y coordinate of the new part origin
			\param vZ the z coordinate of the new part origin
	*/
	inline void SetPartOrigin(const double vX,const double vY, const double vZ)
	{
		m_X=vX;
		m_Y=vY;
		m_Z=vZ;
	};
//#############################################################################
	//! gets PartOrigin
	/*! 			
			\param vX will receive the x coordinate of the part origin
			\param vY will receive the y coordinate of the part origin
			\param vZ will receive the z coordinate of the part origin
	*/
	inline void GetPartOrigin(double& rX,double& rY, double& rZ)
	{
		rX=m_X;
		rY=m_Y;
		rZ=m_Z;
	};
//#############################################################################
	//! gets MWSIMFile
	/*! 			
			\returns the MWSIMFile
	*/
	inline const misc::mwstring& GetMWSIMFile() const
	{
		return m_startupParams.GetSimFile();
	};
//#############################################################################
	//! sets MWSIMFile
	/*! 			
			\param rMWSIMFile the new MWSIMFile
	*/
	inline void SetMWSIMFile(const misc::mwstring& rMWSIMFile)
	{
		m_startupParams.SetSimFile(rMWSIMFile);
	};
//#############################################################################
	//! gets SimulationBinFile
	/*! 			
	\returns the SimulationBinFile
	*/
	inline const misc::mwstring& GetSimulateBinFile() const
	{
		return m_startupParams.GetSimulateBinFile();
	};
//#############################################################################
	//! sets SimulateBinFile
	/*! 			
	\param rSimulateBinFile the new SimulateBinFile
	*/
	inline void SetSimulateBinFile(const misc::mwstring& rSimulateBinFile)
	{
		m_startupParams.SetSimulateBinFile(rSimulateBinFile);
	};
//#############################################################################
	//! gets MachSimFolder
	/*! 			
			\returns the MachSimFolder
	*/
	inline const misc::mwstring& GetMachSimFolder() const
	{
		return m_startupParams.GetMachSimFolder();
	};
//#############################################################################
	//! sets MachSimFolder
	/*! 			
			\param rMachSimFolder the new MachSimFolder
	*/
	inline void SetMachSimFolder(const misc::mwstring& rMachSimFolder)
	{
		m_startupParams.SetMachSimFolder(rMachSimFolder);		
	};
//#############################################################################
		//! gets PartTolerance
	/*! 			
			\returns the PartTolerance
	*/
	inline const misc::mwstring& PartTolerance() const
	{
		return m_startupParams.GetPartTolerance();
	};
//#############################################################################
//! sets ParTolerance
	/*! 			
			\param rPartTolerance the new PartTolerance
	*/
	inline void SetParTolerance(const misc::mwstring& rPartTolerance)
	{
		m_startupParams.SetPartTolerance(rPartTolerance);		
	};
//#############################################################################
	//! gets ParentWindow
	/*! 			
			\returns the ParentWindow
	*/
	inline const HWND& GetParentWindow() const
	{ 
		return m_ParentWindow;
	};
//#############################################################################
	//! sets ParentWindow
	/*! 			
			\param rParentWindow the new ParentWindow
	*/
	inline void SetParentWindow(const HWND& rParentWindow)
	{
		m_ParentWindow=rParentWindow;
	};
	//#############################################################################
	//! gets application title suffix - offers additional information about the simulator core status
	/*! 			
	\returns the title suffix
	*/
	inline const misc::mwstring& GetAppTitleSuffix() const
	{
		return m_appTitleSuffix;
	};
//#############################################################################
	// set the MachinSimulator background color
	void SetMachineSimColor(misc::mwstring iRed, misc::mwstring iGreen, misc::mwstring iBlue);

	void SimulateAllAndWriteReport( const misc::mwstring& reportFile, mwIProgressDialogHandler& progress )
	{
		m_machSimCoreFacade->SimulateAllAndWriteReport( reportFile, &progress );
	}

	void ForceMachSIMGLViewRedraw()
	{
		m_machSimCoreFacade->GetViewService().RenderScene();
	}

	inline const post::mwMXPParam::MachineLimits GetMachineLimitsFromString(const misc::mwstring& machineLimits)
	{
		if(machineLimits == _T("no_limits") || machineLimits == _T(""))
			return post::mwMXPParam::NO_LIMITS;
		else
			if(machineLimits == _T("translational_limits"))
				return post::mwMXPParam::TRANSLATIONAL_LIMITS;
			else
				if(machineLimits == _T("rotational_limits"))
					return post::mwMXPParam::ROTATIONAL_LIMITS;
				else
					if(machineLimits == _T("all_limits"))
						return post::mwMXPParam::ALL_LIMITS;
					else
						throw post::mwPostException(post::mwPostException::UNKNOWN_MACHINE_LIMITS);
	}
	
	const measures::mwUnitsFactory::Units GetUnits();

	HWND GetViewHWND() 
	{
		return m_machSimWndHandle ;
	}

	/*! EnableCommandBasedSimulationController
	 *  Turn on / off the timer which runs internally inside MachSim core.
	 *	If no 'command based simulation controller' is enabled, then nothing is done.	 
	*/
	void EnableCommandBasedSimulationController(bool enable);


	//! Returns false if any modal dialog is opened.
	bool CanQuit() const;
//#############################################################################
private:
//#############################################################################
	enum
	{
		eRed =0,
		eGreen,
		eBlue,

		eRGBSize

	};
	misc::mwstring MachineSimColorArray[eRGBSize];
	const bool DoesUserSpecifiedAscFileExist() const;
	void CopyAscFile();
	void CreateIniFile();
	//to avoid that RGB will be out of the 0->1 and negative
	void NormalRGBValue();
	

//#############################################################################
	bool m_Metric;
	mwCimMachSimCoreStartupParams m_startupParams;
	double m_X;
	double m_Y;
	double m_Z;
	HWND m_ParentWindow;
	
	misc::mwstring m_appTitleSuffix;

	mwMachSimOperationsProgrammePtr m_operationsProgramme;
	
private:
	HINSTANCE			m_machsimGuiDll;
	//! mwMSimDefGUI facade, simplified interface to MachSim's GUI 
	mwMachSimGui*		m_machSimGUIFacade;
	//! mwMSimApp facade, simplified interface to MachSim's core 
	mwMachSimFacade*	m_machSimCoreFacade;
	//! mwMSimDefGUI::CMainFrame frame window handle, necessary to perform resizing, painting, etc.
	HWND				m_machSimWndHandle;
	//! params used to initialize MachSim (GUI & core)
	mwMachSimIniParams	m_iniParams;
	//! parent window handle
	HWND				m_parentHandle;

	unsigned int		m_moveNumberLimitForDisablingCollisionCheckingAndToolPathGraphic;

	misc::mwstring	    m_machineDefName;
	misc::mwstring	    m_currentMachineFolder;
	misc::mwstring      m_xmlPath; 	

	misc::mwAutoPointer<machsim::mwMachsimMachDef>  m_machineDefinition; 

	//! returns the INI file path
	misc::mwFileName GetIniFileName();

	//! initializes MachSim
	BOOL InitializeMachSim();

	BOOL LoadResourceDLLs( mwMachSimIniParams &currentIniParams, misc::mwFileName &iniFileName );

	//! initializes MachSim
	LRESULT OnInitializeMachSim( WPARAM wParam, LPARAM lParam );

	const double ConvertToDoubleUsingLocale(const misc::mwstring& toConvert);

	void PostProcessOperations(std::list<mwMachSimCADCAMInputOperation>& operations);

	void SetAllRapidsAsPhantoms(std::list<mwMachSimCADCAMInputOperation>& operations);

	void SetOrientationChangingRapidsAsPhantoms(std::list<mwMachSimCADCAMInputOperation>& operations);

	void SetMeshesFromStartupParamsReferencedInputFiles(misc::mwAutoPointer<mwMachSimSimulation> simulation);
	
	friend class CCimMachineSimDlg;
	friend class ExternalToolProcess;

public:
	void OnSize(UINT nType, int cx, int cy) ;
	
	//! shuts down MachSim 
	void ShutdownMachsim() ;
	void LoadUI( const mwMachSimIniParams &prms );

};

#endif //__CimMachineSimCore_h__