/******************************************************************************
*               File: mwCim5axResource.hpp                                    *
*******************************************************************************
*               Description: declaration of mwCim5axResource class            *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10.09.2008 15:13:08 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2008 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __MWCIM5AXRESOURCE_HPP__
#define __MWCIM5AXRESOURCE_HPP__
#include "mwResourceDll.hpp"

class mwCim5axResource
{
	public:
		static misc::mwResourceDll handle;
	protected:
	private:
};

#endif // __MWCIM5AXRESOURCE_HPP__