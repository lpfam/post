/******************************************************************************
*               File: mwCimMachSimCoreStartupParams.hpp						  *
*******************************************************************************
*               Description:this module describe the 						  *
*                        mwCimMachSimCoreStartupParams class                  *
*******************************************************************************
*               History:                                                      *
*  11/30/2004 4:10:24 PM Created by: Alex Curutiu                             *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwCimMachSimCoreStartupParams_hpp__
#define __mwCimMachSimCoreStartupParams_hpp__

//#############################################################################

#include <string>
#include "mwStringConversions.hpp"

#ifdef CIMMACHINESIMCORE_EXPORTS
#define CIMMACHINESIMCORE_API __declspec(dllexport)
#else
#define CIMMACHINESIMCORE_API __declspec(dllimport)
#endif

class CIMMACHINESIMCORE_API mwCimMachSimCoreStartupParams
{
public:
	mwCimMachSimCoreStartupParams(){}
	~mwCimMachSimCoreStartupParams(){}
//#############################################################################
	inline mwCimMachSimCoreStartupParams(const mwCimMachSimCoreStartupParams& rToCopy)
	{	
		Copy(rToCopy);
	}
//#############################################################################
	const mwCimMachSimCoreStartupParams& operator=(const mwCimMachSimCoreStartupParams& rToCopy)
	{
		if(&rToCopy == this)
			return *this;
		Copy(rToCopy);
		//
		return *this;
	}
//#############################################################################
	void SetMachDefFolder(const misc::mwstring& machDefFolder)
	{
		m_machDefFolder=machDefFolder;
	}
//#############################################################################
	const misc::mwstring& GetMachDefFolder() const
	{
		return m_machDefFolder;
	}
//#############################################################################
	void SetSimulateBinFile(const misc::mwstring& rsimulateBinFile)
	{
		m_simulateBinFile=rsimulateBinFile;
	}
//#############################################################################
	const misc::mwstring& GetSimulateBinFile() const
	{
		return m_simulateBinFile;
	}
//#############################################################################
	void SetSimFile(const misc::mwstring& simFile)
	{
		m_simFile=simFile;
	}
//#############################################################################
	const misc::mwstring& GetSimFile() const
	{
		return m_simFile;
	}

//#############################################################################
	void SetAscFile( const misc::mwstring& ascFile )
	{
		m_ascFile = ascFile;
	}

//#############################################################################
	const misc::mwstring& GetAscFile() const
	{
		return m_ascFile;
	}

//#############################################################################
	void SetStlFile(const misc::mwstring& stlFile)
	{
		m_stlFile=stlFile;
	}
//#############################################################################
	const misc::mwstring& GetStlFile() const
	{
		return m_stlFile;
	}
//#############################################################################
	void SetStockFile(const misc::mwstring& stlFile)
	{
		m_stockFile=stlFile;
	}
//#############################################################################
	const misc::mwstring& GetStockFile() const
	{
		return m_stockFile;
	}
//#############################################################################
	void SetFixtureFile(const misc::mwstring& stlFile)
	{
		m_fixtureFile=stlFile;
	}
//#############################################################################
	const misc::mwstring& GetFixtureFile() const
	{
		return m_fixtureFile;
	}
//#############################################################################
	void SetMachSimFolder(const misc::mwstring& machSimFolder)
	{
		m_machSimFolder=machSimFolder;
	}
//#############################################################################
	const misc::mwstring& GetMachSimFolder() const
	{
		return m_machSimFolder;
	}
	
//#############################################################################
	void SetPartOffset(const misc::mwstring& aPartOffset)
	{
		m_partOffset = aPartOffset;
	}

//#############################################################################
	const misc::mwstring& GetPartOffset() const
	{
		return m_partOffset;
	}

//#############################################################################
	void SetPartTolerance(const misc::mwstring& aPartTol)
	{
		m_partTol = aPartTol;
	}

//#############################################################################
	const misc::mwstring& GetPartTolerance() const
	{
		return m_partTol;
	}
//#############################################################################
	void SetBetweenMovesCollCheck(const misc::mwstring& aBetweenMovesCollCheck)
	{
		m_betweenMovesCollCheck = aBetweenMovesCollCheck;
	}

//#############################################################################
	const misc::mwstring& GetBetweenMovesCollCheck() const
	{
		return m_betweenMovesCollCheck;
	}

//#############################################################################
	void SetGeomCollCheckWhenVerifierEnabled(const misc::mwstring& aGeomCollCheckWhenVerifierEnabled)
	{
		m_geomCollCheckWhenVerifierEnabled = aGeomCollCheckWhenVerifierEnabled;
	}

//#############################################################################
	const misc::mwstring& GetGeomCollCheckWhenVerifierEnabled() const
	{
		return m_geomCollCheckWhenVerifierEnabled;
	}

//#############################################################################
	void SetRapidMovesCollCheck(const misc::mwstring& aRapidMovesCollCheck)
	{
		m_rapidMovesCollCheck = aRapidMovesCollCheck;
	}

//#############################################################################
	const misc::mwstring& GetRapidMovesCollCheck() const
	{
		return m_rapidMovesCollCheck;
	}

//#############################################################################
	void SetVerifierFileName(const misc::mwstring& aVerifierPath)
	{
		m_verifierFileName = aVerifierPath;
	}

//#############################################################################
	const misc::mwstring& GetVerifierFileName() const
	{
		return m_verifierFileName;
	}

//#############################################################################
	void SetVerifierGUIFileName(const misc::mwstring& aVerifierGUIPath)
	{
		m_verifierGUIFileName = aVerifierGUIPath;
	}

//#############################################################################
	const misc::mwstring& GetVerifierGUIFileName() const
	{
		return m_verifierGUIFileName;
	}

//#############################################################################
	void SetEnforceMachineLimits(const misc::mwstring& aEnforceMachineLimits)
	{
		m_enforceMachineLimits = aEnforceMachineLimits;
	}

//#############################################################################
	const misc::mwstring& GetEnforceMachineLimits() const
	{
		return m_enforceMachineLimits;
	}

//#############################################################################
	void SetMxpFile(const misc::mwstring& mxpFile)
	{
		m_mxpFile = mxpFile;
	}

//#############################################################################
	const misc::mwstring& GetMxpFile() const
	{
		return m_mxpFile;
	}
//#############################################################################
	void SetWindowStyle(const misc::mwstring& windowstyle)
	{
		m_windowstyle = windowstyle ;
	}

//#############################################################################	
	const misc::mwstring& GetWindowStyle() const
	{
		return m_windowstyle ;
	}

//#############################################################################
	void SetReportFileName(const misc::mwstring& toSet)
	{
		m_reportFileName = toSet;
	}

//#############################################################################
	void SetReportFileNotOnTop(const misc::mwstring& toSet)
	{
		m_reportFileNotOnTop = toSet;
	}

	//#############################################################################	
	const misc::mwstring& GetReportFileName() const
	{
		return m_reportFileName;
	}

	//#############################################################################	
	const misc::mwstring& GetReportFileNotOnTop() const
	{
		return m_reportFileNotOnTop;
	}

	//#############################################################################
	void Set5AxisResourceFileName(const misc::mwstring& toSet)
	{
		m_5axisResourceFileName = toSet;
	}

	//#############################################################################	
	const misc::mwstring& Get5AxisResourceFileName() const
	{
		return m_5axisResourceFileName;
	}

        //#############################################################################
	void SetResourceFileName(const misc::mwstring& toSet)
	{
		m_resourceFileName = toSet;
	}

	//#############################################################################	
	const misc::mwstring& GetResourceFileName() const
	{
		return m_resourceFileName;
	}

	//#############################################################################
	void SetMachSimResourceFileName(const misc::mwstring& toSet)
	{
		m_machsimResourceFileName = toSet;
	}

	//#############################################################################	
	const misc::mwstring& GetMachSimResourceFileName() const
	{
		return m_machsimResourceFileName;
	}

	//#############################################################################
	void SetVerifierGUIResourceFileName(const misc::mwstring& toSet)
	{
		m_verifierGUIResourceFileName= toSet;
	}

	//#############################################################################	
	const misc::mwstring& GetVerifierGUIResourceFileName() const
	{
		return m_verifierGUIResourceFileName;
	}
	
	//#############################################################################
	void SetMachineReplacementOption(const misc::mwstring& toSet)
	{
		m_machineReplacementOption= toSet;
	}

	//#############################################################################	
	const misc::mwstring& GetMachineReplacementOption() const
	{
		return m_machineReplacementOption;
	}

	//#############################################################################
	void SetResourcePathName(const misc::mwstring& toSet)
	{
		m_ResourcePathName= toSet;
	}

	//#############################################################################	
	const misc::mwstring& GetResourcePathName() const
	{
		return m_ResourcePathName;
	}

	//#############################################################################	
	//#############################################################################
	void SetInitialMachineName(const misc::mwstring& toSet)
	{
		m_initialMachineName = toSet;
	}

	//#############################################################################	
	const misc::mwstring& GetInitialMachineName() const
	{
		return m_initialMachineName;
	}
	
//#############################################################################
protected:
	void Copy(const mwCimMachSimCoreStartupParams& rToCopy)
	{
		m_machDefFolder = rToCopy.m_machDefFolder;
		m_simFile = rToCopy.m_simFile;
		m_simulateBinFile = rToCopy.m_simulateBinFile;
		m_stlFile = rToCopy.m_stlFile;
		m_stockFile = rToCopy.m_stockFile;
		m_fixtureFile = rToCopy.m_fixtureFile;
		m_ascFile = rToCopy.m_ascFile;
		m_machSimFolder = rToCopy.m_machSimFolder;
		m_partOffset = rToCopy.m_partOffset;
		m_partTol = rToCopy.m_partTol;
		m_betweenMovesCollCheck = rToCopy.m_betweenMovesCollCheck;
		m_rapidMovesCollCheck = rToCopy.m_rapidMovesCollCheck;
		m_geomCollCheckWhenVerifierEnabled = rToCopy.m_geomCollCheckWhenVerifierEnabled;
		m_verifierFileName = rToCopy.m_verifierFileName;
		m_verifierGUIFileName = rToCopy.m_verifierGUIFileName;
		m_enforceMachineLimits = rToCopy.m_enforceMachineLimits;
		m_mxpFile = rToCopy.m_mxpFile;  
		m_windowstyle = rToCopy.m_windowstyle ;
		m_reportFileName = rToCopy.m_reportFileName;
		m_reportFileNotOnTop = rToCopy.m_reportFileNotOnTop;
		m_resourceFileName = rToCopy.m_resourceFileName;
		m_5axisResourceFileName = rToCopy.m_5axisResourceFileName;
		m_machsimResourceFileName = rToCopy.m_machsimResourceFileName;
		m_verifierGUIResourceFileName = rToCopy.m_verifierGUIResourceFileName;
		m_machineReplacementOption = rToCopy.m_machineReplacementOption;
        m_ResourcePathName = rToCopy.m_ResourcePathName;
		m_initialMachineName = rToCopy.m_initialMachineName;
	}
//#############################################################################
	misc::mwstring m_machDefFolder;
	misc::mwstring m_simFile;
	misc::mwstring m_simulateBinFile;
	misc::mwstring m_stlFile;
	misc::mwstring m_stockFile;
	misc::mwstring m_fixtureFile;
	misc::mwstring m_ascFile;
	misc::mwstring m_machSimFolder;
	misc::mwstring m_partOffset;
	misc::mwstring m_partTol;
	misc::mwstring m_betweenMovesCollCheck;
	misc::mwstring m_rapidMovesCollCheck;
	misc::mwstring m_geomCollCheckWhenVerifierEnabled;
	misc::mwstring m_verifierFileName;
	misc::mwstring m_verifierGUIFileName;
	misc::mwstring m_enforceMachineLimits;
	misc::mwstring m_mxpFile;
	misc::mwstring m_windowstyle ;
	misc::mwstring m_reportFileName;
	misc::mwstring m_reportFileNotOnTop;
    misc::mwstring m_resourceFileName;
	misc::mwstring m_5axisResourceFileName;
	misc::mwstring m_machsimResourceFileName;
	misc::mwstring m_verifierGUIResourceFileName;
	misc::mwstring m_machineReplacementOption;
    misc::mwstring m_ResourcePathName;
	misc::mwstring m_initialMachineName;
};
#endif
