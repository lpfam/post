/******************************************************************************
*               File: mwCimMachSimResource.hpp                                *
*******************************************************************************
*               Description: declaration of mwCimMachSimResource class        *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  06.05.2009 15:13:08 Created by: David Chifiriuc                            *
*******************************************************************************
*               (C) 2009 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __MWCIMMACHSIMRESOURCE_HPP__
#define __MWCIMMACHSIMRESOURCE_HPP__
#include "mwResourceDll.hpp"
#include "mwCimMachineSimCoreDllDef.hpp"

class CIMMACHINESIMCORE_API mwCimMachSimResource
{
public:
	static misc::mwResourceDll handle;
protected:  
private:
};

#endif // __MWCIMMACHSIMRESOURCE_HPP__