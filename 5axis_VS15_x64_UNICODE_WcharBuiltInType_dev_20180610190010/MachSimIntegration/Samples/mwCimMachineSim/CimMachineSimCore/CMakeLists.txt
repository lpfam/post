mw_target(
    LIBRARY:
		CimMachineSimCore SHARED
		
	USE_MFC: SHARED
	
	# preprocessor CIMMACHINESIMCORE_EXPORTS is set as CimMachineSimCore_EXPORTS, which is false (linkage error)
	COMPILE_DEFINITIONS:
		PRIVATE
			CIMMACHINESIMCORE_EXPORTS

	COMPILE_OPTIONS-MSVC:
		PRIVATE
			/Yu
			/YuStdAfx.h
 
	UNICODE: off

    LINK_LIBRARIES:
        PUBLIC
			mwsimutil
			multixpost
			mwMSimApp
			mwMSimDefGui
			mwMSimOperationBuilder			
        
)


