SET SOURCE=%1
SET DESTINATION=%2

copy %SOURCE% %DESTINATION%
IF ERRORLEVEL 1 Goto ERROR
Goto End
:ERROR
	ECHO This error was received on the following command:
	ECHO copy %SOURCE% %DESTINATION%
:END
