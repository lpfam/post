@echo OFF
SET COMPILER_CONF_TYPE=%1

if "%COMPILER_CONF_TYPE%"=="Release" (SET QuickStartFolder=quickstart)
if "%COMPILER_CONF_TYPE%"=="Debug"   (SET QuickStartFolder=quickstart_Debug)

if "%COMPILER_CONF_TYPE%"=="ReleaseUnicode" (SET QuickStartFolder=quickstart)
if "%COMPILER_CONF_TYPE%"=="DebugUnicode"   (SET QuickStartFolder=quickstart_Debug)


if "%COMPILER_CONF_TYPE%"=="Release" (SET CompilerConfType=Release)
if "%COMPILER_CONF_TYPE%"=="ReleaseUnicode" (SET CompilerConfType=Release)

if "%COMPILER_CONF_TYPE%"=="Debug" (SET CompilerConfType=Debug)
if "%COMPILER_CONF_TYPE%"=="DebugUnicode" (SET CompilerConfType=Debug)

Call  SafeCopy  ..\CimMachineSimDll\%COMPILER_CONF_TYPE%\CimMachineSimDll.dll  .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\CimMachineSimCore\%COMPILER_CONF_TYPE%\CimMachineSimCore.dll  .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwsimutil.dll  .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwExceptions_res.dll  .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\MultiXPost.dll  .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwMSimApp.dll  .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwMSimDefGUI.dll  .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwMSimOperationBuilder.dll  .\%COMPILER_CONF_TYPE%\

Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwVerifier.dll .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwVerifierGUI.dll .\%COMPILER_CONF_TYPE%\
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwVerifierGUI_res.dll  .\%COMPILER_CONF_TYPE%\


if not exist ..\mwsimfiles  md ..\mwsimfiles
Call  SafeCopy  ..\..\..\..\%QuickStartFolder%\mwMachSim_res.dll  .\%COMPILER_CONF_TYPE%\
copy    ..\..\..\..\..\..\mwmachsim\english\mwMachSim.txt	..\mwsimfiles\exceptions.txt

Call  SafeCopy  .\%CompilerConfType%\CimMachineSim_res.dll  .\%COMPILER_CONF_TYPE%\

if not exist ..\mwsimfiles\xml md  ..\mwsimfiles\xml
xcopy  ..\..\..\..\%QuickStartFolder%\Machines ..\mwsimfiles\xml\ /S /Y

if not exist ..\%COMPILER_CONF_TYPE%Files md  ..\%COMPILER_CONF_TYPE%Files
Call  SafeCopy  ..\CimMachineSimDll\%COMPILER_CONF_TYPE%\CimMachineSimDll.dll  ..\%COMPILER_CONF_TYPE%Files\
Call  SafeCopy  ..\CimMachineSimCore\%COMPILER_CONF_TYPE%\CimMachineSimCore.dll  ..\%COMPILER_CONF_TYPE%Files\
Call  SafeCopy  .\%COMPILER_CONF_TYPE%\CimMachineSimExe.exe  ..\%COMPILER_CONF_TYPE%Files\
Call  SafeCopy  .\English\cimmachinesimexe.txt  ..\%COMPILER_CONF_TYPE%Files\