 // CimMachineSimExe.cpp : Defines the class behaviors for the application.
//

#include "StdAfx.h"
//#include "afxpriv.h"
#include "CimMachineSimExe.h"
#include "NcmwsimCommandLine.h"
#include "NSSimInvoker.h"
#include "mwFileName.hpp"
#include "mwFileSystem.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCimMachineSimExeApp
 
BEGIN_MESSAGE_MAP(CCimMachineSimExeApp, CWinApp)
	//{{AFX_MSG_MAP(CCimMachineSimExeApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CCimMachineSimExeApp::OnHelp)
END_MESSAGE_MAP()

void CCimMachineSimExeApp::OnHelp()
{
}

/////////////////////////////////////////////////////////////////////////////
// CCimMachineSimExeApp construction

CCimMachineSimExeApp::CCimMachineSimExeApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCimMachineSimExeApp object

CCimMachineSimExeApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCimMachineSimExeApp initialization

BOOL CCimMachineSimExeApp::InitInstance() 
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	#if _MSC_VER <1300//VC++ 7.0
		Enable3dControls();			// Call this when using MFC in a shared DLL
	#endif
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	ParseCommandLineInfo();

	mwCimMachSimCoreStartupParams coreStartupParams = PrepareStartupParameters( CNcmwsimCommandLine::Instance() );
	misc::mwstring iRegPath = CNcmwsimCommandLine::Instance()->GetRegPath();

	ResetRegistryKey();

	NSSimInvoker::Invoke ( coreStartupParams, iRegPath );

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
} 

void CCimMachineSimExeApp::ParseCommandLineInfo()
{
	CNcmwsimCommandLine* cmdInfo = CNcmwsimCommandLine::Instance(); 
    cmdInfo->m_nShellCommand = CCommandLineInfo::FileOpen;
	ParseCommandLine( *cmdInfo );
}

misc::mwstring GetParamFullPath(
	misc::mwstring rootPath,
	misc::mwstring path,
	misc::mwstring defpath = _T(""))
{
	if (path.empty())
	{
		if (!rootPath.empty() && !defpath.empty())
		{
			return misc::mwFileSystem::CombinePath(rootPath, defpath);
		}
	}
	else
	{
		if (!rootPath.empty())
		{
			return misc::mwFileSystem::CombinePath(rootPath, path);
		}
	}

	return path;	
}

misc::mwstring GetParamPath(
	misc::mwstring rootPath,
	misc::mwstring path)
{
	if (misc::mwFileSystem::IsPathRelativeToCurrentDir(path))
	{
		return path;
	}
	return GetParamFullPath(rootPath, path);
}

mwCimMachSimCoreStartupParams CCimMachineSimExeApp::PrepareStartupParameters( CNcmwsimCommandLine *cli )
{
	mwCimMachSimCoreStartupParams coreStartupParams;

	misc::mwstring wd(CNcmwsimCommandLine::Instance()->GetWorkDir());
	misc::mwFileSystem::NormalizeDir(wd);
	coreStartupParams.SetMachSimFolder(wd);

	if (wd.size() > 0)
	{
		misc::mwstring xmld;
		xmld = misc::mwFileSystem::CombinePath(wd, _T("xml"));
		misc::mwFileSystem::NormalizeDir(xmld);
		coreStartupParams.SetMachDefFolder(xmld);
	}

	coreStartupParams.SetSimFile(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetMwsimFile())));
	coreStartupParams.SetReportFileName(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetReportFileName())));
	coreStartupParams.SetStlFile(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetStlFile())));
	coreStartupParams.SetStockFile(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetStockFile())));
	coreStartupParams.SetFixtureFile(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetFixtureFile())));
    coreStartupParams.SetSimulateBinFile(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetSimulateBinFile())));
	coreStartupParams.SetAscFile(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetAscFile())));
	coreStartupParams.SetMxpFile(GetParamPath(wd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetMxpFile())));

	coreStartupParams.SetInitialMachineName(misc::mwstring(CNcmwsimCommandLine::Instance()->GetInitialMachineName()));
	coreStartupParams.SetPartOffset(misc::mwstring(CNcmwsimCommandLine::Instance()->GetPartOffset()));
	coreStartupParams.SetPartTolerance(misc::mwstring(CNcmwsimCommandLine::Instance()->GetPartTol()));
	coreStartupParams.SetBetweenMovesCollCheck(misc::mwstring(CNcmwsimCommandLine::Instance()->GetBetweenMovesCollCheck()));
	coreStartupParams.SetRapidMovesCollCheck( misc::mwstring(CNcmwsimCommandLine::Instance()->GetRapidMovesCollCheck() ) );
	coreStartupParams.SetEnforceMachineLimits( misc::mwstring(CNcmwsimCommandLine::Instance()->GetEnforceMachineLimits() ) ); 
	coreStartupParams.SetWindowStyle(misc::mwstring(CNcmwsimCommandLine::Instance()->GetStartupWindowStyle()));
	coreStartupParams.SetReportFileNotOnTop( misc::mwstring(CNcmwsimCommandLine::Instance()->GetReportFileNotOnTop() ) );
	coreStartupParams.SetMachineReplacementOption( misc::mwstring(CNcmwsimCommandLine::Instance()->GetMachineReplacementOption() ) );
	coreStartupParams.SetGeomCollCheckWhenVerifierEnabled(misc::mwstring(CNcmwsimCommandLine::Instance()->GetGeomCollCheckWhenVerifierEnabled()));
	
	/////////////////////////////////////////////
	// resource path & Dlls
	misc::mwstring rd(CNcmwsimCommandLine::Instance()->GetResourcePathName());
	misc::mwFileSystem::NormalizeDir(rd);
	coreStartupParams.SetResourcePathName(rd);
	coreStartupParams.Set5AxisResourceFileName(GetParamFullPath(rd, misc::mwstring(CNcmwsimCommandLine::Instance()->Get5axisResourceFileName()), _T("mwExceptions_res.dll")));
	coreStartupParams.SetMachSimResourceFileName(GetParamFullPath(rd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetMachSimResourceFileName()), _T("mwMachSim_res.dll")));
	coreStartupParams.SetVerifierGUIResourceFileName(GetParamFullPath(rd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetVerifierGUIResourceFileName()), _T("mwVerifierGui_res.dll")));
	coreStartupParams.SetResourceFileName(GetParamFullPath(rd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetResourceFileName())));
	coreStartupParams.SetVerifierFileName(GetParamFullPath(rd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetVerifierCorePath()), _T("mwVerifier.dll")));
	coreStartupParams.SetVerifierGUIFileName(GetParamFullPath(rd, misc::mwstring(CNcmwsimCommandLine::Instance()->GetVerifierGUIPath()), _T("mwVerifierGui.dll")));

	return coreStartupParams;
}

void CCimMachineSimExeApp::ResetRegistryKey()
{
	SetRegistryKey( _T("MW") );
}
