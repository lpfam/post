// CimMachineSimExe.h : main header file for the CIMMACHINESIMEXE application
//

#if !defined(AFX_CIMMACHINESIMEXE_H__0FB46925_182E_4710_AA5D_CE4610905225__INCLUDED_)
#define AFX_CIMMACHINESIMEXE_H__0FB46925_182E_4710_AA5D_CE4610905225__INCLUDED_

#if _MSC_VER > 1000
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

//#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCimMachineSimExeApp:
// See CimMachineSimExe.cpp for the implementation of this class
//

class CNcmwsimCommandLine;
class mwCimMachSimCoreStartupParams;

class CCimMachineSimExeApp : public CWinApp
{
public:
	CCimMachineSimExeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCimMachineSimExeApp)
	public:
	virtual BOOL InitInstance();

	void ResetRegistryKey();

	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCimMachineSimExeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	afx_msg void OnHelp();
private:

	void ParseCommandLineInfo();
	mwCimMachSimCoreStartupParams PrepareStartupParameters( CNcmwsimCommandLine *cli );

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIMMACHINESIMEXE_H__0FB46925_182E_4710_AA5D_CE4610905225__INCLUDED_)
