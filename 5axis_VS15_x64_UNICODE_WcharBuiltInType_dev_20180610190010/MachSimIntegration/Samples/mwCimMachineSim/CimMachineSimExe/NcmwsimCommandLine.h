// CNcmwsimCommandLine.h: interface for the CNcmwsimCommandLine class.
//
//////////////////////////////////////////////////////////////////////

#ifndef CNcmwsimCommandLine_h
#define CNcmwsimCommandLine_h

#include <map>

const CString cWorkDirFlag(_T("mwsimwrkdir"));
const CString cStlfileFlag(_T("stl"));
const CString cStockFile(_T("stock"));
const CString cFixtureFile(_T("fixture"));
const CString cAscfileFlag(_T("asc"));
const CString cMwsimfileFlag(_T("mwsim"));
const CString cSimulationBinfileFlag(_T("simulation_bin"));
const CString cRegPathFlag(_T("regpath"));
const CString cPartOffset(_T("part_offset"));
const CString cPartTol(_T("part_tol"));
const CString cBetweenMovesCollCheck(_T("between_moves_coll_check"));
const CString cRapidMovesCollCheck(_T("between_moves_coll_check_for_rapid_moves"));
const CString cVerifierCorePath(_T("verifier"));
const CString cVerifierGUIPath(_T("verifiergui"));
const CString cMachineLimits(_T("machine_limits"));
const CString cMxpfileFlag(_T("mxp"));
const CString cWindowStyle(_T("wndstyle")) ;
const CString cReportFileName(_T("reportfile"));
const CString cReportFileNotOnTop(_T("notontopreportfile"));
const CString cCimMachineSimResFileName(_T("resource"));
const CString c5axResFileName(_T("five_axis_resource_file"));
const CString cMachSimResFileName(_T("simulator_core_resource_file"));
const CString cVerifierGUIResFileName(_T("verifiergui_resource_file"));
const CString cMachineReplacementOption(_T("machine_replacement_option"));
const CString cResourcePathName(_T("resource_path"));
const CString cGeomCollCheckWhenVerifierEnabled(_T("geom_coll_check_when_verifier_enabled"));
const CString cInitialMachineName(_T("initial_machine"));


class CNcmwsimCommandLine : public CCommandLineInfo  
{
	typedef std::map<CString, CString> TParamMap;

private:
	CNcmwsimCommandLine();
	virtual ~CNcmwsimCommandLine();

public:

	static CNcmwsimCommandLine *Instance()
	{
		if (!stInstance)
			stInstance = new CNcmwsimCommandLine;
		return stInstance;
	}
	
	virtual void ParseParam(const TCHAR* pszParam, BOOL bFlag, BOOL bLast);

	// Get the Work Directory;
	CString GetWorkDir() {return mParamMap[cWorkDirFlag];}
	//Get the Stl file
	CString GetStlFile() {return mParamMap[cStlfileFlag];}
	//Get the stock .stl file
	CString GetStockFile() {return mParamMap[cStockFile];}
	//Get the fixture .stl file
	CString GetFixtureFile() {return mParamMap[cFixtureFile];}
	//Get the Asc file
	CString GetAscFile() {return mParamMap[cAscfileFlag];}
	//Get the Mwsim file
	CString GetMwsimFile() {return mParamMap[cMwsimfileFlag];}
        //Get the SimulationBinFile file
	CString GetSimulateBinFile() {return mParamMap[cSimulationBinfileFlag];}
	//Get the Regestry full path
	CString GetRegPath() {return mParamMap[cRegPathFlag];}
	//Get the part offset
	CString GetPartOffset() {return mParamMap[cPartOffset];}
	//Get the part tolerance
	CString GetPartTol() {return mParamMap[cPartTol];}
	//Get the between moves collision checking flag
	CString GetBetweenMovesCollCheck() {return mParamMap[cBetweenMovesCollCheck];}
	//Get the rapid moves collision checking flag
	CString GetRapidMovesCollCheck() {return mParamMap[cRapidMovesCollCheck];}
	//Get the verifier dll path
	CString GetVerifierCorePath() {return mParamMap[cVerifierCorePath];}
	//Get the verifier gui dll path
	CString GetVerifierGUIPath() {return mParamMap[cVerifierGUIPath];}
	//Get the verifier gui dll path
	CString GetEnforceMachineLimits() {return mParamMap[cMachineLimits];}
	//Get the Asc file
	CString GetMxpFile() {return mParamMap[cMxpfileFlag];}
	//Get the startup window style
	CString GetStartupWindowStyle(){return mParamMap[cWindowStyle];}
	//Get the report file name
	CString GetReportFileName(){return mParamMap[cReportFileName];}
	//Get the report file name NOT ON TOP value
	CString GetReportFileNotOnTop(){return mParamMap[cReportFileNotOnTop];}
	//Get the resource file name
	CString GetResourceFileName(){return mParamMap[cCimMachineSimResFileName];}
	//Get the 5axis resource file name
	CString Get5axisResourceFileName(){return mParamMap[c5axResFileName];}
	//Get the MachSim resource file name
	CString GetMachSimResourceFileName(){return mParamMap[cMachSimResFileName];}
	//Get the Verifier GUI resource file name
	CString GetVerifierGUIResourceFileName(){return mParamMap[cVerifierGUIResFileName];}
	//Get the machine replacement option
	CString GetMachineReplacementOption() {return mParamMap[cMachineReplacementOption];}
    //Get the resource path name
	CString GetResourcePathName() {return mParamMap[cResourcePathName];}
	//Gets the geometry collision checking when verifier is enabled flag
	CString GetGeomCollCheckWhenVerifierEnabled() {return mParamMap[cGeomCollCheckWhenVerifierEnabled];}
	//Gets the initial machine name
	CString GetInitialMachineName() {return mParamMap[cInitialMachineName];}
private:
	
	// holds the parameters names + values
	TParamMap mParamMap;
	TParamMap::iterator mParamMapIter;
	
	// the instance
	static CNcmwsimCommandLine *stInstance;

};

#endif // CNcmwsimCommandLine_h
