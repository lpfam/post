// CNcmwsimCommandLine.cpp: implementation of the CNcmwsimCommandLine class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "NcmwsimCommandLine.h"
#include <iostream>
#include "mwAvoid_Warnings.hpp"
CNcmwsimCommandLine *CNcmwsimCommandLine::stInstance = MW_NULL;

CNcmwsimCommandLine::CNcmwsimCommandLine()
{
	mParamMap[cWorkDirFlag]   = _T("");
	mParamMap[cStlfileFlag]   = _T("");
	mParamMap[cStockFile] = _T("");
	mParamMap[cFixtureFile] = _T("");
	mParamMap[cAscfileFlag]   = _T("");
	mParamMap[cMwsimfileFlag] = _T("");
    mParamMap[cSimulationBinfileFlag]	= _T("");
	mParamMap[cRegPathFlag]   = _T("");
	mParamMap[cPartOffset]    = _T("");
	mParamMap[cPartTol]		  = _T("");
	mParamMap[cBetweenMovesCollCheck] = _T("");
	mParamMap[cRapidMovesCollCheck] = _T("");
	mParamMap[cVerifierCorePath] = _T("");
	mParamMap[cVerifierGUIPath] = _T("");
	mParamMap[cMachineLimits] = _T("");
	mParamMap[cWindowStyle]   = _T("");
	mParamMap[cReportFileName]    = _T("");
	mParamMap[cReportFileNotOnTop]   = _T("");
	mParamMap[c5axResFileName] = _T("");
	mParamMap[cMachSimResFileName] = _T("");
	mParamMap[cVerifierGUIResFileName] = _T("");
	mParamMap[cMachineReplacementOption] = _T("");
    mParamMap[cResourcePathName] = _T("");
	mParamMap[cCimMachineSimResFileName] = _T("");
	mParamMap[cGeomCollCheckWhenVerifierEnabled] = _T("");
	mParamMap[cInitialMachineName] = _T("");
	mParamMap[cMxpfileFlag] = _T("");
}

CNcmwsimCommandLine::~CNcmwsimCommandLine()
{

}

void CNcmwsimCommandLine::ParseParam (const TCHAR* pszParam, BOOL bFlag, BOOL bLast)
{
	//AfxMessageBox(pszParam);
	bool isName=false;
	CString param(pszParam);
	if (bFlag)
	{//test if it's a value with minus or a real name
		double val;



#if _MSC_VER >=1400//VC++ 8.0
		if (_stscanf_s(pszParam,_T("%lf"),&val) != 1)
			
#else//older VC++ versions
		if (_stscanf(pszParam,_T("%lf"),&val) != 1)			
#endif
		{
			//not a value since conversion failed
			isName=true;
		}else
		{//was a value, need to readd the minus sign
			param="-" + param;
		}		
	}
	if (isName)
	// Find which Param (NAME) we're dealing with
	{		
		mParamMapIter = mParamMap.find (param);
		if (!(mParamMapIter != mParamMap.end()))
		{
			ASSERT (0);
			CCommandLineInfo::ParseParam(pszParam,bFlag,bLast);
			return;
		}
	}
	else 
	// Get Param (VALUE) (Assemble white apaces if needed)
	{
		if (!(mParamMapIter != mParamMap.end()))
		{
			ASSERT (0);
			CCommandLineInfo::ParseParam(pszParam,bFlag,bLast);
			return;
		}

		// Assemble Param's name, in case that the string contains white spaces, 
		// Add to previous value space (" ")
		CString aParamVal = (*mParamMapIter).second;
		if (aParamVal.IsEmpty())
			aParamVal = param;
		else
			aParamVal = aParamVal + _T(" ") + param;

		(*mParamMapIter).second = aParamVal;
	}
}

