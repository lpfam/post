/******************************************************************************
*               File: mwMxpuiParamHandler.cpp									  *
*******************************************************************************
*               Description:implements the mwMxpuiParamHandler class				  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 12:48:48 PM Created by: Costin Calisov							  *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "mwMachDef.hpp"
#include "mw5axControlDef.hpp"
#include "mwMXPParam.hpp"
#include "mwMXPWriterParam.hpp"
#include "mwToolPP.hpp"
#include "mwMessages.hpp"
#include "mwMachDefPP.h"
#include "mwMultyXPostPP.h"
#include "mwMxpuiDlgsParams.hpp"
#include "mwMxpuiParamInteractor.hpp"
#include "mwMxpuiSht.h"
#include "mwMxpuiParamHandler.hpp"
#include "mwPostCommonDefinitions.hpp"
#include "mwPostDefinitionContainer.hpp"
#include "mwMachDynamicsPP.hpp"
#include "mwMxpParamContainer.hpp"
#include "mwMachDefUtils.hpp"
//#############################################################################
bool mwMxpuiParamHandler::SetParams(
			   const mwMxpuiParamInteractor &rInteractor,
			   post::mwMachDefPtr& rMachDef,
			   post::mw5axControlDef& rControlDef,
			   post::mwMXPParam& rMXPParam, 
			   mwMXPWriterParam& rWriter,
			   double& rToolLengthCompensation, 
			   post::mwMachDefBuilder::MachDefFactory& rType,
			   post::mwMachDynamics& rmachDynamics,
			   mwMxpParamContainer& rMxpParamC)
{
	post::mwPostDefinitionContainer postDefCont;
	

	post::mwPostDefinition::id id = _T("");

	post::mwPostDefinition postDefinition(
		rMachDef,
		rType,
		rWriter.GetAxisNames(), 
		rmachDynamics,
		rMxpParamC.GetToolName(),
		rMxpParamC.GetWorkpieceName(),
		rMxpParamC.GetFirstTrAxis(),
		rMxpParamC.GetSecondTrAxis(),
		rMxpParamC.GetThirdTrAxis(),
		rMxpParamC.GetSpindles(),
		id,
		post::mwPostDefinition::initialValues(),
		new post::mwMXPParam(rMXPParam));

	post::mwPostDefinitionContainer postDefContainer(new post::mwPostDefinition(postDefinition));

	mwTransfMatrixPtr transfMatrixPtr = rMxpParamC.GetTransfMatrixVect();

	mwMxpParamsPtr paramsPtr  = new mwMxpParams(
		postDefContainer,
		id,
		rControlDef,
		rWriter,
		rToolLengthCompensation,
		transfMatrixPtr,
		false,
		false);
	//
	mwMxpuiSht sheet( rInteractor.GetMsg( MSG_SHEET_TITLE, TXT_SECTION ).c_str(), MW_NULL );
	//
	mwMachDefPP machineDef(paramsPtr,rInteractor);
	mwMultyXPostPP mpx(paramsPtr,rInteractor);
	mwMachDynamicsPP machDynamics(paramsPtr,rInteractor);
	mwToolPP tool(paramsPtr,rInteractor);
	//
	sheet.AddPage (&machineDef);
	sheet.AddPage (&mpx);
	sheet.AddPage(&machDynamics);
	sheet.AddPage (&tool);
	//show sheet
	INT_PTR nRes = sheet.DoModal();

	if( nRes == IDOK )
	{
		const post::mwPostDefinitionPtr postDefPtr = postDefContainer.GetPostDefinition(id);

		rMachDef = postDefPtr->GetMachDefPtr();
		rControlDef = paramsPtr->Get5axControlDef();
		rWriter = paramsPtr->GetMXPWriterParam();
		rToolLengthCompensation = paramsPtr->GetToolLengthCompensation();
		rType = postDefPtr->GetMachDefFactory();
		rmachDynamics = postDefPtr->GetMachDynamics();

		rMxpParamC.SetFirstTrAxis(postDefPtr->GetFirstTrAxis());
		rMxpParamC.SetSecondTrAxis(postDefPtr->GetSecondTrAxis());
		rMxpParamC.SetThirdTrAxis(postDefPtr->GetThirdTrAxis());
		rMxpParamC.SetSpindles(postDefPtr->GetSpindles());
		rMxpParamC.SetTransfMatrixVect(transfMatrixPtr);
		rMxpParamC.SetToolName(postDefPtr->GetToolName());
		rMxpParamC.SetWorkpieceName(postDefPtr->GetWorkpieceName());
		
		return true;
	}
	return false;
}
//#############################################################################
bool mwMxpuiParamHandler::SetParams(
	const mwMxpuiParamInteractor &rInteractor,
	post::mwPostDefinitionContainer& rPostDefCont,
	misc::mwstring& rId,
	post::mw5axControlDef& rControlDef,
	mwMXPWriterParam& rWriter,
	double& rToolLengthCompensation,
	mwTransfMatrixPtr& rTransfMatrixPtr,
	const bool rDisablePostSettingEditing,
	const bool rUseMastercamSettings,
	const bool rDisableAddingAdditionalMoves)
{
	mwMxpParamsPtr paramsPtr  = new mwMxpParams(
		rPostDefCont,
		rId,
		rControlDef,
		rWriter,
		rToolLengthCompensation,
		rTransfMatrixPtr,
		rDisablePostSettingEditing,
		rUseMastercamSettings,
		rDisableAddingAdditionalMoves
		);
	SetDisableAddingAdditionalMoves(rDisableAddingAdditionalMoves);
	//
	mwMxpuiSht sheet( rInteractor.GetMsg( MSG_SHEET_TITLE, TXT_SECTION ).c_str(), MW_NULL );
	//
	mwMachDefPP machineDef(paramsPtr,rInteractor);
	mwMultyXPostPP mpx(paramsPtr,rInteractor);
	mwMachDynamicsPP machDynamics(paramsPtr,rInteractor);
	mwToolPP tool(paramsPtr,rInteractor);
	//
	sheet.AddPage (&machineDef);
	sheet.AddPage (&mpx);
	sheet.AddPage(&machDynamics);
	sheet.AddPage (&tool);
	//show sheet
	INT_PTR nRes = sheet.DoModal();
	
	if( nRes == IDOK )
	{
		rPostDefCont = paramsPtr->GetPostDefinitionContainer();
		rControlDef = paramsPtr->Get5axControlDef();
		rWriter = paramsPtr->GetMXPWriterParam();
		rToolLengthCompensation = paramsPtr->GetToolLengthCompensation();
		rTransfMatrixPtr =  paramsPtr->GetTransfMatrixPtr();
		rId = paramsPtr->GetPostSettingID();
		rWriter.SetAxisNames(
			rPostDefCont.GetPostDefinition(rId)->GetAxisNames(), 
			post::mwMachDefUtils::GetNumbersOfAxes(rPostDefCont.GetPostDefinition(rId)->GetMachDefPtr())),
		rWriter.SetMachineRapideFeedRate(rPostDefCont.GetPostDefinition(rId)->GetMachDynamics().GetRapidRate());
		
		m_disableAddingAdditionalMoves = paramsPtr->GetDisableAddingAdditionalMoves();
		return true;
	}
	return false;
}
//#############################################################################
void mwMxpuiParamHandler::SetDisableAddingAdditionalMoves(const bool disableAddingAdditionalMoves)
{
	m_disableAddingAdditionalMoves = disableAddingAdditionalMoves;
}
//#############################################################################
bool mwMxpuiParamHandler::GetDisableAddingAdditionalMoves() const
{
	return m_disableAddingAdditionalMoves;
}