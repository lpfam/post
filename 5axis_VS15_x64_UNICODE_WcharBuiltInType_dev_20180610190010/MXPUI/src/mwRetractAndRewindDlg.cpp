/******************************************************************************
	(C) 2017 by ModuleWorks GmbH
	Author: Robert-Stefan Dumitru
******************************************************************************/

#include "StdAfx.h"
#include "mwRetractAndRewindDlg.hpp"
#include "mwMessages.hpp"

mwRetractAndRewindDlg::mwRetractAndRewindDlg(
	const post::mwMXPParamPtr& mxpParamPtr,
	misc::mwAutoPointer<mwMxpParams>& pMxpParams,
	const mwMxpuiParamInteractor &rInteractor,
	CWnd* pParent)
	: mwMxpuiDlgsParams(pMxpParams, rInteractor)
	, CDialog(mwRetractAndRewindDlg::IDD, pParent)
	, m_mxpParamPtr(mxpParamPtr)
{
	//{{AFX_DATA_INIT(mwPatchDlg)
	//}}AFX_DATA_INIT

	m_retractToolAtMax = mxpParamPtr->GetRetractToolAtMax();
	m_additionalRetract = mxpParamPtr->GetAdditionalRetractFlag();
	m_additionalRetractToolAtMax = mxpParamPtr->GetAdditionalRetractToolAtMax();
	m_retractAndRewindAngleFlag = mxpParamPtr->GetRetractAndRewindAngleFlag();
	m_retractDistance = mxpParamPtr->GetRetractDistForLargeAngChange();
	m_additionalRetractDistance = mxpParamPtr->GetAdditionalRetractDistForLargeAngChange();
	m_rewindMaxAngleStep = mxpParamPtr->GetRetractAndRewindAngleStep();

	const cadcam::mwPoint3d& additionalRetractDirection = mxpParamPtr->GetAdditionalRetractDirection();

	m_additionalRetractDirectionX = additionalRetractDirection.x();
	m_additionalRetractDirectionY = additionalRetractDirection.y();
	m_additionalRetractDirectionZ = additionalRetractDirection.z();

	m_retractAndRewindDlgToolTip = MW_NULL;
}

mwRetractAndRewindDlg::~mwRetractAndRewindDlg()
{
	delete m_retractAndRewindDlgToolTip;
}

BOOL mwRetractAndRewindDlg::PreTranslateMessage(MSG* pMsg)
{
	if (m_retractAndRewindDlgToolTip)
		m_retractAndRewindDlgToolTip->RelayEvent(pMsg);

	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		CDialog::NextDlgCtrl(); // Pass focus to next control
		return TRUE;			// Don't translate further
	}
	else
		return CDialog::PreTranslateMessage(pMsg);
}

void mwRetractAndRewindDlg::InitToolTip()
{
	m_retractAndRewindDlgToolTip = new CToolTipCtrl;
	if (!m_retractAndRewindDlgToolTip->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_RETRACT_DISTANCE), _T("retract_dist_for_large_angle_change"));
		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_RETRACT_TO_MACH_AX_LIMITS), _T("retract_tool_at_max_enable"));
		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_REWIND_MAX_ANGLE), _T("retract_and_rewind_angle_enable"));
		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_REWIND_MAX_ANGLE), _T("retract_and_rewind_angle_step"));

		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_ADDI_RETRACT), _T("additional_retract_enable"));
		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_ADDI_RETRACT_TO_MACH_AX_LIMITS), _T("additional_retract_tool_at_max_enable"));
		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_ADDI_RETRACT_DISTANCE), _T("additional_retract_dist_for_large_angle_change"));
		m_retractAndRewindDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_REWIND_MAX_ANGLE), _T("additional_retract_direction"));

		m_retractAndRewindDlgToolTip->Activate(TRUE);
	}
}

void mwRetractAndRewindDlg::DoDataExchange(CDataExchange* pDX)
{
	if (pDX->m_bSaveAndValidate == NULL)
	{
		EnableOrDisable();
		SetTexts();
	}
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwPatchDlg)
	//}}AFX_DATA_MAP

	DDX_Check(pDX, IDC_RDN_RETRACT_TO_MACH_AX_LIMITS, m_retractToolAtMax);
	BOOL notRetractToolAtMax = m_retractToolAtMax ? FALSE : TRUE;
	DDX_Check(pDX, IDC_RDN_RETRACT_DISTANCE, notRetractToolAtMax);

	DDX_Text(pDX, IDC_EDT_RETRACT_DISTANCE, m_retractDistance);
	
	DDX_Check(pDX, IDC_CHK_ADDI_RETRACT, m_additionalRetract);

	DDX_Text(pDX, IDC_EDT_ADDI_RETRACT_DIRECTION_X, m_additionalRetractDirectionX);
	DDX_Text(pDX, IDC_EDT_ADDI_RETRACT_DIRECTION_Y, m_additionalRetractDirectionY);
	DDX_Text(pDX, IDC_EDT_ADDI_RETRACT_DIRECTION_Z, m_additionalRetractDirectionZ);

	DDX_Check(pDX, IDC_RDN_ADDI_RETRACT_TO_MACH_AX_LIMITS, m_additionalRetractToolAtMax);
	BOOL notAdditionalRetractToolAtMax = m_additionalRetractToolAtMax ? FALSE : TRUE;
	DDX_Check(pDX, IDC_RDN_ADDI_RETRACT_DISTANCE, notAdditionalRetractToolAtMax);

	DDX_Text(pDX, IDC_EDT_ADDI_RETRACT_DISTANCE, m_additionalRetractDistance);

	BOOL notRewindInOneAngleStep = m_retractAndRewindAngleFlag ? FALSE : TRUE;
	DDX_Check(pDX, IDC_RDN_REWIND_IN_ONE_ANGLE_STEP, notRewindInOneAngleStep);
	DDX_Check(pDX, IDC_RDN_REWIND_MAX_ANGLE, m_retractAndRewindAngleFlag);

	DDX_Text(pDX, IDC_EDT_REWIND_MAX_ANGLE, m_rewindMaxAngleStep);

	EnableOrDisable();
}

BEGIN_MESSAGE_MAP(mwRetractAndRewindDlg, CDialog)
	//{{AFX_MSG_MAP(mwPatchDlg)
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_RDN_RETRACT_TO_MACH_AX_LIMITS, OnSelChangeRadioRetractToMachAxLimits)
	ON_BN_CLICKED(IDC_RDN_RETRACT_DISTANCE, OnSelChangeRadioRetractDistance)
	ON_BN_CLICKED(IDC_CHK_ADDI_RETRACT, OnChkAdditionalRetract)
	ON_BN_CLICKED(IDC_RDN_ADDI_RETRACT_TO_MACH_AX_LIMITS, OnSelChangeRadioAdditionalRetractToMachAxLimits)
	ON_BN_CLICKED(IDC_RDN_ADDI_RETRACT_DISTANCE, OnSelChangeRadioAdditionalRetractDistance)
	ON_BN_CLICKED(IDC_RDN_REWIND_IN_ONE_ANGLE_STEP, OnSelChangeRadioRewindInOneAngleStep)
	ON_BN_CLICKED(IDC_RDN_REWIND_MAX_ANGLE, OnSelChangeRadioRewindMaxAngle)
	ON_BN_CLICKED(IDOK_RETRACT_AND_REWIND_PARAM, OnBnClickedOk)
END_MESSAGE_MAP()

BOOL mwRetractAndRewindDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	InitToolTip();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void mwRetractAndRewindDlg::SetTexts()
{
	SetCntrlText(IDC_STATIC_RETRACT, MSG_STATIC_RETRACT);
	SetCntrlText(IDC_RDN_RETRACT_TO_MACH_AX_LIMITS, MSG_RDN_MAX_RETRACT);
	SetCntrlText(IDC_RDN_RETRACT_DISTANCE, MSG_RDN_TOOL_RETRACT_DISTANCE);
	SetCntrlText(IDC_EDT_RETRACT_DISTANCE, MSG_LBL_SPINDLE_SETTINGS_ID);
	SetCntrlText(IDC_CHK_ADDI_RETRACT, MSG_CHK_ADDI_RETRACT);
	SetCntrlText(IDC_LBL_ADDI_RETRACT_DIRECTION, MSG_LBL_ADDI_RETRACT_DIRECTION);
	SetCntrlText(IDC_LBL_ADDI_RETRACT_DIRECTION_X, MSG_LBL_ADDI_RETRACT_DIRECTION_X);
	SetCntrlText(IDC_LBL_ADDI_RETRACT_DIRECTION_Y, MSG_LBL_ADDI_RETRACT_DIRECTION_Y);
	SetCntrlText(IDC_LBL_ADDI_RETRACT_DIRECTION_Z, MSG_LBL_ADDI_RETRACT_DIRECTION_Z);
	SetCntrlText(IDC_RDN_ADDI_RETRACT_TO_MACH_AX_LIMITS, MSG_RDN_ADDI_RETRACT_TO_MACH_AX_LIMITS);
	SetCntrlText(IDC_RDN_ADDI_RETRACT_DISTANCE, MSG_RDN_ADDI_RETRACT_DISTANCE);
	SetCntrlText(IDC_STATIC_REWIND, MSG_STATIC_REWIND);
	SetCntrlText(IDC_RDN_REWIND_IN_ONE_ANGLE_STEP, MSG_RDN_REWIND_ONE_ANGLE_STEP);
	SetCntrlText(IDC_RDN_REWIND_MAX_ANGLE, MSG_RDN_REWIND_MAX_ANGLE_INTERPOLATION);
}

void mwRetractAndRewindDlg::OnSelChangeRadioRetractToMachAxLimits()
{
	m_retractToolAtMax = true;
	EnableOrDisable();
}

void mwRetractAndRewindDlg::OnSelChangeRadioRetractDistance()
{
	m_retractToolAtMax = false;
	EnableOrDisable();
}

void mwRetractAndRewindDlg::OnChkAdditionalRetract()
{
	m_additionalRetract = !m_additionalRetract;
	EnableOrDisable();
}

void mwRetractAndRewindDlg::OnSelChangeRadioAdditionalRetractToMachAxLimits()
{
	m_additionalRetractToolAtMax = true;
	EnableOrDisable();
}

void mwRetractAndRewindDlg::OnSelChangeRadioAdditionalRetractDistance()
{
	m_additionalRetractToolAtMax = false;
	EnableOrDisable();
}

void mwRetractAndRewindDlg::OnSelChangeRadioRewindInOneAngleStep()
{
	m_retractAndRewindAngleFlag = false;
	EnableOrDisable();
}

void mwRetractAndRewindDlg::OnSelChangeRadioRewindMaxAngle()
{
	m_retractAndRewindAngleFlag = true;
	EnableOrDisable();
}

void mwRetractAndRewindDlg::OnBnClickedOk()
{
	UpdateData();

	m_mxpParamPtr->SetRetractToolAtMax(m_retractToolAtMax != FALSE);
	m_mxpParamPtr->SetAdditionalRetractFlag(m_additionalRetract != FALSE);
	m_mxpParamPtr->SetAdditionalRetractToolAtMax(m_additionalRetractToolAtMax != FALSE);
	m_mxpParamPtr->SetRetractAndRewindAngleFlag(m_retractAndRewindAngleFlag != FALSE);
	m_mxpParamPtr->SetRetractDistForLargeAngChange(m_retractDistance);
	m_mxpParamPtr->SetAdditionalRetractDistForLargeAngChange(m_additionalRetractDistance);
	m_mxpParamPtr->SetRetractAndRewindAngleStep(m_rewindMaxAngleStep);

	const cadcam::mwPoint3d additionalRetractDirection(m_additionalRetractDirectionX, m_additionalRetractDirectionY, m_additionalRetractDirectionZ);

	m_mxpParamPtr->SetAdditionalRetractDirection(additionalRetractDirection);

	CDialog::OnOK();
}

void mwRetractAndRewindDlg::EnableOrDisable()
{
	GetDlgItem(IDC_EDT_RETRACT_DISTANCE)->EnableWindow(!m_retractToolAtMax);

	GetDlgItem(IDC_CHK_ADDI_RETRACT)->EnableWindow(!m_retractToolAtMax);
	
	GetDlgItem(IDC_LBL_ADDI_RETRACT_DIRECTION)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	
	GetDlgItem(IDC_LBL_ADDI_RETRACT_DIRECTION_X)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	GetDlgItem(IDC_LBL_ADDI_RETRACT_DIRECTION_Y)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	GetDlgItem(IDC_LBL_ADDI_RETRACT_DIRECTION_Z)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	
	GetDlgItem(IDC_EDT_ADDI_RETRACT_DIRECTION_X)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	GetDlgItem(IDC_EDT_ADDI_RETRACT_DIRECTION_Y)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	GetDlgItem(IDC_EDT_ADDI_RETRACT_DIRECTION_Z)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	
	GetDlgItem(IDC_RDN_ADDI_RETRACT_TO_MACH_AX_LIMITS)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	
	GetDlgItem(IDC_RDN_ADDI_RETRACT_DISTANCE)->EnableWindow(m_additionalRetract && !m_retractToolAtMax);
	GetDlgItem(IDC_EDT_ADDI_RETRACT_DISTANCE)->EnableWindow(m_additionalRetract && !m_retractToolAtMax &&!m_additionalRetractToolAtMax);
	
	GetDlgItem(IDC_EDT_REWIND_MAX_ANGLE)->EnableWindow(m_retractAndRewindAngleFlag);
}

