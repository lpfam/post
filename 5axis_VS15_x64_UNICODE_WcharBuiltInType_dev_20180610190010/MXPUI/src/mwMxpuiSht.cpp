// 5AxSht.cpp : implementation file
//

#include "StdAfx.h"
#include "resource.h"
#include "mwMxpuiSht.h"
#include "mwMxpuiPage.hpp"

#ifdef _DEBUG
	#define new DEBUG_NEW
	#undef THIS_FILE
	static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// mwMxpuiSht

IMPLEMENT_DYNAMIC(mwMxpuiSht, CPropertySheet)

mwMxpuiSht::mwMxpuiSht( LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage )
:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
}

mwMxpuiSht::~mwMxpuiSht()
{
}

BOOL mwMxpuiSht::OnInitDialog() 
{
    BOOL bResult = CPropertySheet::OnInitDialog();
    ModifyStyleEx(0, WS_EX_CONTEXTHELP); // add ? context help button to sheet title bar
		
	CRect rect, tabrect;
	
	GetDlgItem(IDOK)->GetWindowRect(rect);
	ScreenToClient(rect);

	GetTabControl()->GetWindowRect(tabrect);
	ScreenToClient(tabrect);

	int textPadding = 6;
	CString label = _T("Reset to Default");
	CSize labelSize = GetTextSize(label, GetFont());
	rect.left = tabrect.left; 
	rect.right = tabrect.left + labelSize.cx + 2*textPadding;
	
	bResult = m_resetDefaultBttn.Create(_T("Reset to default"), BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | WS_TABSTOP, rect, this, IDC_RESET_DEFAULT);
	m_resetDefaultBttn.SetFont(GetFont());
	m_resetDefaultBttn.EnableWindow(FALSE);

    return bResult;
} 

BOOL mwMxpuiSht::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	NMHDR* pnmh = (NMHDR*)lParam;
	if (TCN_SELCHANGE == pnmh->code)
	{
		PostMessage(WM_UPDATERESETBTTN);
	}

	return CPropertySheet::OnNotify(wParam, lParam, pResult);
}

LRESULT mwMxpuiSht::OnUpdateResetBttn(WPARAM, LPARAM)
{
	mwMxpuiPage* activePage = (mwMxpuiPage*)GetActivePage();
	BOOL flag = activePage->HasResetOption();

	m_resetDefaultBttn.EnableWindow(flag);

	return 0;
}

BOOL mwMxpuiSht::UpdateData(BOOL bSaveAndValidate)
{
	if (!CPropertySheet::UpdateData(bSaveAndValidate))
		return FALSE;

	// check property sheet. Need when OK button pressed in main dialog
	return GetActivePage()->UpdateData(bSaveAndValidate);
}

BEGIN_MESSAGE_MAP(mwMxpuiSht, CPropertySheet)
	ON_COMMAND(IDHELP, OnCmdMsg)
	ON_BN_CLICKED(IDC_RESET_DEFAULT, OnReset)
	ON_MESSAGE(WM_UPDATERESETBTTN, OnUpdateResetBttn)
END_MESSAGE_MAP()

void mwMxpuiSht::OnCmdMsg() 
{
	//let the page to solve his own help
	CPropertySheet::GetActivePage()->SendMessage(WM_HELP);
}

void mwMxpuiSht::OnReset()
{
	mwMxpuiPage* activePage = (mwMxpuiPage*) GetActivePage();
	activePage->Reset();
}

CSize mwMxpuiSht::GetTextSize(CString text, CFont *pFont)
{
	CDC *pDC = GetDC();
	CFont *pOldFont = pDC->SelectObject(pFont);
	CSize Size = pDC->GetTextExtent(text);
	pDC->SelectObject(pOldFont);
	ReleaseDC(pDC);

	return Size;
}

INT_PTR mwMxpuiSht::DoModal() 
{
	return CPropertySheet::DoModal();
}

