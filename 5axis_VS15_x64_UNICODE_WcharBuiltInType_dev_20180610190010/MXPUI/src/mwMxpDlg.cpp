/******************************************************************************
*               File: mwMxpDlg.cpp									          *
*******************************************************************************
*               Description:implements the mwMxpDlg class				      *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*   10/8/2003 2:59:45 PM Created by: Costin Calisov                           *
*   4/8/2015 Changed by: Tolbariu Ionut-Irinel                                *
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#include "StdAfx.h"
#include "resource.h"
#include "mwPostException.hpp"
#include "mwMxpuiException.hpp"
#include "mwMessages.hpp"
#include "mwMxpDlg.h"
#include "mwMXPParam.hpp"
#include "mwRetractAndRewindDlg.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// mwMxpDlg property page

mwMxpDlg::mwMxpDlg(
	misc::mwAutoPointer<mwMxpParams>& pMxpParams,
	const mwMxpuiParamInteractor &rInteractor,
	CWnd* pParent) : mwMxpuiDlg(pMxpParams, rInteractor, IDD, pParent)
{
	m_AutoAnglePair = TRUE;
	m_MachineLimits = -1;
	m_AngleChangeLimit = 0.0;
	m_PoleAngleTolInDegree = 0.0;
	m_AngleTolInDegForUsingMachLimits = 0.0001;
	m_startAngleType = -1;
	m_provideTransAxis = -1;
	m_solutionForStartAngle = -1;
	m_preferredStartAngleValDeg = 0.0;
	m_firstStartRotationAxisAngle = 0.0;
	m_secondStartRotationAxisAngle = 0.0;
	m_thirdStartRotationAxisAngle = 0.0;
	m_poleHandling = -1;
	m_AlignToolpath = FALSE;
	m_interpolationType = -1;
	m_firstRotAxisAngleLimit = -1;
	m_secondRotAxisAngleLimit = -1;
	m_thirdRotAxisAngleLimit = -1;
	m_provideStartRotationAngle = FALSE;
	m_feedRateInterAngStep = 0.0;
	m_rapidRateInterAngStep = 0.0;
	m_feedRateInterDist = 0.0;
	m_rapidRateInterDist = 0.0;
	m_rapidRateInterDistFlag = FALSE;
	m_feedRateInterDistFlag = FALSE;
	m_rapidRateInterAngleFlag = FALSE;
	m_feedRateInterAngleFlag = FALSE;
	m_retractAndRewindFlag = FALSE;
	m_forceHeadOrTableRotation = FALSE;
	m_limitLinearAxisTravel = FALSE;
	m_customTransAxisDir = FALSE;
	m_customTransAxisDirX = 1.0;
	m_customTransAxisDirY = 0.0;
	m_customTransAxisDirZ = 0.0;

	m_enableMXP = TRUE;

	m_filterDuplicateMoves = TRUE;

	m_hideAngleControlLimits = false;
	m_resetParams = false;

	m_mxpDlgToolTip = MW_NULL;
}

mwMxpDlg::~mwMxpDlg()
{
	m_mxpDlgToolTip = MW_NULL;
}

BOOL mwMxpDlg::OnInitDialog()
{
	BOOL bRet = mwMxpuiDlg::OnInitDialog();

	InitToolTip();

	return bRet;
}


void mwMxpDlg::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if (pDX->m_bSaveAndValidate == NULL)
	{
		GetDlgParams(*pDX);
		UpdateControls();
		SetTexts();
		EnableAllChildControls(m_enableMXP);
	}

	CDialog::DoDataExchange(pDX);

	DDX_Check(pDX, IDC_RDN_AUTO_ANGLE_PAIR, m_AutoAnglePair);
	BOOL NotAutoAnglePair = m_AutoAnglePair ? FALSE : TRUE;
	DDX_Check(pDX, IDC_RDN_KEEP_ANGLE_PAIR, NotAutoAnglePair);

	int NameSolutionRadio = m_startAngleType == mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS;
	int ProvideFirstRotAxisAngleRadio = m_startAngleType == mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE;
	int ProvideSecondRotAxisAngleRadio = m_startAngleType == mwMxpDlg::MXP_USE_SECOND_ROT_ANGLE;
	int ProvideTransAxisDirectionRadio = m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS;
	int ProvideThirdRotAxisAngleRadio = m_startAngleType == mwMxpDlg::MXP_USE_THIRD_ROT_ANGLE;

	int StandardTransAxisDirectionRadio = !m_customTransAxisDir;
	int CustomTransAxisDirectionRadio = m_customTransAxisDir;

	DDX_Check(pDX, IDC_RDN_NAME_SOLUTION, NameSolutionRadio);
	DDX_Check(pDX, IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE, ProvideFirstRotAxisAngleRadio);
	DDX_Check(pDX, IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE, ProvideSecondRotAxisAngleRadio);
	DDX_Check(pDX, IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION, ProvideTransAxisDirectionRadio);
	DDX_Check(pDX, IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE, ProvideThirdRotAxisAngleRadio);

	DDX_Check(pDX, IDC_RDN_STANDARD_TRANS_AXIS_DIRECTION, StandardTransAxisDirectionRadio);
	DDX_Check(pDX, IDC_RDN_CUSTOM_TRANS_AXIS_DIRECTION, CustomTransAxisDirectionRadio);

	DDX_Check(pDX, IDC_CHK_FORCE_HEAD_OR_TABLE_ROTATION, m_forceHeadOrTableRotation);
	DDX_Check(pDX, IDC_CHK_LIMIT_LINEAR_AXIS_TRAVEL, m_limitLinearAxisTravel);

	DDX_Text(pDX, IDC_EDT_ANGLE_CHANGE_LIMIT, m_AngleChangeLimit);
	DDX_Text(pDX, IDC_EDT_POL_ANGLE_TOL, m_PoleAngleTolInDegree);
	DDX_CBIndex(pDX, IDC_CMB_MACHINE_LIMITS, m_MachineLimits);
	DDX_Text(pDX, IDC_EDT_ANGLE_TOL_MACH_LIM, m_AngleTolInDegForUsingMachLimits);
	DDX_CBIndex(pDX, IDC_CMB_SOLUTIONS_FOR_START_TRANS, m_provideTransAxis);
	DDX_CBIndex(pDX, IDC_CMB_SOLUTIONS_FOR_START_ANGLE, m_solutionForStartAngle);

	DDX_Text(pDX, IDC_EDT_CUSTOM_TRANS_AXIS_X, m_customTransAxisDirX);
	DDX_Text(pDX, IDC_EDT_CUSTOM_TRANS_AXIS_Y, m_customTransAxisDirY);
	DDX_Text(pDX, IDC_EDT_CUSTOM_TRANS_AXIS_Z, m_customTransAxisDirZ);

	DDX_Text(pDX, IDC_EDT_FIRST_ROTATION_AXIS_ANGLE, m_firstStartRotationAxisAngle);
	DDX_Text(pDX, IDC_EDT_SECOND_ROTATION_AXIS_ANGLE, m_secondStartRotationAxisAngle);
	DDX_Text(pDX, IDC_EDT_THIRD_ROTATION_AXIS_ANGLE, m_thirdStartRotationAxisAngle);
	DDX_CBIndex(pDX, IDC_CMB_POLE_HANDLING, m_poleHandling);
	DDX_Check(pDX, IDC_CHK_TOOLPATH_ALIGNMENT, m_AlignToolpath);
	DDX_Check(pDX, IDC_CHK_FILTER_DUP_MOVES, m_filterDuplicateMoves);
	DDX_Check(pDX, IDC_CHK_DISABLE_MXP_ADD_MOVES, m_disableAddingAdditionalMoves);

	DDX_CBIndex(pDX, IDC_CMB_FIRST_ROT_AXIS_ANGLE_LIMIT, m_firstRotAxisAngleLimit);
	DDX_CBIndex(pDX, IDC_CMB_SECOND_ROT_AXIS_ANGLE_LIMIT2, m_secondRotAxisAngleLimit);
	DDX_CBIndex(pDX, IDC_CMB_THIRD_ROT_AXIS_ANGLE_LIMIT, m_thirdRotAxisAngleLimit);

	DDX_CBIndex(pDX, IDC_CMB_INTERPOLATION_TYPE, m_interpolationType);

	BOOL DefaultSolution = m_startAngleType == mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS;
	BOOL ProvideStartRotationAngle = m_startAngleType == mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE;
	BOOL ProvideTransAxisDirectionRadio4ax = m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS;

	DDX_Check(pDX, IDC_RDN_DEFAULT_SOLUTION, DefaultSolution);
	DDX_Check(pDX, IDC_RDN_PROVIDE_ROT_AXIS_ANGLE, ProvideStartRotationAngle);
	DDX_Check(pDX, IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS, ProvideTransAxisDirectionRadio4ax);

	DDX_Check(pDX, IDC_CHK_FEED_RATE_INTERPOLATION_DISTANCE, m_feedRateInterDistFlag);
	DDX_Check(pDX, IDC_CHK_RAPID_RATE_INTERPOLATION_DISTANCE, m_rapidRateInterDistFlag);
	DDX_Check(pDX, IDC_CHK_FEED_RATE_INTERPOLATION_ANGLE, m_feedRateInterAngleFlag);
	DDX_Check(pDX, IDC_CHK_RAPID_RATE_INTERPOLATION_ANGLE, m_rapidRateInterAngleFlag);

	DDX_Text(pDX, IDC_EDT_FEED_RATE_INTERPOLATION_DISTANCE, m_feedRateInterDist);
	DDX_Text(pDX, IDC_EDT_RAPID_RATE_INTERPOLATION_DISTANCE, m_rapidRateInterDist);
	DDX_Text(pDX, IDC_EDT_FEED_RATE_INTERPOLATION_ANGLE, m_feedRateInterAngStep);
	DDX_Text(pDX, IDC_EDT_RAPID_RATE_INTERPOLATION_ANGLE, m_rapidRateInterAngStep);

	DDX_Check(pDX, IDC_CHK_RETRACT_AND_REWIND, m_retractAndRewindFlag);

	DDX_Check(pDX, IDC_CHK_ENABLE_MXP, m_enableMXP);

	if (pDX->m_bSaveAndValidate)
	{
		SetDlgParams(*pDX);
	}
}

BOOL mwMxpDlg::PreTranslateMessage(MSG* pMsg)
{
	if (m_mxpDlgToolTip)
		m_mxpDlgToolTip->RelayEvent(pMsg);

	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		mwMxpuiDlg::NextDlgCtrl(); // Pass focus to next control
		return TRUE;			// Don't translate further
	}
	else
		return mwMxpuiDlg::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(mwMxpDlg, mwMxpuiDlg)
	ON_BN_CLICKED(IDC_RDN_AUTO_ANGLE_PAIR, OnSelchangeRadioAutoAnglePair)
	ON_BN_CLICKED(IDC_RDN_KEEP_ANGLE_PAIR, OnSelchangeRadioKeepAnglePair)
	ON_BN_CLICKED(IDC_RDN_NAME_SOLUTION, OnSelchangeRadioNameSolution)
	ON_BN_CLICKED(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE, OnSelchangeRadioProvideFirstRotAxisAngle)
	ON_BN_CLICKED(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE, OnSelchangeRadioProvideSecondRotAxisAngle)
	ON_BN_CLICKED(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION, OnSelchangeRadioProvideTransAxisDirection)
	ON_BN_CLICKED(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE, OnSelchangeRadioProvideThirdRotAxisAngle)
	ON_BN_CLICKED(IDC_RDN_STANDARD_TRANS_AXIS_DIRECTION, OnSelchangeRadioStandardTransAxisDirection)
	ON_BN_CLICKED(IDC_RDN_CUSTOM_TRANS_AXIS_DIRECTION, OnSelchangeRadioCustomTransAxisDirection)
	ON_BN_CLICKED(IDC_CHK_FORCE_HEAD_OR_TABLE_ROTATION, OnChkForceHeadOrTableRotation)
	ON_BN_CLICKED(IDC_CHK_LIMIT_LINEAR_AXIS_TRAVEL, OnChkLimitLinearAxisTravel)
	ON_BN_CLICKED(IDC_CHK_FEED_RATE_INTERPOLATION_DISTANCE, OnChkInterpolationForDistance)
	ON_BN_CLICKED(IDC_CHK_RAPID_RATE_INTERPOLATION_DISTANCE, OnChkRapidRateInterpolationForDistance)
	ON_BN_CLICKED(IDC_CHK_FEED_RATE_INTERPOLATION_ANGLE, OnChkInterpolationForAngle)
	ON_BN_CLICKED(IDC_CHK_RAPID_RATE_INTERPOLATION_ANGLE, OnChkRapidRateInterpolationForAngle)
	ON_CBN_SELCHANGE(IDC_CMB_SOLUTIONS_FOR_START_ANGLE, OnSelchangeComboSolutionForStartAngle)
	ON_CBN_SELCHANGE(IDC_CMB_POLE_HANDLING, OnSelchangeComboPoleHandling)
	ON_CBN_SELCHANGE(IDC_CMB_MACHINE_LIMITS, OnSelchangeComboMachineLimmits)
	ON_CBN_SELCHANGE(IDC_CMB_FIRST_ROT_AXIS_ANGLE_LIMIT, OnSelchangeComboFirstRotAxisAngleLim)
	ON_CBN_SELCHANGE(IDC_CMB_SECOND_ROT_AXIS_ANGLE_LIMIT2, OnSelchangeComboSecondRotAxisAngleLim)
	ON_CBN_SELCHANGE(IDC_CMB_THIRD_ROT_AXIS_ANGLE_LIMIT, OnSelchangeComboThirdRotAxisAngleLim)
	ON_BN_CLICKED(IDC_RDN_DEFAULT_SOLUTION, OnSelchangeRadioDefault)
	ON_BN_CLICKED(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE, OnSelchangeRadioProvideStartRotationAngle)
	ON_BN_CLICKED(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS, OnSelchangeRadioProvideTransAxisDirection4AX)
	ON_BN_CLICKED(IDC_CHK_TOOLPATH_ALIGNMENT, &mwMxpDlg::OnBnClickedChkToolpathAlignment)
	ON_BN_CLICKED(IDC_CHK_RETRACT_AND_REWIND, OnChkRetractAndRewind)
	ON_BN_CLICKED(IDC_CHK_ENABLE_MXP, OnChkEnableMXP)
	ON_BN_CLICKED(IDC_CHK_FILTER_DUP_MOVES, OnChkFilterDuplicateMoves)
	ON_BN_CLICKED(IDC_CHK_DISABLE_MXP_ADD_MOVES, OnChkDisableMXPAddingAdditionalMoves)
	ON_CBN_SELCHANGE(IDC_CMB_SOLUTIONS_FOR_START_TRANS, OnSelchangeComboProvideStartSol)
	ON_BN_CLICKED(IDC_BTN_RETRACT_AND_REWIND_PARAM_DLG, OnClickButtonRetractAndRewindParamDlg)
END_MESSAGE_MAP()

// mwMxpDlg message handlers

void mwMxpDlg::OnSelchangeRadioAutoAnglePair()
{
	m_AutoAnglePair = TRUE;
	SelectStartAngleTypeDependingOnAutoAngle();
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioKeepAnglePair()
{
	m_AutoAnglePair = FALSE;
	SelectStartAngleTypeDependingOnAutoAngle();
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioNameSolution() {
	m_startAngleType = 0;
	((CButton*)GetDlgItem(IDC_RDN_NAME_SOLUTION))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_RDN_DEFAULT_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION))->SetCheck(BST_UNCHECKED);
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioProvideFirstRotAxisAngle() {
	m_startAngleType = 1;
	((CButton*)GetDlgItem(IDC_RDN_NAME_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_DEFAULT_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION))->SetCheck(BST_UNCHECKED);
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioProvideSecondRotAxisAngle() {
	m_startAngleType = 2;
	((CButton*)GetDlgItem(IDC_RDN_NAME_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_DEFAULT_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION))->SetCheck(BST_UNCHECKED);
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioProvideTransAxisDirection() {
	m_startAngleType = 3;
	((CButton*)GetDlgItem(IDC_RDN_NAME_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_DEFAULT_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION))->SetCheck(BST_CHECKED);
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioProvideThirdRotAxisAngle() {
	m_startAngleType = 4;
	((CButton*)GetDlgItem(IDC_RDN_NAME_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_DEFAULT_SOLUTION))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->SetCheck(BST_CHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION))->SetCheck(BST_UNCHECKED);
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioStandardTransAxisDirection() {
	m_customTransAxisDir = FALSE;
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioCustomTransAxisDirection() {
	m_customTransAxisDir = TRUE;
	UpdateControls();
}

void mwMxpDlg::OnChkForceHeadOrTableRotation()
{
	m_forceHeadOrTableRotation = !m_forceHeadOrTableRotation;
	SelectStartAngleTypeDependingOnForceHeadOrTable();
	UpdateControls();
}

void mwMxpDlg::OnChkLimitLinearAxisTravel()
{
	m_limitLinearAxisTravel = !m_limitLinearAxisTravel;
	UpdateControls();
}

void mwMxpDlg::OnChkInterpolationForDistance()
{
	m_feedRateInterDistFlag = !m_feedRateInterDistFlag;
	UpdateControls();
}

void mwMxpDlg::OnChkRapidRateInterpolationForDistance()
{
	m_rapidRateInterDistFlag = !m_rapidRateInterDistFlag;
	UpdateControls();
}

void mwMxpDlg::OnChkInterpolationForAngle()
{
	m_feedRateInterAngleFlag = !m_feedRateInterAngleFlag;
	UpdateControls();
}

void mwMxpDlg::OnChkRapidRateInterpolationForAngle()
{
	m_rapidRateInterAngleFlag = !m_rapidRateInterAngleFlag;
	UpdateControls();
}

void mwMxpDlg::OnChkAlignToolpath()
{
	m_AlignToolpath = !m_AlignToolpath;
	UpdateControls();
}

void mwMxpDlg::OnChkFilterDuplicateMoves()
{
	m_filterDuplicateMoves = !m_filterDuplicateMoves;
	UpdateControls();
}

void mwMxpDlg::OnChkDisableMXPAddingAdditionalMoves()
{
	m_disableAddingAdditionalMoves = !m_disableAddingAdditionalMoves;
	CComboBox * poleHandling = (CComboBox *)GetDlgItem(IDC_CMB_POLE_HANDLING);
	poleHandling->SetCurSel(post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE);
	m_poleHandling = poleHandling->GetCurSel();
	UpdateControls();
}

post::mwPostDefinitionPtr& mwMxpDlg::GetCurrentPostDefinition()
{
	post::mwPostDefinitionContainer::id id = m_pMxpParams->GetPostSettingID();
	return m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(id);
}

void mwMxpDlg::GetParams()
{
	try
	{


		if (m_pMxpParams->GetPostSettingEditing() == false)
		{
			//MPSEDitor case
			GetDlgItem(IDC_CHK_ENABLE_MXP)->ShowWindow(TRUE);
		}
		else
		{
			//SI and Mastercam case
			GetDlgItem(IDC_CHK_ENABLE_MXP)->ShowWindow(FALSE);
		}

		if (GetCurrentPostDefinition()->GetMxpParamPtr().IsNull())
		{
			GetCurrentPostDefinition()->SetMxpParamPtr(new post::mwMXPParam(GetCurrentPostDefinition()->GetMachDefPtr()->GetUnits()));

			m_enableMXP = FALSE;
		}
		else
		{
			m_enableMXP = TRUE;
		}

		post::mwMXPParam mxpParam(GetCurrentPostDefinition()->GetMachDefPtr()->GetUnits());
		if (m_resetParams == false)
			mxpParam = *(GetCurrentPostDefinition()->GetMxpParamPtr());

		m_AngleChangeLimit = mxpParam.GetAngleChangeLimit();
		m_AutoAnglePair = mxpParam.GetAngleSelectAutoFrom2Pairs();
		m_PoleAngleTolInDegree = mxpParam.GetPoleAngleTolInDeg();
		m_MachineLimits = mxpParam.GetMachineLimits();
		ConvertMachineLimitsFor3Axis();
		m_AngleTolInDegForUsingMachLimits = mxpParam.GetAngleTolInDegForUsingMachLimits();
		m_startAngleType = ConvertStartAngleType(mxpParam.GetStartAngleType());
		m_provideTransAxis = ConvertSolutionForStartTrans(mxpParam.GetSolutionForStartTranslation(), m_customTransAxisDir);
		m_solutionForStartAngle = ConvertSolutionForStartAngle(
			mxpParam.GetSolutionForStartAngle(),
			mxpParam.IsFirstSolutionCloserTo0Degree(),
			mxpParam.GetAngleSelectAutoFrom2Pairs(),
			mxpParam.GetAngleSelectOtherPair());
		ConvertPoleHandling(mxpParam.GetPoleHandling(), m_poleHandling, m_forceHeadOrTableRotation);
		m_preferredStartAngleValDeg = mxpParam.GetPreferredStartAngleValInDeg();
		GetPreferredStartAngleValByRotAxisAngle();
		m_AlignToolpath = mxpParam.GetToolpathAlignment();

		m_feedRateInterAngStep = mxpParam.GetInterpolationStep();
		m_rapidRateInterAngStep = mxpParam.GetRapidRateInterpolationStep();
		m_feedRateInterDist = mxpParam.GetInterpolationDist();
		m_rapidRateInterDist = mxpParam.GetRapidRateInterpolationDist();

		m_feedRateInterDistFlag = mxpParam.GetInterpolationDistFlag();
		m_rapidRateInterDistFlag = mxpParam.GetRapidRateInterpolationDistFlag();
		m_feedRateInterAngleFlag = mxpParam.GetInterpolationStepFlag();
		m_rapidRateInterAngleFlag = mxpParam.GetRapidRateInterpolationStepFlag();

		m_interpolationType = ConvertInterpolationType(mxpParam.GetInterpolation());

		GetRotAxisAngleLimit(m_pMxpParams->Get5axControlDef());

		m_provideStartRotationAngle = mxpParam.GetStartRotationAngle();
		if (m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS)
		{
			m_provideStartRotationAngle = FALSE;
		}
		else if (m_provideStartRotationAngle)
		{
			m_startAngleType = mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE;
		}

		m_retractAndRewindFlag = mxpParam.GetRetractAndRewindFlag();

		m_filterDuplicateMoves = mxpParam.ShouldFilterDuplicateMoves();
		m_disableAddingAdditionalMoves = m_pMxpParams->GetDisableAddingAdditionalMoves();

		m_limitLinearAxisTravel = mxpParam.GetLimitLinearAxisTravel();

		const cadcam::mwPoint3d& customTransAxisDirValues = mxpParam.GetCustomTransAxisDirValues();
		m_customTransAxisDirX = customTransAxisDirValues.x();
		m_customTransAxisDirY = customTransAxisDirValues.y();
		m_customTransAxisDirZ = customTransAxisDirValues.z();

		if (!m_enableMXP)
		{
			GetCurrentPostDefinition()->SetMxpParamPtr(MW_NULL);
		}
	}
	catch (post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMxpDlg::GetParams"));
	}
}

void mwMxpDlg::SetParams()
{
	try
	{
		if (m_enableMXP)
		{
			if (GetCurrentPostDefinition()->GetMxpParamPtr().IsNull())
			{
				GetCurrentPostDefinition()->SetMxpParamPtr(new post::mwMXPParam(GetCurrentPostDefinition()->GetMachDefPtr()->GetUnits()));
			}

			post::mwMXPParamPtr& mxpParamPtr = GetCurrentPostDefinition()->GetMxpParamPtr();
			mxpParamPtr->SetAngleChangeLimit(m_AngleChangeLimit);
			mxpParamPtr->SetInterpolationStep(m_feedRateInterAngStep);
			mxpParamPtr->SetRapidRateInterpolationStep(m_rapidRateInterAngStep);
			mxpParamPtr->SetInterpolationDist(m_feedRateInterDist);
			mxpParamPtr->SetRapidRateInterpolationDist(m_rapidRateInterDist);
			mxpParamPtr->SetAngleSelectAutoFrom2Pairs(m_AutoAnglePair != FALSE);
			mxpParamPtr->SetInterpolationDistFlag(m_feedRateInterDistFlag != FALSE);
			mxpParamPtr->SetRapidRateInterpolationDistFlag(m_rapidRateInterDistFlag != FALSE);
			mxpParamPtr->SetInterpolationStepFlag(m_feedRateInterAngleFlag != FALSE);
			mxpParamPtr->SetRapidRateInterpolationStepFlag(m_rapidRateInterAngleFlag != FALSE);
			mxpParamPtr->SetPoleAngleTolInDeg(m_PoleAngleTolInDegree);
			mxpParamPtr->SetMachineLimits(ConvertMachineLimits(m_MachineLimits));
			mxpParamPtr->SetAngleTolInDegForUsingMachLimits(m_AngleTolInDegForUsingMachLimits);
			mxpParamPtr->SetStartAngleType(ConvertStartAngleType(m_startAngleType));
			mxpParamPtr->SetSolutionForStartTranslation(ConvertSolutionForStartTrans(m_provideTransAxis, m_customTransAxisDir));
			ConvertSolutionForStartAngle(m_solutionForStartAngle, m_AutoAnglePair != FALSE, mxpParamPtr);
			SetPreferredStartAngleValByRotAxisAngle();
			mxpParamPtr->SetPreferredStartAngleValInDeg(m_preferredStartAngleValDeg);
			mxpParamPtr->SetPoleHandling(ConvertPoleHandling(m_poleHandling, m_forceHeadOrTableRotation));
			mxpParamPtr->SetToolpathAlignment(m_AlignToolpath != FALSE);
			mxpParamPtr->SetInterpolation(ConvertInterpolationType(m_interpolationType));

			m_pMxpParams->Set5axControlDef(SetRotAxisAngleLimit());

			mxpParamPtr->SetStartRotationAngle(m_provideStartRotationAngle != FALSE);

			mxpParamPtr->SetRetractAndRewindFlag(m_retractAndRewindFlag != FALSE);

			mxpParamPtr->SetFilterDuplicateMoves(m_filterDuplicateMoves != FALSE);
			m_pMxpParams->SetDisableAddingAdditionalMoves(m_disableAddingAdditionalMoves != FALSE);
			mxpParamPtr->SetLimitLinearAxisTravel(m_limitLinearAxisTravel != FALSE);
			const cadcam::mwPoint3d customTransAxisDirValues(m_customTransAxisDirX, m_customTransAxisDirY, m_customTransAxisDirZ);
			mxpParamPtr->SetCustomTransAxisDirValues(customTransAxisDirValues);
		}
		else
		{
			GetCurrentPostDefinition()->GetMxpParamPtr() = MW_NULL;//mxp params is disabled....
		}

	}
	catch (post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMxpDlg::SetParams"));
	}
}

void mwMxpDlg::SetTexts()
{
	SetCntrlText(IDC_LBL_ANGLE_CHANGE_LIMIT, MSG_LBL_ANGLE_CHANGE_LIMIT);

	SetCntrlText(IDC_RDN_AUTO_ANGLE_PAIR, MSG_RDN_AUTO_ANGLE_PAIR);
	SetCntrlText(IDC_RDN_KEEP_ANGLE_PAIR, MSG_RDN_KEEP_ANGLE_PAIR);

	SetCntrlText(IDC_RDN_NAME_SOLUTION, MSG_RDN_NAME_SOLUTION);
	SetCntrlText(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE, MSG_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE);
	SetCntrlText(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE, MSG_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE);
	SetCntrlText(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION, MSG_RDN_PROVIDE_TRANS_AXIS_DIRECTION);
	SetCntrlText(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE, MSG_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE);

	SetCntrlText(IDC_RDN_STANDARD_TRANS_AXIS_DIRECTION, MSG_RDN_STANDARD_TRANS_AXIS_DIRECTION);
	SetCntrlText(IDC_RDN_CUSTOM_TRANS_AXIS_DIRECTION, MSG_RDN_CUSTOM_TRANS_AXIS_DIRECTION);

	SetCntrlText(IDC_CHK_FORCE_HEAD_OR_TABLE_ROTATION, MSG_CHK_FORCE_HEAD_OR_TABLE_ROTATION);
	SetCntrlText(IDC_CHK_LIMIT_LINEAR_AXIS_TRAVEL, MSG_CHK_LIMIT_LINEAR_AXIS_TRAVEL);

	SetCntrlText(IDC_CHK_FEED_RATE_INTERPOLATION_DISTANCE, MSG_CHK_FEED_RATE_INTERPOLATION_FOR_DISTANCE);
	SetCntrlText(IDC_CHK_RAPID_RATE_INTERPOLATION_DISTANCE, MSG_CHK_RAPID_RATE_INTERPOLATION_FOR_DISTANCE);
	SetCntrlText(IDC_CHK_FEED_RATE_INTERPOLATION_ANGLE, MSG_CHK_FEED_RATE_INTERPOLATION_FOR_ANGLE);
	SetCntrlText(IDC_CHK_RAPID_RATE_INTERPOLATION_ANGLE, MSG_CHK_RAPID_RATE_INTERPOLATION_FOR_ANGLE);

	SetCntrlText(IDC_LBL_POL_ANGLE_TOL, MSG_LBL_POLE_BEHAVIOR);
	SetCntrlText(IDC_LBL_POL_ANGLE_TOL, MSG_LBL_POL_ANGLE_TOL);
	SetCntrlText(IDC_LBL_ANGLE_TOL_MACH_LIM, MSG_LBL_ANGLE_TOL_MACH_LIM);
	SetCntrlText(IDC_LBL_LIMITS_TO_RESPECT, MSG_LBL_LIMITS_TO_RESPECT);

	SetCntrlText(IDC_CHK_FILTER_DUP_MOVES, MSG_CHK_FILTER_DUP_MOVES);
	SetCntrlText(IDC_CHK_DISABLE_MXP_ADD_MOVES, MSG_CHK_DISABLE_MXP_ADD_MOVES);
	SetCntrlText(IDC_CHK_TOOLPATH_ALIGNMENT, MSG_CHK_TOOLPATH_ALIGNMENT);

	SetCntrlText(IDC_STATIC_ANGLE_PAIR, MSG_IDC_STATIC_ANGLE_PAIR);
	SetCntrlText(IDC_STATIC_MACHINE_LIMITS, MSG_IDC_STATIC_MACHINE_LIMITS);
	SetCntrlText(IDC_STATIC_POLE_HANDLING, MSG_IDC_STATIC_POLE_HANDLING);
	SetCntrlText(IDC_STATIC_POINT_INTERPOLATION, MSG_IDC_STATIC_POINT_INTERPOLATION);
	SetCntrlText(IDC_STATIC_MOVE_LIST_WRITER, MSG_IDC_STATIC_MOVE_LIST_WRITER);
	SetCntrlText(IDC_STATIC_TOOL_REPOSITIONING, MSG_IDC_STATIC_TOOL_REPOSITIONING);

	SetCntrlText(IDC_LBL_SELECT_SOLUTION_4AX, MSG_LBL_SELECT_SOLUTION_4AX);
	SetCntrlText(IDC_LBL_SELECT_SOLUTION_5AX, MSG_LBL_SELECT_SOLUTION_5AX);
	SetCntrlText(IDC_LBL_SELECT_SOLUTION_6AXCS, MSG_LBL_SELECT_SOLUTION_6AXCS);

	SetCntrlText(IDC_LBL_CUSTOM_TRANS_AXIS_X, MSG_LBL_CUSTOM_TRANS_AXIS_X);
	SetCntrlText(IDC_LBL_CUSTOM_TRANS_AXIS_Y, MSG_LBL_CUSTOM_TRANS_AXIS_Y);
	SetCntrlText(IDC_LBL_CUSTOM_TRANS_AXIS_Z, MSG_LBL_CUSTOM_TRANS_AXIS_Z);

	SetCntrlText(IDC_STATIC_INTERPOLATION_FEED_RATE, MSG_LBL_INTERPOLATION_FEED_RATE);
	SetCntrlText(IDC_STATIC_INTERPOLATION_RAPID_RATE, MSG_LBL_INTERPOLATION_RAPID_RATE);

	SetCntrlText(IDC_LBL_MISC, MSG_LBL_MISC);

	SetCntrlText(IDC_CHK_RETRACT_AND_REWIND, MSG_IDC_CHK_RETRACT_AND_REWIND);

	CComboBox * solForStartAngleCombo = (CComboBox *)GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_ANGLE);
	solForStartAngleCombo->ResetContent();
	solForStartAngleCombo->AddString(GetText(MSG_CMB_SOLUTION_FOR_START_ANGLE_FIRST_SOLUTION).c_str());
	solForStartAngleCombo->AddString(GetText(MSG_CMB_SOLUTION_FOR_START_ANGLE_OTHER_SOLUTION).c_str());
	solForStartAngleCombo->AddString(GetText(MSG_CMB_SOLUTION_FOR_START_ANGLE_CLOSEST_TO_ZERO).c_str());
	solForStartAngleCombo->AddString(GetText(MSG_CMB_SOLUTION_FOR_START_ANGLE_NOT_CLOSEST_TO_ZERO).c_str());

	SetPoleHandlingComboBox();

	//MachineLimits combo
	CComboBox * machLimits = (CComboBox *)GetDlgItem(IDC_CMB_MACHINE_LIMITS);
	machLimits->ResetContent();
	machLimits->AddString(GetText(MSG_CMB_NONE).c_str());
	machLimits->AddString(GetText(MSG_CMB_TRANSLATION).c_str());

	if (m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDefPtr()->GetNumberOfAxes() != post::mwMachDef::MACHINE_3AXIS)
	{
		machLimits->AddString(GetText(MSG_CMB_ROTATION).c_str());
		machLimits->AddString(GetText(MSG_CMB_ALL).c_str());
	}

	SetCntrlText(IDC_LBL_FIRST_ROT_AXIS_ANGLE_LIMIT, MSG_LBL_FIRST_ROT_AXIS_ANG_LIM);
	SetCntrlText(IDC_LBL_SECOND_ROT_AXIS_ANGLE_LIMIT, MSG_LBL_SECOND_ROT_AXIS_ANG_LIM);
	SetCntrlText(IDC_LBL_THIRD_ROT_AXIS_ANGLE_LIMIT, MSG_LBL_THIRD_ROT_AXIS_ANG_LIM);

	((CComboBox *)GetDlgItem(IDC_CMB_FIRST_ROT_AXIS_ANGLE_LIMIT))->ResetContent();
	((CComboBox *)GetDlgItem(IDC_CMB_SECOND_ROT_AXIS_ANGLE_LIMIT2))->ResetContent();
	((CComboBox *)GetDlgItem(IDC_CMB_THIRD_ROT_AXIS_ANGLE_LIMIT))->ResetContent();

	SetCombosSameText(MSG_LBL_CMB_LIM_NO_LIMIT);
	SetCombosSameText(MSG_LBL_CMB_LIM_0_AND_360);
	SetCombosSameText(MSG_LBL_CMB_LIM_MINUS_PLUS_180);

	SetCntrlText(IDC_RDN_DEFAULT_SOLUTION, MSG_RDN_DEFAULT_SOLUTION);
	SetCntrlText(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE, MSG_RDN_PROVIDE_START_ROTATION_ANGLE);

	SetCntrlText(IDC_LBL_INTERPOLATION_TYPE, MSG_LBL_INTERPOLATION_TYPE);
	//InterpolationType combo
	CComboBox * interpolationType = (CComboBox *)GetDlgItem(IDC_CMB_INTERPOLATION_TYPE);
	interpolationType->ResetContent();
	interpolationType->AddString(GetText(MSG_CMB_INTERPOLATION_TYPE_BYVECTOR).c_str());
	interpolationType->AddString(GetText(MSG_CMB_INTERPOLATION_TYPE_BYANGLE).c_str());
	interpolationType->AddString(GetText(MSG_CMB_INTERPOLATION_TYPE_AXIS).c_str());

	SetCntrlText(IDC_CHK_ENABLE_MXP, MSG_CHK_ENABLE_MXP);

	//SolutionForStartTranslation combo
	CComboBox * solForStartTrCombo = (CComboBox *)GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_TRANS);
	solForStartTrCombo->ResetContent();
	solForStartTrCombo->AddString(GetText(MSK_CMB_PROVIDE_TR1_PLUS).c_str());
	solForStartTrCombo->AddString(GetText(MSK_CMB_PROVIDE_TR1_MINUS).c_str());
	solForStartTrCombo->AddString(GetText(MSK_CMB_PROVIDE_TR2_PLUS).c_str());
	solForStartTrCombo->AddString(GetText(MSK_CMB_PROVIDE_TR2_MINUS).c_str());
	solForStartTrCombo->AddString(GetText(MSK_CMB_PROVIDE_TR3_PLUS).c_str());
	solForStartTrCombo->AddString(GetText(MSK_CMB_PROVIDE_TR3_MINUS).c_str());

	SetCntrlText(IDC_BTN_RETRACT_AND_REWIND_PARAM_DLG, MSG_BTN_RETRACT_AND_REWIND_PARAM_DLG);
}

void mwMxpDlg::Reset()
{
	m_resetParams = true;
	UpdateData(FALSE);
	m_resetParams = false;
}

void mwMxpDlg::UpdateControls()
{
	post::mwMachDef::NumbersOfAxes axes = m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDefPtr()->GetNumberOfAxes();

	EnableOrDisable(axes);
	SetVisibility(axes);
}

void mwMxpDlg::OnSelchangeComboFirstRotAxisAngleLim()
{
	m_firstRotAxisAngleLimit = ((CComboBox *)GetDlgItem(IDC_CMB_FIRST_ROT_AXIS_ANGLE_LIMIT))->GetCurSel();
	UpdateControls();
}

void mwMxpDlg::OnSelchangeComboSecondRotAxisAngleLim()
{
	m_secondRotAxisAngleLimit = ((CComboBox *)GetDlgItem(IDC_CMB_SECOND_ROT_AXIS_ANGLE_LIMIT2))->GetCurSel();
	UpdateControls();
}

void mwMxpDlg::OnSelchangeComboThirdRotAxisAngleLim()
{
	m_thirdRotAxisAngleLimit = ((CComboBox *)GetDlgItem(IDC_CMB_THIRD_ROT_AXIS_ANGLE_LIMIT))->GetCurSel();
	UpdateControls();
}

void mwMxpDlg::SetCombosSameText(const int viTextID)
{
	CComboBox* firstRotAxisAngleLimCombo = (CComboBox *)GetDlgItem(IDC_CMB_FIRST_ROT_AXIS_ANGLE_LIMIT);
	CComboBox* secondRotAxisAngleLimCombo = (CComboBox *)GetDlgItem(IDC_CMB_SECOND_ROT_AXIS_ANGLE_LIMIT2);
	CComboBox* thirdRotAxisAngleLimCombo = (CComboBox *)GetDlgItem(IDC_CMB_THIRD_ROT_AXIS_ANGLE_LIMIT);

	firstRotAxisAngleLimCombo->AddString(GetText(viTextID).c_str());
	secondRotAxisAngleLimCombo->AddString(GetText(viTextID).c_str());
	thirdRotAxisAngleLimCombo->AddString(GetText(viTextID).c_str());
}

void mwMxpDlg::OnSelchangeComboSolutionForStartAngle()
{
	CComboBox * solForStartAngleCombo = (CComboBox *)GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_ANGLE);
	m_solutionForStartAngle = solForStartAngleCombo->GetCurSel();
	UpdateControls();
}

void mwMxpDlg::OnSelchangeComboPoleHandling()
{
	CComboBox * poleHandling = (CComboBox *)GetDlgItem(IDC_CMB_POLE_HANDLING);
	m_poleHandling = poleHandling->GetCurSel();
	UpdateControls();
}

void mwMxpDlg::OnSelchangeComboMachineLimmits()
{
	CComboBox * machLimits = (CComboBox *)GetDlgItem(IDC_CMB_MACHINE_LIMITS);
	m_MachineLimits = machLimits->GetCurSel();
	UpdateControls();
}

void mwMxpDlg::OnSelchangeComboProvideStartSol()
{
	CComboBox * solForStartTransCombo = (CComboBox *)GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_TRANS);
	m_provideTransAxis = solForStartTransCombo->GetCurSel();
	UpdateControls();
}

int mwMxpDlg::ConvertStartAngleType(const post::mwMXPParam::StartAngleType& startAngleType)
{
	switch (startAngleType)
	{
	case post::mwMXPParam::ANGTYPE_SEL_BETW_TWO_SOLUTIONS:
		return mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS;
	case post::mwMXPParam::ANGTYPE_USE_FIRST_ROT_ANGLE:
		return mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE;
	case post::mwMXPParam::ANGTYPE_USE_SECOND_ROT_ANGLE:
		return mwMxpDlg::MXP_USE_SECOND_ROT_ANGLE;
	case post::mwMXPParam::TRANSTYPE_PROVIDE_AXIS:
		return mwMxpDlg::MXP_PROVIDE_TRANS_AXIS;
	case post::mwMXPParam::ANGTYPE_USE_THIRD_ROT_ANGLE:
		return mwMxpDlg::MXP_USE_THIRD_ROT_ANGLE;
	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_START_ANGLE_TYPE,
			_T("mwMxpDlg::ConvertStartAngleType"));
	}
}

int mwMxpDlg::ConvertSolutionForStartAngle(
	const post::mwMXPParam::SolutionForStartAngle& solForStartAngle,
	const bool isCloseToZero,
	const bool automaticAnglePair,
	const bool otherAnglePair)
{
	if (automaticAnglePair)
	{
		if (solForStartAngle == post::mwMXPParam::SSA_USE_FIRST_SOLUTION && isCloseToZero == false)
		{
			return mwMxpDlg::MXP_USE_FIRST_SOLUTION;
		}
		else if (solForStartAngle == post::mwMXPParam::SSA_USE_OTHER_SOLUTION && isCloseToZero == false)
		{
			return mwMxpDlg::MXP_USE_OTHER_SOLUTION;
		}
		else if (solForStartAngle == post::mwMXPParam::SSA_USE_FIRST_SOLUTION && isCloseToZero == true)
		{
			return mwMxpDlg::MXP_SOLUTION_CLOSE_TO_0;
		}
		else if (solForStartAngle == post::mwMXPParam::SSA_USE_OTHER_SOLUTION && isCloseToZero == true)
		{
			return mwMxpDlg::MXP_SOLUTION_NOT_CLOSE_TO_0;
		}
		else
		{
			throw mwMxpuiException(mwMxpuiException::UNKNOWN_SOLUTION_FOR_START_ANGLE, _T("mwMxpDlg::ConvertSolutionForStartAngle"));
		}
	}
	else
	{
		if (otherAnglePair == false && isCloseToZero == false)
		{
			return mwMxpDlg::MXP_USE_FIRST_SOLUTION;
		}
		else if (otherAnglePair == true && isCloseToZero == false)
		{
			return mwMxpDlg::MXP_USE_OTHER_SOLUTION;
		}
		else if (otherAnglePair == false && isCloseToZero == true)
		{
			return mwMxpDlg::MXP_SOLUTION_CLOSE_TO_0;
		}
		else if (otherAnglePair == true && isCloseToZero == true)
		{
			return mwMxpDlg::MXP_SOLUTION_NOT_CLOSE_TO_0;
		}
		else
		{
			throw mwMxpuiException(mwMxpuiException::UNKNOWN_SOLUTION_FOR_START_ANGLE, _T("mwMxpDlg::ConvertSolutionForStartAngle"));
		}
	}
}

post::mwMXPParam::StartAngleType mwMxpDlg::ConvertStartAngleType(const int startAngleType)
{
	switch (startAngleType)
	{
	case mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS:
		return post::mwMXPParam::ANGTYPE_SEL_BETW_TWO_SOLUTIONS;
	case mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE:
		return post::mwMXPParam::ANGTYPE_USE_FIRST_ROT_ANGLE;
	case mwMxpDlg::MXP_USE_SECOND_ROT_ANGLE:
		return post::mwMXPParam::ANGTYPE_USE_SECOND_ROT_ANGLE;
	case mwMxpDlg::MXP_PROVIDE_TRANS_AXIS:
		return post::mwMXPParam::TRANSTYPE_PROVIDE_AXIS;
	case mwMxpDlg::MXP_USE_THIRD_ROT_ANGLE:
		return post::mwMXPParam::ANGTYPE_USE_THIRD_ROT_ANGLE;
	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_START_ANGLE_TYPE,
			_T("mwMxpDlg::ConvertStartAngleType"));
	}
}

void mwMxpDlg::ConvertSolutionForStartAngle(
	const int solForStartAngle,
	const bool automaticAnglePair,
	post::mwMXPParamPtr& params)
{
	if (automaticAnglePair)
	{
		switch (solForStartAngle)
		{
		case mwMxpDlg::MXP_USE_FIRST_SOLUTION:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_FIRST_SOLUTION);
			params->SetFirstSolutionCloserTo0Degree(false);
		}break;
		case mwMxpDlg::MXP_USE_OTHER_SOLUTION:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_OTHER_SOLUTION);
			params->SetFirstSolutionCloserTo0Degree(false);
		}break;
		case mwMxpDlg::MXP_SOLUTION_CLOSE_TO_0:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_FIRST_SOLUTION);
			params->SetFirstSolutionCloserTo0Degree(true);
		}break;
		case mwMxpDlg::MXP_SOLUTION_NOT_CLOSE_TO_0:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_OTHER_SOLUTION);
			params->SetFirstSolutionCloserTo0Degree(true);
		}break;
		default:
			throw mwMxpuiException(mwMxpuiException::UNKNOWN_SOLUTION_FOR_START_ANGLE,
				_T("mwMxpDlg::ConvertSolutionForStartAngle"));
		}
	}
	else
	{
		switch (solForStartAngle)
		{
		case mwMxpDlg::MXP_USE_FIRST_SOLUTION:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_FIRST_SOLUTION);
			params->SetAngleSelectOtherPair(false);
			params->SetFirstSolutionCloserTo0Degree(false);
		}break;
		case mwMxpDlg::MXP_USE_OTHER_SOLUTION:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_OTHER_SOLUTION);
			params->SetAngleSelectOtherPair(true);
			params->SetFirstSolutionCloserTo0Degree(false);
		}break;
		case mwMxpDlg::MXP_SOLUTION_CLOSE_TO_0:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_FIRST_SOLUTION);
			params->SetAngleSelectOtherPair(false);
			params->SetFirstSolutionCloserTo0Degree(true);
		}break;
		case mwMxpDlg::MXP_SOLUTION_NOT_CLOSE_TO_0:
		{
			params->SetSolutionForStartAngle(post::mwMXPParam::SSA_USE_OTHER_SOLUTION);
			params->SetAngleSelectOtherPair(true);
			params->SetFirstSolutionCloserTo0Degree(true);
		}break;
		default:
			throw mwMxpuiException(mwMxpuiException::UNKNOWN_SOLUTION_FOR_START_ANGLE,
				_T("mwMxpDlg::ConvertSolutionForStartAngle"));
		}
	}
}

post::mwMXPParam::PoleHandling mwMxpDlg::ConvertPoleHandling(const int poleHandling, const BOOL forceHeadOrTableRotation)
{

	switch (m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDefPtr()->GetNumberOfAxes())
	{
	case post::mwMachDef::MACHINE_3AXIS:
	{
		return post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE;
	}
	case post::mwMachDef::MACHINE_4AXIS:
	{
		if (forceHeadOrTableRotation)
		{
			return post::mwMXPParam::FORCE_HEAD_AND_TABLE_ROTATION;
		}
		else
		{
			return post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE;
		}
	}
	case post::mwMachDef::MACHINE_5AXIS:
	case post::mwMachDef::MACHINE_6AXIS://5+1
	{
		if (forceHeadOrTableRotation)
		{
			return post::mwMXPParam::FORCE_HEAD_AND_TABLE_ROTATION;
		}
		else
		{
			switch (poleHandling)
			{
			case mwMxpDlg::FREEZE_ANGLE_AROUND_POLE:
				return post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE;
			case mwMxpDlg::USE_ROTATION_ANGLE_AROUND_POLE:
				return post::mwMXPParam::USE_ROTATION_ANGLE_AROUND_POLE;
			case mwMxpDlg::LINEAR_INTERPOLATION:
				return post::mwMXPParam::LINEAR_INTERPOLATION;
			case mwMxpDlg::SMOOTH_INTERPOLATION:
				return post::mwMXPParam::SMOOTH_INTERPOLATION;
			default:
				throw mwMxpuiException(mwMxpuiException::UNKNOWN_POLE_HANDLING,
					_T("mwMxpDlg::ConvertPoleHandling"));
			}
		}
	}
	case post::mwMachDef::MACHINE_6AXIS_CONTOUR:
	{
		if (forceHeadOrTableRotation)
		{
			return post::mwMXPParam::FORCE_HEAD_AND_TABLE_ROTATION;
		}
		else
		{
			return post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE;
		}
	}
	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_POLE_HANDLING,
			_T("mwMxpDlg::ConvertPoleHandling"));
	}

}

void mwMxpDlg::ConvertPoleHandling(
	const post::mwMXPParam::PoleHandling poleHandling,
	int& poleHandlingDlg,
	BOOL& forceHeadOrTableRotation)
{
	if (poleHandling < 0 || poleHandling > 4)
	{
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_POLE_HANDLING, _T("mwMxpDlg::ConvertPoleHandling"));
	}

	switch (m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDefPtr()->GetNumberOfAxes())
	{
	case post::mwMachDef::MACHINE_3AXIS:
	case post::mwMachDef::MACHINE_6AXIS_CONTOUR:
	{
		poleHandlingDlg = post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE;
		forceHeadOrTableRotation = FALSE;
	}
	case post::mwMachDef::MACHINE_4AXIS:
	{
		switch (poleHandling)
		{
		case post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE:
		{
			poleHandlingDlg = mwMxpDlg::FREEZE_ANGLE_AROUND_POLE;
			forceHeadOrTableRotation = FALSE;
		}break;
		case post::mwMXPParam::USE_ROTATION_ANGLE_AROUND_POLE:
		case post::mwMXPParam::LINEAR_INTERPOLATION:
		case post::mwMXPParam::SMOOTH_INTERPOLATION:
		{
			poleHandlingDlg = mwMxpDlg::FREEZE_ANGLE_AROUND_POLE;
			forceHeadOrTableRotation = FALSE;
		}break;
		case post::mwMXPParam::FORCE_HEAD_AND_TABLE_ROTATION:
		{
			poleHandlingDlg = post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE;
			forceHeadOrTableRotation = TRUE;
		}break;
		}
	}
	case post::mwMachDef::MACHINE_5AXIS:
	case post::mwMachDef::MACHINE_6AXIS://5+1
		switch (poleHandling)
		{
		case post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE:
		{
			poleHandlingDlg = mwMxpDlg::FREEZE_ANGLE_AROUND_POLE;
			forceHeadOrTableRotation = FALSE;
		}break;
		case post::mwMXPParam::USE_ROTATION_ANGLE_AROUND_POLE:
		{
			poleHandlingDlg = mwMxpDlg::USE_ROTATION_ANGLE_AROUND_POLE;
			forceHeadOrTableRotation = FALSE;
		}break;
		case post::mwMXPParam::LINEAR_INTERPOLATION:
		{
			poleHandlingDlg = mwMxpDlg::LINEAR_INTERPOLATION;
			forceHeadOrTableRotation = FALSE;
		}break;
		case post::mwMXPParam::SMOOTH_INTERPOLATION:
		{
			poleHandlingDlg = mwMxpDlg::SMOOTH_INTERPOLATION;
			forceHeadOrTableRotation = FALSE;
		}break;
		case post::mwMXPParam::FORCE_HEAD_AND_TABLE_ROTATION:
		{
			poleHandlingDlg = mwMxpDlg::FREEZE_ANGLE_AROUND_POLE;
			forceHeadOrTableRotation = TRUE;
		}break;
		}
	}
}

post::mwMXPParam::MachineLimits mwMxpDlg::ConvertMachineLimits(const int machineLimits)
{
	switch (machineLimits)
	{
	case mwMxpDlg::NO_LIMITS:
		return post::mwMXPParam::NO_LIMITS;
	case mwMxpDlg::TRANSLATIONAL_LIMITS:
		return post::mwMXPParam::TRANSLATIONAL_LIMITS;
	case mwMxpDlg::ROTATIONAL_LIMITS:
		return post::mwMXPParam::ROTATIONAL_LIMITS;
	case mwMxpDlg::ALL_LIMITS:
		return post::mwMXPParam::ALL_LIMITS;
	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_MACHINE_LIMITS,
			_T("mwMxpDlg::ConvertMachineLimits"));
	}
}

post::mwMXPParam::Interpolation mwMxpDlg::ConvertInterpolationType(const int vType)
{
	switch (vType)
	{
	case  mwMxpDlg::INTERPOLATION_VECTORS:
		return post::mwMXPParam::INTERPOLATION_VECTORS;

	case  mwMxpDlg::INTERPOLATION_ANGLES:
		return post::mwMXPParam::INTERPOLATION_ANGLES;

	case  mwMxpDlg::INTERPOLATION_AXIS_VALUES:
		return post::mwMXPParam::INTERPOLATION_AXIS_VALUES;

	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_INTERPOLATION_TYPE,
			_T("mwMxpDlg::ConvertInterpolationType"));
	}
}

int mwMxpDlg::ConvertInterpolationType(const post::mwMXPParam::Interpolation& vType)
{
	switch (vType)
	{
	case  post::mwMXPParam::INTERPOLATION_VECTORS:
		return mwMxpDlg::INTERPOLATION_VECTORS;

	case  post::mwMXPParam::INTERPOLATION_ANGLES:
		return mwMxpDlg::INTERPOLATION_ANGLES;

	case  post::mwMXPParam::INTERPOLATION_AXIS_VALUES:
		return mwMxpDlg::INTERPOLATION_AXIS_VALUES;

	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_INTERPOLATION_TYPE,
			_T("mwMxpDlg::ConvertInterpolationType"));
	}
}

post::mwMXPParam::SolutionForStartTranslation mwMxpDlg::ConvertSolutionForStartTrans(const int solForStartTrans, const BOOL customTransAxisDir)
{
	if (customTransAxisDir)
		return post::mwMXPParam::SST_USE_CUSTOM;

	switch (solForStartTrans)
	{
	case  mwMxpDlg::SST_USE_FIRST_POSITIVE:
		return post::mwMXPParam::SST_USE_FIRST_POSITIVE;

	case  mwMxpDlg::SST_USE_FIRST_NEGATIVE:
		return post::mwMXPParam::SST_USE_FIRST_NEGATIVE;

	case mwMxpDlg::SST_USE_SECOND_POSITIVE:
		return post::mwMXPParam::SST_USE_SECOND_POSITIVE;

	case  mwMxpDlg::SST_USE_SECOND_NEGATIVE:
		return post::mwMXPParam::SST_USE_SECOND_NEGATIVE;

	case  mwMxpDlg::SST_USE_THIRD_POSITIVE:
		return post::mwMXPParam::SST_USE_THIRD_POSITIVE;

	case mwMxpDlg::SST_USE_THIRD_NEGATIVE:
		return post::mwMXPParam::SST_USE_THIRD_NEGATIVE;

	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_TRANSALTION_AXIS_TYPE,
			_T("mwMxpDlg::ConvertSolutionForStartTrans"));
	}
}

const int mwMxpDlg::ConvertSolutionForStartTrans(const post::mwMXPParam::SolutionForStartTranslation solForStartTrans, BOOL& customTransAxisDir)
{
	switch (solForStartTrans)
	{
	case  post::mwMXPParam::SST_USE_FIRST_POSITIVE:
		return mwMxpDlg::SST_USE_FIRST_POSITIVE;

	case  post::mwMXPParam::SST_USE_FIRST_NEGATIVE:
		return mwMxpDlg::SST_USE_FIRST_NEGATIVE;

	case post::mwMXPParam::SST_USE_SECOND_POSITIVE:
		return mwMxpDlg::SST_USE_SECOND_POSITIVE;

	case  post::mwMXPParam::SST_USE_SECOND_NEGATIVE:
		return mwMxpDlg::SST_USE_SECOND_NEGATIVE;

	case  post::mwMXPParam::SST_USE_THIRD_POSITIVE:
		return mwMxpDlg::SST_USE_THIRD_POSITIVE;

	case post::mwMXPParam::SST_USE_THIRD_NEGATIVE:
		return mwMxpDlg::SST_USE_THIRD_NEGATIVE;

	case post::mwMXPParam::SST_USE_CUSTOM:
		customTransAxisDir = TRUE;
		return mwMxpDlg::SST_USE_FIRST_POSITIVE;

	default:
		throw mwMxpuiException(mwMxpuiException::UNKNOWN_TRANSALTION_AXIS_TYPE,
			_T("mwMxpDlg::ConvertSolutionForStartTrans"));
	}
}

void mwMxpDlg::OnBnClickedChkToolpathAlignment()
{
	// TODO: Add your control notification handler code here
}

void mwMxpDlg::OnChkRetractAndRewind()
{
	m_retractAndRewindFlag = !m_retractAndRewindFlag;
	UpdateControls();
}

void mwMxpDlg::SetPoleHandlingComboBox()
{
	CComboBox * poleHandling = (CComboBox *)GetDlgItem(IDC_CMB_POLE_HANDLING);
	poleHandling->ResetContent();
	poleHandling->AddString(GetText(MSG_CMB_FREEZE_ANGLE_AROUND_POLE).c_str());

	post::mwMachDef::NumbersOfAxes axes = m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDefPtr()->GetNumberOfAxes();

	switch (axes)
	{
	case post::mwMachDef::MACHINE_3AXIS:
	case post::mwMachDef::MACHINE_4AXIS:
	case post::mwMachDef::MACHINE_6AXIS_CONTOUR:
		break;
	case post::mwMachDef::MACHINE_5AXIS:
	case post::mwMachDef::MACHINE_6AXIS:
		poleHandling->AddString(GetText(MSG_CMB_USE_ROTATION_ANGLE_AROUND_POLE).c_str());
		poleHandling->AddString(GetText(MSG_CMB_LINEAR_INTERPOLATION).c_str());
		poleHandling->AddString(GetText(MSG_CMB_SMOOTH_INTERPOLATION).c_str());
		break;
	}
	poleHandling->SetCurSel(post::mwMXPParam::FREEZE_ANGLE_AROUND_POLE);
}

void mwMxpDlg::OnSelchangeRadioDefault()
{
	m_provideStartRotationAngle = FALSE;
	m_startAngleType = mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS;
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS))->SetCheck(BST_UNCHECKED);
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioProvideStartRotationAngle()
{
	m_provideStartRotationAngle = TRUE;
	m_startAngleType = mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE;
	((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS))->SetCheck(BST_UNCHECKED);
	UpdateControls();
}

void mwMxpDlg::OnSelchangeRadioProvideTransAxisDirection4AX()
{
	m_provideStartRotationAngle = FALSE;
	m_startAngleType = mwMxpDlg::MXP_PROVIDE_TRANS_AXIS;
	UpdateControls();
}

void mwMxpDlg::SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams)
{
	mwMxpuiDlgsParams::SetParams(pMxpParams);
}

void mwMxpDlg::OnChkEnableMXP()
{
	m_enableMXP = !m_enableMXP;
	EnableAllChildControls(m_enableMXP);
}

void mwMxpDlg::EnableAllChildControls(BOOL enable)
{
	CWnd* wnd = CWnd::FromHandle(*this);
	CWnd* pwndChild = wnd->GetWindow(GW_CHILD);
	while (pwndChild)
	{
		pwndChild->EnableWindow(enable);

		pwndChild = pwndChild->GetNextWindow();
	};

	if (m_pMxpParams->GetPostSettingEditing() == false)
	{
		//MPSEDitor case
		GetDlgItem(IDC_CHK_ENABLE_MXP)->EnableWindow(TRUE);
	}
	else
	{
		//SI and Mastercam case
		GetDlgItem(IDC_CHK_ENABLE_MXP)->EnableWindow(FALSE);
	}
	if (enable)
	{
		UpdateControls();
	}
}

void mwMxpDlg::InitToolTip()
{
#ifndef MCAM_CONFIG
	m_mxpDlgToolTip = new CToolTipCtrl;
	if (!m_mxpDlgToolTip->Create(this))
	{
		TRACE0("Unable to create the ToolTip!");
	}
	else
	{
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_AUTO_ANGLE_PAIR), _T("auto_angle_pair"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_ANGLE_CHANGE_LIMIT), _T("angle_change_limit"));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_FEED_RATE_INTERPOLATION_ANGLE), _T("interpolation_step"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_RAPID_RATE_INTERPOLATION_ANGLE), _T("interpolation_rapid_rate_step"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_FEED_RATE_INTERPOLATION_DISTANCE), _T("interpolation_dist"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_RAPID_RATE_INTERPOLATION_DISTANCE), _T("interpolation_rapid_rate_dist"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_FEED_RATE_INTERPOLATION_DISTANCE), _T("interpolation_dist_enable"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_RAPID_RATE_INTERPOLATION_DISTANCE), _T("interpolation_rapid_rate_dist_enable"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_FEED_RATE_INTERPOLATION_ANGLE), _T("interpolation_step_enable"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_RAPID_RATE_INTERPOLATION_ANGLE), _T("interpolation_rapid_rate_step_enable"));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_POL_ANGLE_TOL), _T("pole_angle_tol_degree"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_ANGLE_TOL_MACH_LIM), _T("angle_tol_degree_for_machine_limits"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CMB_MACHINE_LIMITS), _T("machine_limits"));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_NAME_SOLUTION), _T("start_angle_type=\"between_two_solutions\""));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE), _T("start_angle_type=\"first_solution\""));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE), _T("start_angle_type=\"second_solution\""));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION), _T("start_angle_type=\"provide_translation\""));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE), _T("start_angle_type=\"third_solution\""));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_DEFAULT_SOLUTION), _T("start_rotation_angle_enable=\"false\""));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE), _T("start_rotation_angle_enable=\"true\""));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS), _T("start_angle_type=\"provide_translation\""));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_ANGLE), _T("solution_for_start_angle && first_solution_close_to_0_enable"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_FIRST_ROTATION_AXIS_ANGLE), _T("preferred_start_angle_val_degree"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_SECOND_ROTATION_AXIS_ANGLE), _T("preferred_start_angle_val_degree"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_EDT_THIRD_ROTATION_AXIS_ANGLE), _T("preferred_start_angle_val_degree"));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CMB_POLE_HANDLING), _T("pole_handling"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_TOOLPATH_ALIGNMENT), _T("toolpath_alignment_enable"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CMB_INTERPOLATION_TYPE), _T("interpolation_type"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_FILTER_DUP_MOVES), _T("filter_duplicate_moves"));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_RETRACT_AND_REWIND), _T("retract_and_rewind_enable"));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_TRANS), _T("solution_for_start_translation"));
		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_FORCE_HEAD_OR_TABLE_ROTATION), _T("pole_handling=\"force_head_and_table_rotation\""));

		m_mxpDlgToolTip->AddTool(&*GetDlgItem(IDC_CHK_LIMIT_LINEAR_AXIS_TRAVEL), _T("limit_linear_axis_travel"));

		m_mxpDlgToolTip->Activate(TRUE);
	}
#else
	;
#endif

}

void mwMxpDlg::SelectStartAngleTypeDependingOnForceHeadOrTable()
{
	if (m_forceHeadOrTableRotation == FALSE)
	{
		if (((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION))->GetCheck() == BST_CHECKED)
		{
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION))->SetCheck(BST_UNCHECKED);
			((CButton*)GetDlgItem(IDC_RDN_NAME_SOLUTION))->SetCheck(BST_CHECKED);
			m_startAngleType = mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS;
		}

		if (((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS))->GetCheck() == BST_CHECKED)
		{
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS))->SetCheck(BST_UNCHECKED);
			((CButton*)GetDlgItem(IDC_RDN_DEFAULT_SOLUTION))->SetCheck(BST_CHECKED);
			m_startAngleType = mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS;
		}
	}
}

void mwMxpDlg::SelectStartAngleTypeDependingOnAutoAngle()
{
	if (m_AutoAnglePair == FALSE)
	{
		if (((CButton*)GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE))->GetCheck() == BST_CHECKED ||
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE))->GetCheck() == BST_CHECKED ||
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->GetCheck() == BST_CHECKED)
		{
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);

			((CButton*)GetDlgItem(IDC_RDN_NAME_SOLUTION))->SetCheck(BST_CHECKED);
		}

		if (((CButton*)GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE))->GetCheck() == BST_CHECKED)
		{
			((CButton*)GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE))->SetCheck(BST_UNCHECKED);

			((CButton*)GetDlgItem(IDC_RDN_DEFAULT_SOLUTION))->SetCheck(BST_CHECKED);
		}
	}
}

void mwMxpDlg::OnClickButtonRetractAndRewindParamDlg()
{
	post::mwMXPParamPtr& mxpParamPtr = GetCurrentPostDefinition()->GetMxpParamPtr();
	if (mxpParamPtr.IsNull())
	{
		mxpParamPtr = new post::mwMXPParam(GetCurrentPostDefinition()->GetMachDefPtr()->GetUnits());
	}
	mwRetractAndRewindDlg dlg(mxpParamPtr, m_pMxpParams, GetInteractor(), this);

	if (dlg.DoModal() == IDOK)
	{
		// pass
	}
}
//#############################################################################
const bool mwMxpDlg::GetRetractToolAtMax() 
{
	post::mwMXPParamPtr& mxpParamPtr = GetCurrentPostDefinition()->GetMxpParamPtr();
	return mxpParamPtr->GetRetractToolAtMax();
}
//#############################################################################
void mwMxpDlg::SetRetractToolAtMax(const bool retractToolAtMax)
{
	post::mwMXPParamPtr& mxpParamPtr = GetCurrentPostDefinition()->GetMxpParamPtr();
	mxpParamPtr->SetRetractToolAtMax(retractToolAtMax);
}
//#############################################################################

void mwMxpDlg::HideComboMachineLimits()
{
	GetDlgItem(IDC_CMB_MACHINE_LIMITS)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_LBL_LIMITS_TO_RESPECT)->ShowWindow(SW_HIDE);
}

void mwMxpDlg::HideAngleControlLimits()
{
	m_hideAngleControlLimits = true;
}

void mwMxpDlg::EnableOrDisable(const post::mwMachDef::NumbersOfAxes& axes)
{
	GetDlgItem(IDC_RDN_DEFAULT_SOLUTION)->EnableWindow(axes == post::mwMachDef::MACHINE_4AXIS);
	GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE)->EnableWindow(axes == post::mwMachDef::MACHINE_4AXIS);
	GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS)->EnableWindow(m_forceHeadOrTableRotation && axes == post::mwMachDef::MACHINE_4AXIS);

	GetDlgItem(IDC_RDN_AUTO_ANGLE_PAIR)->EnableWindow(axes == post::mwMachDef::MACHINE_5AXIS || axes == post::mwMachDef::MACHINE_6AXIS || axes == post::mwMachDef::MACHINE_6AXIS_CONTOUR);
	GetDlgItem(IDC_RDN_KEEP_ANGLE_PAIR)->EnableWindow(axes == post::mwMachDef::MACHINE_5AXIS || axes == post::mwMachDef::MACHINE_6AXIS || axes == post::mwMachDef::MACHINE_6AXIS_CONTOUR);

	GetDlgItem(IDC_CHK_LIMIT_LINEAR_AXIS_TRAVEL)->EnableWindow(m_forceHeadOrTableRotation);

	GetDlgItem(IDC_LBL_SELECT_SOLUTION_4AX)->EnableWindow(!(axes == post::mwMachDef::MACHINE_3AXIS));
	GetDlgItem(IDC_LBL_SELECT_SOLUTION_5AX)->EnableWindow(!(axes == post::mwMachDef::MACHINE_3AXIS));
	GetDlgItem(IDC_LBL_SELECT_SOLUTION_6AXCS)->EnableWindow(!(axes == post::mwMachDef::MACHINE_3AXIS));

	GetDlgItem(IDC_RDN_NAME_SOLUTION)->EnableWindow(!(axes == post::mwMachDef::MACHINE_3AXIS));
	GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE)->EnableWindow(m_AutoAnglePair && !(axes == post::mwMachDef::MACHINE_3AXIS));
	GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE)->EnableWindow(m_AutoAnglePair && !(axes == post::mwMachDef::MACHINE_3AXIS));
	GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION)->EnableWindow(m_forceHeadOrTableRotation && !(axes == post::mwMachDef::MACHINE_3AXIS));
	GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE)->EnableWindow(m_AutoAnglePair && (axes == post::mwMachDef::MACHINE_6AXIS_CONTOUR));

	GetDlgItem(IDC_RDN_STANDARD_TRANS_AXIS_DIRECTION)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS));
	GetDlgItem(IDC_RDN_CUSTOM_TRANS_AXIS_DIRECTION)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS));

	GetDlgItem(IDC_LBL_CUSTOM_TRANS_AXIS_X)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS) && m_customTransAxisDir);
	GetDlgItem(IDC_LBL_CUSTOM_TRANS_AXIS_Y)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS) && m_customTransAxisDir);
	GetDlgItem(IDC_LBL_CUSTOM_TRANS_AXIS_Z)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS) && m_customTransAxisDir);
	GetDlgItem(IDC_EDT_CUSTOM_TRANS_AXIS_X)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS) && m_customTransAxisDir);
	GetDlgItem(IDC_EDT_CUSTOM_TRANS_AXIS_Y)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS) && m_customTransAxisDir);
	GetDlgItem(IDC_EDT_CUSTOM_TRANS_AXIS_Z)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS) && m_customTransAxisDir);

	GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_ANGLE)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS && !(axes == post::mwMachDef::MACHINE_3AXIS || axes == post::mwMachDef::MACHINE_4AXIS));

	GetDlgItem(IDC_EDT_FIRST_ROTATION_AXIS_ANGLE)->EnableWindow((m_AutoAnglePair && m_startAngleType == mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE) || (axes == post::mwMachDef::MACHINE_4AXIS && m_provideStartRotationAngle));
	GetDlgItem(IDC_EDT_SECOND_ROTATION_AXIS_ANGLE)->EnableWindow((m_AutoAnglePair && m_startAngleType == mwMxpDlg::MXP_USE_SECOND_ROT_ANGLE) || (axes == post::mwMachDef::MACHINE_4AXIS && m_provideStartRotationAngle));
	GetDlgItem(IDC_EDT_THIRD_ROTATION_AXIS_ANGLE)->EnableWindow((m_AutoAnglePair && m_startAngleType == mwMxpDlg::MXP_USE_THIRD_ROT_ANGLE));

	GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_TRANS)->EnableWindow(m_startAngleType == mwMxpDlg::MXP_PROVIDE_TRANS_AXIS && !(axes == post::mwMachDef::MACHINE_3AXIS) && !(m_customTransAxisDir));
	GetDlgItem(IDC_LBL_ANGLE_TOL_MACH_LIM)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && (m_MachineLimits == mwMxpDlg::ROTATIONAL_LIMITS || m_MachineLimits == mwMxpDlg::ALL_LIMITS));
	GetDlgItem(IDC_EDT_ANGLE_TOL_MACH_LIM)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && (m_MachineLimits == mwMxpDlg::ROTATIONAL_LIMITS || m_MachineLimits == mwMxpDlg::ALL_LIMITS));
	GetDlgItem(IDC_LBL_ANGLE_CHANGE_LIMIT)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && !m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_EDT_ANGLE_CHANGE_LIMIT)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && !m_disableAddingAdditionalMoves);

	GetDlgItem(IDC_CMB_POLE_HANDLING)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && axes != post::mwMachDef::MACHINE_6AXIS_CONTOUR && !m_forceHeadOrTableRotation && !m_disableAddingAdditionalMoves);

	GetDlgItem(IDC_EDT_POL_ANGLE_TOL)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && axes != post::mwMachDef::MACHINE_6AXIS_CONTOUR && !m_forceHeadOrTableRotation);

	GetDlgItem(IDC_LBL_INTERPOLATION_TYPE)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS);
	GetDlgItem(IDC_CMB_INTERPOLATION_TYPE)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS);

	GetDlgItem(IDC_CHK_FEED_RATE_INTERPOLATION_DISTANCE)->EnableWindow(!m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_EDT_FEED_RATE_INTERPOLATION_DISTANCE)->EnableWindow(m_feedRateInterDistFlag && !m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_CHK_RAPID_RATE_INTERPOLATION_DISTANCE)->EnableWindow(!m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_EDT_RAPID_RATE_INTERPOLATION_DISTANCE)->EnableWindow(m_rapidRateInterDistFlag && !m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_CHK_FEED_RATE_INTERPOLATION_ANGLE)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && !m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_EDT_FEED_RATE_INTERPOLATION_ANGLE)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && m_feedRateInterAngleFlag && !m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_CHK_RAPID_RATE_INTERPOLATION_ANGLE)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && !m_disableAddingAdditionalMoves);
	GetDlgItem(IDC_EDT_RAPID_RATE_INTERPOLATION_ANGLE)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS && m_rapidRateInterAngleFlag && !m_disableAddingAdditionalMoves);

	const BOOL angleControlCheck = m_firstRotAxisAngleLimit == CD_NO_LIMIT || m_secondRotAxisAngleLimit == CD_NO_LIMIT || m_thirdRotAxisAngleLimit == CD_NO_LIMIT;
	const BOOL machineLimitsCheck = m_MachineLimits == ROTATIONAL_LIMITS || m_MachineLimits == ALL_LIMITS;
	GetDlgItem(IDC_CHK_RETRACT_AND_REWIND)->EnableWindow(angleControlCheck && machineLimitsCheck && axes != post::mwMachDef::MACHINE_3AXIS && !m_disableAddingAdditionalMoves);

	GetDlgItem(IDC_CHK_TOOLPATH_ALIGNMENT)->EnableWindow(axes == post::mwMachDef::MACHINE_3AXIS || axes == post::mwMachDef::MACHINE_4AXIS);

	GetDlgItem(IDC_LBL_FIRST_ROT_AXIS_ANGLE_LIMIT)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS);
	GetDlgItem(IDC_CMB_FIRST_ROT_AXIS_ANGLE_LIMIT)->EnableWindow(axes != post::mwMachDef::MACHINE_3AXIS);

	GetDlgItem(IDC_LBL_SECOND_ROT_AXIS_ANGLE_LIMIT)->EnableWindow(!(axes == post::mwMachDef::MACHINE_3AXIS || axes == post::mwMachDef::MACHINE_4AXIS));
	GetDlgItem(IDC_CMB_SECOND_ROT_AXIS_ANGLE_LIMIT2)->EnableWindow(!(axes == post::mwMachDef::MACHINE_3AXIS || axes == post::mwMachDef::MACHINE_4AXIS));

	GetDlgItem(IDC_LBL_THIRD_ROT_AXIS_ANGLE_LIMIT)->EnableWindow(axes == post::mwMachDef::MACHINE_6AXIS_CONTOUR);
	GetDlgItem(IDC_CMB_THIRD_ROT_AXIS_ANGLE_LIMIT)->EnableWindow(axes == post::mwMachDef::MACHINE_6AXIS_CONTOUR);

	GetDlgItem(IDC_BTN_RETRACT_AND_REWIND_PARAM_DLG)->EnableWindow(m_retractAndRewindFlag && angleControlCheck && machineLimitsCheck && axes != post::mwMachDef::MACHINE_3AXIS && !m_disableAddingAdditionalMoves);
}

void mwMxpDlg::SetVisibility(const post::mwMachDef::NumbersOfAxes& axes)
{
	if (m_hideAngleControlLimits)
	{
		GetDlgItem(IDC_STATIC_MOVE_LIST_WRITER)->ShowWindow(FALSE);
		GetDlgItem(IDC_LBL_FIRST_ROT_AXIS_ANGLE_LIMIT)->ShowWindow(FALSE);
		GetDlgItem(IDC_CMB_FIRST_ROT_AXIS_ANGLE_LIMIT)->ShowWindow(FALSE);
		GetDlgItem(IDC_LBL_SECOND_ROT_AXIS_ANGLE_LIMIT)->ShowWindow(FALSE);
		GetDlgItem(IDC_CMB_SECOND_ROT_AXIS_ANGLE_LIMIT2)->ShowWindow(FALSE);
		GetDlgItem(IDC_LBL_THIRD_ROT_AXIS_ANGLE_LIMIT)->ShowWindow(FALSE);
		GetDlgItem(IDC_CMB_THIRD_ROT_AXIS_ANGLE_LIMIT)->ShowWindow(FALSE);
	}
	// axis = 0 -> 3_axis_machine
	// axis = 1 -> 4_axis_machine
	// axis = 2 -> 5_axis_machine
	// axis = 3 -> 6_axis_machine
	// axis = 4 -> 6_axis_chain_saw_machine

	GetDlgItem(IDC_RDN_AUTO_ANGLE_PAIR)->ShowWindow(axes == 0 || axes == 1 ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_RDN_KEEP_ANGLE_PAIR)->ShowWindow(axes == 0 || axes == 1 ? SW_HIDE : SW_SHOW);

	GetDlgItem(IDC_CHK_LIMIT_LINEAR_AXIS_TRAVEL)->ShowWindow(axes == 1 || axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_CHK_FORCE_HEAD_OR_TABLE_ROTATION)->ShowWindow(axes == 0 ? SW_HIDE : SW_SHOW);

	GetDlgItem(IDC_LBL_SELECT_SOLUTION_4AX)->ShowWindow(axes == 1 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_LBL_SELECT_SOLUTION_5AX)->ShowWindow(axes == 2 || axes == 3 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_LBL_SELECT_SOLUTION_6AXCS)->ShowWindow(axes == 4 ? SW_SHOW : SW_HIDE);


	GetDlgItem(IDC_RDN_NAME_SOLUTION)->ShowWindow(axes == 0 || axes == 1 ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE)->ShowWindow(axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_RDN_STANDARD_TRANS_AXIS_DIRECTION)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_RDN_CUSTOM_TRANS_AXIS_DIRECTION)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_LBL_CUSTOM_TRANS_AXIS_X)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_LBL_CUSTOM_TRANS_AXIS_Y)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_LBL_CUSTOM_TRANS_AXIS_Z)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_CUSTOM_TRANS_AXIS_X)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_CUSTOM_TRANS_AXIS_Y)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_CUSTOM_TRANS_AXIS_Z)->ShowWindow(axes != 0 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_RDN_DEFAULT_SOLUTION)->ShowWindow(axes == 1 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_RDN_PROVIDE_ROT_AXIS_ANGLE)->ShowWindow(axes == 1 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_RDN_PROVIDE_TRANS_AXIS_DIRECTION_4AXIS)->ShowWindow(axes == 1 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_ANGLE)->ShowWindow(axes == 0 || axes == 1 ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_EDT_FIRST_ROTATION_AXIS_ANGLE)->ShowWindow(axes == 1 || axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_SECOND_ROTATION_AXIS_ANGLE)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_CMB_SOLUTIONS_FOR_START_TRANS)->ShowWindow(axes == 1 || axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_THIRD_ROTATION_AXIS_ANGLE)->ShowWindow(axes == 4 ? SW_SHOW : SW_HIDE);

	// GetDlgItem(IDC_BTN_RETRACT_AND_REWIND_PARAM_DLG)->ShowWindow()
}

void mwMxpDlg::GetRotAxisAngleLimit(const post::mw5axControlDef& axControlDef)
{
	switch (axControlDef.GetAngleLimitForFirstRotAxis())
	{
	case post::mw5axControlDef::NO_LIMIT:
		m_firstRotAxisAngleLimit = mwMxpDlg::CD_NO_LIMIT;
		break;
	case post::mw5axControlDef::LIM_BETW_0_AND_360_DEG:
		m_firstRotAxisAngleLimit = mwMxpDlg::CD_LIM_BETW_0_AND_360_DEG;
		break;
	case post::mw5axControlDef::LIM_BETW_MINUS_180_AND_PLUS_180_DEG:
		m_firstRotAxisAngleLimit = mwMxpDlg::CD_LIM_BETW_MINUS_180_AND_PLUS_180_DEG;
		break;
	};

	switch (axControlDef.GetAngleLimitForSecondRotAxis())
	{
	case post::mw5axControlDef::NO_LIMIT:
		m_secondRotAxisAngleLimit = mwMxpDlg::CD_NO_LIMIT;
		break;
	case post::mw5axControlDef::LIM_BETW_0_AND_360_DEG:
		m_secondRotAxisAngleLimit = mwMxpDlg::CD_LIM_BETW_0_AND_360_DEG;
		break;
	case post::mw5axControlDef::LIM_BETW_MINUS_180_AND_PLUS_180_DEG:
		m_secondRotAxisAngleLimit = mwMxpDlg::CD_LIM_BETW_MINUS_180_AND_PLUS_180_DEG;
		break;
	};

	switch (axControlDef.GetAngleLimitForThirdRotAxis())
	{
	case post::mw5axControlDef::NO_LIMIT:
		m_thirdRotAxisAngleLimit = mwMxpDlg::CD_NO_LIMIT;
		break;
	case post::mw5axControlDef::LIM_BETW_0_AND_360_DEG:
		m_thirdRotAxisAngleLimit = mwMxpDlg::CD_LIM_BETW_0_AND_360_DEG;
		break;
	case post::mw5axControlDef::LIM_BETW_MINUS_180_AND_PLUS_180_DEG:
		m_thirdRotAxisAngleLimit = mwMxpDlg::CD_LIM_BETW_MINUS_180_AND_PLUS_180_DEG;
		break;
	};
}

const post::mw5axControlDef mwMxpDlg::SetRotAxisAngleLimit()
{
	post::mw5axControlDef controlDef;
	switch (m_firstRotAxisAngleLimit)
	{
	case mwMxpDlg::CD_NO_LIMIT:
		controlDef.SetAngleLimitForFirstRotAxis(post::mw5axControlDef::NO_LIMIT);
		break;
	case mwMxpDlg::CD_LIM_BETW_0_AND_360_DEG:
		controlDef.SetAngleLimitForFirstRotAxis(post::mw5axControlDef::LIM_BETW_0_AND_360_DEG);
		break;
	case mwMxpDlg::CD_LIM_BETW_MINUS_180_AND_PLUS_180_DEG:
		controlDef.SetAngleLimitForFirstRotAxis(post::mw5axControlDef::LIM_BETW_MINUS_180_AND_PLUS_180_DEG);
		break;
	};

	switch (m_secondRotAxisAngleLimit)
	{
	case mwMxpDlg::CD_NO_LIMIT:
		controlDef.SetAngleLimitForSecondRotAxis(post::mw5axControlDef::NO_LIMIT);
		break;
	case mwMxpDlg::CD_LIM_BETW_0_AND_360_DEG:
		controlDef.SetAngleLimitForSecondRotAxis(post::mw5axControlDef::LIM_BETW_0_AND_360_DEG);
		break;
	case mwMxpDlg::CD_LIM_BETW_MINUS_180_AND_PLUS_180_DEG:
		controlDef.SetAngleLimitForSecondRotAxis(post::mw5axControlDef::LIM_BETW_MINUS_180_AND_PLUS_180_DEG);
		break;
	};

	switch (m_thirdRotAxisAngleLimit)
	{
	case mwMxpDlg::CD_NO_LIMIT:
		controlDef.SetAngleLimitForThirdRotAxis(post::mw5axControlDef::NO_LIMIT);
		break;
	case mwMxpDlg::CD_LIM_BETW_0_AND_360_DEG:
		controlDef.SetAngleLimitForThirdRotAxis(post::mw5axControlDef::LIM_BETW_0_AND_360_DEG);
		break;
	case mwMxpDlg::CD_LIM_BETW_MINUS_180_AND_PLUS_180_DEG:
		controlDef.SetAngleLimitForThirdRotAxis(post::mw5axControlDef::LIM_BETW_MINUS_180_AND_PLUS_180_DEG);
		break;
	};

	return controlDef;
}

void mwMxpDlg::SetPreferredStartAngleValByRotAxisAngle() {
	switch (m_startAngleType)
	{
	case mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE:
		m_preferredStartAngleValDeg = m_firstStartRotationAxisAngle;
		break;
	case mwMxpDlg::MXP_USE_SECOND_ROT_ANGLE:
		m_preferredStartAngleValDeg = m_secondStartRotationAxisAngle;
		break;
	case mwMxpDlg::MXP_USE_THIRD_ROT_ANGLE:
		m_preferredStartAngleValDeg = m_thirdStartRotationAxisAngle;
		break;
	case mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS:
	case mwMxpDlg::MXP_PROVIDE_TRANS_AXIS:
		break;
	}
}

void mwMxpDlg::GetPreferredStartAngleValByRotAxisAngle() {
	switch (m_startAngleType)
	{
	case mwMxpDlg::MXP_USE_FIRST_ROT_ANGLE:
	{
		m_firstStartRotationAxisAngle = m_preferredStartAngleValDeg;
		m_secondStartRotationAxisAngle = 0.0;
		m_thirdStartRotationAxisAngle = 0.0;
	}break;
	case mwMxpDlg::MXP_USE_SECOND_ROT_ANGLE:
	{
		m_firstStartRotationAxisAngle = 0.0;
		m_secondStartRotationAxisAngle = m_preferredStartAngleValDeg;
		m_thirdStartRotationAxisAngle = 0.0;
	}break;
	case mwMxpDlg::MXP_USE_THIRD_ROT_ANGLE:
	{
		m_firstStartRotationAxisAngle = 0.0;
		m_secondStartRotationAxisAngle = 0.0;
		m_thirdStartRotationAxisAngle = m_preferredStartAngleValDeg;
	}break;
	case mwMxpDlg::MXP_SEL_BETW_TWO_SOLUTIONS:
	case mwMxpDlg::MXP_PROVIDE_TRANS_AXIS:
		break;
	}
}

void mwMxpDlg::ConvertMachineLimitsFor3Axis()
{
	if (m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDefPtr()->GetNumberOfAxes() == post::mwMachDef::MACHINE_3AXIS)
	{
		if (m_MachineLimits == ROTATIONAL_LIMITS)
		{
			m_MachineLimits = NO_LIMITS;
		}
		else if (m_MachineLimits == ALL_LIMITS)
		{
			m_MachineLimits = TRANSLATIONAL_LIMITS;
		}
	}
}