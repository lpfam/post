/******************************************************************************
*               File: mwMxpuiDlg.cpp										  *
*******************************************************************************
*               Description: implements mwMxpuiDlg                            *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 12:15:43 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "resource.h"
#include "mwMxpuiDlg.hpp"
//#############################################################################
mwMxpuiDlg::mwMxpuiDlg(misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor &rInteractor,int vIDD, CWnd* pParent)
: CDialog(vIDD, pParent), mwMxpuiDlgsParams(pMxpParams,rInteractor)
{}
//#############################################################################
BOOL mwMxpuiDlg::PreTranslateMessage(MSG* pMsg) 
{
   // Check for enter key (return) press.
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
 		CDialog::NextDlgCtrl(); // Pass focus to next control
		return TRUE;			// Don't translate further
	}
	else
		return CDialog::PreTranslateMessage(pMsg);
}
