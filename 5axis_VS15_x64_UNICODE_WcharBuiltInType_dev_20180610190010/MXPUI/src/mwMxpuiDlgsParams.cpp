/******************************************************************************
*               File: mwMxpuiDlgsParams.cpp									  *
*******************************************************************************
*               Description: implements the mwMxpuiDlgsParams class			  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 11:34:21 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "mwMessages.hpp"
#include "mwMxpuiExceptionHandler.hpp"
#include "mwPostException.hpp"
#include "mwMxpuiDlgsParams.hpp"
#include "mwBMPUtils.h"
//#############################################################################
const misc::mwstring MSG_SECTION=TXT_SECTION;
//#############################################################################
mwMxpuiDlgsParams::mwMxpuiDlgsParams( misc::mwAutoPointer<mwMxpParams>& pMxpParams, const mwMxpuiParamInteractor &rInteractor):
m_pMxpParams(pMxpParams),
m_interactor(rInteractor)
{
}
//#############################################################################
void mwMxpuiDlgsParams::SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams)
{
	m_pMxpParams = pMxpParams; 
}
//#############################################################################
misc::mwAutoPointer<mwMxpParams>& mwMxpuiDlgsParams::GetMXPParams()
{
	return m_pMxpParams;
}
//#############################################################################
const mwMxpuiParamInteractor& mwMxpuiDlgsParams::GetInteractor() const
{
	return m_interactor;
}
//#############################################################################
const misc::mwstring mwMxpuiDlgsParams::GetText(const int vTextID) const
{
	return m_interactor.GetMsg(vTextID, MSG_SECTION);
}
//#############################################################################
void mwMxpuiDlgsParams::SetDlgParams(CDataExchange& rDX)
{
	try
	{
		SetParams();
	}
	catch(mwMxpuiException& eMXPUIException )
	{
		mwMxpuiExceptionHandler::HandleErrInDlg(eMXPUIException, m_interactor, rDX);
	}
	catch (...)
	{
		mwMxpuiExceptionHandler::HandleErrInDlg(rDX);
	}
}
//#############################################################################
void mwMxpuiDlgsParams::GetDlgParams(CDataExchange& rDX)
{
	try
	{
		GetParams();
	}
	catch (mwMxpuiException &e)
	{
		mwMxpuiExceptionHandler::HandleErrInDlg(e ,m_interactor, rDX);
	}
}
//#############################################################################
void mwMxpuiDlgsParams::PaintBMP(int vResourceID, CWnd& destWnd)
{
	CBitmap *bmp = CBitmap::FromHandle( mwBMPUtils::LoadTransparentBitmap3D( vResourceID ) );
	if( !bmp )
		AfxMessageBox( _T("Loading bitmap failed") );
	else	
		if( !mwBMPUtils::SetBitmap( destWnd, *bmp, mwBMPUtils::BMP_UTIL_STR_TO_FIT_AW ) )
		{
			DeleteObject( (HBITMAP)*bmp );
			AfxMessageBox( _T("Setting bitmap failed") );
		}
		else
			DeleteObject( (HBITMAP)*bmp );
}
//#############################################################################
void mwMxpuiDlgsParams::ValidateDlgParams()
{
	try
	{
		Validate();
	}
	catch (mwMxpuiException &e)
	{
		mwMxpuiExceptionHandler::HandleErr(e ,m_interactor);
	}
}
//#############################################################################
void mwMxpuiDlgsParams::Validate()
{
	//Nothing to do in parent class...
}
//#############################################################################

