//#############################################################################
#include "StdAfx.h"
#include "mwMXPParams.hpp"
//#############################################################################
mwMxpParams::mwMxpParams( 
	 const post::mwPostDefinitionContainer& rPostDefCont,
	 const post::mwPostDefinitionContainer::id rID,
	 const post::mw5axControlDef& rControlDef,
	 const mwMXPWriterParam& rWriter,
	 const double rToolLengthCompensation,
	 const mwTransfMatrixPtr& rTransfMatrixPtr,
	 const bool rDisablePostSettingEditing,
	 const bool rUseMastercamSettings,
	 const bool rDisableAddingAdditionalMoves)
		: m_postDefCont(rPostDefCont)
		, m_id(rID)
		, m_controlDef(rControlDef)
		, m_writer(rWriter)
		, m_toolLengthCompensation(rToolLengthCompensation)
		, m_transfMatrixPtr(rTransfMatrixPtr)
		, m_postSettingEditing(rDisablePostSettingEditing)
		, m_useMastercamSettings(rUseMastercamSettings)
		, m_disableAddingAdditionalMoves(rDisableAddingAdditionalMoves)
{
}
//#############################################################################
mwMxpParams::mwMxpParams(const mwMxpParams& mxpParam)
	: m_postDefCont(mxpParam.m_postDefCont)
	, m_id(mxpParam.m_id)
	, m_controlDef(mxpParam.m_controlDef)
	, m_writer(mxpParam.m_writer)
	, m_toolLengthCompensation(mxpParam.m_toolLengthCompensation)
	, m_transfMatrixPtr(mxpParam.m_transfMatrixPtr)
	, m_postSettingEditing(mxpParam.m_postSettingEditing)
	, m_useMastercamSettings(mxpParam.m_useMastercamSettings)
	, m_disableAddingAdditionalMoves(mxpParam.m_disableAddingAdditionalMoves)
{
}

//#############################################################################
misc::mwAutoPointer<mwMxpParams> mwMxpParams::Clone() const
{
	return new mwMxpParams(*this);
}

//#############################################################################
mwMxpParams& mwMxpParams::operator=( const mwMxpParams& right)
{
	m_postDefCont = right.m_postDefCont;
	m_id = right.m_id;
	m_controlDef = right.m_controlDef;
	m_writer = right.m_writer;
	m_toolLengthCompensation = right.m_toolLengthCompensation;
	m_transfMatrixPtr = right.m_transfMatrixPtr;
	m_useMastercamSettings = right.m_useMastercamSettings;
	m_postSettingEditing = right.m_postSettingEditing;
	m_disableAddingAdditionalMoves = right.m_disableAddingAdditionalMoves;

	return (*this);
}
//#############################################################################
void mwMxpParams::DefaultPostSettings()
{
	ASSERT(FALSE);
}
//#############################################################################
void mwMxpParams::DefaultMachineDefinition(const misc::mwstring& )
{
	ASSERT(FALSE);
}
//#############################################################################
void mwMxpParams::SetUnits(measures::mwUnitsFactory::Units )
{
	ASSERT(FALSE);
}
//#############################################################################
bool mwMxpParams::ReadMachineDefinition(
	const misc::mwstring& /*xmlFileName*/ ,
	const misc::mwstring& postSettingId,
	const post::mwStringPairVectPtr& adapters,
	const bool throwExceptions,
	const bool readAllpostSettingsId,
	const misc::mwstring& machSimResourceDllPath
	)
{
	UNREFERENCED_PARAMETER (postSettingId);
	UNREFERENCED_PARAMETER (adapters);
	UNREFERENCED_PARAMETER (throwExceptions);
	UNREFERENCED_PARAMETER (readAllpostSettingsId);
	UNREFERENCED_PARAMETER (machSimResourceDllPath);
	ASSERT(FALSE);
	return false;
}
//#############################################################################
post::mwPostDefinitionContainer& mwMxpParams::GetPostDefinitionContainer()
{
	return m_postDefCont;
}
//#############################################################################
const post::mwPostDefinitionContainer& mwMxpParams::GetPostDefinitionContainer() const
{
	return m_postDefCont;
}
//#############################################################################
void mwMxpParams::SetPostDefinitionContainer(const post::mwPostDefinitionContainer& postDefCont)
{
	m_postDefCont = postDefCont;
}
//#############################################################################
post::mw5axControlDef& mwMxpParams::Get5axControlDef()
{
	return m_controlDef;
}
//#############################################################################
const post::mw5axControlDef& mwMxpParams::Get5axControlDef() const
{
	return m_controlDef;
}
//#############################################################################
void mwMxpParams::Set5axControlDef(const post::mw5axControlDef& controlDef)
{
	m_controlDef = controlDef;
}
//#############################################################################
mwMXPWriterParam& mwMxpParams::GetMXPWriterParam()
{
	return m_writer;
}
//#############################################################################
const mwMXPWriterParam& mwMxpParams::GetMXPWriterParam() const
{
	return m_writer;
}
//#############################################################################
void mwMxpParams::SetMXPWriterParam(const mwMXPWriterParam& writer)
{
	m_writer = writer;
}
//#############################################################################
double& mwMxpParams::GetToolLengthCompensation()
{
	return m_toolLengthCompensation;
}
//#############################################################################
const double& mwMxpParams::GetToolLengthCompensation() const
{
	return m_toolLengthCompensation;
}
//#############################################################################
void mwMxpParams::SetToolLengthCompensation(const double toolLengthCompensation)
{
	m_toolLengthCompensation = toolLengthCompensation;
}//#############################################################################
bool& mwMxpParams::GetDisableAddingAdditionalMoves()
{
	return m_disableAddingAdditionalMoves;
}
//#############################################################################
const bool& mwMxpParams::GetDisableAddingAdditionalMoves() const
{
	return m_disableAddingAdditionalMoves;
}
//#############################################################################
void mwMxpParams::SetDisableAddingAdditionalMoves(const bool disableAddingAdditionalMoves)
{
	m_disableAddingAdditionalMoves = disableAddingAdditionalMoves;
}
//#############################################################################
mwTransfMatrixPtr& mwMxpParams::GetTransfMatrixPtr()
{
	return m_transfMatrixPtr;
}
//#############################################################################
const mwTransfMatrixPtr& mwMxpParams::GetTransfMatrixPtr() const
{
	return m_transfMatrixPtr;
}
//#############################################################################
void mwMxpParams::SetTransfMatrixPtr(const mwTransfMatrixPtr& transfMatrixPtr)
{
	m_transfMatrixPtr = transfMatrixPtr;
}
//#############################################################################
bool& mwMxpParams::GetMastercamSettings()
{
	return m_useMastercamSettings;
}
//#############################################################################
const bool& mwMxpParams::GetMastercamSettings() const
{
	return m_useMastercamSettings;
}
//#############################################################################
void mwMxpParams::SetMastercamSettings(const bool useMastercamSettings)
{
	m_useMastercamSettings = useMastercamSettings;
}
//#############################################################################
post::mwPostDefinitionContainer::id& mwMxpParams::GetPostSettingID()
{
	return m_id;
}
//#############################################################################
const post::mwPostDefinitionContainer::id& mwMxpParams::GetPostSettingID() const
{
	return m_id;
}
//#############################################################################
void mwMxpParams::SetPostSettingID(const post::mwPostDefinitionContainer::id& id)
{
	m_id = id;
}
//#############################################################################
bool& mwMxpParams::GetPostSettingEditing()
{
	return m_postSettingEditing;
}
//#############################################################################
const bool& mwMxpParams::GetPostSettingEditing() const
{
	return m_postSettingEditing;
}
//#############################################################################
void mwMxpParams::SetPostSettingEditing(const bool postSettingEditing)
{
	m_postSettingEditing = postSettingEditing;
}
//#############################################################################
const post::mwMXPParamPtr& mwMxpParams::GetMXPParam() const
{
	return m_mxpParamPtr;
}
//#############################################################################
post::mwMXPParamPtr& mwMxpParams::GetMXPParam() 
{
	return m_mxpParamPtr;
}
//#############################################################################
void mwMxpParams::FlushMXPParam()
{
	if (m_mxpParamPtr.IsNotNull())
	{
		post::mwPostDefinitionContainer::idVector ids = m_postDefCont.GetAvailablePostDefinitionsIDs();
		for (size_t i = 0; i < ids.size(); i++)
		{
			if (m_postDefCont.GetPostDefinition(ids[i])->GetMxpParamPtr().IsNull())
			{
				m_postDefCont.GetPostDefinition(ids[i])->SetMxpParamPtr(m_mxpParamPtr);
			}
		}

		m_mxpParamPtr = MW_NULL;
	}
}
//#############################################################################