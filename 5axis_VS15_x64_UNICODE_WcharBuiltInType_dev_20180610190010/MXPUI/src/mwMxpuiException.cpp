/******************************************************************************
*               File: mwMxpuiException.cpp									  *
*******************************************************************************
*               Description:implements the mwMxpuiException class			  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 12:20:57 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "mwException.hpp"
#include "mwMxpuiException.hpp"
#include "mwPostException.hpp"
//#############################################################################
mwMxpuiException::mwMxpuiException(const mwMxpuiException::Code vExcpID,const misc::mwstring& rLocation,const mwMxpuiException *pPrevLevel)
: misc::mwException(vExcpID,_T("Unknown exception."),pPrevLevel)
{
	//
	m_Type=LOCAL;
	//
	misc::mwstring errMsg;
	switch(vExcpID) 
	{
		case UNKNOWN_MACHINE_TYPE:
			errMsg=_T("Unknown machine type.");
			break;
		case UNKNOWN_START_ANGLE_TYPE:
			errMsg= _T("Unknown start angle type.");
			break;
		case UNKNOWN_SOLUTION_FOR_START_ANGLE:
			errMsg = _T("Unknown solution for start angle.");
			break;
		case UNKNOWN_COORDINATES_TYPE:
			errMsg = _T("Unknown coordinates type.");
			break;
		case UNKNOWN_POLE_HANDLING:
			errMsg = _T("Unknown pole handling type.");
			break;
		case UNKNOWN_MACHINE_LIMITS:
			errMsg = _T("Unknown machine limits type.");
			break;
		case UNKNOWN_NC_FORMAT_TYPE:
			errMsg = _T("Unknown NC output format type.");
			break;
		case NEGATIVE_FEED_RATE:
			errMsg = _T("Negative Feed Rate.");
			break;
		case NEGATIVE_RAPID_RATE:
			errMsg = _T("Negative Rapid Rate.");
			break;
		case NEGATIVE_TOOL_CHANGE_TIME:
			errMsg = _T("Negative Tool Change Time.");
			break;
		case UNKNOWN_SPINDLE:
			errMsg = _T("The value for current spindle is missing.");
			break;
		case UNKNOWN_ARC_FORMAT_TYPE:
			errMsg = _T("Unknown ARC output format type.");
			break;
		case UNKNOWN_INTERPOLATION_TYPE:
			errMsg = _T("Unknown Interpolation type.");
			break;
		case MACHINE_NOT_SUPPORTED_IN_MASTERCAM:
			errMsg = _T("6axis Chain-Saw machine definition is not supported in Mastercam.");
			break;
		case UNKNOWN_ROTATION_AXIS_TYPE:
			errMsg = _T("One of rotation axis type is undefined (Must be Head or Table).");
			break;
		case UNKNOWN_TRANSALTION_AXIS_TYPE:
			errMsg = _T("Transaltion axis provided is unknown (Must be X+/-, Y+/- or Z+/-).");
			break;

		default:
			errMsg=_T("Unknown exception.");
			break;
	}
	if (!rLocation.empty())
	{
		errMsg+=_T(" Occurred in ");
		errMsg+=rLocation;
	}
	SetErrorMessage( errMsg );
}
//#############################################################################
mwMxpuiException::mwMxpuiException(const post::mwPostException &rException, const misc::mwstring& rLocation)
: misc::mwException(rException.GetErrorCode(), rException.GetErrorMessage() ,&rException)
{
	m_Type=MXP_EXCEPTION;
	misc::mwstring errMsg;
	errMsg=GetErrorMessage();
	errMsg+=_T(" Occurred in ");
	errMsg+=rLocation;
}
//#############################################################################
mwMxpuiException::mwMxpuiException(const misc::mwException &rException, const misc::mwstring& rLocation)
: misc::mwException(rException.GetErrorCode(),_T("mwException exception."),&rException)
{
	m_Type=MW_STD_EXCEPTION;
	misc::mwstring errMsg;
	errMsg=GetErrorMessage();
	errMsg+=_T(" Occurred in ");
	errMsg+=rLocation;
}
//#############################################################################
