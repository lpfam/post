/******************************************************************************
*               File: mwMxpParamContainer.cpp	                              *
*******************************************************************************
*               Description: this module describes the						  *
*							mwMxpParamContainer class                         *
*******************************************************************************
*               History:                                                      *
*  17.05.2010  Created by: Tolbariu Ionut-Irinel		                      *
*******************************************************************************
*               (C) 2003-2010 by ModuleWorks GmbH                             *
******************************************************************************/
//#############################################################################
#include "StdAfx.h"
#include "mwMxpParamContainer.hpp"
//#############################################################################
mwMxpParamContainer::mwMxpParamContainer(const cadcam::mwPoint3d& spindle)
{
	m_workpieceName = _T("workpiece");
	m_toolName = _T("tool");
	m_firstTrAxis = post::HEAD;
	m_secondTrAxis = post::HEAD;
	m_thirdTrAxis = post::HEAD;
	mwTransfMatrix::TransfMatrixd I;
	I.LoadIdentity();
	m_transfVectros = new mwTransfMatrix();
	m_transfVectros->AddWorkpieceMatrix(m_workpieceName, I);
	m_transfVectros->AddHolderMatrix(m_toolName, I);
	m_spindles[m_toolName] = spindle;
}
//#############################################################################
mwMxpParamContainer::mwMxpParamContainer(
	const mwTransfMatrixPtr& transfVectros,
	const misc::mwstring& workpieceName,
	const misc::mwstring& toolName,
	const TranslationMountPos firstTrAxis,
	const TranslationMountPos secondTrAxis,
	const TranslationMountPos thirdTrAxis,
	const Spindles& spindles)
	: m_transfVectros(transfVectros)
	, m_workpieceName(workpieceName)
	, m_toolName(toolName)
	, m_firstTrAxis(firstTrAxis)
	, m_secondTrAxis(secondTrAxis)
	, m_thirdTrAxis(thirdTrAxis)
	, m_spindles(spindles)
{
}
//#############################################################################
const mwMxpParamContainer::TranslationMountPos mwMxpParamContainer::GetFirstTrAxis() const
{
	return m_firstTrAxis;
}
//#############################################################################
mwMxpParamContainer::TranslationMountPos& mwMxpParamContainer::GetFirstTrAxis()
{
	return m_firstTrAxis;
}
//#############################################################################
void mwMxpParamContainer::SetFirstTrAxis(const TranslationMountPos firstTrAxis)
{
	m_firstTrAxis = firstTrAxis;
}
//#############################################################################
const mwMxpParamContainer::TranslationMountPos mwMxpParamContainer::GetSecondTrAxis() const
{
	return m_secondTrAxis;
}
//#############################################################################
mwMxpParamContainer::TranslationMountPos& mwMxpParamContainer::GetSecondTrAxis()
{
	return m_secondTrAxis;
}
//#############################################################################
void mwMxpParamContainer::SetSecondTrAxis(const TranslationMountPos secondTrAxis)
{
	m_secondTrAxis = secondTrAxis;
}
//#############################################################################
const mwMxpParamContainer::TranslationMountPos mwMxpParamContainer::GetThirdTrAxis() const
{
	return m_thirdTrAxis;
}
//#############################################################################
mwMxpParamContainer::TranslationMountPos& mwMxpParamContainer::GetThirdTrAxis() 
{
	return m_thirdTrAxis;
}
//#############################################################################
void mwMxpParamContainer::SetThirdTrAxis(const TranslationMountPos thirdTrAxis)
{
	m_thirdTrAxis = thirdTrAxis;
}
//#############################################################################
const mwMxpParamContainer::Spindles& mwMxpParamContainer::GetSpindles() const
{
	return m_spindles;
}
//#############################################################################
mwMxpParamContainer::Spindles& mwMxpParamContainer::GetSpindles() 
{
	return m_spindles;
}
//#############################################################################
void mwMxpParamContainer::SetSpindles(const Spindles& spindles)
{
	m_spindles = spindles;
}
//#############################################################################
const mwTransfMatrixPtr& mwMxpParamContainer::GetTransfMatrixVect() const
{
	return m_transfVectros;
}
//#############################################################################
mwTransfMatrixPtr& mwMxpParamContainer::GetTransfMatrixVect() 
{
	return m_transfVectros;
}
//#############################################################################
void mwMxpParamContainer::SetTransfMatrixVect(const mwTransfMatrixPtr& transfVectros)
{
	m_transfVectros = transfVectros;
}
//#############################################################################
const misc::mwstring& mwMxpParamContainer::GetWorkpieceName() const
{
	return m_workpieceName;
}
//#############################################################################
misc::mwstring& mwMxpParamContainer::GetWorkpieceName()
{
	return m_workpieceName;
}
//#############################################################################
void mwMxpParamContainer::SetWorkpieceName(const misc::mwstring& workpiece)
{
	m_workpieceName = workpiece;
}
//#############################################################################
const misc::mwstring& mwMxpParamContainer::GetToolName() const
{
	return m_toolName;
}
//#############################################################################
misc::mwstring& mwMxpParamContainer::GetToolName()
{
	return m_toolName;
}
//#############################################################################
void mwMxpParamContainer::SetToolName(const misc::mwstring& holder)
{
	m_toolName = holder;
}
//#############################################################################
const bool mwMxpParamContainer::IsDefaultConfiguration() const
{
	if(m_transfVectros.IsNull())
		return true;

	if(m_transfVectros->GetWorkpieceMatrix().size() == 0 || m_transfVectros->GetHolderMatrix().size() == 0)
		return true;

	if(m_transfVectros->GetWorkpieceMatrix().size() == 1 || m_transfVectros->GetHolderMatrix().size() == 1)
		return true;

	// else m_transfVectros is not MW_NULL
	if(m_transfVectros->GetWorkpieceMatrix().size() > 1 || m_transfVectros->GetHolderMatrix().size() > 1)
		return false;
	// else m_transfVectros != MW_NULL and m_transfVectros->GetHolderVectorSize() == 1 and m_transfVectros->GetHolderVectorSize() == 1
	if(m_transfVectros->GetWorkpieceMatrix().begin()->second.IsIdentity() && m_transfVectros->GetHolderMatrix().begin()->second.IsIdentity())
		return true;
	else
		return false;
}
//#############################################################################