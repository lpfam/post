/******************************************************************************
*               File: mwMultyXPostPP.cpp									  *
*******************************************************************************
*               Description:implements the mwMultyXPostPP class				  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*   10/8/2003 2:59:45 PM Created by: Costin Calisov                           *
#   4/8/2015 Changed by: Tolbariu Ionut-Irinel
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#include "StdAfx.h"
#include "mwMultyXPostPP.h"
#include "resource.h"
#include "mwMxpuiException.hpp"
#include "mwPostException.hpp"
#include "mwMessages.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//#############################################################################
/////////////////////////////////////////////////////////////////////////////
// mwMultyXPostPP property page
//IMPLEMENT_DYNCREATE(mwMultyXPostPP, CPropertyPage)
//#############################################################################
mwMultyXPostPP::mwMultyXPostPP(misc::mwAutoPointer<mwMxpParams>& pMxpParams, const mwMxpuiParamInteractor &rInteractor)
	: mwMxpuiPage(pMxpParams, rInteractor, IDD_MXP_PP, MSG_SHEET_TITLE)
{
	IDD = IDD_MXP;
	m_hideAngleControlLimits = false;
	//{{AFX_DATA_INIT(mwMultyXPostPP)
	//}}AFX_DATA_INIT
}
//#############################################################################
mwMultyXPostPP::~mwMultyXPostPP()
{
}
//#############################################################################
BOOL mwMultyXPostPP::OnInitDialog()
{
	AddMXPDefSubPages();
	mwMxpuiPage::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
//#############################################################################
void mwMultyXPostPP::AddMXPDefSubPage(mwMxpDlg& rDlg, int vX, int vY)
{
	CPoint pt;
	pt.x = vX;
	pt.y = vY;
	ScreenToClient(&pt);
	rDlg.Create(mwMxpDlg::IDD, this);
	rDlg.SetWindowPos(NULL, pt.x, pt.y, 0, 0, SWP_NOSIZE | SWP_NOREPOSITION);
	rDlg.ShowWindow(SW_SHOW);
}

//#############################################################################
void mwMultyXPostPP::AddMXPDefSubPages()
{
	CRect rcpos;
	GetDlgItem(IDC_MXP_DEF_DLG_POSITION)->GetWindowRect(&rcpos);
	CWnd* test = GetDlgItem(IDC_MXP_DEF_DLG_POSITION); test;
	int x = rcpos.left;
	int y = rcpos.top;

	if (m_mxpDlg.IsNull())
		m_mxpDlg = new mwMxpDlg(m_pMxpParams, m_interactor);

	if (m_hideAngleControlLimits)
		m_mxpDlg->HideAngleControlLimits();

	if (!::IsWindow(m_mxpDlg->m_hWnd))
		AddMXPDefSubPage(*m_mxpDlg, x, y);
}

//#############################################################################
void mwMultyXPostPP::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if (pDX->m_bSaveAndValidate == NULL)
	{
		GetDlgParams(*pDX);
		SetTexts();
		if (m_mxpDlg.IsNotNull())
			m_mxpDlg->UpdateData(false);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwMultyXPostPP)
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate)
	{
		if (!m_mxpDlg->UpdateData())
		{
			pDX->Fail();
			return;
		}
		SetDlgParams(*pDX);
	}
}

//#############################################################################
void mwMultyXPostPP::Reset()
{
	m_mxpDlg->Reset();
}

//#############################################################################
BEGIN_MESSAGE_MAP(mwMultyXPostPP, mwMxpuiPage)
	//{{AFX_MSG_MAP(mwMultyXPostPP)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//#############################################################################
void mwMultyXPostPP::GetParams()
{
	try
	{
	}
	catch (post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMultyXPostPP::GetParams"));
	}
}
//#############################################################################
void mwMultyXPostPP::SetParams()
{
	try
	{
	}
	catch (post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMultyXPostPP::SetParams"));
	}
}
//#############################################################################
void mwMultyXPostPP::SetTexts()
{
}
//#############################################################################
void mwMultyXPostPP::SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams)
{
	m_mxpDlg->SetParams(pMxpParams);
}
//#############################################################################
void mwMultyXPostPP::UpdateControls()
{
	m_mxpDlg->UpdateControls();
}
//#############################################################################
void mwMultyXPostPP::SetAutoAnglePair(const bool toSet)
{
	m_mxpDlg->m_AutoAnglePair = toSet;
}
//#############################################################################
void mwMultyXPostPP::HideComboMachineLimits()
{
	m_mxpDlg->HideComboMachineLimits();
}
//#############################################################################
void mwMultyXPostPP::HideAngleControlLimits()
{
	m_hideAngleControlLimits = true;
}
//#############################################################################
const bool mwMultyXPostPP::GetRetractToolAtMax() 
{
	return m_mxpDlg->GetRetractToolAtMax();
}
//#############################################################################
void mwMultyXPostPP::SetRetractToolAtMax(const bool retractToolAtMax)
{
	m_mxpDlg->SetRetractToolAtMax(retractToolAtMax);
}
//#############################################################################