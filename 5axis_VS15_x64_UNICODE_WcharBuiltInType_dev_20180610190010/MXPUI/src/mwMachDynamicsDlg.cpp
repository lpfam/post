/******************************************************************************
*               File: mwMachDynamicsDlg.cpp						              *
*******************************************************************************
*               Description:this module describe the mwMachDynamicsDlg class  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  19.02.2010  Created by: Tolbariu Ionut-Irinel                              *
#  4/8/2015 Changed by: Tolbariu Ionut-Irinel                                 *
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#include "StdAfx.h"
#include "resource.h"
#include "mwMessages.hpp"
#include "mwMxpuiException.hpp"
#include "mwPostException.hpp"
#include "mwMachDynamicsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


mwMachDynamicsDlg::mwMachDynamicsDlg(
	misc::mwAutoPointer<mwMxpParams>& pMxpParams, 
	const mwMxpuiParamInteractor &rInteractor,
	CWnd* pParent) : mwMxpuiDlg(pMxpParams, rInteractor, IDD, pParent)
{
	m_feedRate = 0.0;
	m_rapidRate = 0.0;
	m_toolChangeTime = 0.0;

	m_resetParams = false;
}

mwMachDynamicsDlg::~mwMachDynamicsDlg()
{
}

void mwMachDynamicsDlg::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		GetDlgParams(*pDX);
		SetTexts();
	}

	CDialog::DoDataExchange(pDX);
	
	DDX_Text(pDX, IDC_EDT_FEED_RATE, m_feedRate);
	DDX_Text(pDX, IDC_EDT_RAPID_RATE, m_rapidRate);
	DDX_Text(pDX, IDC_EDT_TOOL_CHANGE_TIME, m_toolChangeTime);

	if (pDX->m_bSaveAndValidate)
	{
		CheckValue();
		SetDlgParams(*pDX);
	}
}

BEGIN_MESSAGE_MAP(mwMachDynamicsDlg, mwMxpuiDlg)
END_MESSAGE_MAP()

// mwMachDynamicsPP message handlers

void mwMachDynamicsDlg::GetParams()
{
	try
	{
		post::mwMachDynamics machDyn;
		if(m_resetParams == false)
			machDyn = m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDynamics();
		measures::mwMeasurable::Units units = machDyn.GetUnits();
		if (units == measures::mwUnitsFactory::METRIC)
		{
			SetCntrlText(IDC_LBL_FEED_RATE_UNITS, MSG_IDC_CMB_FEED_RATE_M_MIN);
			SetCntrlText(IDC_LBL_RAPID_RATE_UNITS, MSG_IDC_CMB_RAPID_RATE_M_MIN);
		}
		else
		{
			SetCntrlText(IDC_LBL_FEED_RATE_UNITS, MSG_IDC_CMB_FEED_RATE_INCH_MIN);
			SetCntrlText(IDC_LBL_RAPID_RATE_UNITS, MSG_IDC_CMB_RAPID_RATE_INCH_MIN);
		}

		m_feedRate = machDyn.GetFeedRate();
		m_rapidRate = machDyn.GetRapidRate();
		m_toolChangeTime = machDyn.GetToolChangeTime();
	}
	catch (const post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMachDynamicsDlg::GetParams"));
	}
}

void mwMachDynamicsDlg::SetParams()
{
	try
	{
		post::mwMachDynamics machDyn(
			m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->GetMachDynamics().GetUnits(),
			m_feedRate, m_rapidRate, m_toolChangeTime);
		m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_pMxpParams->GetPostSettingID())->SetMachDynamics(machDyn);
	}
	catch (const post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMachDynamicsDlg::SetParams"));
	}
}

void mwMachDynamicsDlg::SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams)
{
	mwMxpuiDlgsParams::SetParams(pMxpParams);
}

void mwMachDynamicsDlg::SetTexts()
{
	SetCntrlText(IDC_LBL_FEED_RATE, MSG_IDC_LBL_FEED_RATE);
	SetCntrlText(IDC_LBL_RAPID_RATE, MSG_IDC_LBL_RAPID_RATE);
	SetCntrlText(IDC_LBL_TOOL_CHANGE_TIME, MSG_IDC_LBL_TOOL_CHANGE_TIME);

	SetCntrlText(IDC_LBL_TOOL_CHANGE_TIME_UNITS, MSG_IDC_LBL_TOOL_CHANGE_TIME_UNITS);
}

void mwMachDynamicsDlg::Reset()
{
	m_resetParams = true;
	UpdateData(FALSE);
	m_resetParams = false;
}


void mwMachDynamicsDlg::CheckValue()
{
	if (m_feedRate < 0)
		throw mwMxpuiException(mwMxpuiException::NEGATIVE_FEED_RATE, _T("mwMachDynamicsDlg::CheckValue"));
	if (m_rapidRate < 0)
		throw mwMxpuiException(mwMxpuiException::NEGATIVE_RAPID_RATE, _T("mwMachDynamicsDlg::CheckValue"));
	if (m_toolChangeTime < 0)
		throw mwMxpuiException(mwMxpuiException::NEGATIVE_TOOL_CHANGE_TIME, _T("mwMachDynamicsDlg::CheckValue"));

	m_feedRate = floor(m_feedRate * 100.0) / 100.0;
	m_rapidRate = floor(m_rapidRate * 100.0) / 100.0;
	m_toolChangeTime = floor(m_toolChangeTime * 100.0) / 100.0;
}
