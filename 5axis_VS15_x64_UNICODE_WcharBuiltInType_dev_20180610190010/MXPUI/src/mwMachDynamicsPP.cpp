/******************************************************************************
*               File: mwMachDynamicsPP.cpp						              *
*******************************************************************************
*               Description:this module describe the mwMachDynamicsPP class   *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  19.02.2010  Created by: Tolbariu Ionut-Irinel                              *
*  4/8/2015 Changed by: Tolbariu Ionut-Irinel
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#include "StdAfx.h"
#include "mwMachDynamicsPP.hpp"
#include "resource.h"
#include "mwMxpuiException.hpp"
#include "mwPostException.hpp"
#include "mwMessages.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//#############################################################################
/////////////////////////////////////////////////////////////////////////////
// mwMachDynamicsPP property page
//IMPLEMENT_DYNCREATE(mwMachDynamicsPP, CPropertyPage)
//#############################################################################
mwMachDynamicsPP::mwMachDynamicsPP ( misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor &rInteractor)
	: mwMxpuiPage(pMxpParams, rInteractor, IDD_DYNAMICS_PP, MSG_MACH_DYNAMICS_PP_TITLE)
{
	IDD = IDD_MACH_DYNAMICS;
	//{{AFX_DATA_INIT(mwMachDynamicsPP)
	//}}AFX_DATA_INIT
}
//#############################################################################
mwMachDynamicsPP::~mwMachDynamicsPP()
{
}
//#############################################################################
BOOL mwMachDynamicsPP::OnInitDialog()
{
	AddMachDefSubPages();
	mwMxpuiPage::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
//#############################################################################
void mwMachDynamicsPP::AddMachDefSubPage(mwMachDynamicsDlg& rDlg, int vX, int vY)
{
	CPoint pt;
	pt.x = vX;
	pt.y = vY;
	ScreenToClient(&pt);
	rDlg.Create(mwMachDynamicsDlg::IDD, this);
	rDlg.SetWindowPos(NULL, pt.x, pt.y, 0, 0, SWP_NOSIZE | SWP_NOREPOSITION);
	rDlg.ShowWindow(SW_SHOW);
}
//#############################################################################
void mwMachDynamicsPP::AddMachDefSubPages()
{
	CRect rcpos;
	GetDlgItem(IDC_MACH_DYNAMICS_DLG_POSITION)->GetWindowRect(&rcpos);
	int x = rcpos.left;
	int y = rcpos.top;

	if (m_machDynamicsDlg.IsNull())
		m_machDynamicsDlg = new mwMachDynamicsDlg(m_pMxpParams, m_interactor);

	if (!::IsWindow(m_machDynamicsDlg->m_hWnd))
		AddMachDefSubPage(*m_machDynamicsDlg, x, y);

	//UpdateData(false);
}
//#############################################################################
void mwMachDynamicsPP::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if (pDX->m_bSaveAndValidate == NULL)
	{
		GetDlgParams(*pDX);
		SetTexts();
		if (m_machDynamicsDlg.IsNotNull())
			m_machDynamicsDlg->UpdateData(false);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwMachDefPP)
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate)
	{
		if (!m_machDynamicsDlg->UpdateData())
		{
			pDX->Fail();
			return;
		}
		SetDlgParams(*pDX);
	}
}
//#############################################################################
BEGIN_MESSAGE_MAP(mwMachDynamicsPP, mwMxpuiPage)
	//{{AFX_MSG_MAP(mwMachDynamicsPP)
	// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//#############################################################################
void mwMachDynamicsPP::GetParams()
{
	try
	{
	}
	catch (post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMachDynamicsPP::GetParams"));
	}
}
//#############################################################################
void mwMachDynamicsPP::SetParams()
{
	try
	{
	}
	catch (post::mwPostException &e)
	{
		throw mwMxpuiException(e, _T("mwMachDynamicsPP::SetParams"));
	}
}
//#############################################################################
void mwMachDynamicsPP::SetTexts()
{
}

void mwMachDynamicsPP::Reset()
{
	m_machDynamicsDlg->Reset();
}

//#############################################################################
void mwMachDynamicsPP::SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams)
{
	m_machDynamicsDlg->SetParams(pMxpParams);
}
//#############################################################################
