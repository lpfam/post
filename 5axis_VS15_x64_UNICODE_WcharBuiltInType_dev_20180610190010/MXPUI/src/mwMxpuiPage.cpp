/******************************************************************************
*               File: mwMxpuiPage.cpp										  *
*******************************************************************************
*               Description: implements mwMxpuiPage class                     *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 11:53:40 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
//#############################################################################
#include "StdAfx.h"
#include "resource.h"
#include "mwMxpuiPage.hpp"
#include "mwRCImageWnd.hpp"
#include "mwMxpuiExceptionHandler.hpp"
#include "mwMfcUIUtilsException.hpp"
//#############################################################################
mwMxpuiPage::mwMxpuiPage(misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor &rInteractor,int vIDD,int vTitle )
: CPropertyPage(vIDD), mwMxpuiDlgsParams(pMxpParams,rInteractor)
, m_rcImagesCustomType (_T("MXPUI_IMGS"))
, m_dlgInitialized(false)
{
	m_IDD=vIDD;
	mTitle = GetText(vTitle);
	m_psp.pszTitle = _tcsdup(mTitle.c_str());
	m_psp.dwFlags |= PSP_USETITLE;
	m_psp.dwFlags |= PSP_HASHELP;
}
mwMxpuiPage::~mwMxpuiPage()
{
	free( (LPVOID)m_psp.pszTitle );
	m_psp.pszTitle = MW_NULL;
}
//#############################################################################
BEGIN_MESSAGE_MAP(mwMxpuiPage, CPropertyPage)
	//{{AFX_MSG_MAP(mwMxpuiPage)
	ON_WM_HELPINFO()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//#############################################################################
BOOL mwMxpuiPage::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	if (pHelpInfo==MW_NULL)
		AfxGetApp()->WinHelp(m_IDD)	;
	else
		AfxGetApp()->WinHelp(pHelpInfo->iCtrlId, HELP_CONTEXTPOPUP);
	return TRUE;
}

BOOL mwMxpuiPage::HasResetOption()
{
	return TRUE;
}

//#############################################################################
BOOL mwMxpuiPage::PreTranslateMessage(MSG* pMsg) 
{
   // Check for enter key (return) press.
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
 		CPropertyPage::NextDlgCtrl(); // Pass focus to next control
		return TRUE;			// Don't translate further
	}else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		static_cast <CPropertySheet *> (CPropertyPage::GetParent())->EndDialog(IDCANCEL);
		return TRUE;
	}
	else
		return CPropertyPage::PreTranslateMessage(pMsg);

}
//#############################################################################
void mwMxpuiPage::SetRCImage(const int rcImageID, 
							unsigned int rcImgWndID,
							const misc::mwstring& locationToSetImage)
{
	if (m_dlgInitialized)
	{
		try
		{
			mwRCImageWnd* rcImgWnd =   static_cast < mwRCImageWnd* > ( this->GetDlgItem(rcImgWndID) );
			rcImgWnd->SetImage(m_interactor.GetMxpuiNeutralResHandler(), rcImageID, m_rcImagesCustomType, locationToSetImage);
		}
		catch (mwMfcUIUtilsException& exp)
		{
			mwMxpuiExceptionHandler::HandleErr(exp, m_interactor);
		}
	}
}
//#############################################################################
void mwMxpuiPage::OnDestroy()
{
	CPropertyPage::OnDestroy();
	m_dlgInitialized = false;
}
//#############################################################################
BOOL mwMxpuiPage::OnInitDialog()
{
	BOOL result = CPropertyPage::OnInitDialog();

	m_dlgInitialized = (result == TRUE);

	return result;//::OnInitDialog();
}
//#############################################################################
void mwMxpuiPage::PaintBMP(int vResourceID, unsigned int vCntrID)
{
	mwMxpuiDlgsParams::PaintBMP (vResourceID,*(this->GetDlgItem( vCntrID )));
}
//#############################################################################
void mwMxpuiPage::OnPaint() 
{
	CPropertyPage::OnPaint();
	PaintBMPs();
}
//#############################################################################
