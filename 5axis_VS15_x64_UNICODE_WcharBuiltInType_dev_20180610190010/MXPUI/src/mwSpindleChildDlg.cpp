/******************************************************************************
*               File: mwSpindleChildDlg.cpp									  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  17.05.2010 Created by: Tolbariu Ionut-Irinel                               *
*******************************************************************************
*               (C) 2003-2010 by ModuleWorks GmbH                             *
******************************************************************************/
//#############################################################################/
#include "StdAfx.h"
//#include "Patch.h"
#include "mwSpindleChildDlg.hpp"
//#include "shlwapi.h"
//#include "mwStringOps.hpp"
#include "mwMessages.hpp"
//#############################################################################/
IMPLEMENT_DYNAMIC(mwSpindleChildDlg, CDialog)
//#############################################################################/
mwSpindleChildDlg::mwSpindleChildDlg(
	 const misc::mwstring name, 
	 const cadcam::mwVector3d& orientation,
	 const misc::mwAutoPointer<cadcam::mwVector3d>& directionPtr,/*only for Contour machine*/
	 CWnd* pParent)
		: CDialog(mwSpindleChildDlg::IDD, pParent)
		, m_orientation(orientation)
		, m_directionPtr(directionPtr)
{
	m_name = name.c_str();
	//TCHAR exePath[MAX_PATH];
	//GetModuleFileName( MW_NULL, exePath, MAX_PATH );
	//PathRemoveFileSpec( exePath );
}
//#############################################################################/
mwSpindleChildDlg::~mwSpindleChildDlg()
{
}
//#############################################################################/
void mwSpindleChildDlg::DoDataExchange(CDataExchange* pDX)
{
	if (pDX->m_bSaveAndValidate == NULL)
	{
		SetTexts();
	}

	CDialog::DoDataExchange(pDX);
	double x = m_orientation.x();
	DDX_Text(pDX, IDC_EDIT_X, x);
	m_orientation.x(x);
	double y = m_orientation.y();
	DDX_Text(pDX, IDC_EDIT_Y, y);
	m_orientation.y(y);
	double z = m_orientation.z();
	DDX_Text(pDX, IDC_EDIT_Z, z);
	m_orientation.z(z);
	if(m_directionPtr.IsNotNull())
	{
		double dx = m_directionPtr->x();
		DDX_Text(pDX, IDC_EDIT_X2, dx);
		m_directionPtr->x(dx);
		double dy = m_directionPtr->y();
		DDX_Text(pDX, IDC_EDIT_Y2, dy);
		m_directionPtr->y(dy);
		double dz = m_directionPtr->z();
		DDX_Text(pDX, IDC_EDIT_Z2, dz);
		m_directionPtr->z(dz);
	}
	DDX_Text(pDX, IDC_EDIT_NAME, m_name);
	//DDX_CBString(pDX, IDC_EDIT_NAME, m_name);
}
//#############################################################################/
BEGIN_MESSAGE_MAP(mwSpindleChildDlg, CDialog)
	//{{AFX_MSG_MAP(mwPatchDlg)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_REMOVE, &mwSpindleChildDlg::OnBnClickedBtnRemove)
END_MESSAGE_MAP()
//#############################################################################/
void mwSpindleChildDlg::OnBnClickedBtnRemove()
{
	// TODO: Add your control notification handler code here
	//nothing , in this time....
}
//#############################################################################
BOOL mwSpindleChildDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(m_directionPtr.IsNull())
	{
		GetDlgItem(IDC_EDIT_X2)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_Y2)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_Z2)->EnableWindow(FALSE);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}
//#############################################################################
void mwSpindleChildDlg::EnableRemoveButton(const bool state)
{
	GetDlgItem(IDC_BTN_REMOVE)->EnableWindow(state ? TRUE : FALSE);
}
//#############################################################################
BOOL mwSpindleChildDlg::PreTranslateMessage(MSG* pMsg)
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;                // Do not process further
		}
	}

	return CWnd::PreTranslateMessage(pMsg);
}
//#############################################################################
void mwSpindleChildDlg::SetTexts()
{
	SetCntrlText(IDC_LBL_SPINDLE_CHILD_ORIENTATION, MSG_LBL_SPINDLE_CHILD_ORIENTATION);
	SetCntrlText(IDC_LBL_SPINDLE_CHILD_DIRECTION, MSG_LBL_SPINDLE_CHILD_DIRECTION);
	SetCntrlText(IDC_BTN_SPINDLE_CHILD_REMOVE, MSG_LBL_SPINDLE_CHILD_REMOVE);
}
//#############################################################################
