/******************************************************************************
*               File: mwMachDefPP.cpp										  *
*******************************************************************************
*               Description:implements the mwMachDefPP class				  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/7/2003 10:55:23 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "resource.h"
#include "mw5axMachDef.hpp"
#include "mwMxpuiException.hpp"
#include "mwPostException.hpp"
#include "mwMessages.hpp"
#include "mwMachDefPP.h"

#ifdef _DEBUG
#define new DEBUG_NEW	
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//#############################################################################
/////////////////////////////////////////////////////////////////////////////
// mwMachDefPP property page
//IMPLEMENT_DYNCREATE(mwMachDefPP, CPropertyPage)
//#############################################################################
mwMachDefPP::mwMachDefPP ( misc::mwAutoPointer<mwMxpParams>& pMxpParams, const mwMxpuiParamInteractor &rInteractor)
: mwMxpuiPage(pMxpParams,rInteractor,IDD_MACH_DEF_PP,MSG_MACH_DEF_PP_TITLE)
{
	IDD = IDD_MACH_DEF;
	//{{AFX_DATA_INIT(mwMachDefPP)
	//}}AFX_DATA_INIT
}
//#############################################################################
mwMachDefPP::~mwMachDefPP()
{
}
//#############################################################################
BOOL mwMachDefPP::OnInitDialog() 
{
	AddMachDefSubPages();
	mwMxpuiPage::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//#############################################################################
void mwMachDefPP::AddMachDefSubPage(mwMachDefDLG& rDlg, int vX,int vY)
{
	CPoint pt;
	pt.x = vX;
	pt.y = vY;
	ScreenToClient(&pt);
	rDlg.Create(mwMachDefDLG::IDD, this);
	rDlg.SetWindowPos(NULL, pt.x, pt.y, 0, 0, SWP_NOSIZE | SWP_NOREPOSITION);	
	rDlg.ShowWindow(SW_SHOW);
}
//#############################################################################
void mwMachDefPP::AddMachDefSubPages()
{
	CRect rcpos;
	GetDlgItem(IDC_MACH_DEF_DLG_POSITION)->GetWindowRect(&rcpos);
	CWnd* test = GetDlgItem(IDC_MACH_DEF_DLG_POSITION); test;
	int x=rcpos.left;
	int y=rcpos.top;

	if ( m_MachDefDlg.IsNull() )
		m_MachDefDlg = new mwMachDefDLG(m_pMxpParams,m_interactor);

	if ( !::IsWindow( m_MachDefDlg->m_hWnd ) )
		AddMachDefSubPage(*m_MachDefDlg,x,y);

	//UpdateData(false);
}
//#############################################################################
void mwMachDefPP::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if( pDX->m_bSaveAndValidate == NULL )
	{
		GetDlgParams(*pDX);
		SetTexts();
		if (m_MachDefDlg.IsNotNull())
			m_MachDefDlg->UpdateData(false);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwMachDefPP)
	//}}AFX_DATA_MAP
	
	if( pDX->m_bSaveAndValidate )
	{
		if(	!m_MachDefDlg->UpdateData() )
		{
			pDX->Fail();
			return;
		}
		SetDlgParams(*pDX);
	}
}

void mwMachDefPP::Reset()
{
}

BOOL mwMachDefPP::HasResetOption()
{
	return FALSE;
}

//#############################################################################
BEGIN_MESSAGE_MAP(mwMachDefPP, mwMxpuiPage)
	//{{AFX_MSG_MAP(mwMachDefPP)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//#############################################################################
void mwMachDefPP::GetParams() 
{
	try
	{
	}catch (post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefPP::GetParams"));
	}
}
//#############################################################################
void mwMachDefPP::SetParams() 
{
	try
	{
	}catch (post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefPP::SetParams"));
	}
}
//#############################################################################
void mwMachDefPP::SetTexts()
{	
}
//#############################################################################
void mwMachDefPP::SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams)
{
	m_MachDefDlg->SetParams(pMxpParams);
}
//#############################################################################
