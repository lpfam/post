/******************************************************************************
*               File: mwMachDefDLG.cpp										  *
*******************************************************************************
*               Description:implements the mwMachDefDLG class				  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/7/2003 10:55:23 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
//#############################################################################
#include "StdAfx.h"
#include "mw3axMachDef.hpp"
#include "mw4axMachDef.hpp"
#include "mw5axMachDef.hpp"
#include "mw6axMachDef.hpp"
#include "mwMxpuiException.hpp"
#include "mwPostException.hpp"
#include "mwMessages.hpp"
#include "mwException.hpp"
#include "mwMachDefDLG.h"
#include "mwMachDefUtils.hpp"
#include "mwSpindleParentDlg.hpp"
//#############################################################################
#ifdef _DEBUG
#define new DEBUG_NEW	
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//#############################################################################
mwMachDefDLG::mwMachDefDLG ( 
	misc::mwAutoPointer<mwMxpParams>& pMxpParams,
	const mwMxpuiParamInteractor &rInteractor,
	CWnd* pParent) : mwMxpuiDlg(pMxpParams, rInteractor, IDD, pParent)
{
	//{{AFX_DATA_INIT(mwMachDefDLG)
	m_MachType = -1;
	m_Kinematic = -1;
	m_RotAxis0X = 0.0;
	m_RotAxis0Y = 0.0;
	m_RotAxis0Z = 0.0;
	m_RotAxis1X = 0.0;
	m_RotAxis1Y = 0.0;
	m_RotAxis1Z = 0.0;
	m_RotAxis2X = -1.0;
	m_RotAxis2Y = 0.0;
	m_RotAxis2Z = 0.0;
	m_RotAxisBasePnt0X = 0.0;
	m_RotAxisBasePnt0Y = 0.0;
	m_RotAxisBasePnt0Z = 0.0;
	m_RotAxisBasePnt1X = 0.0;
	m_RotAxisBasePnt1Y = 0.0;
	m_RotAxisBasePnt1Z = 0.0;
	m_RotAxisBasePnt2X = 0.0;
	m_RotAxisBasePnt2Y = 0.0;
	m_RotAxisBasePnt2Z = 0.0;
	m_RotAxisLimit0Max = 0.0;
	m_RotAxisLimit0Min = 0.0;
	m_RotAxisLimit1Max = 0.0;
	m_RotAxisLimit2Min = 0.0;
	m_RotAxisLimit1Max = 0.0;
	m_RotAxisLimit2Min = 0.0;
	m_TransAxisLimit0Min = 0.0;
	m_TransAxisLimit0Max = 0.0;
	m_TransAxisLimit1Min = 0.0;
	m_TransAxisLimit1Max = 0.0;
	m_TransAxisLimit2Min = 0.0;
	m_TransAxisLimit2Max = 0.0;
	m_BuildingType = -1;
	m_1stRotType = -1;
	m_2ndRotType = -1;
	m_3rdRotType = -1;

	m_1stTrType = -1;
	m_2ndTrType = -1;
	m_3rdTrType = -1;
	m_x_x = 0.;
	m_x_y = 0.;
	m_x_z = 0.;
	m_y_x = 0.;
	m_y_y = 0.;
	m_y_z = 0.;
	m_z_x = 0.;
	m_z_y = 0.;
	m_z_z = 0.;

	m_1stRotationAxisName = _T("");
	m_2ndRotationAxisName = _T("");
	m_3rdRotationAxisName = _T("");
	m_1stLinearAxisName = _T("");
	m_2ndLinearAxisName = _T("");
	m_3rdLinearAxisName = _T("");

	m_postSettingID = _T("");

	m_workpiece =  -1;
	m_holder =  -1;

	m_sawDirX = 0.0;
	m_sawDirY = 0.0;
	m_sawDirZ = 0.0;

	//}}AFX_DATA_INIT
}
//#############################################################################
mwMachDefDLG::~mwMachDefDLG()
{
}
//#############################################################################
BOOL mwMachDefDLG::OnInitDialog()
{
	//BOOL bRet = mwMxpuiDlg::OnInitDialog();
	//return bRet;
	return TRUE;
}
//#############################################################################
void mwMachDefDLG::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if( pDX->m_bSaveAndValidate == NULL )
	{
		GetDlgParams(*pDX);
		SetTexts();
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwMachDefDLG)
	DDX_CBIndex(pDX, IDC_CMB_MACH_TYPE, m_MachType);
	DDX_CBIndex(pDX, IDC_CMB_MACH_KINEMATIC, m_Kinematic);

	DDX_CBString(pDX, IDC_CMB_1ST_ROTATION_AXIS_NAME, m_1stRotationAxisName);
	DDX_CBString(pDX, IDC_CMB_2ND_ROTATION_AXIS_NAME, m_2ndRotationAxisName);
	DDX_CBString(pDX, IDC_CMB_3RD_ROTATION_AXIS_NAME, m_3rdRotationAxisName);

	if(GetPostSettingIDComboBox() == IDC_CMB_POST_SETTING_ID)
	{
		CString tmp = m_postSettingID;
		DDX_CBString(pDX, GetPostSettingIDComboBox(), m_postSettingID);
		if(tmp != m_postSettingID && pDX->m_bSaveAndValidate && CheckPostSettingsIDs(m_postSettingID.GetString()))
		{
			CString tmp2 = m_postSettingID;
			m_postSettingID = tmp;
			GetCurrentPostDefinition()->SetID(tmp2.GetString());
			m_pMxpParams->SetPostSettingID(tmp2.GetString());
			m_postSettingID = tmp2;
		}
	}

	DDX_Text(pDX, IDC_EDT_1ST_LINEAR_AXIS_NAME, m_1stLinearAxisName);
	DDX_Text(pDX, IDC_EDT_2ND_LINEAR_AXIS_NAME, m_2ndLinearAxisName);
	DDX_Text(pDX, IDC_EDT_3RD_LINEAR_AXIS_NAME, m_3rdLinearAxisName);

	DDX_Text(pDX, IDC_EDT_ROT_AXIS_0_X, m_RotAxis0X);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_0_Y, m_RotAxis0Y);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_0_Z, m_RotAxis0Z);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_1_X, m_RotAxis1X);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_1_Y, m_RotAxis1Y);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_1_Z, m_RotAxis1Z);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_2_X, m_RotAxis2X);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_2_Y, m_RotAxis2Y);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_2_Z, m_RotAxis2Z);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_0_X, m_RotAxisBasePnt0X);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_0_Y, m_RotAxisBasePnt0Y);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_0_Z, m_RotAxisBasePnt0Z);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_1_X, m_RotAxisBasePnt1X);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_1_Y, m_RotAxisBasePnt1Y);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_1_Z, m_RotAxisBasePnt1Z);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_2_X, m_RotAxisBasePnt2X);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_2_Y, m_RotAxisBasePnt2Y);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_BASE_POINT_2_Z, m_RotAxisBasePnt2Z);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_LIMIT_0_MAX, m_RotAxisLimit0Max);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_LIMIT_0_MIN, m_RotAxisLimit0Min);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_LIMIT_1_MAX, m_RotAxisLimit1Max);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_LIMIT_1_MIN, m_RotAxisLimit1Min);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_LIMIT_2_MAX, m_RotAxisLimit2Max);
	DDX_Text(pDX, IDC_EDT_ROT_AXIS_LIMIT_2_MIN, m_RotAxisLimit2Min);
	DDX_Text(pDX, IDC_EDT_TRANS_AXIS_LIMIT_0_MIN, m_TransAxisLimit0Min);
	DDX_Text(pDX, IDC_EDT_TRANS_AXIS_LIMIT_0_MAX, m_TransAxisLimit0Max);
	DDX_Text(pDX, IDC_EDT_TRANS_AXIS_LIMIT_1_MIN, m_TransAxisLimit1Min);
	DDX_Text(pDX, IDC_EDT_TRANS_AXIS_LIMIT_1_MAX, m_TransAxisLimit1Max);
	DDX_Text(pDX, IDC_EDT_TRANS_AXIS_LIMIT_2_MIN, m_TransAxisLimit2Min);
	DDX_Text(pDX, IDC_EDT_TRANS_AXIS_LIMIT_2_MAX, m_TransAxisLimit2Max);

	DDX_Text(pDX, IDC_EDT_X_X2, m_x_x);
	DDX_Text(pDX, IDC_EDT_X_Y2, m_x_y);
	DDX_Text(pDX, IDC_EDT_X_Z2, m_x_z);
	DDX_Text(pDX, IDC_EDT_Y_X2, m_y_x);
	DDX_Text(pDX, IDC_EDT_Y_Y2, m_y_y);
	DDX_Text(pDX, IDC_EDT_Y_Z2, m_y_z);
	DDX_Text(pDX, IDC_EDT_Z_X2, m_z_x);
	DDX_Text(pDX, IDC_EDT_Z_Y2, m_z_y);
	DDX_Text(pDX, IDC_EDT_Z_Z2, m_z_z);

	DDX_CBIndex(pDX, IDC_CMB_1ST_ROT_AXIS_TYPE, m_1stRotType);
	DDX_CBIndex(pDX, IDC_CMB_2ND_ROT_AXIS_TYPE, m_2ndRotType);
	DDX_CBIndex(pDX, IDC_CMB_3RD_ROT_AXIS_TYPE, m_3rdRotType);

	DDX_CBIndex(pDX, IDC_CMB_1ST_TR_AXIS_TYPE, m_1stTrType);
	DDX_CBIndex(pDX, IDC_CMB_2ND_TR_AXIS_TYPE, m_2ndTrType);
	DDX_CBIndex(pDX, IDC_CMB_3RD_TR_AXIS_TYPE, m_3rdTrType);

	DDX_CBIndex(pDX, IDC_CMB_WORKPIECE, m_workpiece);
	DDX_CBIndex(pDX, IDC_CMB_HOLDER, m_holder);

	//}}AFX_DATA_MAP
	
	if( pDX->m_bSaveAndValidate )
	{
		CheckAxisNames();
		SetDlgParams(*pDX);
	}

	if(GetPostSettingIDComboBox() != IDC_CMB_POST_SETTING_ID)
	{
		DDX_CBString(pDX, GetPostSettingIDComboBox(), m_postSettingID);
	}

	if( pDX->m_bSaveAndValidate == NULL )
	{
		ValidateDlgParams();
	}
}
//#############################################################################
void mwMachDefDLG::Validate()
{
	try
	{
		GetCurrentMachine()->Validate();
	}
	catch (mwMxpuiException &e)
	{
		throw e;
	}
	catch (post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::Validate"));
	}
	catch (misc::mwException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::Validate"));
	}
}
//#############################################################################
BEGIN_MESSAGE_MAP(mwMachDefDLG, mwMxpuiDlg)
	//{{AFX_MSG_MAP(mwMachDefDLG)
		// NOTE: the ClassWizard will add message map macros here
		ON_CBN_SELCHANGE(IDC_CMB_MACH_TYPE, OnSelchangeComboMachineType)
		ON_CBN_SELCHANGE(IDC_CMB_MACH_BUILDING_TYPE, OnSelchangeComboDetectionMode)
		ON_CBN_SELCHANGE(IDC_CMB_MACH_KINEMATIC, OnSelchangeComboKinematic)
		ON_BN_CLICKED(IDC_BTN_SPINDLE, &mwMachDefDLG::OnBnClickedBtnSpindle)
		ON_CBN_SELCHANGE(IDC_CMB_POST_SETTING_ID, OnSelchangeComboPostSettingID)
		ON_CBN_SELCHANGE(IDC_CMB_POST_SETTING_ID_STATIC, OnSelchangeComboPostSettingID)
		ON_CBN_KILLFOCUS(IDC_CMB_POST_SETTING_ID, &mwMachDefDLG::OnCbnKillfocusPostSettingID)
		ON_CBN_DROPDOWN(IDC_CMB_POST_SETTING_ID, &mwMachDefDLG::OnCbnDropdownComboPostSettingID)
		ON_BN_CLICKED(IDC_BTN_POST_SETTING_NEW, &mwMachDefDLG::OnBnClickedBtnNew)
		ON_BN_CLICKED(IDC_BTN_POST_SETTING_REMOVE, &mwMachDefDLG::OnBnClickedBtnRemove)
		ON_EN_CHANGE(IDC_EDT_ROT_AXIS_LIMIT_1_MIN, &mwMachDefDLG::OnEnChangeEditTurret)
		//ON_CBN_SELCHANGE(IDC_CMB_WORKPIECE, OnSelchangeComboWorkpiece)
		//ON_CBN_SELCHANGE(IDC_CMB_HOLDER, OnSelchangeComboHolder)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//#############################################################################
/////////////////////////////////////////////////////////////////////////////
// mwMachDefDLG message handlers
//#############################################################################
const post::mwPostDefinitionPtr& mwMachDefDLG::GetCurrentPostDefinition() const
{
	post::mwPostDefinitionContainer::id id = m_postSettingID;
	return m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(id);
}
//#############################################################################
bool mwMachDefDLG::CheckPostSettingsIDs(const post::mwPostDefinitionContainer::id& id) const
{
	const post::mwPostDefinitionContainer::idVector idVect = m_pMxpParams->GetPostDefinitionContainer().GetAvailablePostDefinitionsIDs();

	for(size_t i = 0; i < idVect.size(); i++)
		if(id == idVect[i])
		{
			return false;	
		}

	return true;
}
//#############################################################################
post::mwPostDefinitionPtr& mwMachDefDLG::GetCurrentPostDefinition()
{
	post::mwPostDefinitionContainer::id id = m_postSettingID;
	return m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(id);
}
//#############################################################################
const post::mwMachDefPtr& mwMachDefDLG::GetCurrentMachine() const
{
	const post::mwPostDefinitionPtr& postDefPtr = GetCurrentPostDefinition();
	return postDefPtr->GetMachDefPtr();
}
//#############################################################################
void mwMachDefDLG::GetParams() 
{
	try
	{
		/// keep the current post definitions...
		//m_postDefCont = m_pMxpParams->GetPostDefinitionContainer();
		m_postSettingID = m_pMxpParams->GetPostSettingID().c_str(); // ???? NEED to check this....
		m_NumbersOfAxes = GetCurrentMachine()->GetNumberOfAxes();
		///	
		if(m_pMxpParams->GetMastercamSettings() && GetCurrentMachine()->GetNumberOfAxes() == post::mwMachDef::MACHINE_6AXIS_CONTOUR)
			throw mwMxpuiException(mwMxpuiException::MACHINE_NOT_SUPPORTED_IN_MASTERCAM, _T("mwMachDefDLG::GetParams"));
		/// 
		ReadParams();
		///for now
		((CButton*)GetDlgItem(IDC_CHK_TR_AXIS_DIR_1))->SetCheck(BST_CHECKED);
		((CButton*)GetDlgItem(IDC_CHK_TR_AXIS_DIR_2))->SetCheck(BST_CHECKED);
		((CButton*)GetDlgItem(IDC_CHK_TR_AXIS_DIR_3))->SetCheck(BST_CHECKED);
		///
	}
	catch (mwMxpuiException &e)
	{
		throw e;
	}catch (post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::GetParams"));
	}catch (misc::mwException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::SetParams"));
	}
}
//#############################################################################
void mwMachDefDLG::ReadParams()
{
	const post::mwPostDefinitionPtr& postDefPtr = GetCurrentPostDefinition();
	const post::mwMachDefPtr machDefPtr = GetCurrentMachine();
	if ( machDefPtr.IsNull() )
		return;
	///
	m_TransAxisLimit0Min = machDefPtr->GetTransAxisMinLimit().x();
	m_TransAxisLimit1Min = machDefPtr->GetTransAxisMinLimit().y();
	m_TransAxisLimit2Min = machDefPtr->GetTransAxisMinLimit().z();
	///
	m_TransAxisLimit0Max = machDefPtr->GetTransAxisMaxLimit().x();
	m_TransAxisLimit1Max = machDefPtr->GetTransAxisMaxLimit().y();
	m_TransAxisLimit2Max = machDefPtr->GetTransAxisMaxLimit().z();
	///
	m_MachType = static_cast<int>(machDefPtr->GetMachineDefinitionType());
	///
	switch(machDefPtr->GetMachineDefinitionType())
	{
	case post::mwMachDef::MILLING_MACHINE:
		m_Kinematic = static_cast<int>(machDefPtr->GetNumberOfAxes());
		break;
	case post::mwMachDef::TURNING_MACHINE:
		m_Kinematic = static_cast<int>(machDefPtr->GetMachineTurningType()) - 1;
		break;
	case post::mwMachDef::CONTOUR_MACHINE:
		m_Kinematic = 0;
		break;
	}//end switch
	///
	m_BuildingType = static_cast<int>(postDefPtr->GetMachDefFactory());
	
	post::mwMachDef::point3d tmpPoint;
	switch(machDefPtr->GetNumberOfAxes())
	{
	case post::mwMachDef::MACHINE_3AXIS:
	break;
	case post::mwMachDef::MACHINE_4AXIS:
		{
			post::mw4axMachDefPtr _4axMachDefPtr = post::mwMachDefUtils::SafeCast4ax(machDefPtr);

			tmpPoint = _4axMachDefPtr->GetRotAxisDir();
			m_RotAxis0X = tmpPoint.x();
			m_RotAxis0Y = tmpPoint.y();
			m_RotAxis0Z = tmpPoint.z();
			tmpPoint = _4axMachDefPtr->GetRotAxisBasePt();
			m_RotAxisBasePnt0X = tmpPoint.x();
			m_RotAxisBasePnt0Y = tmpPoint.y();
			m_RotAxisBasePnt0Z = tmpPoint.z();
			m_RotAxisLimit0Min = _4axMachDefPtr->GetRotAxisMinLimit();
			m_RotAxisLimit0Max = _4axMachDefPtr->GetRotAxisMaxLimit();
			m_1stRotType = (int)_4axMachDefPtr->GetMachType();
		}
	break;
	case post::mwMachDef::MACHINE_5AXIS:
		{
			post::mw5axMachDefPtr _5axMachDefPtr = post::mwMachDefUtils::SafeCast5ax(machDefPtr);

			tmpPoint = _5axMachDefPtr->GetRotAxisDir0();
			m_RotAxis0X = tmpPoint.x();
			m_RotAxis0Y = tmpPoint.y();
			m_RotAxis0Z = tmpPoint.z();
			tmpPoint = _5axMachDefPtr->GetRotAxisDir1();
			m_RotAxis1X = tmpPoint.x();
			m_RotAxis1Y = tmpPoint.y();
			m_RotAxis1Z = tmpPoint.z();
			tmpPoint = _5axMachDefPtr->GetRotAxisBasePt0();
			m_RotAxisBasePnt0X = tmpPoint.x();
			m_RotAxisBasePnt0Y = tmpPoint.y();
			m_RotAxisBasePnt0Z = tmpPoint.z();
			tmpPoint = _5axMachDefPtr->GetRotAxisBasePt1();
			m_RotAxisBasePnt1X = tmpPoint.x();
			m_RotAxisBasePnt1Y = tmpPoint.y();
			m_RotAxisBasePnt1Z = tmpPoint.z();
			m_RotAxisLimit0Min = _5axMachDefPtr->GetRotAxisMinLimit0();
			m_RotAxisLimit0Max = _5axMachDefPtr->GetRotAxisMaxLimit0();
			m_RotAxisLimit1Min = _5axMachDefPtr->GetRotAxisMinLimit1();
			m_RotAxisLimit1Max = _5axMachDefPtr->GetRotAxisMaxLimit1();
			switch(_5axMachDefPtr->GetMachType())
			{
			case post::mw5axMachDef::TABLE_TABLE_LEGACY :
			case post::mw5axMachDef::TABLE_TABLE_NEW :
				m_1stRotType = 1;
				m_2ndRotType = 1;
			break;
			case post::mw5axMachDef::HEAD_HEAD :
				m_1stRotType = 0;
				m_2ndRotType = 0;
			break;
			case post::mw5axMachDef::HEAD_TABLE :
				//smart detect...
				{
					if(postDefPtr->GetAxisNames()[3] == _T("C"))
					{
						m_1stRotType = 1;
						m_2ndRotType = 0;
					}
					else
						if(postDefPtr->GetAxisNames()[4] == _T("C"))
						{
							m_1stRotType = 0;
							m_2ndRotType = 1;
						}
						else
							if(mathdef::is_eq(fabs(_5axMachDefPtr->GetRotAxisDir0() * _5axMachDefPtr->GetSpindleDir()), 1))
							{
								m_1stRotType = 1;
								m_2ndRotType = 0;
							}
							else
								if(mathdef::is_eq(fabs(_5axMachDefPtr->GetRotAxisDir1() * _5axMachDefPtr->GetSpindleDir()), 1))
								{
									m_1stRotType = 0;
									m_2ndRotType = 1;
								}
								else//we don't know
								{
									m_1stRotType = 1;
									m_2ndRotType = 0;
								}

				}
			break;
			default:
				throw mwMxpuiException( mwMxpuiException::UNKNOWN_MACHINE_TYPE, _T("mwMachDefDLG::GetParams"));
			}
		}
	break;
	case post::mwMachDef::MACHINE_6AXIS:
		{
			post::mw6axMachDefPtr _6axMachDefPtr = post::mwMachDefUtils::SafeCast6ax(machDefPtr);
			
			tmpPoint = _6axMachDefPtr->GetRotAxisDir0();
			m_RotAxis0X = tmpPoint.x();
			m_RotAxis0Y = tmpPoint.y();
			m_RotAxis0Z = tmpPoint.z();
			tmpPoint = _6axMachDefPtr->GetRotAxisDir1();
			m_RotAxis1X = tmpPoint.x();
			m_RotAxis1Y = tmpPoint.y();
			m_RotAxis1Z = tmpPoint.z();
			tmpPoint = _6axMachDefPtr->GetRotAxisDir2();
			m_RotAxis2X = tmpPoint.x();
			m_RotAxis2Y = tmpPoint.y();
			m_RotAxis2Z = tmpPoint.z();
			tmpPoint = _6axMachDefPtr->GetRotAxisBasePt0();
			m_RotAxisBasePnt0X = tmpPoint.x();
			m_RotAxisBasePnt0Y = tmpPoint.y();
			m_RotAxisBasePnt0Z = tmpPoint.z();
			tmpPoint = _6axMachDefPtr->GetRotAxisBasePt1();
			m_RotAxisBasePnt1X = tmpPoint.x();
			m_RotAxisBasePnt1Y = tmpPoint.y();
			m_RotAxisBasePnt1Z = tmpPoint.z();
			tmpPoint = _6axMachDefPtr->GetRotAxisBasePt2();
			m_RotAxisBasePnt2X = tmpPoint.x();
			m_RotAxisBasePnt2Y = tmpPoint.y();
			m_RotAxisBasePnt2Z = tmpPoint.z();
			m_RotAxisLimit0Min = _6axMachDefPtr->GetRotAxisMinLimit0();
			m_RotAxisLimit0Max = _6axMachDefPtr->GetRotAxisMaxLimit0();
			m_RotAxisLimit1Min = _6axMachDefPtr->GetRotAxisMinLimit1();
			m_RotAxisLimit1Max = _6axMachDefPtr->GetRotAxisMaxLimit1();
			//m_RotAxisLimit2Min = _6axMachDefPtr->GetRotAxisMinLimit2();
			m_RotAxisLimit2Max = _6axMachDefPtr->GetThirdAngle();
			m_1stRotType = (int)_6axMachDefPtr->GetFirstAxisType();
			m_2ndRotType = (int)_6axMachDefPtr->GetSecondAxisType();
			m_3rdRotType = (int)_6axMachDefPtr->GetThirdAxisType();
		}
	break;
	case post::mwMachDef::MACHINE_6AXIS_CONTOUR:
		{
			post::mw6axCSMachDefPtr _6axCSMachDefPtr = post::mwMachDefUtils::SafeCast6axCS(machDefPtr);

			tmpPoint = _6axCSMachDefPtr->GetRotAxisDir0();
			m_RotAxis0X = tmpPoint.x();
			m_RotAxis0Y = tmpPoint.y();
			m_RotAxis0Z = tmpPoint.z();
			tmpPoint = _6axCSMachDefPtr->GetRotAxisDir1();
			m_RotAxis1X = tmpPoint.x();
			m_RotAxis1Y = tmpPoint.y();
			m_RotAxis1Z = tmpPoint.z();
			tmpPoint = _6axCSMachDefPtr->GetRotAxisDir2();
			m_RotAxis2X = tmpPoint.x();
			m_RotAxis2Y = tmpPoint.y();
			m_RotAxis2Z = tmpPoint.z();
			tmpPoint = _6axCSMachDefPtr->GetRotAxisBasePt0();
			m_RotAxisBasePnt0X = tmpPoint.x();
			m_RotAxisBasePnt0Y = tmpPoint.y();
			m_RotAxisBasePnt0Z = tmpPoint.z();
			tmpPoint = _6axCSMachDefPtr->GetRotAxisBasePt1();
			m_RotAxisBasePnt1X = tmpPoint.x();
			m_RotAxisBasePnt1Y = tmpPoint.y();
			m_RotAxisBasePnt1Z = tmpPoint.z();
			tmpPoint = _6axCSMachDefPtr->GetRotAxisBasePt2();
			m_RotAxisBasePnt2X = tmpPoint.x();
			m_RotAxisBasePnt2Y = tmpPoint.y();
			m_RotAxisBasePnt2Z = tmpPoint.z();
			m_RotAxisLimit0Min = _6axCSMachDefPtr->GetRotAxisMinLimit0();
			m_RotAxisLimit0Max = _6axCSMachDefPtr->GetRotAxisMaxLimit0();
			m_RotAxisLimit1Min = _6axCSMachDefPtr->GetRotAxisMinLimit1();
			m_RotAxisLimit1Max = _6axCSMachDefPtr->GetRotAxisMaxLimit1();
			m_RotAxisLimit2Min = _6axCSMachDefPtr->GetRotAxisMinLimit2();
			m_RotAxisLimit2Max = _6axCSMachDefPtr->GetRotAxisMaxLimit2();
			m_1stRotType = (int)_6axCSMachDefPtr->GetFirstAxisType();
			m_2ndRotType = (int)_6axCSMachDefPtr->GetSecondAxisType();
			m_3rdRotType = (int)_6axCSMachDefPtr->GetThirdAxisType();
			tmpPoint = _6axCSMachDefPtr->GetSawDirection();
			m_sawDirX = tmpPoint.x();
			m_sawDirY = tmpPoint.y();
			m_sawDirZ = tmpPoint.z();
		}
		break;
	}// end switch

	m_x_x = machDefPtr->GetTranslationX().x();
	m_x_y = machDefPtr->GetTranslationX().y();
	m_x_z = machDefPtr->GetTranslationX().z();

	m_y_x = machDefPtr->GetTranslationY().x();
	m_y_y = machDefPtr->GetTranslationY().y();
	m_y_z = machDefPtr->GetTranslationY().z();

	m_z_x = machDefPtr->GetTranslationZ().x();
	m_z_y = machDefPtr->GetTranslationZ().y();
	m_z_z = machDefPtr->GetTranslationZ().z();

	m_1stTrType = (int)postDefPtr->GetFirstTrAxis();
	m_2ndTrType = (int)postDefPtr->GetSecondTrAxis();
	m_3rdTrType = (int)postDefPtr->GetThirdTrAxis();

	GetParamsAxes();

	TurningSetup();

	EnableOrDisable(postDefPtr->GetMachDefFactory());

	DisableEditing(m_pMxpParams->GetPostSettingEditing());
}
//#############################################################################
void mwMachDefDLG::SetParams() 
{
	try
	{
		WriteParams();

	}catch (mwMxpuiException &e)
	{
		throw e;
	}catch (post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::SetParams"));
	}catch (misc::mwException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::SetParams"));
	}
}
//#############################################################################
void mwMachDefDLG::WriteParams() 
{
	FixWrongInput();

	post::mwPostDefinitionPtr& postDef = GetCurrentPostDefinition();

	int buildingType = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->GetCurSel(); 
	postDef->SetMachDefFactory(static_cast<post::mwMachDefBuilder::MachDefFactory>(buildingType));

	post::mwMachDef::vector3d spindlerDir = ExtractSpindleOrientation();// check name and extract a value...
	post::mwMachDef::vector3d rotAxisDirection0(m_RotAxis0X,m_RotAxis0Y,m_RotAxis0Z);
	post::mwMachDef::vector3d rotAxisDirection1(m_RotAxis1X,m_RotAxis1Y,m_RotAxis1Z);
	post::mwMachDef::vector3d rotAxisDirection2(m_RotAxis2X,m_RotAxis2Y,m_RotAxis2Z);
	post::mwMachDef::point3d rotAxisBasePoint0(m_RotAxisBasePnt0X,m_RotAxisBasePnt0Y,m_RotAxisBasePnt0Z);
	post::mwMachDef::point3d rotAxisBasePoint1(m_RotAxisBasePnt1X,m_RotAxisBasePnt1Y,m_RotAxisBasePnt1Z);
	post::mwMachDef::point3d rotAxisBasePoint2(m_RotAxisBasePnt2X,m_RotAxisBasePnt2Y,m_RotAxisBasePnt2Z);
	post::mwMachDef::point3d transAxisLimitMin(m_TransAxisLimit0Min,m_TransAxisLimit1Min,m_TransAxisLimit2Min);
	post::mwMachDef::point3d transAxisLimitMax(m_TransAxisLimit0Max,m_TransAxisLimit1Max,m_TransAxisLimit2Max);
	post::mwMachDef::vector3d sawDirection(m_sawDirX, m_sawDirY, m_sawDirZ);

	misc::mwAutoPointer<post::mwMachDef::vector3d> ptr = ExtractSpindleDirectionPtr();
	if(ptr.IsNotNull())
		sawDirection = *ptr;
	post::mw5axMachDef::MachType _5axMachType = post::mw5axMachDef::TABLE_TABLE_NEW;
	if(m_NumbersOfAxes == post::mwMachDef::MACHINE_5AXIS)
	{
		if(m_1stRotType == 0 && m_2ndRotType == 0)
		{
			_5axMachType = post::mw5axMachDef::HEAD_HEAD;
		}
		else
			if(m_1stRotType == 0 && m_2ndRotType == 1 || m_1stRotType == 1 && m_2ndRotType == 0)
			{
				_5axMachType = post::mw5axMachDef::HEAD_TABLE;
			}
			else
			{
				_5axMachType = post::mw5axMachDef::TABLE_TABLE_NEW;
			}
	}

	misc::mwAutoPointer<cadcam::mwVector3d> vectorPtr = postDef->GetMachDefPtr()->GetArrowDirectionPtr();

	if(m_MachType == 2 && m_3rdRotType==-1)
	{
		throw mwMxpuiException( mwMxpuiException::UNKNOWN_ROTATION_AXIS_TYPE, _T("mwMachDefDLG::WriteParams"));
	}


	post::mwMachDefPtr newMachDefPtr = post::mwMachDefUtils::BuildMachDef(
		static_cast<post::mwMachDef::NumbersOfAxes>(m_NumbersOfAxes),
		postDef->GetMachDefPtr()->GetUnits(),
		spindlerDir,
		transAxisLimitMin,
		transAxisLimitMax,
		postDef->GetMachDefPtr()->GetTranslationX(),
		postDef->GetMachDefPtr()->GetTranslationY(),
		postDef->GetMachDefPtr()->GetTranslationZ(),
		postDef->GetMachDefPtr()->GetWorkpieceTransform(),
		postDef->GetMachDefPtr()->GetHolderTransform(),
		rotAxisDirection0,
		rotAxisDirection1,
		rotAxisDirection2,
		rotAxisBasePoint0,
		rotAxisBasePoint1,
		rotAxisBasePoint2,
		m_RotAxisLimit0Min,
		m_RotAxisLimit1Min,
		m_RotAxisLimit2Min,
		m_RotAxisLimit0Max,
		m_RotAxisLimit1Max,
		m_RotAxisLimit2Max,
		static_cast<post::mw6axMachDef::AxisType>(m_1stRotType),
		static_cast<post::mw6axMachDef::AxisType>(m_2ndRotType),
		static_cast<post::mw6axMachDef::AxisType>(m_3rdRotType),
		static_cast<post::mw5axMachDef::MachType>(_5axMachType),
		static_cast<post::mw4axMachDef::MachType>(m_1stRotType),
		sawDirection,
		static_cast<post::mwMachDef::MachineDefinitionType>(m_MachType),
		postDef->GetMachDefFactory() == post::mwMachDefBuilder::MANUAL_MODE);

	if(vectorPtr.IsNotNull())
		newMachDefPtr->SetArrowDirectionPtr(vectorPtr);


	const post::mwMachDef::vector3d dx(m_x_x, m_x_y, m_x_z);
	const post::mwMachDef::vector3d dy(m_y_x, m_y_y, m_y_z);
	const post::mwMachDef::vector3d dz(m_z_x, m_z_y, m_z_z);

	if(m_x_x == m_x_y && m_x_y == m_x_z ||
		m_y_x == m_y_y && m_y_y == m_y_z ||
		m_z_x == m_z_y && m_z_y == m_z_z ||
		m_x_x == m_y_x && m_y_x == m_z_x ||
		m_x_y == m_y_y && m_y_y == m_z_y ||
		m_x_z == m_y_z && m_y_z == m_z_z)
	{
		;
	}
	else
	{
		newMachDefPtr->SetTranslationXYZ(dx, dy, dz);
	}

	TCHAR szTool[128];
	TCHAR szWorkpiecce[128];

	GetDlgItem(IDC_CMB_HOLDER)->GetWindowText(szTool, 128);
	postDef->SetToolName(szTool);
	GetDlgItem(IDC_CMB_WORKPIECE)->GetWindowText(szWorkpiecce, 128);
	postDef->SetWorkpieceName(szWorkpiecce);

	if(m_pMxpParams->GetTransfMatrixPtr().IsNotNull())
	{
		post::mwMachDef::mwMatrix4d matrix;

		if(m_pMxpParams->GetTransfMatrixPtr()->FindWorkpiece(szWorkpiecce, matrix))
			newMachDefPtr->SetWorkpieceTransform(new post::mwMachDef::mwMatrix4d(matrix));

		if(m_pMxpParams->GetTransfMatrixPtr()->FindHolder(szTool, matrix))
			newMachDefPtr->SetHolderTransform(new post::mwMachDef::mwMatrix4d(matrix));
	}

	postDef->SetFirstTrAxis( static_cast<post::mwTranslationPos>(m_1stTrType));
	postDef->SetSecondTrAxis( static_cast<post::mwTranslationPos>(m_2ndTrType));
	postDef->SetThirdTrAxis( static_cast<post::mwTranslationPos>(m_3rdTrType));

	postDef->SetMachDefPtr(newMachDefPtr);

	SetParamsAxes();
}
//#############################################################################
void mwMachDefDLG::GetParamsAxes() 
{
	try
	{
		const post::mwPostDefinitionPtr postDef = GetCurrentPostDefinition();
		
		std::vector<misc::mwstring> axisName = postDef->GetAxisNames();
		m_1stLinearAxisName=axisName[0].c_str();
		m_2ndLinearAxisName=axisName[1].c_str();
		m_3rdLinearAxisName=axisName[2].c_str();
		m_1stRotationAxisName=_T("1_NONAME");
		m_2ndRotationAxisName=_T("2_NONAME");
		m_3rdRotationAxisName=_T("3_NONAME");
		switch(postDef->GetMachDefPtr()->GetNumberOfAxes())
		{
		case post::mwMachDef::MACHINE_3AXIS:
			break;
		case post::mwMachDef::MACHINE_4AXIS:
			if(axisName.size() >= 4)
				m_1stRotationAxisName=axisName[3].c_str();
			break;
		case post::mwMachDef::MACHINE_5AXIS:
			if(axisName.size() >= 5)
			{
				m_1stRotationAxisName=axisName[3].c_str();
				m_2ndRotationAxisName=axisName[4].c_str();
			}
			break;
		case post::mwMachDef::MACHINE_6AXIS:
		case post::mwMachDef::MACHINE_6AXIS_CONTOUR:
			if(axisName.size() >= 6)
			{
				m_1stRotationAxisName=axisName[3].c_str();
				m_2ndRotationAxisName=axisName[4].c_str();
				m_3rdRotationAxisName=axisName[5].c_str();
			}
			break;
		}

	}catch (post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::GetParamsAxes"));
	}
}
//#############################################################################
void mwMachDefDLG::SetParamsAxes() 
{
	try
	{
		post::mwPostDefinitionPtr& postDef = GetCurrentPostDefinition();
		std::vector<misc::mwstring> axisNames;
		axisNames.push_back(misc::mwstring ((LPCTSTR)m_1stLinearAxisName));
		axisNames.push_back(misc::mwstring ((LPCTSTR)m_2ndLinearAxisName));
		axisNames.push_back(misc::mwstring ((LPCTSTR)m_3rdLinearAxisName));
		switch(postDef->GetMachDefPtr()->GetNumberOfAxes())
		{
		case post::mwMachDef::MACHINE_3AXIS:
			m_pMxpParams->GetMXPWriterParam().SetAxisNames(axisNames, 3);
			break;
		case post::mwMachDef::MACHINE_4AXIS:
			axisNames.push_back(misc::mwstring ((LPCTSTR)m_1stRotationAxisName));
			m_pMxpParams->GetMXPWriterParam().SetAxisNames(axisNames, 4);
			break;
		case post::mwMachDef::MACHINE_5AXIS:
			axisNames.push_back(misc::mwstring ((LPCTSTR)m_1stRotationAxisName));
			axisNames.push_back(misc::mwstring ((LPCTSTR)m_2ndRotationAxisName));
			m_pMxpParams->GetMXPWriterParam().SetAxisNames(axisNames, 5);
			break;
		case post::mwMachDef::MACHINE_6AXIS:
		case post::mwMachDef::MACHINE_6AXIS_CONTOUR:
			axisNames.push_back(misc::mwstring ((LPCTSTR)m_1stRotationAxisName));
			axisNames.push_back(misc::mwstring ((LPCTSTR)m_2ndRotationAxisName));
			axisNames.push_back(misc::mwstring ((LPCTSTR)m_3rdRotationAxisName));
			m_pMxpParams->GetMXPWriterParam().SetAxisNames(axisNames, 6);
			break;
		}
		postDef->SetAxisNames(axisNames);

	}catch (post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwMachDefDLG::SetParamsAxes"));
	}
}

//#############################################################################
void mwMachDefDLG::SetTexts()
{	
	const post::mwPostDefinitionPtr postDef = GetCurrentPostDefinition();

	if ( postDef->GetMachDefPtr().IsNull() )
		return;

	SetCntrlText(IDC_STATIC_POST_SETTINGS_SELECTION,MSG_LBL_POST_SETTINGS_SELECTION);
	SetCntrlText(IDC_LBL_POST_SETTING_ID,MSG_LBL_POST_SETTING_ID);
	SetCntrlText(IDC_BTN_POST_SETTING_NEW,MSG_BTN_POST_SETTING_NEW);
	SetCntrlText(IDC_BTN_POST_SETTING_REMOVE,MSG_BTN_POST_SETTING_REMOVE);
	static_cast<CComboBox * >(GetDlgItem(GetPostSettingIDComboBox()))->ResetContent();
	post::mwPostDefinitionContainer::idVector ids = m_pMxpParams->GetPostDefinitionContainer().GetAvailablePostDefinitionsIDs();
	for(size_t i = 0; i < ids.size(); i++)
	{
		static_cast<CComboBox * >(GetDlgItem(GetPostSettingIDComboBox()))->AddString( ids[i].c_str() );
	}

	SetCntrlText(IDC_LBL_MACH_TYPE,MSG_LBL_MACH_TYPE);
	SetCntrlText(IDC_LBL_MACH_NUMBER_OF_AXES,MSG_LBL_NUMBER_OF_AXES);
	SetCntrlText(IDC_LBL_SPINDLE_DIR,MSG_LBL_SPINDLE_DIR);

	SetCntrlText(IDC_LBL_ROT_AXIS_DIR_0,MSG_LBL_ROT_AXIS_DIR_0);
	SetCntrlText(IDC_LBL_ROT_AXIS_DIR_1,MSG_LBL_ROT_AXIS_DIR_1);
	SetCntrlText(IDC_LBL_ROT_AXIS_DIR_2,MSG_LBL_ROT_AXIS_DIR_2);

	SetCntrlText(IDC_CHK_TR_AXIS_DIR_1,MSG_LBL_TR_AXIS_DIR_1);
	SetCntrlText(IDC_CHK_TR_AXIS_DIR_2,MSG_LBL_TR_AXIS_DIR_2);
	SetCntrlText(IDC_CHK_TR_AXIS_DIR_3,MSG_LBL_TR_AXIS_DIR_3);

	SetCntrlText(IDC_LBL_ROT_AXIS_BASE_PNT_0,MSG_LBL_ROT_AXIS_BASE_PNT_0);
	SetCntrlText(IDC_LBL_ROT_AXIS_BASE_PNT_1,MSG_LBL_ROT_AXIS_BASE_PNT_1);
	SetCntrlText(IDC_LBL_ROT_AXIS_BASE_PNT_2,MSG_LBL_ROT_AXIS_BASE_PNT_2);
	SetCntrlText(IDC_LBL_THE_THIRD_ANGLE,MSG_LBL_THE_THIRD_ANGLE);
	SetCntrlText(IDC_LBL_MACH_BUILDING_TYPE,MSG_LBL_BUILDING_TYPE);

	SetCntrlText(IDC_STATIC_SPINDLE, MSG_IDC_STATIC_SPINDLE );
	SetCntrlText(IDC_STATIC_ROTATION_AXIS, MSG_IDC_STATIC_ROTATION_AXIS );
	SetCntrlText(IDC_STATIC_LINEAR_AXIS, MSG_IDC_STATIC_LINEAR_AXIS );

	SetTextInKinematicComboBox(postDef->GetMachDefPtr()->GetMachineDefinitionType());

	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->AddString( GetText(MSG_CMB_MILLING_MACHINE_TYPE).c_str() );
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->AddString( GetText(MSG_CMB_TURNING_MACHINE_TYPE).c_str() );
	if(!m_pMxpParams->GetMastercamSettings())
	{
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->AddString( GetText(MSG_CMB_CHAIN_SAW_MACHINE_TYPE).c_str() );
	}

	static_cast<CComboBox *>(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->ResetContent();
	static_cast<CComboBox *>(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->AddString(GetText(MSG_CMB_AUTODETECT_FULL).c_str());
	static_cast<CComboBox *>(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->AddString(GetText(MSG_CMB_AUTODETECT_SEMI).c_str());
	static_cast<CComboBox *>(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->AddString(GetText(MSG_CMB_MANUAL_MODE).c_str());
	static_cast<CComboBox *>(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->SetCurSel(m_BuildingType);

	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROTATION_AXIS_NAME))->ResetContent();

	if(m_1stRotationAxisName != _T("a"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_A ).c_str() );
	if(m_1stRotationAxisName != _T("b"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_B ).c_str() );
	if(m_1stRotationAxisName != _T("c"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_C ).c_str() );
	if(m_1stRotationAxisName != _T("d"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_D ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROTATION_AXIS_NAME))->ResetContent();

	if(m_2ndRotationAxisName != _T("a"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_A ).c_str() );
	if(m_2ndRotationAxisName != _T("b"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_B ).c_str() );
	if(m_2ndRotationAxisName != _T("c"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_C ).c_str() );
	if(m_2ndRotationAxisName != _T("d"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_D ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROTATION_AXIS_NAME))->ResetContent();

	if(m_3rdRotationAxisName != _T("a"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_A ).c_str() );
	if(m_3rdRotationAxisName != _T("b"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_B ).c_str() );
	if(m_3rdRotationAxisName != _T("c"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_C ).c_str() );
	if(m_3rdRotationAxisName != _T("d"))
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROTATION_AXIS_NAME))->AddString( GetText(MSG_CMB_ROTATION_AXIS_NAME_D ).c_str() );
	//
	SetCntrlText(IDC_LBL_TYPE,MSG_LBL_TYPE);
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROT_AXIS_TYPE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROT_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_HEAD ).c_str() );
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_ROT_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_TABLE ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROT_AXIS_TYPE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROT_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_HEAD ).c_str() );
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_ROT_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_TABLE ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROT_AXIS_TYPE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROT_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_HEAD ).c_str() );
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROT_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_TABLE ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_TR_AXIS_TYPE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_TR_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_HEAD ).c_str() );
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_1ST_TR_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_TABLE ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_TR_AXIS_TYPE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_TR_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_HEAD ).c_str() );
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_2ND_TR_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_TABLE ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_TR_AXIS_TYPE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_TR_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_HEAD ).c_str() );
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_TR_AXIS_TYPE))->AddString( GetText(MSG_CMB_MACH4AX_TYPE_TABLE ).c_str() );
	//
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_WORKPIECE))->ResetContent();
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->ResetContent();

	SetCntrlText(IDC_LBL_WORKPIECE, MSG_LBL_WORKPIECE );
	SetCntrlText(IDC_LBL_HOLDER, MSG_LBL_HOLDER );
	//////////////////////////////////////////////////////////////////////////
	int currentPos;
	bool invalide;

	currentPos = 0;
	invalide = true;

	if(m_pMxpParams->GetTransfMatrixPtr().IsNotNull())
	{
		mwTransfMatrix::const_iterator itw = m_pMxpParams->GetTransfMatrixPtr()->GetWorkpieceMatrix().begin();
		for(int i = 0; itw != m_pMxpParams->GetTransfMatrixPtr()->GetWorkpieceMatrix().end(); ++itw, i++)
		{
			if(itw->first == postDef->GetWorkpieceName())
				currentPos = i;
			static_cast<CComboBox * >(GetDlgItem(IDC_CMB_WORKPIECE))->AddString( itw->first.c_str() );
			invalide = false;
		}
	}
	if(invalide)
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_WORKPIECE))->AddString( GetText(MSG_IDC_CMB_WORKPIECE_DEFAULT ).c_str() );

	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_WORKPIECE))->SetCurSel(currentPos);

	m_workpiece = currentPos;

	currentPos = 0;
	invalide = true;

	post::mwSpindles::const_iterator itt = postDef->GetSpindles().begin();
	for(int i = 0; itt != postDef->GetSpindles().end(); ++itt, i++)
	{
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->AddString( itt->first.c_str() );
		if(itt->first == postDef->GetToolName())
			currentPos = i;
		invalide = false;
	}
	if(invalide)
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->AddString( GetText(MSG_IDC_CMB_HOLDER_DEFAULT ).c_str() );	

	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->SetCurSel(currentPos);

	m_holder = currentPos;
	//////////////////////////////////////////////////////////////////////////
	//
	SetVisibility();
	//
	EnableOrDisable(postDef->GetMachDefFactory());
}
//#############################################################################
void mwMachDefDLG::DisableEditing(const bool value)
{
	if(value)
	{
		GetDlgItem(IDC_BTN_POST_SETTING_NEW)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_POST_SETTING_REMOVE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CMB_POST_SETTING_ID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CMB_POST_SETTING_ID_STATIC)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_CMB_POST_SETTING_ID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CMB_POST_SETTING_ID_STATIC)->ShowWindow(SW_HIDE);
	}
}
//#############################################################################
void mwMachDefDLG::OnSelchangeComboKinematic()
{
	const int machType = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->GetCurSel();
	const int kinematic = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->GetCurSel();

	m_BuildingType = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->GetCurSel();

	switch(machType)
	{
	case 0:	
		m_NumbersOfAxes = static_cast<post::mwMachDef::NumbersOfAxes>(kinematic);
		break;
	case 1:
		{
			if(kinematic == 0)//3tT+1R
			{
				m_NumbersOfAxes = post::mwMachDef::MACHINE_4AXIS;
			}
			else
			{
				m_NumbersOfAxes = post::mwMachDef::MACHINE_5AXIS;
			}
		}
		break;
	case 2:
		m_NumbersOfAxes = post::mwMachDef::MACHINE_6AXIS_CONTOUR;
		break;
	}
	
	SetVisibility();


	if(m_NumbersOfAxes == post::mwMachDef::MACHINE_6AXIS && m_3rdRotType == -1)
	{
		m_3rdRotType = 0;
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_3RD_ROT_AXIS_TYPE))->SetCurSel(m_3rdRotType);
	}

	TurningSetup();
}
//#############################################################################
void mwMachDefDLG::SetVisibility()
{
	const int axes = static_cast<int>(m_NumbersOfAxes);
	// axis = 0 -> 3_axis_machine
	// axis = 1 -> 4_axis_machine
	// axis = 2 -> 5_axis_machine
	// axis = 3 -> 6_axis_machine
	// axis = 4 -> 6_axis_chain_saw_machine

	GetDlgItem(IDC_LBL_ROT_AXIS_DIR_0)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_0_X)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_0_Y)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_0_Z)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_LBL_ROT_AXIS_DIR_1)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_1_X)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_1_Y)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_1_Z)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_LBL_ROT_AXIS_DIR_2)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_2_X)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_2_Y)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_2_Z)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_0)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_0_X)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_0_Y)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_0_Z)->ShowWindow( (axes == 1 || axes == 2 || axes == 3 || axes == 4) ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_1)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_1_X)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_1_Y)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_1_Z)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_2)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_2_X)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_2_Y)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_2_Z)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_0_MIN)->ShowWindow(axes == 0 ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_0_MAX)->ShowWindow(axes == 0 ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MIN)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MAX)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_LBL_THE_THIRD_ANGLE)->ShowWindow(axes == 3 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_2_MIN)->ShowWindow(axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_2_MAX)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_CMB_1ST_ROTATION_AXIS_NAME)->ShowWindow(axes == 0 ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_CMB_2ND_ROTATION_AXIS_NAME)->ShowWindow(axes == 2 || axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_CMB_3RD_ROTATION_AXIS_NAME)->ShowWindow(axes == 3 || axes == 4 ? SW_SHOW : SW_HIDE);

	GetDlgItem(IDC_CMB_1ST_ROT_AXIS_TYPE)->ShowWindow(axes == 0 ?  SW_HIDE : SW_SHOW);

	GetDlgItem(IDC_CMB_2ND_ROT_AXIS_TYPE)->ShowWindow(axes == 0 || axes == 1 ? SW_HIDE : SW_SHOW);

	GetDlgItem(IDC_CMB_3RD_ROT_AXIS_TYPE)->ShowWindow(axes == 0 || axes == 1 || axes == 2 ? SW_HIDE : SW_SHOW);
}

//#############################################################################
void mwMachDefDLG::SetTextInKinematicComboBox(const int type)
{
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->ResetContent();

	switch(type)
	{
	case 0:
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_3AXES_MACHINE).c_str() );
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_4AXES_MACHINE).c_str() );
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_5AXES_MACHINE).c_str() );
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_6AXES_MACHINE).c_str() );
		break;
	case 1:
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_TURNING_3TR1ROT).c_str() );
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_TURNING_3TR2ROT).c_str() );
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_TURNING_3TR1ROT1TUR).c_str() );
		break;
	case 2:
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->AddString( GetText(MSG_CMB_6AXES_CHAIN_SAW_MACHINE).c_str() );
		break;
	}	
}
//#############################################################################
void mwMachDefDLG::OnSelchangeComboDetectionMode()
{
	m_BuildingType = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_BUILDING_TYPE))->GetCurSel();
	
	//UpdateData(FALSE);
	EnableOrDisable(static_cast<post::mwMachDefBuilder::MachDefFactory>(m_BuildingType));

	post::mwPostDefinitionPtr& postDefPtr = GetCurrentPostDefinition();
	postDefPtr->SetMachDefFactory(static_cast<post::mwMachDefBuilder::MachDefFactory>(m_BuildingType));
}

//#############################################################################
void mwMachDefDLG::EnableOrDisable(const MachDefFactory type)
{
	GetDlgItem(IDC_CMB_MACH_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE || type == post::mwMachDefBuilder::AUTODETECT_SEMIAUTOMATIC ? TRUE : FALSE);
	GetDlgItem(IDC_LBL_MACH_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE || type == post::mwMachDefBuilder::AUTODETECT_SEMIAUTOMATIC ? TRUE : FALSE);

	GetDlgItem(IDC_LBL_ROT_AXIS_DIR_0)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_LBL_ROT_AXIS_DIR_1)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_LBL_ROT_AXIS_DIR_2)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	if ( GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_0) != NULL )
		GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_0)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	if ( GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_1) != NULL )
		GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_1)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	if ( GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_2) != NULL )
		GetDlgItem(IDC_LBL_ROT_AXIS_BASE_PNT_2)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_0_MIN)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_0_MAX)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MIN)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MAX)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_2_MIN)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_2_MAX)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	GetDlgItem(IDC_EDT_ROT_AXIS_0_X)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_0_Y)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_0_Z)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_1_X)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_1_Y)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_1_Z)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_2_X)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_2_Y)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_2_Z)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_0_X)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_0_Y)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_0_Z)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_1_X)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_1_Y)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_1_Z)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_2_X)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_2_Y)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_ROT_AXIS_BASE_POINT_2_Z)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	//GetDlgItem(IDC_LBL_THE_THIRD_ANGLE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	//GetDlgItem(IDC_EDT_THE_THIRD_ANGLE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_LBL_MACH_NUMBER_OF_AXES)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	GetDlgItem(IDC_CMB_MACH_KINEMATIC)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	
	GetDlgItem(IDC_EDT_TRANS_AXIS_LIMIT_0_MIN)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_TRANS_AXIS_LIMIT_0_MAX)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_TRANS_AXIS_LIMIT_1_MIN)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_TRANS_AXIS_LIMIT_1_MAX)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_TRANS_AXIS_LIMIT_2_MIN)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_TRANS_AXIS_LIMIT_2_MAX)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	
	GetDlgItem(IDC_LBL_SPINDLE_DIR)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	GetDlgItem(IDC_BTN_SPINDLE)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);

	GetDlgItem(IDC_EDT_X_X2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_X_Y2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_X_Z2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	GetDlgItem(IDC_EDT_Y_X2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_Y_Y2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_Y_Z2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	GetDlgItem(IDC_EDT_Z_X2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_Z_Y2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_EDT_Z_Z2 )->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	GetDlgItem(IDC_EDT_1ST_LINEAR_AXIS_NAME)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);

	GetDlgItem(IDC_EDT_2ND_LINEAR_AXIS_NAME)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);

	GetDlgItem(IDC_EDT_3RD_LINEAR_AXIS_NAME)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);

	GetDlgItem(IDC_CMB_1ST_ROTATION_AXIS_NAME)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);

	GetDlgItem(IDC_CMB_2ND_ROTATION_AXIS_NAME)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);

	GetDlgItem(IDC_CMB_3RD_ROTATION_AXIS_NAME)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	
	//const BOOL multi = !m_pMxpParams->GetMxpParamContainer().IsDefaultConfiguration();

	GetDlgItem(IDC_CMB_WORKPIECE)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	GetDlgItem(IDC_LBL_WORKPIECE)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	GetDlgItem(IDC_CMB_HOLDER)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);
	GetDlgItem(IDC_LBL_HOLDER)->EnableWindow(type == post::mwMachDefBuilder::AUTODETECT_FULL ? FALSE : TRUE);

	GetDlgItem(IDC_CMB_1ST_ROT_AXIS_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_CMB_2ND_ROT_AXIS_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_CMB_3RD_ROT_AXIS_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);

	GetDlgItem(IDC_CMB_1ST_TR_AXIS_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_CMB_2ND_TR_AXIS_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_CMB_3RD_TR_AXIS_TYPE)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);	

	GetDlgItem(IDC_BTN_POST_SETTING_REMOVE)->EnableWindow(m_pMxpParams->GetPostDefinitionContainer().GetAllPostDefinitions().size() > 1 ? TRUE : FALSE);	

	GetDlgItem(IDC_CHK_TR_AXIS_DIR_1)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_CHK_TR_AXIS_DIR_2)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
	GetDlgItem(IDC_CHK_TR_AXIS_DIR_3)->EnableWindow(type == post::mwMachDefBuilder::MANUAL_MODE ? TRUE : FALSE);
}
//#############################################################################
void mwMachDefDLG::CheckAxisNames()
{
	// check if there are no duplicates
	std::vector<CString> axis;
	axis.push_back(m_1stLinearAxisName);
	axis.push_back(m_2ndLinearAxisName);
	axis.push_back(m_3rdLinearAxisName);

	size_t numberOfAxes = post::mwMachDefUtils::GetNumbersOfAxes(GetCurrentMachine());
	if(numberOfAxes > 6)
		numberOfAxes = 6;

	if(numberOfAxes > 3)
	{
		axis.push_back(m_1stRotationAxisName);
		if(numberOfAxes > 4)
		{
			axis.push_back(m_2ndRotationAxisName);
			if(numberOfAxes > 5)
				axis.push_back(m_3rdRotationAxisName);
		}
	}
	for (size_t i = 0; i < numberOfAxes - 1; i++)
		for (size_t j = i + 1; j < numberOfAxes; j++)
			if (axis[i] == axis[j])
				throw misc::mwException(0,_T("Duplicate axis name found"));
}
//#############################################################################
void mwMachDefDLG::FixWrongInput()
{
	if(m_NumbersOfAxes == post::mwMachDef::MACHINE_6AXIS && m_BuildingType != 2 && m_RotAxis2X == 0 && m_RotAxis2Y == 0 && m_RotAxis2Z == 0)
	{
		post::mwMachDef::vector3d rotAxisDirection0(m_RotAxis0X,m_RotAxis0Y,m_RotAxis0Z);
		post::mwMachDef::vector3d rotAxisDirection1(m_RotAxis1X,m_RotAxis1Y,m_RotAxis1Z);
		post::mwMachDef::vector3d rotAxisDirection2(m_RotAxis2X,m_RotAxis2Y,m_RotAxis2Z);

		if(rotAxisDirection0.Length2() == 0)
		{
			if(rotAxisDirection1 != post::mwMachDef::vector3d(1., 0., 0.))
				rotAxisDirection0 = post::mwMachDef::vector3d(1., 0., 0.);
			else
				rotAxisDirection0 = post::mwMachDef::vector3d(0., 1., 0.);
		}
		if(rotAxisDirection1.Length2() == 0)
		{
			rotAxisDirection1 = post::mwMachDef::vector3d(0., 1., 0.);
		}
		rotAxisDirection2 = (rotAxisDirection0 % rotAxisDirection1).Normalized();
		m_RotAxis2X = rotAxisDirection2.x();
		m_RotAxis2Y = rotAxisDirection2.y();
		m_RotAxis2Z = rotAxisDirection2.z();
	}
}
//#############################################################################
void mwMachDefDLG::SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams)
{
	mwMxpuiDlgsParams::SetParams(pMxpParams);
}
//#############################################################################
void mwMachDefDLG::OnBnClickedBtnSpindle()
{
	post::mwPostDefinitionPtr& postDefPtr = GetCurrentPostDefinition();
	post::mwSpindles spindles = postDefPtr->GetSpindles();
	mwSpindleParentDlg dlg(spindles, m_pMxpParams, GetInteractor(), this);
	if(dlg.DoModal() == IDOK)
	{
		postDefPtr->SetSpindles(spindles);

		TCHAR szTool[128];
		GetDlgItem(IDC_CMB_HOLDER)->GetWindowText(szTool, 128);
	
		post::mwSpindles::const_iterator it = spindles.begin();
		post::mwSpindles::const_iterator end = spindles.end();
		static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->ResetContent();
		for(; it != end; ++it)
			static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->AddString( it->first.c_str() );

		int nIndex = 0;
		nIndex = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->FindString(nIndex, szTool);
		if(nIndex == CB_ERR)
			static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->SetCurSel(0);
		else
			static_cast<CComboBox * >(GetDlgItem(IDC_CMB_HOLDER))->SetCurSel(nIndex);
	}
}
//#############################################################################
const cadcam::mwVector3d& mwMachDefDLG::ExtractSpindleOrientation()
{
	TCHAR szTool[128];
	GetDlgItem(IDC_CMB_HOLDER)->GetWindowText(szTool, 128);
	const misc::mwstring toolName(szTool);

	post::mwSpindles::const_iterator it = GetCurrentPostDefinition()->GetSpindles().find(toolName);
	post::mwSpindles::const_iterator end = GetCurrentPostDefinition()->GetSpindles().end();

	if(it == end)
	{
		throw mwMxpuiException( mwMxpuiException::UNKNOWN_SPINDLE, _T("mwMachDefDLG::GetParams"));
	}

	return it->second.m_orientation;
}
//#############################################################################
const misc::mwAutoPointer<cadcam::mwVector3d>& mwMachDefDLG::ExtractSpindleDirectionPtr()
{
	TCHAR szTool[128];
	GetDlgItem(IDC_CMB_HOLDER)->GetWindowText(szTool, 128);
	const misc::mwstring toolName(szTool);

	post::mwSpindles::const_iterator it = GetCurrentPostDefinition()->GetSpindles().find(toolName);
	post::mwSpindles::const_iterator end = GetCurrentPostDefinition()->GetSpindles().end();

	if(it == end)
	{
		throw mwMxpuiException( mwMxpuiException::UNKNOWN_SPINDLE, _T("mwMachDefDLG::GetParams"));
	}

	return it->second.m_directionPtr;
}
//#############################################################################
void mwMachDefDLG::OnBnClickedBtnNew()
{
	UpdateData(TRUE); // save current def...

	if(m_postSettingID == _T(""))
	{
		post::mwPostDefinitionContainer::id newId = GenerateNewPostDefinitionID();
		GetCurrentPostDefinition()->SetID(newId);
		m_postSettingID = newId.c_str();
	}

	post::mwPostDefinitionContainer::id newId = GenerateNewPostDefinitionID();
	
	std::vector<misc::mwstring> axisNames;axisNames.push_back(_T("X"));axisNames.push_back(_T("Y"));axisNames.push_back(_T("Z"));axisNames.push_back(_T("C"));axisNames.push_back(_T("B"));

	post::mwPostDefinition postDefinition = *GetCurrentPostDefinition();
	postDefinition.SetID(newId);

	m_pMxpParams->GetPostDefinitionContainer().AddPostDefinition(new post::mwPostDefinition(postDefinition));
	m_pMxpParams->SetPostSettingID(newId);

	UpdateData(FALSE); // update def...
}
//#############################################################################
void mwMachDefDLG::OnBnClickedBtnRemove()
{
	if(m_pMxpParams->GetPostDefinitionContainer().GetAllPostDefinitions().size() > 1)
	{
		post::mwPostDefinitionContainer::id currentId = GetCurrentPostDefinition()->GetID();

		m_pMxpParams->GetPostDefinitionContainer().RemovePostDefinition(currentId);

		currentId = m_pMxpParams->GetPostDefinitionContainer().GetAvailablePostDefinitionsIDs()[0];

		m_pMxpParams->SetPostSettingID(currentId);

		UpdateData(FALSE);// select first definition...
	}
}
//#############################################################################
void mwMachDefDLG::OnCbnDropdownComboPostSettingID()
{
	CString str;
	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_POST_SETTING_ID))->GetWindowText(str);
	if(str != m_postSettingID)
	{
		post::mwPostDefinitionContainer::id ids = str.GetString();
		if(!CheckPostSettingsIDs(ids))
		{
			AfxMessageBox(_T("This Post Definition ID already exist"));
			int newIdNr = 1;
			while (!CheckPostSettingsIDs(ids))
			{
				ids += misc::from_value(newIdNr);
				newIdNr++;
			}
		}

		GetCurrentPostDefinition()->SetID(ids);
		m_pMxpParams->SetPostSettingID(ids);
		UpdateData(FALSE);
	}
}
//#############################################################################
void mwMachDefDLG::OnCbnKillfocusPostSettingID()
{
	CString str;
	static_cast<CComboBox * >(GetDlgItem(GetPostSettingIDComboBox()))->GetWindowText(str);
	if(str != m_postSettingID)
	{
		post::mwPostDefinitionContainer::id ids = str.GetString();
		if(!CheckPostSettingsIDs(ids))
		{
			AfxMessageBox(_T("This Post Definition ID already exist"));
			int newIdNr = 1;
			while (!CheckPostSettingsIDs(ids))
			{
				ids += misc::from_value(newIdNr);
				newIdNr++;
			}
		}
		if(m_pMxpParams->GetPostDefinitionContainer().GetAllPostDefinitions().size() > 0 && ids == _T(""))
		{
			AfxMessageBox(_T("Post Setting ID can not be null if there are more than one post definitions"));

			ids = GenerateNewPostDefinitionID();
		}
	
		m_pMxpParams->GetPostDefinitionContainer().GetPostDefinition(m_postSettingID.GetString()/*old id*/)->SetID(ids/*new id*/);
		m_pMxpParams->SetPostSettingID(ids);
		m_postSettingID = ids.c_str();
	}
}
//#############################################################################
const post::mwPostDefinitionContainer::id mwMachDefDLG::GenerateNewPostDefinitionID() const
{
	post::mwPostDefinitionContainer::id newId = _T("NewPostSettingId");
	int newIdNr = 1;
	while (!CheckPostSettingsIDs(newId))
	{
		newId += misc::from_value(newIdNr);
		newIdNr++;
	}
	return newId;
}
//#############################################################################
void mwMachDefDLG::OnSelchangeComboPostSettingID()
{
	UpdateData(TRUE); // write current def...

	const int id = static_cast<CComboBox * >(GetDlgItem(GetPostSettingIDComboBox()))->GetCurSel();
	post::mwPostDefinitionContainer::id ids = m_pMxpParams->GetPostDefinitionContainer().GetAvailablePostDefinitionsIDs()[id];
	m_pMxpParams->SetPostSettingID(ids);

	UpdateData(FALSE); // extract a new def...
}
//#############################################################################
void mwMachDefDLG::OnSelchangeComboMachineType()
{
	const int machType = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->GetCurSel();

	switch(machType)
	{
	case 0:
		m_NumbersOfAxes = post::mwMachDef::MACHINE_3AXIS;
		break;
	case 1:
		m_NumbersOfAxes = post::mwMachDef::MACHINE_4AXIS;
		break;
	case 2:
		m_NumbersOfAxes = post::mwMachDef::MACHINE_6AXIS_CONTOUR;
		break;
	}

	TCHAR szTool[128];
	GetDlgItem(IDC_CMB_HOLDER)->GetWindowText(szTool, 128);
	const misc::mwstring toolName(szTool);

	if(machType == 2)
	{
		post::mwSpindles spindles = GetCurrentPostDefinition()->GetSpindles();
		post::mwSpindles::iterator it = spindles.find(toolName);
		post::mwSpindles::iterator end = spindles.end();

		for(; it != end; ++it)
		{
			if(it->second.m_directionPtr == MW_NULL)
			{
				it->second.m_directionPtr = new cadcam::mwVector3d(0,0,0);
			}
		}

		GetCurrentPostDefinition()->SetSpindles(spindles);
	}
	else
	{
		post::mwSpindles spindles = GetCurrentPostDefinition()->GetSpindles();
		post::mwSpindles::iterator it = spindles.find(toolName);
		post::mwSpindles::iterator end = spindles.end();

		for(; it != end; ++it)
		{
			if(it->second.m_directionPtr != MW_NULL)
			{
				it->second.m_directionPtr = MW_NULL;
			}
		}

		GetCurrentPostDefinition()->SetSpindles(spindles);
	}	

	SetTextInKinematicComboBox(machType);

	static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->SetCurSel(0);

	SetVisibility();
}
//#############################################################################
void mwMachDefDLG::OnEnChangeEditTurret()
{
	const int machType = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->GetCurSel();
	const int kinematic = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->GetCurSel();
	if(machType == 1 && kinematic == 2)
	{
		CString str;
		GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MIN)->GetWindowText(str);
		GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MAX)->SetWindowText(str);
	}
}
//#############################################################################
void mwMachDefDLG::TurningSetup()
{
	const int machType = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_TYPE))->GetCurSel();
	const int kinematic = static_cast<CComboBox * >(GetDlgItem(IDC_CMB_MACH_KINEMATIC))->GetCurSel();
	if(machType == 1)
	{
		if(kinematic == 2)
			GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MAX)->EnableWindow(FALSE);
		else
			GetDlgItem(IDC_EDT_ROT_AXIS_LIMIT_1_MAX)->EnableWindow(TRUE);
	}
}
//#############################################################################
int mwMachDefDLG::GetPostSettingIDComboBox() 
{
	if(m_pMxpParams->GetPostSettingEditing())
		return IDC_CMB_POST_SETTING_ID_STATIC;
	else
		return IDC_CMB_POST_SETTING_ID;
}
//#############################################################################