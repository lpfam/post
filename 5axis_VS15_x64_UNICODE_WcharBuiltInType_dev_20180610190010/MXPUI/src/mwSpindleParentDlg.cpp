/******************************************************************************
*               File: mwSpindleParentDlg.cpp								  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  17.05.2010 Created by: Tolbariu Ionut-Irinel                               *
*******************************************************************************
*               (C) 2003-2010 by ModuleWorks GmbH                             *
******************************************************************************/
//#############################################################################
#include "StdAfx.h"
#include "mwSpindleParentDlg.hpp"
//#include "mwFileSystem.hpp"
//#include "shlwapi.h"
//#include "mwStringOps.hpp"
#include "mwMessages.hpp"
//#############################################################################
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//#############################################################################
controlerMwSpindleChildDlg::controlerMwSpindleChildDlg(
	mwSpindleParentDlg& dlg,
	const misc::mwstring name, 
	const cadcam::mwVector3d& orientation,
	const misc::mwAutoPointer<cadcam::mwVector3d>& directionPtr,/*only for Contour machine*/
	CWnd* pParent) 
		: mwSpindleChildDlg(name, orientation, directionPtr, pParent)
		, m_dlg(dlg)
{
}
void controlerMwSpindleChildDlg::SetCntrlText(int viItemID, int viTextID)
{
#ifdef MCAM_CONFIG
	UNREFERENCED_PARAMETER(viItemID);
	UNREFERENCED_PARAMETER(viTextID);
#else
	SetDlgItemText(viItemID, m_dlg.GetInteractor().GetMsg(viTextID, TXT_SECTION).c_str());
#endif
}
//#############################################################################
BEGIN_MESSAGE_MAP(controlerMwSpindleChildDlg, mwSpindleChildDlg)
	//{{AFX_MSG_MAP(CMPSEditorDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_REMOVE, &controlerMwSpindleChildDlg::OnBnClickedBtnRemove)
END_MESSAGE_MAP()
//#############################################################################
void controlerMwSpindleChildDlg::OnBnClickedBtnRemove()
{
	if(AfxMessageBox(_T("Remove this entry ?"), MB_YESNO | MB_ICONWARNING) == IDYES)
	{
		m_dlg.Remove(*this);
		mwSpindleChildDlg::OnBnClickedBtnRemove();
	}
}
//#############################################################################
mwSpindleParentDlg::mwSpindleParentDlg(
	post::mwSpindles& spindles, 
	misc::mwAutoPointer<mwMxpParams>& pMxpParams,
	const mwMxpuiParamInteractor &rInteractor,
	CWnd* pParent /*=MW_NULL*/)
	: mwMxpuiDlgsParams(pMxpParams, rInteractor)
	, m_spindles(spindles)
	, CDialog(mwSpindleParentDlg::IDD, pParent)
	, m_z(0) // children dialog size (hight)
{
	//{{AFX_DATA_INIT(mwPatchDlg)
	//}}AFX_DATA_INIT
}
//#############################################################################
void mwSpindleParentDlg::DoDataExchange(CDataExchange* pDX)
{
	if (pDX->m_bSaveAndValidate == NULL)
	{
		SetTexts();
	}
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwPatchDlg)
	//}}AFX_DATA_MAP
}
//#############################################################################
BEGIN_MESSAGE_MAP(mwSpindleParentDlg, CDialog)
	//{{AFX_MSG_MAP(mwPatchDlg)
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_ADD, &mwSpindleParentDlg::OnBnClickedBtnAdd)
	ON_BN_CLICKED(IDOK, &mwSpindleParentDlg::OnBnClickedOk)
END_MESSAGE_MAP()
//#############################################################################
BOOL mwSpindleParentDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	UpdateDlg();

#ifndef MCAM_CONFIG
	this->SetWindowText(GetText(MSG_DLG_SPINDLE_PAREN).c_str());
#endif //
	return TRUE;  // return TRUE  unless you set the focus to a control
}
//#############################################################################
void mwSpindleParentDlg::OnBnClickedBtnAdd()
{
	misc::mwAutoPointer<cadcam::mwVector3d> directionPtr = MW_NULL;
	if(m_spindles.size() && m_spindles.begin()->second.m_directionPtr != MW_NULL)
	{
		directionPtr = new cadcam::mwVector3d(0,0,0);
	}
	Add(_T(""), cadcam::mwVector3d(0,0,0));
	if(m_contrPtrVect.size() > 1)
		m_contrPtrVect[0]->EnableRemoveButton(true);
}
//#############################################################################
void mwSpindleParentDlg::Add(
	const misc::mwstring name, 
	const cadcam::mwVector3d& orientation,
	const misc::mwAutoPointer<cadcam::mwVector3d>& directionPtr/*only for Contour machine*/)
{
	controlerPtr cptr = new controlerMwSpindleChildDlg(*this, name, orientation, directionPtr, MW_NULL);

	cptr->Create(mwSpindleChildDlg::IDD, this);

	tagRECT lprectFirst;
	this->GetWindowRect(&lprectFirst);
	if(m_z == 0)
	{
		tagRECT lprectSecond;
		cptr->GetWindowRect(&lprectSecond);
		m_z = lprectSecond.bottom - lprectSecond.top;
	}

	MovePosition(IDC_BTN_ADD, 0, m_z);
	MovePosition(IDOK, 0, m_z);
	MovePosition(IDCANCEL, 0, m_z);
	
	cptr->SetWindowPos(MW_NULL, 0, 32 + m_z * static_cast<int>(m_contrPtrVect.size()), 0, 0, SWP_NOSIZE | SWP_NOREPOSITION);
	cptr->ShowWindow(SW_SHOW);
	m_contrPtrVect.push_back(cptr);

	tagRECT lprect;
	this->GetWindowRect(&lprect);
	this->MoveWindow(lprect.left, lprect.top, lprect.right - lprect.left, lprect.bottom - lprect.top + m_z);
}
//#############################################################################
void mwSpindleParentDlg::MovePosition(int ID, int cx, int cy)
{
	CRect rect;
	GetDlgItem(ID)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	GetDlgItem(ID)->SetWindowPos(MW_NULL, rect.left + cx, rect.top + cy,
		0,0, SWP_NOSIZE | SWP_NOZORDER);
}
//#############################################################################
void mwSpindleParentDlg::Remove(controlerMwSpindleChildDlg& address)
{
	controlerPtrVect::iterator it = m_contrPtrVect.begin();
	controlerPtrVect::iterator end = m_contrPtrVect.end();
	for(unsigned int i = 0; it != end; ++it, i++)
		if(**it == address)
		{
			m_contrPtrVect.erase(it);
			Resize(i);
			if(m_contrPtrVect.size() < 2)
			{
				m_contrPtrVect[0]->EnableRemoveButton(false);
			}
			InvalidateRect(MW_NULL, FALSE);
			return;
		}
}
//#############################################################################
void mwSpindleParentDlg::UpdateDlg()
{
	post::mwSpindles::const_iterator it = m_spindles.begin();
	post::mwSpindles::const_iterator end = m_spindles.end();

	for(; it != end; ++it)
		Add(it->first, it->second.m_orientation, it->second.m_directionPtr);

	if(m_contrPtrVect.size() < 2)
	{
		m_contrPtrVect[0]->EnableRemoveButton(false);
	}
}
//#############################################################################
void mwSpindleParentDlg::OnBnClickedOk()
{
	//
	UpdateData();
	for(size_t i = 0; i < m_contrPtrVect.size(); i++)
		m_contrPtrVect[i]->UpdateData();

	if(FindDuplicate())
	{
		AfxMessageBox(_T("Error: Duplicate entries found."), MB_ICONERROR);
		return;
	}
	if(FindNullVector())
	{
		AfxMessageBox(_T("Error: Vector with length 0 found."), MB_ICONERROR);
		return;
	}
	if(FindInvalideName())
	{
		AfxMessageBox(_T("Error: Invalide name."), MB_ICONERROR);
		return;
	}

	bool updateS = true;
	if(m_contrPtrVect.size() == 0)
		updateS = AfxMessageBox(_T("The spindle is missing.\nDo you want to continue?"), MB_YESNO | MB_ICONWARNING) == IDYES;

	if(updateS)
	{
		m_spindles.clear();
		for(unsigned int i = 0; i < m_contrPtrVect.size(); i++)
		{
			m_contrPtrVect[i]->UpdateData();
			m_spindles[m_contrPtrVect[i]->GetName()] = post::mwSpindle(m_contrPtrVect[i]->GetOrientation(), m_contrPtrVect[i]->GetDirectionPtr());
		}
	}
	//
	OnOK();
}
//#############################################################################
void mwSpindleParentDlg::Resize(unsigned int start)
{
	for(unsigned int i = start; i < m_contrPtrVect.size(); i++)
	{
		CRect rect;
		m_contrPtrVect[i]->GetWindowRect(&rect);
		ScreenToClient(&rect);
		//unsigned int z = rect.bottom - rect.top;
		
		m_contrPtrVect[i]->SetWindowPos(MW_NULL, rect.left, rect.top - m_z, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}

	MovePosition(IDC_BTN_ADD, 0, -m_z);
	MovePosition(IDOK, 0, -m_z);
	MovePosition(IDCANCEL, 0, -m_z);

	tagRECT lprect;
	this->GetWindowRect(&lprect);
	this->MoveWindow(lprect.left, lprect.top, lprect.right - lprect.left, lprect.bottom - lprect.top - m_z);
}
//#############################################################################
const bool mwSpindleParentDlg::FindDuplicate()
{
	for(size_t i = 0; i < m_contrPtrVect.size(); i++)
		for(size_t j = i + 1; j < m_contrPtrVect.size(); j++)
		{
			if(m_contrPtrVect[i]->GetName() == m_contrPtrVect[j]->GetName())
			{
				return true;
			}
		}
	return false;
}
//#############################################################################
const bool mwSpindleParentDlg::FindInvalideName()
{
	for(size_t i = 0; i < m_contrPtrVect.size(); i++)
		if(m_contrPtrVect[i]->GetName() == _T(""))
		{
			return true;
		}
		
	return false;
}
//#############################################################################
const bool mwSpindleParentDlg::FindNullVector()
{
	for(size_t i = 0; i < m_contrPtrVect.size(); i++)
		if(m_contrPtrVect[i]->GetOrientation() == cadcam::mwVector3d(0,0,0) && m_contrPtrVect[i]->GetDirectionPtr().IsNotNull() && *m_contrPtrVect[i]->GetDirectionPtr() == cadcam::mwVector3d(0,0,0))
			return true;
			
	return false;
}
//#############################################################################
void mwSpindleParentDlg::SetTexts()
{
	SetCntrlText(IDC_LBL_SPINDLE_SETTINGS_ID, MSG_LBL_SPINDLE_SETTINGS_ID);
	SetCntrlText(IDC_BTN_SPINDLE_SETTINGS_ADD, MSG_LBL_SPINDLE_SETTINGS_ADD);
	SetCntrlText(IDCANCEL, MSG_LBL_SPINDLE_CHILD_CANCEL);
	SetCntrlText(IDOK, MSG_LBL_SPINDLE_CHILD_OK);
}
//#############################################################################
