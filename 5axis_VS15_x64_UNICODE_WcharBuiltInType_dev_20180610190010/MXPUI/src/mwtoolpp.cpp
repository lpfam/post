/******************************************************************************
*               File: mwToolPP.cpp											  *
*******************************************************************************
*               Description:implements the mwToolPP class					  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*   11/12/2003 3:15:54 PM Created by: Costin Calisov                          *
*   11/16/2003 17:46:00 Sergej Nevstruyev: added mwPostException.hpp to make  *
*                       it compilable under VS .NET                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "resource.h"
#include "mwMxpuiException.hpp"
#include "mwMessages.hpp"
#include "mwToolPP.hpp"
#include "mwPostException.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// mwToolPP property page

mwToolPP::mwToolPP ( misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor &rInteractor)
: mwMxpuiPage(pMxpParams,rInteractor,IDD,MSG_TOOL_PP_TITLE)
{
	m_ToolLengthCompensation = 0.0;
	m_resetParams = false;
}

mwToolPP::~mwToolPP()
{
}

void mwToolPP::Reset()
{
	m_resetParams = true;
	UpdateData(FALSE);
	m_resetParams = false;
}


void mwToolPP::DoDataExchange(CDataExchange* pDX)
{
	// if initializing dialog items
	if( pDX->m_bSaveAndValidate == NULL )
	{
		GetDlgParams(*pDX);
		SetTexts();
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(mwToolPP)
	DDX_Text(pDX, IDC_EDT_TOOL_LENGTH_COMPENSATION, m_ToolLengthCompensation);
	//}}AFX_DATA_MAP
	
	if( pDX->m_bSaveAndValidate )
	{
		SetDlgParams(*pDX);
	}
}

BEGIN_MESSAGE_MAP(mwToolPP, mwMxpuiPage)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// mwToolPP message handlers

void mwToolPP::GetParams() 
{
	try
	{
		m_ToolLengthCompensation = 0;
		if(m_resetParams == false)
			m_ToolLengthCompensation = m_pMxpParams->GetToolLengthCompensation();
	}
	catch (const post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwToolPP::GetParams"));
	}
}

void mwToolPP::SetParams() 
{
	try
	{
		m_pMxpParams->SetToolLengthCompensation(m_ToolLengthCompensation);
	}
	catch (const post::mwPostException &e)
	{
		throw mwMxpuiException( e, _T("mwToolPP::SetParams"));
	}
}

void mwToolPP::SetTexts()
{	
	SetCntrlText(IDC_LBL_TOOL_LENGTH_COMPENSATION,MSG_LBL_TOOL_LENGTH_COMPENSATION);
}
