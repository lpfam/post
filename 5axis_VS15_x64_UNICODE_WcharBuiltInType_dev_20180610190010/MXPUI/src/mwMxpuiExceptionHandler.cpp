/******************************************************************************
*               File: mwMxpuiExceptionHandler.cpp							  *
*******************************************************************************
*               Description:implements the mwMxpuiExceptionHandler class	  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 12:42:10 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#include "StdAfx.h"
#include "mwMxpuiException.hpp"
#include "mwException.hpp"
#include "mwMxpuiParamInteractor.hpp"
#include "mwMessages.hpp"
#include "mwStringConversions.hpp"
#include "mwMxpuiExceptionHandler.hpp"
#include "mwMfcUIUtilsException.hpp"
//#############################################################################
void mwMxpuiExceptionHandler::HandleErrInDlg(const mwMxpuiException &rException,const mwMxpuiParamInteractor &rInteractor, CDataExchange& rDX)
{
	HandleErr(rException,rInteractor);
	rDX.Fail();
}

//#############################################################################
void mwMxpuiExceptionHandler::HandleErrInDlg(CDataExchange& rDX)
{
	AfxMessageBox(_T("Unknown error"));
	rDX.Fail();
}

//#############################################################################
void mwMxpuiExceptionHandler::HandleErr(const mwMxpuiException &rException,const mwMxpuiParamInteractor &rInteractor)
{
	AfxMessageBox( GetErrorMsg(rException,rInteractor).c_str() );
}
//#############################################################################
void mwMxpuiExceptionHandler::HandleErr(const mwMfcUIUtilsException &rException,const mwMxpuiParamInteractor &rInteractor)
{
	AfxMessageBox( GetErrorMsg(rException,rInteractor).c_str() );
}
//#############################################################################
const misc::mwstring mwMxpuiExceptionHandler::GetErrorMsg(const mwMxpuiException &rException,const mwMxpuiParamInteractor &rInteractor)
{
	misc::mwstring errMsg;
	try
	{
		switch (rException.GetType())
		{
			case mwMxpuiException::LOCAL :
				errMsg=rInteractor.GetMsg(rException.GetErrorCode(),TXT_SECTION_EXCEPTION);
			break;
			case mwMxpuiException::MXP_EXCEPTION :
				errMsg=rInteractor.GetMsg(rException.GetErrorCode(),TXT_SECTION_EXTERNAL_EXCEPTION);
			break;
			default :
				errMsg= _T("The following error of an unknown type had occur:\n") + rException.GetErrorMessage();
			break;
		};
	}
	catch(misc::mwException& localExp)
	{
		errMsg= localExp.GetCompleteErrorMessage();
	}
/*
	#pragma message( "TODO: following lines should be wrote in a log file not added to user MSG" )
	
	const misc::mwException::exceptionStack &stack = 
		rException.GetExceptionStack();
	
	for(unsigned int i = 0; i < stack.size(); i++ )
	{
		errMsg += _T("( ");
		errMsg += stack[i].second;
		errMsg += _T(" )");
	}
*/
	return errMsg;
}
//#############################################################################
const misc::mwstring mwMxpuiExceptionHandler::GetErrorMsg(
	const mwMfcUIUtilsException &rException,
	const mwMxpuiParamInteractor &/*rInteractor*/)
{
	misc::mwstring errMsg = rException.GetCompleteErrorMessage();
	return errMsg;
}
//#############################################################################
