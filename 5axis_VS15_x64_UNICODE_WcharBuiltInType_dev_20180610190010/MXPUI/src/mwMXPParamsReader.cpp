/******************************************************************************
*               File: mwMXPParamsReader.cpp								      *
*******************************************************************************
*               Description:this module describe the mwMXPParamsReader        *
*               class :                                                       *
*******************************************************************************
*               History:                                                      *
*  18.05.2009   15:45:00    Created by: Tolbariu Ionut Irinel                 *
*******************************************************************************
*               (C) 2003 - 2009 by ModuleWorks GmbH                           *
******************************************************************************/
//#############################################################################
#include "StdAfx.h"
#include "mwMXPParamsReader.hpp"
#include "mwMachDefReader.hpp"
#include "mwPostException.hpp"
#include "mw5axMachDef.hpp"
//#############################################################################
mwMXPParamsReader::mwMXPParamsReader(
	post::mwPostDefinitionContainer& machDefCont,
	post::mwPostDefinitionContainer::id& id,
	mwTransfMatrixPtr& transfMatrixPtr)
		: m_machDefCont(machDefCont)
		, m_id(id)
		, m_transfMatrixPtr(transfMatrixPtr)
{
}
//#############################################################################
void mwMXPParamsReader::Read(
	const misc::mwstring& pathToXML, 
	const misc::mwstring& postSettingsId,
	const post::mwStringPairVectPtr& adapters,
	const bool doValidations,
	const bool readAll,
	const misc::mwstring& machSimResourceDllPath
	)
{
	post::mwMachDefBuilder machDefBlt;

	mwMachDefReader machDefReader(
		pathToXML, 
		postSettingsId, 
		false,
		false,
		adapters,
		true,
		false,
		false,
		_T(""),
		_T(""),
		readAll,
		machSimResourceDllPath
		);

	try
	{
		machDefReader.Read();
	}
	catch(post::mwPostException e)
	{
		SetDefaultValue();
		m_translationAxisFound = machDefReader.GetTranAxisFound();
		m_rotationAxisFound = machDefReader.GetRotAxisFound();
		throw;
	}
	m_machDefCont = machDefReader.GetAllDefinitions();
	
	m_id = machDefReader.GetCurrentPostSettingsID();
	
	m_translationAxisFound = machDefReader.GetTranAxisFound();
	m_rotationAxisFound = machDefReader.GetRotAxisFound();
	
	m_transfMatrixPtr = machDefReader.GetMachDefBuilder().GetTransfMatrixVect();

	
	if(doValidations)
		m_machDefCont.GetPostDefinition(machDefReader.GetCurrentPostSettingsID())->GetMachDefPtr()->Validate();
}
//#############################################################################
void mwMXPParamsReader::SetDefaultValue()
{
	std::vector<misc::mwstring> temp;
	temp.push_back(_T("X"));
	temp.push_back(_T("Y"));
	temp.push_back(_T("Z"));
	temp.push_back(_T("C"));
	temp.push_back(_T("B"));

	post::mwPostDefinition postDefinition(
		new post::mw5axMachDef(),
		post::mwMachDefBuilder::MANUAL_MODE,
		temp, 
		post::mwMachDynamics(),
		_T("tool"),
		_T("workpiece"),
		post::HEAD,
		post::HEAD,
		post::HEAD,
		post::mwPostDefinition::GenerateDefaultSpindle(new post::mw5axMachDef()),
		_T(""));

	m_machDefCont.AddPostDefinition(new post::mwPostDefinition(postDefinition));
}//#############################################################################