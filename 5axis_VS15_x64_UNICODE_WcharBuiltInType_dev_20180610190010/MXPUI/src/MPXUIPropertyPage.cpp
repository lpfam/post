// MPXUIPropertyPage.cpp : implementation file
//

#include "StdAfx.h"
#include "MXPUI.h"
#include "MPXUIPropertyPage.h"
//#include "MachDefDlgResource.h"

// MPXUIPropertyPage dialog

IMPLEMENT_DYNAMIC(MPXUIPropertyPage, CPropertyPage)

MPXUIPropertyPage::MPXUIPropertyPage(mwMxpParams &rMxpParams,const mwMxpuiParamInteractor &rInteractor)
	//: CPropertyPage(IDD_MACH_DEF/*MPXUIPropertyPage::IDD*/)
	: mwMxpuiPage(rMxpParams,rInteractor,IDD_MACH_DEF,0)
{

}

MPXUIPropertyPage::~MPXUIPropertyPage()
{
}

void MPXUIPropertyPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(MPXUIPropertyPage, CPropertyPage)
END_MESSAGE_MAP()


// MPXUIPropertyPage message handlers
