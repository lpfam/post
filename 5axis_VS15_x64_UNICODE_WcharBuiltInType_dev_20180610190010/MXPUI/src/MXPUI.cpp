/******************************************************************************
*               File: machsim.cpp										      *
*******************************************************************************
*               Description:implements the machsim class				      *
*                                                                             *
*				This DLL is a extension MFC DLL
*******************************************************************************
*               History:                                                      *
*  10/7/2003 10:55:23 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/

// MXPUI.cpp : Defines the initialization routines for the DLL.
//

#include "StdAfx.h"
#include <afxdllx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef MCAM_CONFIG

#include "./boost/thread/detail/tss_hooks.hpp"

namespace boost
{

	void tss_cleanup_implemented(void)
	{
		// function which should do nothing, implemented this function such as to avoid getting a link error
		// the link error is a warning such as to make sure on_thread_exit() function is called 
		// on thread_detach and on process_detach of dllMain function, of course only in case of dll. 
		// In case of .exe nothing is required to be implemented, except this dummy function to avoid of course the link error.
	}
}
#endif

static AFX_EXTENSION_MODULE MXPUIDLL= { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("MXPUI.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(MXPUIDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(MXPUIDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("MXPUI.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(MXPUIDLL);

#ifdef MCAM_CONFIG
		boost::on_thread_exit();
	}
	else if (dwReason == DLL_THREAD_DETACH)
	{
		boost::on_thread_exit();
#endif 
	}

	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
__declspec(dllexport) void __cdecl InitMXPUI()
{
	// create a new CDynLinkLibrary for this app
	#pragma warning(push)
	#pragma warning(disable:6014) //silence prefast warning because CDynLinkLibrary is cleaned up elsewhere
	new CDynLinkLibrary(MXPUIDLL);
	#pragma warning(pop)

	// nothing more to do
}


