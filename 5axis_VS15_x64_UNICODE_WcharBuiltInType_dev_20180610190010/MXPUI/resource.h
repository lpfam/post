//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MXPUI.rc
//
#define IDC_MACH_DEF_DLG_POSITION       2621
#define IDC_MXP_DEF_DLG_POSITION        2700
#define IDC_DYNAMICS_DEF_DLG_POSITION   2701
#define IDD_WRITER                      3100
#define IDD_TOOL                        3101
#define IDD_MACH_DEF_PP                 3102
#define IDD_MXP_PP                      3103
#define IDD_DYNAMICS_PP                 3104
#define IDC_MACH_DYNAMICS_DLG_POSITION  4131
#define IDC_LBL_TOOL_LENGTH_COMPENSATION 4066
#define IDC_EDT_TOOL_LENGTH_COMPENSATION 4067
#define IDC_RESET_DEFAULT               4132
#define IDC_LBL_FEED_RATE               5176
#define IDC_LBL_RAPID_RATE              5177
#define IDC_LBL_TOOL_CHANGE_TIME        5178
#define IDC_LBL_TOOL_CHANGE_TIME_UNITS  5179
#define IDC_EDT_FEED_RATE               5180
#define IDC_EDT_RAPID_RATE              5181
#define IDC_EDT_TOOL_CHANGE_TIME        5182
#define IDC_LBL_FEED_RATE_UNITS         5183
#define IDC_LBL_RAPID_RATE_UNITS        5184

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        3015
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         5185
#define _APS_NEXT_SYMED_VALUE           3050
#endif
#endif
