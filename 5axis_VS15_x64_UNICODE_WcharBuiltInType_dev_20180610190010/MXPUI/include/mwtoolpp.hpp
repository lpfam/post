/******************************************************************************
*               File: mwToolPP.h											  *
*******************************************************************************
*               Description:this module describe the mwToolPP class			  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  11/12/2003 3:13:10 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwToolPP_h__
#define __mwToolPP_h__

#include "mwMxpuiPage.hpp"

	//! implements a property page for setting tool related parameters
	/*! This class is derived from mwMxpuiPage
	*/

class mwToolPP : public mwMxpuiPage
{
public:
	//! Constructor
	/*! 
		\param rMxpParams the params to be setup by this property page
		\param rInteractor the interactor between property page and outside world
	*/
	mwToolPP(misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor& rInteractor);   

	//! Destructor
	/*! 
	*/
	~mwToolPP();

	void Reset();

// Dialog Data
	enum { IDD = IDD_TOOL };

// Overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX) ;    // DDX/DDV-Support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()

	//! see mwMxpuiPage::GetParams
	/*!	
	*/
	void GetParams() ;

	//! see mwMxpuiPage::SetParams
	/*!	
	*/
	void SetParams() ;

	//! see mwMxpuiPage::SetTexts
	/*!	
	*/
	void SetTexts();

protected:
	double m_ToolLengthCompensation;
	bool m_resetParams;
};

#endif //__mwToolPP_h__