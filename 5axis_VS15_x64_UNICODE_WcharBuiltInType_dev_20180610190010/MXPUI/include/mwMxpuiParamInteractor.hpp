/******************************************************************************
*               File: mwMxpuiParamInteractor.hpp							  *
*******************************************************************************
*               Description:this module describe the mwMxpuiParamInteractor   *
*                           class and mwMxpuiDummyParamInteractor class       *
*******************************************************************************
*               History:                                                      *
*  9/2/2003 3:18:29 PM Created by: Costin Calisov                             *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiParamInteractor_hpp__
#define __mwMxpuiParamInteractor_hpp__
//#############################################################################
#include "mwStringConversions.hpp"
#include "mwUnitsFactory.hpp"
#include "MXPUIDef.h"
//#############################################################################
	//! this class implements the functions for interaction between Mxpui params and out side world
	/*! 			
	*/
//#############################################################################
class MXPUI_API mwMxpuiParamInteractor{
public:
	typedef enum measures::mwUnitsFactory::Units					Units;
	virtual const misc::mwstring GetMsg( unsigned long msgNr, const misc::mwstring &section ) const = 0;
	virtual Units GetUnits() const = 0;
	//#############################################################################
	mwMxpuiParamInteractor(const HINSTANCE& neutralResHandler)
		: m_neutralResHandler(neutralResHandler)
	{
	}
	//#############################################################################
	void SetMxpuiNeutralResHandler(const HINSTANCE& neutralResHandler)
	{
		m_neutralResHandler = neutralResHandler;
	}
	//#############################################################################
	const HINSTANCE GetMxpuiNeutralResHandler() const
	{
		return m_neutralResHandler;
	}
	//#############################################################################
protected:
	//#############################################################################
	HINSTANCE m_neutralResHandler;
	//#############################################################################
};
//#############################################################################
//#############################################################################
//#############################################################################
class MXPUI_API mwMxpuiDummyParamInteractor : public mwMxpuiParamInteractor
{
public:
	//#############################################################################
	mwMxpuiDummyParamInteractor(const HINSTANCE& neutralResHandler)
		: mwMxpuiParamInteractor(neutralResHandler)
	{
	}
	//#############################################################################
	inline const misc::mwstring GetMsg( unsigned long /*msgNr*/, const misc::mwstring &/*section*/ ) const
	{
		m_Msg=_T("Implement your message picking function!");
		return m_Msg;
	}
	//#############################################################################
	virtual Units GetUnits() const
	{
		return measures::mwUnitsFactory::METRIC;
	}
	//#############################################################################
private:
	//#############################################################################
	mutable misc::mwstring m_Msg;
	//#############################################################################
};
//#############################################################################
#endif //__mwMxpuiParamInteractor_hpp__
