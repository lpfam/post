/******************************************************************************
*               File: mwMxpDlg.h										      *
*******************************************************************************
*               Description:this module describe the mwMxpDlg class	          *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/8/2003 3:01:01 PM Created by: Costin Calisov                            *
*  4/8/2015 Changed by: Tolbariu Ionut-Irinel                                 *
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#ifndef __mwMxpDlg_h__
#define __mwMxpDlg_h__
//#############################################################################
#include "mwMxpuiDlg.hpp"
#include "MachDefDlgResource.h"
#include "MXPUIDef.h"

//#############################################################################
//! implements a property page for setting MXP params
/*! This class is derived from mwMxpuiDlg
*/

class MXPUI_API mwMxpDlg : public mwMxpuiDlg
{
	//	DECLARE_DYNCREATE(mwMxpDlg)
public:
	//! Constructor
	/*!
	\param rMxpParams the params to be setup by this property page
	\param rInteractor the intercator between property page and outside world
	*/
	mwMxpDlg(misc::mwAutoPointer<mwMxpParams>& pMxpParams, const mwMxpuiParamInteractor& rInteractor, CWnd* pParent = MW_NULL);
	//#############################################################################
	//! Destructor
	/*!
	*/
	~mwMxpDlg();
	//#############################################################################
	// Dialog Data
	//{{AFX_DATA(mwMxpDlg)
	enum { IDD = IDD_MXP };

	BOOL	m_AutoAnglePair;
	int     m_MachineLimits;
	double	m_AngleChangeLimit;
	double  m_PoleAngleTolInDegree;
	double	m_AngleTolInDegForUsingMachLimits;
	int		m_startAngleType;
	int     m_provideTransAxis;
	BOOL    m_customTransAxisDir;
	double  m_customTransAxisDirX;
	double  m_customTransAxisDirY;
	double  m_customTransAxisDirZ;
	int     m_solutionForStartAngle;
	double  m_preferredStartAngleValDeg;
	double  m_firstStartRotationAxisAngle;
	double  m_secondStartRotationAxisAngle;
	double  m_thirdStartRotationAxisAngle;
	int     m_poleHandling;
	BOOL    m_AlignToolpath;
	int     m_interpolationType;
	int     m_firstRotAxisAngleLimit;
	int     m_secondRotAxisAngleLimit;
	int	    m_thirdRotAxisAngleLimit; 
	int     m_coordType2DisplayInSimulator;
	BOOL    m_provideStartRotationAngle;

	double	m_feedRateInterAngStep;
	double  m_rapidRateInterAngStep;
	double  m_feedRateInterDist;
	double	m_rapidRateInterDist;
	BOOL	m_rapidRateInterDistFlag;
	BOOL	m_feedRateInterDistFlag;
	BOOL    m_rapidRateInterAngleFlag;
	BOOL    m_feedRateInterAngleFlag;

	BOOL    m_retractAndRewindFlag;

	BOOL    m_enableMXP;

	BOOL	m_filterDuplicateMoves;
	BOOL    m_forceHeadOrTableRotation;
	BOOL    m_limitLinearAxisTravel;
	BOOL    m_disableAddingAdditionalMoves;

	virtual void SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams);
	void GetParams();
	void SetParams();
	void SetTexts();
	void Reset();
	//#############################################################################
	//}}AFX_DATA
	//#############################################################################
	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(mwMxpDlg)
	//#############################################################################
	//! hides/shows controls
	/*!	updates property page controls based on current member vars values
	*/
	virtual void UpdateControls();
	//#############################################################################
	void HideComboMachineLimits();
	void HideAngleControlLimits();
	void SetVisibility(const post::mwMachDef::NumbersOfAxes& axes);
	void EnableOrDisable(const post::mwMachDef::NumbersOfAxes& axes);
	void InitToolTip();
	void SelectStartAngleTypeDependingOnForceHeadOrTable();
	void SelectStartAngleTypeDependingOnAutoAngle();
	const bool GetRetractToolAtMax();
	void SetRetractToolAtMax(const bool retractToolAtMax);
	//#############################################################################
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Support
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
	//}}AFX_VIRTUAL
	//#############################################################################
	// Implementation
	// Generated message map functions
	//{{AFX_MSG(mwMxpDlg)

	afx_msg void OnSelchangeRadioAnglePair();
	afx_msg void OnSelchangeRadioAutoAnglePair();
	afx_msg void OnSelchangeRadioKeepAnglePair();

	afx_msg void OnSelchangeRadioNameSolution();
	afx_msg void OnSelchangeRadioProvideFirstRotAxisAngle();
	afx_msg void OnSelchangeRadioProvideSecondRotAxisAngle();
	afx_msg void OnSelchangeRadioProvideTransAxisDirection();
	afx_msg void OnSelchangeRadioProvideThirdRotAxisAngle();

	afx_msg void OnSelchangeRadioStandardTransAxisDirection();
	afx_msg void OnSelchangeRadioCustomTransAxisDirection();

	afx_msg void OnChkForceHeadOrTableRotation();
	afx_msg void OnChkLimitLinearAxisTravel();

	afx_msg void OnChkInterpolationForDistance();
	afx_msg void OnChkRapidRateInterpolationForDistance();
	afx_msg void OnChkInterpolationForAngle();
	afx_msg void OnChkRapidRateInterpolationForAngle();

	afx_msg void OnSelchangeComboStartAngleType();
	afx_msg void OnSelchangeComboSolutionForStartAngle();
	afx_msg void OnSelchangeComboPoleHandling();
	afx_msg void OnChkAlignToolpath();
	afx_msg void OnSelchangeComboMachineLimmits();

	afx_msg void OnSelchangeComboFirstRotAxisAngleLim();
	afx_msg void OnSelchangeComboSecondRotAxisAngleLim();
	afx_msg void OnSelchangeComboThirdRotAxisAngleLim();

	afx_msg void OnSelchangeRadioDefault();
	afx_msg void OnSelchangeRadioProvideStartRotationAngle();
	afx_msg void OnSelchangeRadioProvideTransAxisDirection4AX();

	afx_msg void OnChkRetractAndRewind();

	afx_msg void OnBnClickedChkToolpathAlignment();

	afx_msg void OnChkEnableMXP();

	afx_msg void OnChkFilterDuplicateMoves();
	afx_msg void OnChkDisableMXPAddingAdditionalMoves();

	afx_msg void OnSelchangeComboProvideStartSol();
	
	afx_msg void OnClickButtonRetractAndRewindParamDlg();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	//#############################################################################
	enum MXP_StartAngleType
	{
		MXP_SEL_BETW_TWO_SOLUTIONS = 0,
		MXP_USE_FIRST_ROT_ANGLE,
		MXP_USE_SECOND_ROT_ANGLE,
		MXP_PROVIDE_TRANS_AXIS,
		MXP_USE_THIRD_ROT_ANGLE
 	};
	//#############################################################################
	enum MXP_SolutionForStartAngle
	{
		MXP_USE_FIRST_SOLUTION = 0,
		MXP_USE_OTHER_SOLUTION,
		MXP_SOLUTION_CLOSE_TO_0,
		MXP_SOLUTION_NOT_CLOSE_TO_0
	};
	//#############################################################################
	enum MXP_PoleHandling
	{
		FREEZE_ANGLE_AROUND_POLE = 0,
		USE_ROTATION_ANGLE_AROUND_POLE,
		LINEAR_INTERPOLATION,
		SMOOTH_INTERPOLATION
	};
	//#############################################################################
	enum MXP_MachineLimits
	{
		NO_LIMITS = 0, // No limit
		TRANSLATIONAL_LIMITS, // Translation limits
		ROTATIONAL_LIMITS, // Rotation limits
		ALL_LIMITS // Translation + Rotation limits
	};
	//#############################################################################
	enum CD_angleLimit
	{
		CD_NO_LIMIT = 0,
		CD_LIM_BETW_0_AND_360_DEG = 1,
		CD_LIM_BETW_MINUS_180_AND_PLUS_180_DEG = 2,

	};
	//#############################################################################
	enum MXP_InterpolationType
	{
		INTERPOLATION_VECTORS = 0,
		INTERPOLATION_ANGLES, //// RTCP ON
		INTERPOLATION_AXIS_VALUES //// RTCP OFF
	};
	//#############################################################################
	enum MXP_SolutionForStartTranslation
	{
		SST_USE_FIRST_POSITIVE = 0,
		SST_USE_FIRST_NEGATIVE,
		SST_USE_SECOND_POSITIVE,
		SST_USE_SECOND_NEGATIVE,
		SST_USE_THIRD_POSITIVE,
		SST_USE_THIRD_NEGATIVE,
		SST_USE_CUSTOM
	};
	//#############################################################################
	int ConvertStartAngleType(const post::mwMXPParam::StartAngleType& startAngleType);
	//#############################################################################
	int ConvertSolutionForStartAngle(
		const post::mwMXPParam::SolutionForStartAngle& solForStartAngle, 
		const bool isClosestToZero,
		const bool automaticAnglePair,
		const bool otherAnglePair);
	//#############################################################################
	post::mwMXPParam::StartAngleType ConvertStartAngleType(const int startAngleType);
	//#############################################################################
	void ConvertSolutionForStartAngle(
		const int solForStartAngle, 
		const bool automaticAnglePair,
		post::mwMXPParamPtr& params);
	//#############################################################################
	post::mwMXPParam::PoleHandling ConvertPoleHandling(const int poleHandling, const BOOL forceHeadOrTableRotation);
	//#############################################################################
	void ConvertPoleHandling(
		const post::mwMXPParam::PoleHandling poleHandling,
		int& poleHandlingDlg,
		BOOL& forceHeadOrTableRotation);
	//#############################################################################
	post::mwMXPParam::MachineLimits ConvertMachineLimits(const int machineLimits);
	//#############################################################################
	void SetPoleHandlingComboBox();
	//#############################################################################
	post::mwMXPParam::Interpolation ConvertInterpolationType(const int vType);
	//#############################################################################
	int ConvertInterpolationType(const post::mwMXPParam::Interpolation& vType);
	//#############################################################################
	void SetCombosSameText(const int viTextID);
	//#############################################################################
	post::mwPostDefinitionPtr& GetCurrentPostDefinition();
	//#############################################################################
	void EnableAllChildControls(BOOL enable);
	//#############################################################################
	post::mwMXPParam::SolutionForStartTranslation ConvertSolutionForStartTrans(const int solForStartTrans, const BOOL customTransAxisDir);
	//#############################################################################
	const int ConvertSolutionForStartTrans(const post::mwMXPParam::SolutionForStartTranslation solForStartTrans, BOOL& customTransAxisDir);
	//#############################################################################
	const post::mw5axControlDef SetRotAxisAngleLimit();
	//#############################################################################
	void GetRotAxisAngleLimit(const post::mw5axControlDef& axControlDef);
	//#############################################################################
	void GetPreferredStartAngleValByRotAxisAngle();
	//#############################################################################
	void SetPreferredStartAngleValByRotAxisAngle();
	//#############################################################################
	void ConvertMachineLimitsFor3Axis();
	//#############################################################################
	bool m_hideAngleControlLimits;
	//#############################################################################
	bool m_resetParams;

	CToolTipCtrl* m_mxpDlgToolTip;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif //__mwMxpDlg_h__
