/******************************************************************************
*               File: mwMxpuiPage.hpp											  *
*******************************************************************************
*               Description: this module describe the mwMxpuiPage class	      *
*				This class is inherited by each CPropertyPage from	          * 
*				the project													  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 11:40:46 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiPage_hpp__
#define __mwMxpuiPage_hpp__

//#############################################################################
#include "mwMxpuiDlgsParams.hpp"
//#############################################################################
//! implements common properties and functions used by each property page from the project
/*!
*/
#include "MXPUIDef.h"

class MXPUI_API mwMxpuiPage : public CPropertyPage, public mwMxpuiDlgsParams
{
public:
	typedef cadcam::mwTPoint3d< double >		point3d;
	typedef cadcam::mwTPoint3d< double >		vector3d;

	//! Constructor
	/*!
		\param rMxpParams params object;
		\param rInteractor the interactor between property page and outside world
		\param vIDD the id from resource file corresponding to the current property page
		\param vTitle the message id from the text file corresponding to the property page title
	*/
	mwMxpuiPage(misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor &rInteractor,int vIDD,int vTitle);

	~mwMxpuiPage();

	virtual void Reset() = 0;
	virtual BOOL HasResetOption();

//#############################################################################
protected:
	//{{AFX_VIRTUAL(mw5axuiPage)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(mwMxpuiPage)
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
//#############################################################################
	//! see mwMxpuiDlgsParams::SetCntrlText
	/*!	
	*/
	inline void SetCntrlText(int vItemID,int vTextID)
		{
#ifdef MCAM_CONFIG
		UNREFERENCED_PARAMETER(vItemID);
		UNREFERENCED_PARAMETER(vTextID);
#else
		SetDlgItemText( vItemID ,  GetText(vTextID).c_str() );
#endif
	}

//#############################################################################
//! see mwMxpuiDlgsParams::SetParams
/*!	
*/
	virtual void SetParams() = 0;
//#############################################################################
	//! see mwMxpuiDlgsParams::GetParams
	/*!	
	*/
	virtual void GetParams() = 0;
//#############################################################################
	//! see mwMxpuiDlgsParams::SetTexts
	/*!	
	*/
	virtual void SetTexts() = 0;
//#############################################################################
	void PaintBMP(int vResourceID, unsigned int vCntrID);
//#############################################################################
	void SetRCImage(const int rcImageID, 
		unsigned int rcImgWndID,
		const misc::mwstring& locationToSetImage = _T(""));
//#############################################################################
	int m_IDD; //Page IDD used in function OnHelpInfo

	misc::mwstring mTitle;//control caption displayed in title bar

	const misc::mwstring m_rcImagesCustomType;

	bool m_dlgInitialized;
//#############################################################################
};

#endif //__mwMxpuiPage_hpp__
