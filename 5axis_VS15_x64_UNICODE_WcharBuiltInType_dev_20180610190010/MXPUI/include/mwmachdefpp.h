/******************************************************************************
*               File: mwMachDefPP.h											  *
*******************************************************************************
*               Description:this module describe the mwMachDefPP class		  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/7/2003 10:37:28 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMachDefPP_h__
#define __mwMachDefPP_h__
//#############################################################################
#include "mwMxpuiPage.hpp"
#include "mwMachDefDLG.h"
#include "mwAutoPointer.hpp"
#include "resource.h"
#include "MXPUIDef.h"

//#############################################################################
	//! implements a property page for setting machine definition params
	/*! This class is derived from mwMxpuiPage
	*/
class MXPUI_API  mwMachDefPP : public mwMxpuiPage
{
//	DECLARE_DYNCREATE(mwMachDefPP)
public:
	//! Constructor
	/*! 
		\param rMxpParams the params to be setup by this property page
		\param rInteractor the interactor between property page and outside world
	*/
	mwMachDefPP(misc::mwAutoPointer<mwMxpParams>& pMxpParams, const mwMxpuiParamInteractor &rInteractor);   
//#############################################################################
	//! Destructor
	/*! 
	*/
	~mwMachDefPP();
//#############################################################################
	virtual void Reset();
	virtual BOOL HasResetOption();
//#############################################################################
// Dialog Data
	//{{AFX_DATA(mwMachDefPP)
	UINT IDD;
	//}}AFX_DATA
//#############################################################################
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(mwMachDefPP)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX) ;    // DDX/DDV-Support
	//}}AFX_VIRTUAL
//#############################################################################
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(mwMachDefPP)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	virtual void SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams);

//#############################################################################
	//! see mwMxpuiPage::GetParams
	/*!	
	*/
	void GetParams() ;
//#############################################################################
	//! see mwMxpuiPage::SetParams
	/*!	
	*/
	void SetParams() ;
//#############################################################################
	//! see mwMxpuiPage::SetTexts
	/*!	
	*/
	void SetTexts();
//#############################################################################
private:
//#############################################################################
	//! adds machine definition subpages to the current page
	/*!	adds machine definition subpages to the current page
	*/
	void AddMachDefSubPages();
//#############################################################################
	//! adds a specifyed machine definition subpage to a specifyed position
	/*!	adds a specifyed machine definition subpage to a specifyed position
			\param rDlg the machine definition subpage to be added
			\param vX the x coordinate
			\param vY the y coordinate
	*/
	void AddMachDefSubPage(mwMachDefDLG& rDlg, int vX,int vY);
//#############################################################################
	misc::mwAutoPointer<mwMachDefDLG>	m_MachDefDlg;


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif //__mwMachDefPP_h__
