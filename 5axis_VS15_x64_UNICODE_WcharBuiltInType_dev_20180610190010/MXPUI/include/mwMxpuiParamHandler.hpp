/******************************************************************************
*               File: mwMxpuiParamHandler.hpp									  *
*******************************************************************************
*               Description:this module describe the mwMxpuiParamHandler class	  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 12:49:05 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiParamHandler_hpp__
#define __mwMxpuiParamHandler_hpp__

#include "MXPUIDef.h"
#include "mwTransfMatrix.hpp"
//#############################################################################
class mwMxpParams;
class mwMxpuiParamInteractor;
class mwMxpParamContainer;
namespace post
{
	class mwPostDefinitionContainer;
	class mw5axControlDef;
	class mwMXPParam;
};
//#############################################################################
	//! input short description
	/*! input detailed description
	\param rInteractor the interactor between Mxpui application and outside world
	*/
class MXPUI_API mwMxpuiParamHandler
{
public:
//#############################################################################	
	//! this function displays the UI and let the user to setup the params (old function)
	/*! 			
			\param rMachDef machine def params to be setup
			\param rControlDef contain informations about angles
			\param rMXPParam MXP params to be setup
			\param rWriter writer params to be setup.
			\param rToolLengthCompensation the tool length compensation value to be setup
			\param rType contains information about how machine definition is extracted, there are 3 possible ways (AUTODETECT_FULL, AUTODETECT_SEMIAUTOMATIC, MANUAL_MODE)
			\param rmachDynamics contains mwMachDynamics obj.
			\param rMxpParamC contains some informations needed for machine definition dialog...
			\returns true if the user setup the params, or false if the user canceled the operation
	*/
	bool SetParams(
		const mwMxpuiParamInteractor &rInteractor,
		post::mwMachDefPtr& rMachDef,
	   post::mw5axControlDef& rControlDef,
	   post::mwMXPParam& rMXPParam, 
	   mwMXPWriterParam& rWriter,
	   double& rToolLengthCompensation, 
	   post::mwMachDefBuilder::MachDefFactory& rType,
	   post::mwMachDynamics& rmachDynamics,
	   mwMxpParamContainer& rMxpParamC);	
//#############################################################################
	//! this function displays the UI and let the user to setup the params (new function)
	/*! 			
			\param rPostDefCont contains a vector of post definitions (every post definition contains a machine definition + some additional informations)
			\param rId identify which of post definition (from rPostDefCont) was selected
			\param rControlDef contain informations about angles
			\param rMXPParam MXP params to be setup
			\param rWriter writer params to be setup.
			\param rToolLengthCompensation the tool length compensation value to be setup
			\param rTransfMatrixPtr defines a map of tools and workpieces and their corresponding matrix
			\param rDisablePostSettingEditing enable/disable post setting editing (if disable there can not be added or removed new post definitions)
			\param rUseMastercamSettings enable a special configuration for mastercam
			\returns true if the user setup the params, or false if the user canceled the operation
	*/
	bool SetParams(
		const mwMxpuiParamInteractor& rInteractor,
		post::mwPostDefinitionContainer& rPostDefCont,
		misc::mwstring& rId,
		post::mw5axControlDef& rControlDef,
		mwMXPWriterParam& rWriter,
		double& rToolLengthCompensation,
		mwTransfMatrixPtr& rTransfMatrixPtr,
		const bool rDisablePostSettingEditing = false,
		const bool rUseMastercamSettings = false,
		const bool rDisableAddingAdditionalMoves = false
		);	
//#############################################################################
	void SetDisableAddingAdditionalMoves(const bool disableAddingAdditionalMoves);
//#############################################################################
	bool GetDisableAddingAdditionalMoves() const;
//#############################################################################
private:
	bool m_disableAddingAdditionalMoves;
//#############################################################################	
};
#endif //__mwMxpuiParamHandler_hpp__
