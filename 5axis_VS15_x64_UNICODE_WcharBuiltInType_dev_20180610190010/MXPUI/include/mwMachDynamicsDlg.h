/******************************************************************************
*               File: mwMachDynamicsDlg.h						              *
*******************************************************************************
*               Description:this module de the mwMachDynamicsDlg class        *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  19.02.2010  Created by: Tolbariu Ionut-Irinel                              *
*  4/8/2015 Changed by: Tolbariu Ionut-Irinel
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#ifndef __mwMachDynamicsDlg_h__
#define __mwMachDynamicsDlg_h__

#include "mwMxpuiDlg.hpp"
#include "MachDefDlgResource.h"

/*! This class is derived from mwMxpuiDlg
*/
class mwMachDynamicsDlg : public mwMxpuiDlg
{
public:
	//! Constructor
	/*!
	\param rMxpParams the params to be setup by this property page
	\param rInteractor the interactor between property page and outside world
	*/
	mwMachDynamicsDlg(mwMxpParamsPtr& pMxpParams, const mwMxpuiParamInteractor& rInteractor, CWnd* pParent = MW_NULL);
	
	//! Destructor
	/*!
	*/
	~mwMachDynamicsDlg();
	
	// Dialog Data
	enum { IDD = IDD_MACH_DYNAMICS };
	
	// Overrides
	virtual void SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams);
	void GetParams();
	void SetParams();
	void SetTexts();
	void Reset();
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Support
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()
	
private:
	void CheckValue();

protected:
	double m_feedRate;
	double m_rapidRate;
	double m_toolChangeTime;

	bool m_resetParams;
};

#endif //__mwMachDynamicsDlg_h__();