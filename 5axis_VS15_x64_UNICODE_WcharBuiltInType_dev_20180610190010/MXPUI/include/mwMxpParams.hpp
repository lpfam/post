/******************************************************************************
*               File: mwMxpuiDlgsParams.hpp									  *
*******************************************************************************
*               Description: this module describe the mwMxpParams class		  *
*				This class is inherited by each CDialog or CPropertySheet from* 
*				the project													  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2009 10:36:33 AM Created by: Gabi SCARLAT                             *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
//#############################################################################
#ifndef __mwMxpParams_hpp__
#define __mwMxpParams_hpp__
//#############################################################################
#include "mwStringConversions.hpp"
#include "mw5axControlDef.hpp"
#include "mwMXPParam.hpp"
#include "mwMXPWriterParam.hpp"
#include "mwMxpuiParamInteractor.hpp"
#include "mwMxpuiException.hpp"
#include "mwPostDefinitionContainer.hpp"
//#############################################################################
#include "MXPUIDef.h"
//#############################################################################
class MXPUI_API mwMxpParams
{
public:
	mwMxpParams(
		const post::mwPostDefinitionContainer& rPostDefCont,
		const post::mwPostDefinitionContainer::id rID,
		const post::mw5axControlDef& rControlDef,
		const mwMXPWriterParam& rWriter,
		const double rToolLengthCompensation,
		const mwTransfMatrixPtr& rTransfMatrixPtr,
		const bool rDisablePostSettingEditing = false,
		const bool rUseMastercamSettings = false,
		const bool rDisableAddingAdditionalMoves = false);

	mwMxpParams(const mwMxpParams& mxpParam);
	mwMxpParams& operator=( const mwMxpParams& right);

	virtual misc::mwAutoPointer<mwMxpParams> Clone() const ;


	virtual void DefaultPostSettings();
	virtual void DefaultMachineDefinition(const misc::mwstring& xmlFileName);
	virtual void SetUnits(measures::mwUnitsFactory::Units units);
	virtual bool ReadMachineDefinition(
		const misc::mwstring& xmlFileName,
		const misc::mwstring& postSettingId=_T(""),
		const post::mwStringPairVectPtr& adapters = MW_NULL,
		const bool throwExceptions = false,
		const bool readAllpostSettingsId = true,
		const misc::mwstring& machSimResourceDllPath = _T("")
		);

	post::mwPostDefinitionContainer& GetPostDefinitionContainer();
	const post::mwPostDefinitionContainer& GetPostDefinitionContainer() const;
	void SetPostDefinitionContainer(const post::mwPostDefinitionContainer& postDefCont);

	post::mw5axControlDef& Get5axControlDef();
	const post::mw5axControlDef& Get5axControlDef() const;
	void Set5axControlDef(const post::mw5axControlDef& controlDef);

	mwMXPWriterParam& GetMXPWriterParam();
	const mwMXPWriterParam& GetMXPWriterParam() const;
	void SetMXPWriterParam(const mwMXPWriterParam& writer);

	double& GetToolLengthCompensation();
	const double& GetToolLengthCompensation() const;
	void SetToolLengthCompensation(const double toolLengthCompensation);

	bool& GetDisableAddingAdditionalMoves();
	const bool& GetDisableAddingAdditionalMoves() const;
	void SetDisableAddingAdditionalMoves(const bool disableAddingAdditionalMoves);

	mwTransfMatrixPtr& GetTransfMatrixPtr();
	const mwTransfMatrixPtr& GetTransfMatrixPtr() const;
	void SetTransfMatrixPtr(const mwTransfMatrixPtr& transfMatrixPtr);

	bool& GetMastercamSettings();
	const bool& GetMastercamSettings() const;
	void SetMastercamSettings(const bool useMastercamSettings);

	bool& GetPostSettingEditing();
	const bool& GetPostSettingEditing() const;
	void SetPostSettingEditing(const bool postSettingEditing);

	post::mwPostDefinitionContainer::id& GetPostSettingID();
	const post::mwPostDefinitionContainer::id& GetPostSettingID() const;
	void SetPostSettingID(const post::mwPostDefinitionContainer::id& id);

	//mastercam use only, special case...
	const post::mwMXPParamPtr& GetMXPParam() const;
	post::mwMXPParamPtr& GetMXPParam();
	void FlushMXPParam();
protected:
	post::mwPostDefinitionContainer m_postDefCont;
	post::mw5axControlDef m_controlDef;
	mwMXPWriterParam m_writer;
	double m_toolLengthCompensation;
	mwTransfMatrixPtr m_transfMatrixPtr;
	post::mwPostDefinitionContainer::id m_id;
	bool m_useMastercamSettings;
	bool m_postSettingEditing;
	post::mwMXPParamPtr m_mxpParamPtr;
	bool m_disableAddingAdditionalMoves;
};
//#############################################################################
typedef misc::mwAutoPointer<mwMxpParams> mwMxpParamsPtr;
//#############################################################################
#endif //__mwMxpParams_hpp__
