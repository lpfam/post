/******************************************************************************
	(C) 2017 by ModuleWorks GmbH
	Author: Robert-Stefan Dumitru
******************************************************************************/

#ifndef __mwRetractAndRewindDlg__hpp__
#define __mwRetractAndRewindDlg__hpp__

#include "mwMxpuiDlgsParams.hpp"
#include "afxwin.h"
#include "..\MachDefDlgResource.h"

class mwRetractAndRewindDlg : public CDialog, public mwMxpuiDlgsParams
{
public:
	mwRetractAndRewindDlg(
		const post::mwMXPParamPtr& mxpParamPtr,
		misc::mwAutoPointer<mwMxpParams>& pMxpParams,
		const mwMxpuiParamInteractor &rInteractor,
		CWnd* pParent = MW_NULL);

	~mwRetractAndRewindDlg();

	// Dialog Data
	//{{AFX_DATA(mwPatchDlg)
	enum { IDD = IDD_RETRACT_REWIND_PARAM };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	inline void SetCntrlText(int viItemID, int viTextID)
	{
#ifdef MCAM_CONFIG
		UNREFERENCED_PARAMETER(viItemID);
		UNREFERENCED_PARAMETER(viTextID);
#else
		SetDlgItemText(viItemID, GetText(viTextID).c_str());
#endif
	};
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(mwPatchDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(mwPatchDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnSelChangeRadioRetractToMachAxLimits();
	afx_msg void OnSelChangeRadioRetractDistance();
	afx_msg void OnChkAdditionalRetract();
	afx_msg void OnSelChangeRadioAdditionalRetractToMachAxLimits();
	afx_msg void OnSelChangeRadioAdditionalRetractDistance();
	afx_msg void OnSelChangeRadioRewindInOneAngleStep();
	afx_msg void OnSelChangeRadioRewindMaxAngle();
	afx_msg void OnBnClickedOk();

	void EnableOrDisable();

	void InitToolTip();
	void SetTexts();
	virtual void SetParams() {};
	virtual void GetParams() {};

	//mwRetractAndRewindParam m_retractAndRewindParam;

	BOOL m_retractToolAtMax;
	BOOL m_additionalRetract;
	BOOL m_additionalRetractToolAtMax;
	BOOL m_retractAndRewindAngleFlag;
	
	double m_retractDistance;
	double m_additionalRetractDistance;
	double m_rewindMaxAngleStep;

	double m_additionalRetractDirectionX;
	double m_additionalRetractDirectionY;
	double m_additionalRetractDirectionZ;
	
	post::mwMXPParamPtr m_mxpParamPtr;

	CToolTipCtrl* m_retractAndRewindDlgToolTip;
};
//{{AFX_INSERT_LOCATION}}

#endif  // __mwRetractAndRewindDlg__hpp__
