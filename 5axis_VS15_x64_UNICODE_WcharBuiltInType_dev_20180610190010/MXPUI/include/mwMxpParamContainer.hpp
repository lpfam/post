/******************************************************************************
*               File: mwMxpParamContainer.hpp	                              *
*******************************************************************************
*               Description: this module describes the						  *
*							mwMxpParamContainer class                         *
*******************************************************************************
*               History:                                                      *
*  17.05.2010 Created by: Tolbariu Ionut-Irinel		                          *
*******************************************************************************
*               (C) 2003-2010 by ModuleWorks GmbH                             *
******************************************************************************/
//#############################################################################
#ifndef __MW_MXP_PARAM_CONTAINER_HPP__
#define __MW_MXP_PARAM_CONTAINER_HPP__
#pragma warning (disable : 4251)
//#############################################################################
#include "MXPUIDef.h"
#include "mwTransfMatrix.hpp"
#include "mwString.hpp"
#include "mwTPoint3d.hpp"
#include "mwPostCommonDefinitions.hpp"
//#############################################################################
class MXPUI_API mwMxpParamContainer
{
public:
	//#############################################################################
	typedef post::mwTranslationPos                             TranslationMountPos;
	typedef post::mwSpindles                                   Spindles;
	//#############################################################################
	//! Constructor
	/*!
		\param spindle defines spindle direction
	*/
	mwMxpParamContainer(const cadcam::mwPoint3d& spindle = cadcam::mwPoint3d(0,0,1));//default constructor. old behavior
	//#############################################################################
	//! Constructor 
	/*!
		\param transfVectros defines all holder and workpieces and thir names
		\param workpieceName defines workpiece name
		\param toolName defines holder name
		\param firstTrAxis defines first translation axis (X) mounting position (HEAD or TABLE)
		\param firstTrAxis defines second translation axis (Y) mounting position (HEAD or TABLE)
		\param firstTrAxis defines third translation axis (Z) mounting position (HEAD or TABLE)
		\param spindles defines all spindles directions and thir names
	*/
	mwMxpParamContainer(
		const mwTransfMatrixPtr& transfVectros,
		const misc::mwstring& workpieceName,
		const misc::mwstring& toolName,
		const TranslationMountPos firstTrAxis,
		const TranslationMountPos secondTrAxis,
		const TranslationMountPos thirdTrAxis,
		const Spindles& spindles);
	//#############################################################################
	//! Gets transformation matrix pointer as const reference
	const mwTransfMatrixPtr& GetTransfMatrixVect() const;
	//#############################################################################
	//! Gets transformation matrix pointer as non-const reference
	mwTransfMatrixPtr& GetTransfMatrixVect();
	//#############################################################################
	//! Sets transformation matrix pointer 
	void SetTransfMatrixVect(const mwTransfMatrixPtr& transfVectros);
	//#############################################################################
	//! Gets workpiece name as const reference
	const misc::mwstring& GetWorkpieceName() const;
	//#############################################################################
	//! Gets workpiece name as non-const reference
	misc::mwstring& GetWorkpieceName();
	//#############################################################################
	//! Sets workpiece name
	void SetWorkpieceName(const misc::mwstring& workpiece);
	//#############################################################################
	//! Gets tool name as const reference
	const misc::mwstring& GetToolName() const;
	//#############################################################################
	//! Gets tool name as non-const reference
	misc::mwstring& GetToolName();
	//#############################################################################
	//! Sets tool name
	void SetToolName(const misc::mwstring& holder);
	//#############################################################################
	//! Return true if default configuration is enable
	const bool IsDefaultConfiguration() const;
	//#############################################################################
	//! Gets first translation axis (X) mounting position (HEAD or TABLE)
	const TranslationMountPos GetFirstTrAxis() const;
	//#############################################################################
	//! Gets first translation axis (X) mounting position (HEAD or TABLE)
	TranslationMountPos& GetFirstTrAxis();
	//#############################################################################
	//! Sets first translation axis (X) mounting position (HEAD or TABLE)
	void SetFirstTrAxis(const TranslationMountPos firstTrAxis);
	//#############################################################################
	//! Gets second translation axis (Y) mounting position (HEAD or TABLE)
	const TranslationMountPos GetSecondTrAxis() const;
	//#############################################################################
	//! Gets second translation axis (Y) mounting position (HEAD or TABLE)
	TranslationMountPos& GetSecondTrAxis();
	//#############################################################################
	//! Sets second translation axis (Y) mounting position (HEAD or TABLE)
	void SetSecondTrAxis(const TranslationMountPos secondTrAxis);
	//#############################################################################
	//! Gets third translation axis (Z) mounting position (HEAD or TABLE)
	const TranslationMountPos GetThirdTrAxis() const;
	//#############################################################################
	//! Gets third translation axis (Z) mounting position (HEAD or TABLE)
	TranslationMountPos& GetThirdTrAxis();
	//#############################################################################
	//! Sets third translation axis (Z) mounting position (HEAD or TABLE)
	void SetThirdTrAxis(const TranslationMountPos thirdTrAxis);
	//#############################################################################
	//! Gets spindle directions
	const Spindles& GetSpindles() const;
	//#############################################################################
	//! Gets spindle directions
	Spindles& GetSpindles();
	//#############################################################################
	//! Sets spindle directions
	void SetSpindles(const Spindles& spindles);
	//#############################################################################
private:
	//#############################################################################
	mwTransfMatrixPtr   m_transfVectros;
	misc::mwstring      m_workpieceName;
	misc::mwstring      m_toolName;
	TranslationMountPos m_firstTrAxis;
	TranslationMountPos m_secondTrAxis;
	TranslationMountPos m_thirdTrAxis;
	Spindles            m_spindles;
	//#############################################################################
};
//#############################################################################
#endif // !defined(__MW_MXP_PARAM_CONTAINER_HPP__)