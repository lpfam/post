/******************************************************************************
*               File: mwSpindleChildDlg.hpp									  *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  17.05.2010 Created by: Tolbariu Ionut-Irinel                               *
*******************************************************************************
*               (C) 2003-2010 by ModuleWorks GmbH                             *
******************************************************************************/
//#############################################################################
#ifndef __MW__SPINDLE_CHILD_DLG__HPP__
#define __MW__SPINDLE_CHILD_DLG__HPP__
//#############################################################################
#include "MachDefDlgResource.h"
#include "mwstring.hpp"
#include "afxwin.h"
#include "mwTPoint3d.hpp"
#include "mwAutoPointer.hpp"
//#############################################################################
class mwSpindleChildDlg : public CDialog
{
	DECLARE_DYNAMIC(mwSpindleChildDlg)

public:
	mwSpindleChildDlg(
		const misc::mwstring name = _T(""), 
		const cadcam::mwVector3d& orientation = cadcam::mwVector3d(0,0,1),
		const misc::mwAutoPointer<cadcam::mwVector3d>& directionPtr = MW_NULL,/*only for Contour machine*/
		CWnd* pParent = MW_NULL);   // standard constructor

	virtual ~mwSpindleChildDlg();

	const misc::mwstring GetName() const
	{
		return misc::mwstring(m_name);
	}
	const cadcam::mwVector3d& GetOrientation() const
	{
		return m_orientation;
	}
	const misc::mwAutoPointer<cadcam::mwVector3d>& GetDirectionPtr() const
	{
		return m_directionPtr;
	}
	// Dialog Data
	enum { IDD = IDD_DLG_SPINDLE_CHILD };
	afx_msg void OnBnClickedBtnRemove();

	void EnableRemoveButton(const bool state);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	// Generated message map functions
	//{{AFX_MSG(mwPatchDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString	m_name;
	double m_x;
	double m_y;
	double m_z;
	cadcam::mwVector3d m_orientation;
	misc::mwAutoPointer<cadcam::mwVector3d> m_directionPtr;/*only for Contour machine*/

	void SetTextMessage();

	BOOL PreTranslateMessage(MSG* pMsg);

	void SetTexts();

	virtual void SetCntrlText(int /*viItemID*/, int /*viTextID*/){}
};
//#############################################################################
#endif //__MW__SPINDLE_CHILD_DLG__HPP__
