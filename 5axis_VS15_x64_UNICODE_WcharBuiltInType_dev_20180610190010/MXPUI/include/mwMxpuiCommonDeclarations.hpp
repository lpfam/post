/******************************************************************************
*               File: mwMxpuiCommonDeclarations.hpp							  *
*******************************************************************************
*               Description: this file stores some declarations common to     *
*							 diffrent classes from the project                *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 10:20:16 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiCommonDeclarations_hpp__
#define __mwMxpuiCommonDeclarations_hpp__

#ifdef MW_MXPUI_EXPORTS
	#define MW_MXPUI_API __declspec(dllexport)
#else
	#define MW_MXPUI_API __declspec(dllimport)
#endif


#endif //__mwMxpuiCommonDeclarations_hpp__