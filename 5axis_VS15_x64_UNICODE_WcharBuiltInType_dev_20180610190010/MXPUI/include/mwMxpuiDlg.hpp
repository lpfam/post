/******************************************************************************
*               File: mwMxpuiDlg.hpp											  *
*******************************************************************************
*               Description: this module describe the mwMxpuiDlg class	      *
*				This class is inherited by each CDialog from the project	  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  05/18/2003 8:52:02 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiDlg_hpp__
#define __mwMxpuiDlg_hpp__

//#############################################################################
#include "mwMxpuiDlgsParams.hpp"
#include "MXPUIDef.h"

//#############################################################################
//! implemets common propertyes and functions used by each dialog from the project
/*!
*/
class MXPUI_API mwMxpuiDlg : public CDialog, public mwMxpuiDlgsParams
{
public:
	//! Consturctor
	/*!
		\param rMxpParams params object;
		\param rInteractor the intercator between dlg and outside world
		\param vIDD the idd from resource file coresponding to the current dialog
		\param pParent pointer to the parrent of this dialog
	*/
	mwMxpuiDlg( misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor &rInteractor,int vIDD, CWnd* pParent=MW_NULL);
//#############################################################################
	//{{AFX_VIRTUAL(mw5axuiDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL
//#############################################################################
protected:
	//! see mwMxpuiDlgsParams::SetCntrlText
	/*!	
	*/
	inline void SetCntrlText(int viItemID,int viTextID)
		{
#ifdef MCAM_CONFIG
		UNREFERENCED_PARAMETER(viItemID);
		UNREFERENCED_PARAMETER(viTextID);
#else
		SetDlgItemText(viItemID, GetText(viTextID).c_str());
#endif
		}
//#############################################################################
	//! see mwMxpuiDlgsParams::SetParams
	/*!	
	*/
	virtual void SetParams() = 0;
//#############################################################################
	//! see mwMxpuiDlgsParams::GetParams
	/*!	
	*/
	virtual void GetParams() = 0;
//#############################################################################
	//! see mwMxpuiDlgsParams::SetTexts
	/*!	
	*/
	virtual void SetTexts() = 0;

};
#endif //__mwMxpuiDlg_hpp__
