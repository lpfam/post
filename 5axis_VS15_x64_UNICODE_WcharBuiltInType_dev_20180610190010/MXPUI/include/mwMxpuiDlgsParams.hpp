/******************************************************************************
*               File: mwMxpuiDlgsParams.hpp									  *
*******************************************************************************
*               Description: this module describe the mwMxpuiDlgsParams class *
*				This class is inherited by each CDialog or CPropertySheet from* 
*				the project													  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 10:36:33 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiDlgsParams_hpp__
#define __mwMxpuiDlgsParams_hpp__
//#############################################################################
#include "mwStringConversions.hpp"
#include "mwMachDef.hpp"
#include "mw5axControlDef.hpp"
#include "mwMXPParam.hpp"
#include "mwMXPWriterParam.hpp"
#include "mwMxpuiParamInteractor.hpp"
#include "mwMxpuiException.hpp"
#include "mwMachDefBuilder.hpp"
#include "resource.h"//for all the pages and dialogs that are extending this class
//#############################################################################
//! implemets common propertyes and functions used by each dialog and proprety sheet form the project
/*!
*/

#include "MXPUIDef.h"
#include "mwMxpParams.hpp"

class MXPUI_API mwMxpuiDlgsParams
{
public:
//#############################################################################
//! stores setable parameters for dialogs
/*!
*/
//#############################################################################
	//! Constructor
	/*!
		\param rMxpParams params object;
		\param rInteractor the intercator between property page/dlg and outside world
	*/
	mwMxpuiDlgsParams( misc::mwAutoPointer<mwMxpParams>& pMxpParams, const mwMxpuiParamInteractor &rInteractor);
//#############################################################################
public:
//#############################################################################
	virtual void SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams);
//#############################################################################
	misc::mwAutoPointer<mwMxpParams>& GetMXPParams();
//#############################################################################
	const mwMxpuiParamInteractor& GetInteractor() const;
//#############################################################################
protected:
//#############################################################################
	//! asks child class to save user settings in m_MxpParams params
	/*!	it's calling child class implementation of virtual function SetParams.
			Catches and handles any mwMxpuiException thrown by the child class during execution of SetParams function,
			by calling mwMxpuiExceptionHandler::HandleErrInDlg function.
			\param rDX the CDataExchange object of child class
	*/
	void SetDlgParams(CDataExchange& rDX) ;
//#############################################################################
	//! asks child class to get the settings from m_MxpParams params and display them to the user
	/*!	it's calling child class implementation of virtual function GetParams.
			Catches and handles any mwMxpuiException thrown by the child class during execution of GetParams function,
			by calling mwMxpuiExceptionHandler::HandleErrInDlg function.
			\param rDX the CDataExchange object of child class
	*/
	void GetDlgParams(CDataExchange& rDX);
//#############################################################################
	//! gets from the text file the message corresponding to the passed number
	/*!	
			\param vTextID the message number
			\returns the message body
	*/
	inline const misc::mwstring GetText(const int vTextID) const;
//#############################################################################
	//! sets a passed control text
	/*!	gets from the text file the message corresponding to the passed number
		and sets it as text of the control indicated by the passed ID
			\param vItemID control ID
			\param vTextID message ID
	*/
	virtual void SetCntrlText(int vItemID,int vTextID)=0;
//#############################################################################
	//! Saves user settings in m_MxpParams params.
	/*!	If there is any invalid param an exception is throw.
			This function is called by SetDlgParams()
			\throws mwMxpuiException if any invalid param is found
	*/
	virtual void SetParams() = 0;
//#############################################################################
	//! Gets the settings from m_MxpParams params and show them to the user.
	/*!	If there is any invalid param an exception is throw.
			This function is called by GetDlgParams()
			\throws mwMxpuiException if any invalid param is found
	*/
	virtual void GetParams() = 0;
//#############################################################################
	//! set the text of each control from the dialog
	/*!	
	*/
	virtual void SetTexts() = 0;	
//#############################################################################
	virtual inline void PaintBMPs(){};
//#############################################################################
	virtual void PaintBMP(int vResourceID, CWnd& destWnd);
//#############################################################################
	void ValidateDlgParams();
//#############################################################################
	virtual void Validate();
//#############################################################################
protected:
//#############################################################################
	misc::mwAutoPointer<mwMxpParams>& m_pMxpParams;//params object
	const mwMxpuiParamInteractor& m_interactor;//interactor object
//#############################################################################
	//static const misc::mwstring MSG_SECTION;
};
#endif //__mwMxpuiDlgsParams_hpp__
