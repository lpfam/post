/******************************************************************************
*               File: mwSpindleParentDlg.hpp							      *
*******************************************************************************
*               Description:                                                  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  17.05.2010 Created by: Tolbariu Ionut-Irinel                               *
*******************************************************************************
*               (C) 2003-2010 by ModuleWorks GmbH                             *
******************************************************************************/
//#############################################################################
//#############################################################################
#ifndef __MW__SPINDLE_PARENT_DLG__HPP__
#define __MW__SPINDLE_PARENT_DLG__HPP__
//#############################################################################
#include "mwSpindleChildDlg.hpp"
#include "mwPostCommonDefinitions.hpp"
#include "mwAutoPointer.hpp"
#include "afxwin.h"
#include <map>
#include "mwTPoint3d.hpp"
#include "mwMxpuiDlgsParams.hpp"
#include "mwMxpuiParamInteractor.hpp"
//#############################################################################
class mwSpindleParentDlg;
//#############################################################################
class controlerMwSpindleChildDlg : public mwSpindleChildDlg
{
public: 
	controlerMwSpindleChildDlg(
		mwSpindleParentDlg& dlg,
		const misc::mwstring name = _T(""), 
		const cadcam::mwVector3d& orientation = cadcam::mwVector3d(0,0,1),
		const misc::mwAutoPointer<cadcam::mwVector3d>& directionPtr = MW_NULL,/*only for Contour machine*/
		CWnd* pParent = MW_NULL);
	//void InitVisible();???
protected:
	virtual void SetCntrlText(int viItemID, int viTextID);
	// Generated message map functions
	//{{AFX_MSG(mwPatchDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedBtnRemove();
private:
	mwSpindleParentDlg& m_dlg;
};
//#############################################################################
// mwSpindleParentDlg dialog
class mwSpindleParentDlg : public CDialog, public mwMxpuiDlgsParams
{
	//#############################################################################
public:
	//#############################################################################
	typedef misc::mwAutoPointer<controlerMwSpindleChildDlg> controlerPtr;
	typedef std::vector<controlerPtr>                       controlerPtrVect;
	//#############################################################################
	mwSpindleParentDlg(
		post::mwSpindles& spindles, 
		misc::mwAutoPointer<mwMxpParams>& pMxpParams,
		const mwMxpuiParamInteractor &rInteractor,
		CWnd* pParent = MW_NULL);	// standard constructor
	//#############################################################################
	// Dialog Data
	//{{AFX_DATA(mwPatchDlg)
	enum { IDD = IDD_DLG_SPINDLE_PARENT };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	//#############################################################################
	void Remove(controlerMwSpindleChildDlg& address);
	//#############################################################################
	afx_msg void OnBnClickedOk();
	//#############################################################################
	inline void SetCntrlText(int viItemID, int viTextID)
	{
#ifdef MCAM_CONFIG
		UNREFERENCED_PARAMETER(viItemID);
		UNREFERENCED_PARAMETER(viTextID);
#else
		SetDlgItemText(viItemID, GetText(viTextID).c_str());
#endif
	};
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(mwPatchDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	//#############################################################################
	HICON m_hIcon;
	//#############################################################################
	// Generated message map functions
	//{{AFX_MSG(mwPatchDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	//#############################################################################
	post::mwSpindles&             m_spindles;
	int                           m_z;
	controlerPtrVect              m_contrPtrVect;
	//#############################################################################
	afx_msg void OnBnClickedBtnAdd();
	void Add(
		const misc::mwstring name = _T(""), 
		const cadcam::mwVector3d& orientation = cadcam::mwVector3d(0,0,1),
		const misc::mwAutoPointer<cadcam::mwVector3d>& directionPtr = MW_NULL/*only for Contour machine*/);
	void MovePosition(int ID, int cx, int cy);
	void UpdateDlg();
	void Resize(unsigned int i);
	const bool FindDuplicate();
	const bool FindNullVector();
	const bool FindInvalideName();
	void SetTexts();
	virtual void SetParams()
	{

	};
	virtual void GetParams()
	{

	};
	//void DeleteDuplicate();
	//#############################################################################	
};
//{{AFX_INSERT_LOCATION}}
//#############################################################################
#endif // !defined(__MW__SPINDLE_PARENT_DLG__HPP__)
