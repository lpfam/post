/******************************************************************************
*               File: mwMxpuiExceptionHandler.hpp							  *
*******************************************************************************
*               Description:this module describe the mwMxpuiExceptionHandler  *
*							class											  *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 12:24:07 PM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiExceptionHandler_hpp__
#define __mwMxpuiExceptionHandler_hpp__
//#############################################################################
class mwMxpuiException;
class mwMxpuiParamInteractor;
class CDataExchange;
class mwMfcUIUtilsException;
//#############################################################################
	//! implements an handler for application exception
	/*! 			
	*/
class mwMxpuiExceptionHandler
{
public:
	//! handles an mwMxpuiException error
	/*! this function displays the error message associated with the passed exception
			\param rException the exception to be handled
			\param rInteractor the link to outside world
	*/
	static void HandleErr(const mwMxpuiException& rException,const mwMxpuiParamInteractor& rInteractor);
//#############################################################################
	static void HandleErr(const mwMfcUIUtilsException &rException,const mwMxpuiParamInteractor &rInteractor);
//#############################################################################
	//! handles an mwMxpuiException error occured in a dialog
	/*! this function displays the error message associated with the passed exception
			and calls Fail() functions of the passed CDataExchange object
			\param rException the exception to be handled
			\param rInteractor the link to outside world
			\param rDX the dialog CDataExcahnge object
	*/
	static void HandleErrInDlg(const mwMxpuiException &rException,const mwMxpuiParamInteractor &rInteractor, CDataExchange& rDX);
//#############################################################################
	static void HandleErrInDlg(CDataExchange& rDX);
//#############################################################################
private:
//#############################################################################
	//! gets an user friendly error mesage for an occured exception
	/*! 
			\param rException the exception to be handled
			\param rInteractor the link to outside world
			\returns the error message
	*/
	static const misc::mwstring GetErrorMsg(const mwMxpuiException &rException,const mwMxpuiParamInteractor &rInteractor);
	//! Constructor not allowed since it's a static class
//#############################################################################
	static const misc::mwstring GetErrorMsg(
		const mwMfcUIUtilsException &rException,
		const mwMxpuiParamInteractor &rInteractor);
//#############################################################################
	mwMxpuiExceptionHandler();
};
#endif //__mwMxpuiExceptionHandler_hpp__