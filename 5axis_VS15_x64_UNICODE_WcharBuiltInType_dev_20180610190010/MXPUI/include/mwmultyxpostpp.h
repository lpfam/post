 /******************************************************************************
*               File: mwMultyXPostPP.h										  *
*******************************************************************************
*               Description:this module describe the mwMachDynamicsPP class   *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  19.02.2010  Created by: Tolbariu Ionut-Irinel                              *
*  4/8/2015 Changed by: Tolbariu Ionut-Irinel
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#ifndef __mwMultyXPostPP_h__
#define __mwMultyXPostPP_h__
//#############################################################################
#include "mwMxpuiPage.hpp"
#include "mwMxpDlg.h"
#include "mwAutoPointer.hpp"

//#############################################################################
//! implements a property page for setting machine dynamics params
	/*! This class is derived from mwMxpuiPage
	*/
class MXPUI_API mwMultyXPostPP : public mwMxpuiPage
{
public:
	//! Constructor
	/*! 
		\param rMxpParams the params to be setup by this property page
	\param rInteractor the interactor between property page and outside world
	*/
	mwMultyXPostPP(misc::mwAutoPointer<mwMxpParams>& pMxpParams,const mwMxpuiParamInteractor& rInteractor);   
//#############################################################################
	//! Destructor
	/*! 
	*/
	~mwMultyXPostPP();
//#############################################################################
	//! Reset
	/*!
	*/
	void Reset();
//#############################################################################
// Dialog Data
	//{{AFX_DATA(mwMultyXPostPP)
	UINT IDD;
	//}}AFX_DATA
//#############################################################################
	void HideAngleControlLimits();
//#############################################################################
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(mwMultyXPostPP)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX) ;    // DDX/DDV-Support
	//}}AFX_VIRTUAL
//#############################################################################
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(mwMultyXPostPP)
	// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	virtual void UpdateControls();
	const bool GetRetractToolAtMax();
	void SetRetractToolAtMax(const bool retractToolAtMax);
	void SetAutoAnglePair(const bool toSet);
	void HideComboMachineLimits();

public:
	virtual void SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams);

//#############################################################################
	//! see mwMxpuiPage::GetParams
	/*!	
	*/
	void GetParams() ;
//#############################################################################
	//! see mwMxpuiPage::SetParams
	/*!	
	*/
	void SetParams() ;
//#############################################################################
	//! see mwMxpuiPage::SetTexts
	/*!	
	*/
	void SetTexts();
//#############################################################################
private:
//#############################################################################
	//! adds machine dynamics subpages to the current page
	void AddMXPDefSubPages();
//#############################################################################
	//! adds a specifyed mxp param subpage to a specifyed position
	/*!	adds a specifyed mxp param subpage to a specifyed position
	\param rDlg the mxp param subpage to be added
	\param vX the x coordinate
	\param vY the y coordinate
	*/
	void AddMXPDefSubPage(mwMxpDlg& rDlg, int vX, int vY);
//#############################################################################
	misc::mwAutoPointer<mwMxpDlg>	m_mxpDlg;
	bool m_hideAngleControlLimits;
//#############################################################################
};

#endif //__mwMultiXPostPP_h__
