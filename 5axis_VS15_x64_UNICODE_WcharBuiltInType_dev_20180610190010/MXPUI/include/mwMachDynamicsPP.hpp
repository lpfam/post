/******************************************************************************
*               File: mwMachDynamicsPP.hpp						              *
*******************************************************************************
*               Description:this module describe the mwMachDynamicsPP class   *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  19.02.2010  Created by: Tolbariu Ionut-Irinel                              *
*  4/8/2015 Changed by: Tolbariu Ionut-Irinel
*******************************************************************************
*               (C) 2003-2015 by ModuleWorks GmbH                             *
******************************************************************************/
#ifndef __mwMachDynamicsPP_h__
#define __mwMachDynamicsPP_h__
//#############################################################################
#include "mwMxpuiPage.hpp"
#include "mwMachDynamicsDlg.h"
#include "mwAutoPointer.hpp"

//#############################################################################
//! implements a property page for setting machine dynamics params
/*! This class is derived from mwMxpuiPage
*/
class MXPUI_API  mwMachDynamicsPP : public mwMxpuiPage
{
public:
	//! Constructor
	/*! 
		\param rMxpParams the params to be setup by this property page
	\param rInteractor the interactor between property page and outside world
	*/
	mwMachDynamicsPP(misc::mwAutoPointer<mwMxpParams>& pMxpParams, const mwMxpuiParamInteractor &rInteractor);
	//#############################################################################
	//! Destructor
	/*! 
	*/
	~mwMachDynamicsPP();
	//#############################################################################
	// Dialog Data
	//{{AFX_DATA(mwMachDynamicsPP)
	UINT IDD;
	//}}AFX_DATA
	//#############################################################################
	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(mwMachDynamicsPP)
protected:
	virtual void DoDataExchange(CDataExchange* pDX) ;    // DDX/DDV-Support
	//}}AFX_VIRTUAL
	//#############################################################################
	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(mwMachDynamicsPP)
	// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	virtual void SetParams(misc::mwAutoPointer<mwMxpParams>& pMxpParams);

	//#############################################################################
	//! see mwMxpuiPage::GetParams
	/*!	
	*/
	void GetParams() ;
	//#############################################################################
	//! see mwMxpuiPage::SetParams
	/*!	
	*/
	void SetParams() ;
	//#############################################################################
	//! see mwMxpuiPage::SetTexts
	/*!	
	*/
	void SetTexts();
	//#############################################################################
	void Reset();

private:
	//#############################################################################
	//! adds machine dynamics subpages to the current page
	void AddMachDefSubPages();
	//#############################################################################
	//! adds a specified machine dynamics subpage to a specified position
	/*!	adds a specified machine dynamics subpage to a specified position
	\param rDlg the machine dynamics subpage to be added
	\param vX the x coordinate
	\param vY the y coordinate
	*/
	void AddMachDefSubPage(mwMachDynamicsDlg& rDlg, int vX, int vY);
	//#############################################################################
	misc::mwAutoPointer<mwMachDynamicsDlg>	m_machDynamicsDlg;
	//#############################################################################
};

#endif //__mwMachDynamicsPP_h__
