/******************************************************************************
*               File: mwMxpuiException.hpp									  *
*******************************************************************************
*               Description:this module describe the mwMxpuiException class   *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*   10/6/2003 12:18:35 PM Created by: Costin Calisov						  *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMxpuiException_hpp__
#define __mwMxpuiException_hpp__
//#############################################################################
#include "mwException.hpp"
//#############################################################################
namespace post
{
	class mwPostException;
}
//#############################################################################
	//! extends misc::mwException
	/*! used to hilight application defined errors
			
	*/
class mwMxpuiException : public misc::mwException
{
public:
	enum Code
	{
		UNKNOWN_MACHINE_TYPE			            = 1,
		UNKNOWN_START_ANGLE_TYPE		            = 2,
		UNKNOWN_SOLUTION_FOR_START_ANGLE            = 3,
		UNKNOWN_COORDINATES_TYPE                    = 4,
		UNKNOWN_POLE_HANDLING                       = 5,
		UNKNOWN_MACHINE_LIMITS                      = 6,
		UNKNOWN_NC_FORMAT_TYPE                      = 7,
		NEGATIVE_FEED_RATE                          = 8,
		NEGATIVE_RAPID_RATE                         = 9,
		NEGATIVE_TOOL_CHANGE_TIME                   = 10,
		UNKNOWN_SPINDLE                             = 11,
		UNKNOWN_ARC_FORMAT_TYPE                     = 12,
		UNKNOWN_INTERPOLATION_TYPE                  = 13,
		MACHINE_NOT_SUPPORTED_IN_MASTERCAM          = 14,
		UNKNOWN_ROTATION_AXIS_TYPE                  = 15,
		UNKNOWN_TRANSALTION_AXIS_TYPE               = 16
	};
	enum ExceptionTypes
	{
		LOCAL=										0,
		MXP_EXCEPTION=								1,
		MW_STD_EXCEPTION=							2
	};
//#############################################################################
	//! Constructor
	/*! 
			\param vExcpID the id of the exception
			\param rLocation the location where the error occured
			\param pPrevLevel the previous error if there is any
	*/
	mwMxpuiException(const Code vExcpID,const misc::mwstring& rLocation,const mwMxpuiException *pPrevLevel = 0 );
//#############################################################################
	//! Constructor
	/*! construct a mwMxpuiException based on a passed mwPostException
			\param rException the occured mwPostException exception
			\param rLocation the location where the error occured
	*/
	mwMxpuiException(const post::mwPostException &rException, const misc::mwstring& rLocation);
//#############################################################################
	//! Constructor
	/*! construct a mwMxpuiException based on a passed mwException
			\param rException the occured mwException exception
			\param rLocation the location where the error occured
	*/
	mwMxpuiException(const misc::mwException &rException, const misc::mwstring& rLocation);
//#############################################################################
	//! gets Type
	/*! 			
			\returns the Type
	*/
	inline const ExceptionTypes& GetType() const
		{ return m_Type;};
//#############################################################################
	//! sets Type
	/*! 			
			\param rType the new Type
	*/
	inline void SetType(const ExceptionTypes& rType)
		{m_Type=rType;};
//#############################################################################
private:
	ExceptionTypes m_Type;
};
//#############################################################################
#endif //__mwMxpuiException_hpp__
