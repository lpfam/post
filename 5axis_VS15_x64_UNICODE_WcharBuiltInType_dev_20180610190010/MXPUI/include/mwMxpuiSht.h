#ifndef __MWMXPUISHT_H__
#define __MWMXPUISHT_H__

/////////////////////////////////////////////////////////////////////////////
// mwMxpuiSht

#include "resource.h"
#include "MXPUIDef.h"

#define WM_UPDATERESETBTTN WM_USER+1001

class MXPUI_API mwMxpuiSht : public CPropertySheet
{
	DECLARE_DYNAMIC(mwMxpuiSht)

public:
	mwMxpuiSht( LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage = 0);
	virtual ~mwMxpuiSht();
	
	virtual INT_PTR DoModal();
	virtual BOOL UpdateData(BOOL bSaveAndValidate);

protected:
	virtual BOOL OnInitDialog();
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual LRESULT OnUpdateResetBttn(WPARAM, LPARAM);

	//{{AFX_MSG(mwMxpuiSht)
	afx_msg void OnCmdMsg();
	afx_msg void OnReset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	virtual CSize GetTextSize(CString text, CFont *pFont);

protected:
	CButton m_resetDefaultBttn;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__MWMXPUISHT_H__)
