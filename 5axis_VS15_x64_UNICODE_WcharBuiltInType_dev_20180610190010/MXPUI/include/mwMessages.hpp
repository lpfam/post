/******************************************************************************
*               File: mwMessages.hpp										  *
*******************************************************************************
*               Description:this module defines the Messages constants		  *
*							used for reading from the app text file		      *
*                                                                             *
*******************************************************************************
*               History:                                                      *
*  10/6/2003 10:33:35 AM Created by: Costin Calisov                           *
*******************************************************************************
*               (C) 2003 by ModuleWorks GmbH                                  *
******************************************************************************/
#ifndef __mwMessages_hpp__
#define __mwMessages_hpp__
//#############################################################################
#include "mwStringConversions.hpp"
//#############################################################################
//Sections
const misc::mwstring  TXT_SECTION =_T("Mxpui-PARAMPAGES");
const misc::mwstring  TXT_SECTION_EXCEPTION =_T("Mxpui-EXCEPTIONS");
const misc::mwstring  TXT_SECTION_EXTERNAL_EXCEPTION =_T("exceptions");
//#############################################################################
//Msgs IDS
const int MSG_SHEET_TITLE=										1;
//#############################################################################
const int MSG_MACH_DEF_PP_TITLE=								2;
const int MSG_LBL_MACH_TYPE=									3;
const int MSG_LBL_SPINDLE_DIR=									4;

const int MSG_LBL_ROT_AXIS_DIR_0=								6;
const int MSG_LBL_ROT_AXIS_DIR_1=								7;
const int MSG_LBL_ROT_AXIS_DIR_2=								80;

const int MSG_LBL_TR_AXIS_DIR_1=								123;
const int MSG_LBL_TR_AXIS_DIR_2=								124;
const int MSG_LBL_TR_AXIS_DIR_3=								125;

const int MSG_LBL_ROT_AXIS_BASE_PNT_0=							9;
const int MSG_LBL_ROT_AXIS_BASE_PNT_1=							10;
const int MSG_LBL_ROT_AXIS_BASE_PNT_2=							81;
const int MSG_CMB_MACH4AX_TYPE_HEAD=				            70;
const int MSG_CMB_MACH4AX_TYPE_TABLE=						    71;
const int MSG_LBL_NUMBER_OF_AXES=							    69;
const int MSG_CMB_3AXES_MACHINE=				                72;
const int MSG_CMB_4AXES_MACHINE=				                73;
const int MSG_CMB_5AXES_MACHINE=				                74;
const int MSG_CMB_6AXES_MACHINE=				                75;
const int MSG_CMB_6AXES_CHAIN_SAW_MACHINE=				        152;
const int MSG_CMB_TURNING_3TR1ROT=				                161;
const int MSG_CMB_TURNING_3TR2ROT=				                162;
const int MSG_CMB_TURNING_3TR1ROT1TUR=				            163;
//#############################################################################
const int MSG_LBL_ANGLE_CHANGE_LIMIT=							15;
const int MSG_RDN_AUTO_ANGLE_PAIR =                             19;
const int MSG_RDN_KEEP_ANGLE_PAIR =                             20;
const int MSG_CHK_FORCE_HEAD_OR_TABLE_ROTATION =                189;
const int MSG_CHK_LIMIT_LINEAR_AXIS_TRAVEL =                    67;
const int MSG_CHK_FEED_RATE_INTERPOLATION_FOR_DISTANCE=		    21;
const int MSG_CHK_RAPID_RATE_INTERPOLATION_FOR_DISTANCE=        119;
const int MSG_CHK_FEED_RATE_INTERPOLATION_FOR_ANGLE=            104;
const int MSG_CHK_RAPID_RATE_INTERPOLATION_FOR_ANGLE=           105;
const int MSG_LBL_POL_ANGLE_TOL=								44;
const int MSG_LBL_ANGLE_TOL_MACH_LIM=							51;
const int MSG_LBL_LIMITS_TO_RESPECT =                           188;
const int MSG_RDN_NAME_SOLUTION =                               190;
const int MSG_RDN_PROVIDE_FIRST_ROT_AXIS_ANGLE =                191;
const int MSG_RDN_PROVIDE_SECOND_ROT_AXIS_ANGLE =               192;
const int MSG_RDN_PROVIDE_TRANS_AXIS_DIRECTION =                193;
const int MSG_RDN_PROVIDE_THIRD_ROT_AXIS_ANGLE =                201;
const int MSG_RDN_STANDARD_TRANS_AXIS_DIRECTION =               196;
const int MSG_RDN_CUSTOM_TRANS_AXIS_DIRECTION =                 197;
const int MSG_LBL_CUSTOM_TRANS_AXIS_X = 198;
const int MSG_LBL_CUSTOM_TRANS_AXIS_Y = 199;
const int MSG_LBL_CUSTOM_TRANS_AXIS_Z = 200;
const int MSG_RDN_DEFAULT_SOLUTION =                            58;
const int MSG_RDN_PROVIDE_START_ROTATION_ANGLE =                59;
const int MSG_CMB_SOLUTION_FOR_START_ANGLE_FIRST_SOLUTION	=	56;
const int MSG_CMB_SOLUTION_FOR_START_ANGLE_OTHER_SOLUTION	=	57;
const int MSG_CMB_SOLUTION_FOR_START_ANGLE_CLOSEST_TO_ZERO =    194;
const int MSG_CMB_SOLUTION_FOR_START_ANGLE_NOT_CLOSEST_TO_ZERO= 195;
const int MSG_LBL_INTERPOLATION_FEED_RATE=                      150;
const int MSG_LBL_INTERPOLATION_RAPID_RATE=                     151;
//#############################################################################
const int MSG_LBL_INTERPOLATION_TYPE=						    16;
const int MSG_CMB_INTERPOLATION_TYPE_BYVECTOR=				    148;
const int MSG_CMB_INTERPOLATION_TYPE_BYANGLE=				    149;
const int MSG_CMB_INTERPOLATION_TYPE_AXIS=                      202;
//#############################################################################
const int MSG_CMB_ROTATION_AXIS_NAME_A=							35;
const int MSG_CMB_ROTATION_AXIS_NAME_B=							36;
const int MSG_CMB_ROTATION_AXIS_NAME_C=							37;
const int MSG_CMB_ROTATION_AXIS_NAME_D=							84;
//#############################################################################
const int MSG_TOOL_PP_TITLE=									39;
const int MSG_LBL_TOOL_LENGTH_COMPENSATION=						40;
//#############################################################################
const int MSG_LBL_FIRST_ROT_AXIS_ANG_LIM =                      61;
const int MSG_LBL_SECOND_ROT_AXIS_ANG_LIM =                     62;
const int MSG_LBL_THIRD_ROT_AXIS_ANG_LIM =                     185;
const int MSG_LBL_CMB_LIM_NO_LIMIT =							63;
const int MSG_LBL_CMB_LIM_0_AND_360 =							64;
const int MSG_LBL_CMB_LIM_MINUS_PLUS_180 =                      65;
const int MSG_CHK_FILTER_DUP_MOVES =                           186;
const int MSG_CHK_DISABLE_MXP_ADD_MOVES =                      187;
const int MSG_CHK_TOOLPATH_ALIGNMENT =						    94;
//#############################################################################
const int MSG_LBL_THE_THIRD_ANGLE=						        82;
//#############################################################################
const int MSG_CMB_AUTODETECT_FULL =                             85;
const int MSG_CMB_AUTODETECT_SEMI =                             86;
const int MSG_CMB_MANUAL_MODE =                                 87;
const int MSG_LBL_BUILDING_TYPE =                               88;
//#############################################################################
const int MSG_CMB_FREEZE_ANGLE_AROUND_POLE=	                    90;
const int MSG_CMB_USE_ROTATION_ANGLE_AROUND_POLE=	            91;
const int MSG_CMB_LINEAR_INTERPOLATION=	                        92;
const int MSG_CMB_SMOOTH_INTERPOLATION=	                        93;
//#############################################################################
const int MSG_CMB_NONE=	                                        95;
const int MSG_CMB_TRANSLATION=	                                96;
const int MSG_CMB_ROTATION=	                                    97;
const int MSG_CMB_ALL=	                                        98;
//#############################################################################
const int MSG_RDN_TOOL_RETRACT_DISTANCE                       = 106;
const int MSG_RDN_MAX_RETRACT                                 = 118;
//#############################################################################
const int MSG_LBL_TYPE=                                         107;
//USED 108;
//#############################################################################
const int MSG_IDC_STATIC_ANGLE_PAIR=                            110;
const int MSG_IDC_STATIC_MACHINE_LIMITS=                        111;
const int MSG_IDC_STATIC_POLE_HANDLING=                         112;
const int MSG_IDC_STATIC_POINT_INTERPOLATION=                   113;
const int MSG_IDC_STATIC_MOVE_LIST_WRITER=                      114;
const int MSG_IDC_STATIC_TOOL_REPOSITIONING =                   115;
const int MSG_LBL_SELECT_SOLUTION_4AX =                         141;
const int MSG_LBL_SELECT_SOLUTION_5AX =                         116;
const int MSG_LBL_SELECT_SOLUTION_6AXCS =                       203;
//#############################################################################
const int MSG_IDC_STATIC_SPINDLE=				                120;
const int MSG_IDC_STATIC_ROTATION_AXIS=				            121;
const int MSG_IDC_STATIC_LINEAR_AXIS=				            122;
//#############################################################################
const int MSG_IDC_LBL_FEED_RATE=				                127;
const int MSG_IDC_LBL_RAPID_RATE=				                128;
const int MSG_IDC_LBL_TOOL_CHANGE_TIME=				            129;
const int MSG_IDC_CMB_FEED_RATE_M_MIN=				            130;
const int MSG_IDC_CMB_FEED_RATE_INCH_MIN=				        131;
const int MSG_IDC_CMB_RAPID_RATE_M_MIN=				            132;
const int MSG_IDC_CMB_RAPID_RATE_INCH_MIN=				        133;
const int MSG_IDC_LBL_TOOL_CHANGE_TIME_UNITS=				    134;
const int MSG_MACH_DYNAMICS_PP_TITLE=						    135;
//#############################################################################
const int MSG_IDC_CMB_WORKPIECE_DEFAULT=				        138;
const int MSG_IDC_CMB_HOLDER_DEFAULT=				            139;
//#############################################################################
const int MSG_LBL_WORKPIECE=				                    153;
const int MSG_LBL_HOLDER=				                        165;
//#############################################################################
const int MSG_LBL_MISC=          				                109;
//#############################################################################
const int MSG_LBL_POST_SETTINGS_SELECTION=                      154;
const int MSG_LBL_POST_SETTING_ID=                              155;
const int MSG_BTN_POST_SETTING_NEW=                             156;
const int MSG_BTN_POST_SETTING_REMOVE=                          157;
const int MSG_CMB_MILLING_MACHINE_TYPE=                         158;
const int MSG_CMB_CHAIN_SAW_MACHINE_TYPE=                       159;
const int MSG_CMB_TURNING_MACHINE_TYPE=                         160;
//#############################################################################
const int MSG_IDC_CHK_RETRACT_AND_REWIND=                       52;
const int MSG_RDN_REWIND_MAX_ANGLE_INTERPOLATION =              17;
const int MSG_RDN_REWIND_ONE_ANGLE_STEP =                       108;
//#############################################################################
const int MSG_CHK_ENABLE_MXP=                                   166;
//#############################################################################
const int MSK_CMB_PROVIDE_TR1_PLUS=                             172;
const int MSK_CMB_PROVIDE_TR1_MINUS=                            173;
const int MSK_CMB_PROVIDE_TR2_PLUS=                             174;
const int MSK_CMB_PROVIDE_TR2_MINUS=                            175;
const int MSK_CMB_PROVIDE_TR3_PLUS=                             176;
const int MSK_CMB_PROVIDE_TR3_MINUS=                            177;
//#############################################################################
const int MSG_DLG_SPINDLE_PAREN =                               178;
const int MSG_LBL_SPINDLE_SETTINGS_ID =                         179;
const int MSG_LBL_SPINDLE_SETTINGS_ADD =                        180;
//#############################################################################
const int MSG_LBL_SPINDLE_CHILD_ORIENTATION =                   181;
const int MSG_LBL_SPINDLE_CHILD_DIRECTION =                     182;
const int MSG_LBL_SPINDLE_CHILD_REMOVE =                        183;
const int MSG_LBL_SPINDLE_CHILD_CANCEL =                        164;
const int MSG_LBL_SPINDLE_CHILD_OK =                            168;
//#############################################################################
const int MSG_LBL_POLE_BEHAVIOR =                               184;
//#############################################################################
const int MSG_STATIC_RETRACT =                                  204;
const int MSG_CHK_ADDI_RETRACT =                                205;
const int MSG_LBL_ADDI_RETRACT_DIRECTION =                      206;
const int MSG_LBL_ADDI_RETRACT_DIRECTION_X =                    207;
const int MSG_LBL_ADDI_RETRACT_DIRECTION_Y =                    208;
const int MSG_LBL_ADDI_RETRACT_DIRECTION_Z =                    209;
const int MSG_RDN_ADDI_RETRACT_TO_MACH_AX_LIMITS =              210;
const int MSG_RDN_ADDI_RETRACT_DISTANCE =                       211;
const int MSG_STATIC_REWIND =                                   212;
const int MSG_BTN_RETRACT_AND_REWIND_PARAM_DLG =                213;

#endif //__mwMessages_hpp__
