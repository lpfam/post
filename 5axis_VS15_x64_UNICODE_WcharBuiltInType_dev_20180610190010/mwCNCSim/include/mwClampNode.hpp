#pragma once

#include "mwMachineItemNode.hpp"

#include "mwClampingResult.hpp"
#include "mwUnclampingResult.hpp"
#include "mwDeprecated.hpp"

namespace cncsim
{
	class mwClampNodeImpl;

	/// A node in the kinematic tree representing a clamp.
	/**
	* A clamp node is a logical node in the kinematic tree that provides basic clamping functionality.
	* Clamping is the logical linking between a geometry node and a clamp node, and defines that a geometry is being held by a clamp.
	* mwClampNode is the abstract class that implements basic clamping methods.
	* A more detailed description of clamping and unclamping can be found \ref secClamping "here".
	* \note Please note that clamping currently is only supported for stock nodes!
	*/
	class MW_CNCSIM_API mwClampNode : public mwMachineItemNode
	{
	public:
		typedef mwMachineItemNode baseclass;
		typedef mwClampNodeImpl Impl;

	public:
		//! Clamps a specific geometry that is located within the clamping range
		/**
		* Clamping is performed exactly like unspecific clamping, but clamping of the requested geometry is evaluated.
		* \note Please note that other geometries that are located within the clamping range can also be clamped!
		* \param node The node that should be clamped.
		* \return Returns a successful clamping state if the requested geometry could be clamped.
		* A different clamping result informs about the failing reason.
		* \note Please note that clamping currently is only supported for stock nodes!
		*/
		mwClampingResult Clamp(mwGeometryNode node);

		//! Clamps a specific geometry that is located within the clamping range
		/**
		* Clamping is performed exactly like unspecific clamping, but clamping of the requested geometry is evaluated.
		* \note Please note that other geometries that are located within the clamping range can also be clamped!
		* \param geoId The node id of the geometry that should be clamped.
		* \return Returns a successful clamping state if the requested geometry could be clamped.
		* A different clamping result informs about the failing reason.
		* \note Please note that clamping currently is only supported for stock nodes!
		*/
		MW_DEPRECATED("Deprecated since 2018.08, please use node handles instead: Clamp(mwGeometryNode node)")
		mwClampingResult Clamp(const int geoId);

		//! Clamps any geometry that is located within the clamping range
		/**
		* \note Please note that multiple geometries can be clamped if they are located within the clamping range!
		* \return Returns the clamping state: Clamping will succeed if a geometry is within the clamping range.
		* A different clamping result informs about the failing reason.
		* \note Please note that clamping currently is only supported for stock nodes!
		*/
		mwClampingResult Clamp();

		//! Releases a clamped geometry
		mwUnclampingResult Unclamp();

		//! Indicates whether a geometry is clamped
		/**
		* \return True if a geometry is clamped
		*/
		bool IsClamping() const;

		//! Get a list of stocks that are currently clamped by this clamp
		/**
		* \return A list of clamped stock node ids
		*/
		IdList MW_DEPRECATED("Deprecated since 2018.04 - Please use 'GetClampedGeometries' instead.")
		GetClampedStocks() const;

		//! Get a list of geometries that are currently clamped by this clamp
		/**
		* \return A list of clamped geometry nodes
		*/
		std::vector<mwGeometryNode> GetClampedGeometries() const;

	public:
		//! for internal usage only
		const mwClampNodeImpl& GetPImpl() const;
		mwClampNodeImpl& GetPImpl();
	};
}
