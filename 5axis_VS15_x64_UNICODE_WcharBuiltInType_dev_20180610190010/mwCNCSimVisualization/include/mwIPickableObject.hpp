#pragma once

#include "mwCNCSimVisualizationDllImportExport.hpp"
#include "mwColor.hpp"
#include "mwAutoPointer.hpp"

namespace mwCNCSimVisualization
{
	class mwDetailedPickingHelper;
	class mwPickingInfo;
	class mwDetailedPickingInfo;

	class MW_CNCSIM_VISUALIZATION_API mwIPickableObject
	{
	public:
		typedef misc::mwAutoPointer<mwDetailedPickingInfo> DetailedPickingInfoPtr;

	public:
		mwIPickableObject();
		virtual ~mwIPickableObject();

		int GetPickingId() const;

		misc::mwColor GetPickingColor() const;

		virtual bool PickDetailed(mwDetailedPickingHelper& pickingHelper, const mwPickingInfo& pickingInfo, DetailedPickingInfoPtr& detailedOutput) = 0;

	private:
		int m_pickingId;
    #pragma warning(push)
    #pragma warning(disable:4251)// hide warning C4251 (class needs to have dll-interface) for included core class
		misc::mwColor m_pickingColor;
    #pragma warning(pop)
	};
}
